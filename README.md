# About

Capi City 2 is a server and client for playing Monopoly like games over the internet

# License

CapiCity2 is licensed under the terms of the "GNU GENERAL PUBLIC LICENSE" Version 3 (GPL-3.0)
See copying.txt for details