#include "CapidTestBase.h"
#include "JsonResultChecker.h"

#include <QUuid>

void CapidTestBase::SetUp() {
    Test::SetUp();

    this->rng = std::make_shared<TestRNG>();
    int argc = 0;
    app = new QCoreApplication(argc, nullptr);
    capid = new Capid("test/resources/games", rng);
    capid->enableTestMode();

    testName = QString(::testing::UnitTest::GetInstance()->current_test_info()->name());
}

void CapidTestBase::TearDown() {
    testConnections.clear();
    delete capid;
    delete app;

    Test::TearDown();
}

std::shared_ptr<CapidPlayer> CapidTestBase::addConnection(const QString& connectionName) {
    discardAllMessages();

    auto connection = new TestClientConnection();
    auto p = capid->addConnection(connection);

    testConnections.insert(connectionName, connection);

    return p;
}

void CapidTestBase::removeConnection(const QString& connectionName) {
    discardAllMessages();

    testConnections.value(connectionName)->clientDisconnect();
    testConnections.remove(connectionName);
}

void CapidTestBase::send(const QString& connectionName, const QJsonObject& object) {
    discardAllMessages();

    testConnections.value(connectionName)->clientSend(object);
}

QString CapidTestBase::sendCommand(const QString& connectionName, const QString& scope, const QString& command, const QJsonObject& arguments) {
    QString nonce = QUuid::createUuid().toString();

    send(
        connectionName,
        {
            {"nonce",     nonce},
            {"scope",     scope},
            {"command",   command},
            {"arguments", arguments}
        }
    );

    return nonce;
}

QString CapidTestBase::sendLogin(const QString& connectionName, const QString& playerName) {
    QString nonce = sendCommand(connectionName, "server", "login", {{"name", playerName}});
    discardNextMessage(connectionName);
    return nonce;
}

QString CapidTestBase::sendChat(const QString& connectionName, const QString& message) {
    QString nonce = sendCommand(connectionName, "server", "chat", {{"message", message}});
    discardNextMessage(connectionName);
    return nonce;
}

QString CapidTestBase::sendChatKeepResponse(const QString& connectionName, const QString& message) {
    return sendCommand(connectionName, "server", "chat", {{"message", message}});
}

QString CapidTestBase::sendChat(const QString& connectionName, const QString& to, const QString& message) {
    QString nonce = sendCommand(
        connectionName, "server", "chat",
        {
            {"target",  to},
            {"message", message}
        }
    );
    discardNextMessage(connectionName);
    return nonce;
}

QString CapidTestBase::sendChatKeepResponse(const QString& connectionName, const QString& to, const QString& message) {
    return sendCommand(
        connectionName, "server", "chat",
        {
            {"target",  to},
            {"message", message}
        }
    );
}


QString CapidTestBase::sendRequest(const QString& connectionName, const QString& scope, const QString& type, const QString& nonce, const QJsonObject& arguments) {
    return sendCommand(
        connectionName, scope, "request",
        {
            {"type",      type},
            {"nonce",     nonce},
            {"arguments", arguments}
        }
    );
}

QString CapidTestBase::sendCreateGame(const QString& connectionName, const QString& templateId) {
    sendCommand(
        connectionName, "server", "createGame",
        {{"type", templateId}}
    );
    discardNextMessage("1");

    return getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
}

QJsonValue CapidTestBase::getNextMessage(const QString& connectionName, const QString& testCase) {
    auto data = testConnections.value(connectionName)->getNewMessage();
    EXPECT_FALSE(data.isNull()) << QString("Missing response object: %1 - %2").arg(testName, testCase).toUtf8().data();

    return data;
}

void CapidTestBase::checkNewMessage(const QString& connectionName, const QString& testCase, const std::shared_ptr<JsonValueCheck>& check) {
    checkNewMessage_internal(connectionName, testCase, check);
}

void CapidTestBase::checkNewMessage(const QList<QString>& connectionNames, const QString& testCase, const std::shared_ptr<JsonValueCheck>& check) {
    for (const QString& connectionName : connectionNames) {
        checkNewMessage_internal(connectionName, testCase + " - " + connectionName, check);
    }
}

void CapidTestBase::checkNoNewMessages(const QString& connectionName, const QString& testCase) {
    EXPECT_FALSE(testConnections.value(connectionName)->hasMessage())
                    << QString("Unexpected messages: %1 - %2\nMessage: %3").arg(
                        testName,
                        testCase,
                        testConnections.value(connectionName)->getNewMessage().toString()
                    ).toUtf8().data();
}

void CapidTestBase::checkNoNewMessages(const QList<QString>& connectionNames, const QString& testCase) {
    for (const auto& connectionName : connectionNames) {
        checkNoNewMessages(connectionName, testCase + " - " + connectionName);
    }
}

void CapidTestBase::discardNextMessage(const QString& connectionName) {
    testConnections.value(connectionName)->discardNextMessage();
}

void CapidTestBase::discardNextMessages(const QString& connectionName, int count) {
    for (int i = 0; i < count; i++) {
        discardNextMessage(connectionName);
    }
}

void CapidTestBase::discardMessages(const QString& connectionName) {
    testConnections.value(connectionName)->discardMessages();
}

void CapidTestBase::discardMessages(const QList<QString>& connectionNames) {
    for (const auto& connectionName : connectionNames) {
        discardMessages(connectionName);
    }
}

void CapidTestBase::checkNewMessage_internal(const QString& connectionName, const QString& testCase, const std::shared_ptr<JsonValueCheck>& check) {
    auto message = getNextMessage(connectionName, testCase);

    if (message.isNull()) {
        return;
    }

    QJsonObject const messageObject = message.toObject();
    JsonResultChecker::checkJsonResult(
        messageObject, check,
        testName, testCase
    );
}

void CapidTestBase::discardAllMessages() {
    for (auto connection : testConnections) {
        connection->discardMessages();
    }
}
