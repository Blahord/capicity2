#ifndef CAPICITY2_TESTRNG_H
#define CAPICITY2_TESTRNG_H

#include <QList>

#include "RNG.h"

class TestRNG : public RNG {

    public:
        int next() override;
        int next(int min, int max) override;

        void addNextNumbers(const QList<int>& newNumbers);

    private:
        QList<int> numbers;
};


#endif //CAPICITY2_TESTRNG_H
