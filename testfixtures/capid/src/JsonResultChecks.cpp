#include "JsonResultChecks.h"

#include "json/JsonChecks.h"

using namespace JsonChecks;

namespace JsonResultChecks {

    std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, const std::shared_ptr<JsonObjectCheck>& fieldCheck) {
        return serverMessage(type, {fieldCheck});
    }

    std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, const std::shared_ptr<JsonObjectCheck>& fieldCheck1, const std::shared_ptr<JsonObjectCheck>& fieldCheck2) {
        return serverMessage(type, {fieldCheck1, fieldCheck2});
    }

    std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, std::initializer_list<std::shared_ptr<JsonObjectCheck>> fieldChecks) {
        return objectValue(
            requiredField("type", is(type)),
            requiredField("data", fieldChecks)
        );
    }

    std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, const std::shared_ptr<JsonArrayCheck>& arrayCheck) {
        return serverMessage(type, {arrayCheck});
    }

    std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, const std::shared_ptr<JsonArrayCheck>& arrayCheck1, const std::shared_ptr<JsonArrayCheck>& arrayCheck2) {
        return serverMessage(type, {arrayCheck1, arrayCheck2});
    }

    std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, std::initializer_list<std::shared_ptr<JsonArrayCheck>> arrayChecks) {
        return objectValue(
            requiredField("type", is(type)),
            requiredField("data", arrayChecks)
        );
    }

    std::unique_ptr<JsonValueCheck> errorMessage(std::initializer_list<std::shared_ptr<JsonArrayCheck>> errorChecks) {
        return serverMessage(
            "error",
            {
                requiredField("list", errorChecks)
            }
        );
    }

    std::unique_ptr<JsonValueCheck> commandResponseOk(const QString& expectedNonce) {
        return serverMessage(
            "commandResponse",
            {
                requiredField("nonce", is(expectedNonce)),
                requiredField("result", is("OK"))
            }
        );
    }

    std::unique_ptr<JsonValueCheck> commandResponseOk(const QString& expectedNonce, std::initializer_list<std::shared_ptr<JsonObjectCheck>> dataChecks) {
        return serverMessage(
            "commandResponse",
            {
                requiredField("nonce", is(expectedNonce)),
                requiredField("result", is("OK")),
                requiredField("data", dataChecks)
            }
        );
    }

    std::unique_ptr<JsonValueCheck> commandResponseUnknownScope(const QString& expectedNonce) {
        return serverMessage(
            "commandResponse",
            {
                requiredField("nonce", is(expectedNonce)),
                requiredField("result", is("UNKNOWN_SCOPE"))
            }
        );
    }

    std::unique_ptr<JsonValueCheck> commandResponseUnknownCommand(const QString& expectedNonce) {
        return serverMessage(
            "commandResponse",
            {
                requiredField("nonce", is(expectedNonce)),
                requiredField("result", is("UNKNOWN_COMMAND"))
            }
        );
    }

    std::unique_ptr<JsonValueCheck> commandResponseFormalError(const QString& expectedNonce, std::initializer_list<std::shared_ptr<JsonArrayCheck>> dataChecks) {
        return serverMessage(
            "commandResponse",
            {
                requiredField("nonce", is(expectedNonce)),
                requiredField("result", is("FORMAL_ERROR")),
                requiredField("errors", dataChecks)
            }
        );
    }

    std::unique_ptr<JsonValueCheck> commandResponseStateError(const QString& expectedNonce, std::initializer_list<std::shared_ptr<JsonArrayCheck>> dataChecks) {
        return serverMessage(
            "commandResponse",
            {
                requiredField("nonce", is(expectedNonce)),
                requiredField("result", is("STATE_ERROR")),
                requiredField("errors", dataChecks)
            }
        );
    }

    std::unique_ptr<JsonValueCheck> responseMessage(const QString& nonce, bool success, std::unique_ptr<JsonValueCheck> check) {
        return serverMessage(
            "response",
            {
                requiredField("nonce", is(nonce)),
                requiredField("success", success ? isTrue() : isFalse()),
                requiredField("data", std::move(check))
            }
        );
    }

    std::unique_ptr<JsonValueCheck> eventMessage(const QString& type, std::unique_ptr<JsonValueCheck> eventDataCheck) {
        return serverMessage(
            "gameEvent",
            {
                requiredField("type", is(type)),
                requiredField("data", std::move(eventDataCheck))
            }
        );
    }

}