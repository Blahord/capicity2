#include "TestClientConnection.h"

TestClientConnection::TestClientConnection(QObject* parent) : ClientConnection(parent) {

}

TestClientConnection::~TestClientConnection() = default;

void TestClientConnection::send(const QJsonObject& data) {
    receivedMessages.append(data);
}

void TestClientConnection::disconnect() {
}

void TestClientConnection::clientSend(const QJsonObject& data) {
    emit(gotCommand(data));
}

void TestClientConnection::clientDisconnect() {
    emit(disconnected());
}

void TestClientConnection::discardNextMessage() {
    receivedMessages.removeFirst();
}

void TestClientConnection::discardMessages() {
    receivedMessages.clear();
}

bool TestClientConnection::hasMessage() {
    return !receivedMessages.isEmpty();
}

QJsonValue TestClientConnection::getNewMessage() {
    if (receivedMessages.isEmpty()) {
        return QJsonValue::Null;
    }

    QJsonValue v = receivedMessages.at(0);
    receivedMessages.removeAt(0);
    return v;
}
