#ifndef CAPICITY2_CAPIDTESTBASE_H
#define CAPICITY2_CAPIDTESTBASE_H

#include <gtest/gtest.h>

#include <QtCore/QCoreApplication>
#include "Capid.h"
#include "TestClientConnection.h"
#include "TestRNG.h"

class CapidTestBase : public ::testing::Test {

    protected:
        void SetUp() override;
        void TearDown() override;

        std::shared_ptr<CapidPlayer> addConnection(const QString& connectionName);
        void removeConnection(const QString& connectionName);

        void send(const QString& connectionName, const QJsonObject& object);
        QString sendCommand(const QString& connectionName, const QString& scope, const QString& command, const QJsonObject& arguments);

        QString sendLogin(const QString& connectionName, const QString& playerName);
        QString sendChat(const QString& connectionName, const QString& message);
        QString sendChatKeepResponse(const QString& connectionName, const QString& message);
        QString sendChat(const QString& connectionName, const QString& to, const QString& message);
        QString sendChatKeepResponse(const QString& connectionName, const QString& to, const QString& message);
        QString sendRequest(const QString& connectionName, const QString& scope, const QString& type, const QString& nonce, const QJsonObject& arguments);
        QString sendCreateGame(const QString& connectionName, const QString& templateId);

        QJsonValue getNextMessage(const QString& connectionName, const QString& testCase);
        void checkNewMessage(const QString& connectionName, const QString& testCase, const std::shared_ptr<JsonValueCheck>& check);
        void checkNewMessage(const QList<QString>& connectionNames, const QString& testCase, const std::shared_ptr<JsonValueCheck>& check);
        void checkNoNewMessages(const QString& connectionName, const QString& testCase);
        void checkNoNewMessages(const QList<QString>& connectionNames, const QString& testCase);

        void discardNextMessage(const QString& connectionName);
        void discardNextMessages(const QString& connectionName, int count);
        void discardMessages(const QString& connectionName);
        void discardMessages(const QList<QString>& connectionNames);

        std::shared_ptr<TestRNG> rng;
        QCoreApplication* app = nullptr;
        Capid* capid = nullptr;
        QMap<QString, TestClientConnection*> testConnections;

        QString testName;

    private:
        void checkNewMessage_internal(const QString& connectionName, const QString& testCase, const std::shared_ptr<JsonValueCheck>& check);

        void discardAllMessages();
};

#endif //CAPICITY2_CAPIDTESTBASE_H
