#include "JsonResultChecker.h"

#include <QStringList>
#include <QJsonDocument>
#include <QJsonArray>
#include <gtest/gtest.h>

void JsonResultChecker::checkJsonResult(const QJsonObject& result, const std::shared_ptr<JsonValueCheck>& check, const QString& testName, const QString& resultCase) {
    QList<JsonError> errors;

    check->check(result, "", errors);

    if (!errors.isEmpty()) {
        QString errorList = "";

        for (const JsonError& error : errors) {
            errorList = errorList.append(QString("%1: %2\n").arg(error.getPath()).arg(error.getMessage()));
        }

        QJsonDocument const resultDoc(result);
        QString const resultString(resultDoc.toJson(QJsonDocument::Indented));

        errorList = errorList + QString("\nValidated object:\n%1\n").arg(resultString);

        EXPECT_TRUE(false) << QString("%1 - %2: %3").arg(testName, resultCase, errorList).toUtf8().data();
    }
}
