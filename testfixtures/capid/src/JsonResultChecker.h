#ifndef CAPICITY2_JSONRESULTCHECKER_H
#define CAPICITY2_JSONRESULTCHECKER_H

#include <QtCore/QJsonObject>
#include <utility>
#include "json/checks/JsonValueCheck.h"

class JsonResultChecker {
    public:
        static void checkJsonResult(const QJsonObject& result, const std::shared_ptr<JsonValueCheck>& check, const QString& testName, const QString& resultCase);
};


#endif //CAPICITY2_JSONRESULTCHECKER_H
