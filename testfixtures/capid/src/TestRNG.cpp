#include "TestRNG.h"

int TestRNG::next() {
    if (numbers.isEmpty()) {
        return 0;
    }

    int number = numbers.at(0);
    numbers.removeAt(0);
    return number;
}

int TestRNG::next(int min, int max) {
    return next();
}

void TestRNG::addNextNumbers(const QList<int>& newNumbers) {
    for (int number : newNumbers) {
        numbers.append(number);
    }
}
