#ifndef CAPICITY2_TESTCLIENTCONNECTION_H
#define CAPICITY2_TESTCLIENTCONNECTION_H

#include "ClientConnection.h"

class TestClientConnection : public ClientConnection {

    public:
        explicit TestClientConnection(QObject* parent = nullptr);
        ~TestClientConnection() override;

        //Server side use
        void send(const QJsonObject& data) override;
        void disconnect() override;

        //Test side use
        void clientSend(const QJsonObject& data);
        void clientDisconnect();
        void discardNextMessage();
        void discardMessages();
        bool hasMessage();
        QJsonValue getNewMessage();

    private:
        QList<QJsonValue> receivedMessages;
};


#endif //CAPICITY2_TESTCLIENTCONNECTION_H
