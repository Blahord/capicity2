#ifndef CAPICITY2_JSONRESULTCHECKS_H
#define CAPICITY2_JSONRESULTCHECKS_H

#include "json/checks/JsonValueCheck.h"
#include "json/checks/JsonObjectCheck.h"
#include "json/checks/JsonArrayCheck.h"

namespace JsonResultChecks {
    extern std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, const std::shared_ptr<JsonObjectCheck>& fieldCheck);
    extern std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, const std::shared_ptr<JsonObjectCheck>& fieldCheck1, const std::shared_ptr<JsonObjectCheck>& fieldCheck2);
    extern std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, std::initializer_list<std::shared_ptr<JsonObjectCheck>> fieldChecks);

    extern std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, const std::shared_ptr<JsonArrayCheck>& arrayCheck);
    extern std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, const std::shared_ptr<JsonArrayCheck>& arrayCheck1, const std::shared_ptr<JsonArrayCheck>& arrayCheck2);
    extern std::unique_ptr<JsonValueCheck> serverMessage(const QString& type, std::initializer_list<std::shared_ptr<JsonArrayCheck>> arrayChecks);

    extern std::unique_ptr<JsonValueCheck> errorMessage(std::initializer_list<std::shared_ptr<JsonArrayCheck>> errorChecks);
    extern std::unique_ptr<JsonValueCheck> commandResponseOk(const QString& expectedNonce);
    extern std::unique_ptr<JsonValueCheck> commandResponseOk(const QString& expectedNonce, std::initializer_list<std::shared_ptr<JsonObjectCheck>> dataChecks);
    extern std::unique_ptr<JsonValueCheck> commandResponseUnknownScope(const QString& expectedNonce);
    extern std::unique_ptr<JsonValueCheck> commandResponseUnknownCommand(const QString& expectedNonce);
    extern std::unique_ptr<JsonValueCheck> commandResponseFormalError(const QString& expectedNonce, std::initializer_list<std::shared_ptr<JsonArrayCheck>> dataChecks);
    extern std::unique_ptr<JsonValueCheck> commandResponseStateError(const QString& expectedNonce, std::initializer_list<std::shared_ptr<JsonArrayCheck>> dataChecks);

    extern std::unique_ptr<JsonValueCheck> responseMessage(const QString& nonce, bool success, std::unique_ptr<JsonValueCheck> check);
    extern std::unique_ptr<JsonValueCheck> eventMessage(const QString& type, std::unique_ptr<JsonValueCheck> dataCheck);
}

#endif //CAPICITY2_JSONRESULTCHECKS_H
