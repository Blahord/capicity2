const STATE_ERROR_NOT_LOGGED_IN = {
    name: "NOT_LOGGED_IN",
    reason: "The client is not logged in"
}

const STATE_ERROR_ALREADY_LOGGED_IN = {
    name: "ALREADY_LOGGED_IN",
    reason: "The client is already logged in"
};

const STATE_ERROR_ALREADY_IN_GAME = {
    name: "ALREADY_IN_GAME",
    reason: "The client is already in a game"
};

const STATE_ERROR_NOT_IN_GAME = {
    name: "NOT_IN_GAME",
    reason: "The client is currently not in a game"
};

const STATE_ERROR_TOO_FEW_PLAYERS = {
    name: "TOO_FEW_PLAYERS",
    reason: "The game has too few players to get started"
}

const STATE_ERROR_GAME_RUNNING = {
    name: "GAME_RUNNING",
    reason: "The game is already running, ended or is stopped"
};

const STATE_ERROR_NOT_ON_TURN = {
    name: "NOT_ON_TURN",
    reason: "The clients player is currently not on turn"
}

const STATE_ERROR_WRONG_TURN_STATE = {
    name: "WRONG_TURN_STATE",
    reason: "The currents game turn state does not allow this command to be executed"
}

const STATE_ERROR_NOT_ENOUGH_MONEY = {
    name: "NOT_ENOUGH_MONEY",
    reason: "The clients player has not enough money to execute this command"
};

const COMMANDS_SERVER = [
    {
        name: "login",
        description: "Needed to complete the connection and to execute other commands",
        args: [
            {
                name: "name",
                type: TYPE_STRING,
                description: "The wanted name of the player. If there is already a player with that name, your name will be appended with a number, so it is unique"
            },
            {
                name: "language",
                type: TYPE_LANGUAGE_CODE,
                required: false,
                description: "The wanted language for the board"
            }
        ],
        errors: [STATE_ERROR_ALREADY_LOGGED_IN]
    },
    {
        name: "reconnect",
        description: "Restores a lost connection to the server",
        args: [
            {
                name: "token",
                type: TYPE_STRING,
                description: "The token that was sent to the player on successful login"
            }
        ],
        errors: [
            STATE_ERROR_ALREADY_LOGGED_IN,
            {
                name: "INVALID_TOKEN",
                reason: "The given token does not belong to any client"
            }
        ]
    },
    {
        name: "quit",
        description: "Leave the server Your current player will be deleted from the server. Feel free to rejoin at any time",
        errors: [STATE_ERROR_NOT_LOGGED_IN]
    },
    {
        name: "setAvatar",
        description: "Sets the players' avatar to given image",
        args: [
            {
                name: "image",
                type: TYPE_IMAGE,
                description: "The new avatar"
            }
        ],
        errors: [STATE_ERROR_NOT_LOGGED_IN]
    },
    {
        name: "clearAvatar",
        description: "Removes the current players avatar",
        errors: [STATE_ERROR_NOT_LOGGED_IN]
    },
    {
        name: "setName",
        description: "Sets the players name Arguments",
        args: [
            {
                name: "name",
                type: TYPE_STRING,
                description: "The new players name"
            }
        ],
        errors: [STATE_ERROR_NOT_LOGGED_IN]
    },
    {
        name: "chat",
        description: "Sends a message to all players in the main lobby or players game",
        args: [
            {
                name: "target",
                type: TYPE_STRING,
                required: false,
                description: "If you want to send a message to a specific player in the lobby/game or game only"
            },
            {
                name: "message",
                type: TYPE_STRING,
                description: "The message"
            }
        ],
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            {
                name: "UNKNOWN_TARGET",
                reason: "The target player is not known"
            }
        ]
    },
    {
        name: "createGame",
        description: "Creates a new game",
        args: [
            {
                name: "type",
                type: TYPE_STRING,
                description: "The game type to create a game from"
            }
        ],
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            STATE_ERROR_ALREADY_IN_GAME,
            {
                name: "UNKNOWN_TYPE",
                reason: "The game type is not known"
            }
        ]
    },
    {
        name: "joinGame",
        description: "Join a game",
        args: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The id of the game to join"
            }
        ],
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            STATE_ERROR_ALREADY_IN_GAME,
            {
                name: "UNKNOWN_GAME",
                reason: "The game is not known"
            },
            {
                name: "GAME_FULL",
                reason: "The game is full. It has the maximum number of players"
            }
        ]
    },
    {
        name: "leaveGame",
        description: "Leaves the current game",
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            STATE_ERROR_NOT_IN_GAME
        ]
    }
];

const COMMANDS_GAME = [
    {
        name: "setReady",
        description: "Sets the current player ready to start the game",
        args: [
            {
                name: "ready",
                type: TYPE_BOOLEAN,
                description: "The new \"ready\" status"
            }
        ],
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            STATE_ERROR_NOT_IN_GAME,
            STATE_ERROR_TOO_FEW_PLAYERS,
            STATE_ERROR_GAME_RUNNING
        ]
    },
    {
        name: "roll",
        description: "Rolls the dice. This is only possible, if client is on turn in its current game",
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            STATE_ERROR_NOT_IN_GAME,
            STATE_ERROR_NOT_ON_TURN,
            STATE_ERROR_WRONG_TURN_STATE
        ]
    },
    {
        name: "moveComplete",
        description: `Indicates, that a client has finished the movement animation. Should be sent after animating a player movement based on the <a href="#message_gamePlayerMove">gamePlayerMove</a> message.`,
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            STATE_ERROR_NOT_IN_GAME,
            STATE_ERROR_WRONG_TURN_STATE
        ]
    },
    {
        name: "buy",
        description: "Buy the estate on the players current position",
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            STATE_ERROR_NOT_IN_GAME,
            STATE_ERROR_NOT_ON_TURN,
            STATE_ERROR_WRONG_TURN_STATE,
            STATE_ERROR_NOT_ENOUGH_MONEY
        ]
    },
    {
        name: "payTax",
        description: "If there are taxes to pay and the taxes cannot be payed directly. Execute this command to continue the game",
        args: [
            {
                name: "mode",
                required: false,
                type: TYPE_STRING,
                description: "Must be either <b>\"amount\"</b> or <b>\"percent\"</b>. <ul>" +
                    "<li>\"amount\": Pay the estate's defined tax amount of this estate</li>" +
                    "<li>\"percent\": Pay the estate's defined percentage of your assets</li>" +
                    "</ul>",
                notes: [
                    "If the estate's defined tax amount if 0 or less, \"percent\" will be always assumed",
                    "If the estate's defined tax percent if 0 or less, \"amount\" will be always assumed"
                ]
            }
        ],
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            STATE_ERROR_NOT_IN_GAME,
            STATE_ERROR_NOT_ON_TURN,
            STATE_ERROR_WRONG_TURN_STATE,
            STATE_ERROR_NOT_ENOUGH_MONEY
        ]
    },
    {
        name: "skipBuy",
        description: "Does not buy or set the estate on the players current position to auction. Turn continues without anything else is happening",
        errors: [
            STATE_ERROR_NOT_LOGGED_IN,
            STATE_ERROR_NOT_IN_GAME,
            STATE_ERROR_NOT_ON_TURN,
            STATE_ERROR_WRONG_TURN_STATE
        ]
    }
];

const COMMANDS_DATA = [
    {
        name: "ping",
        description: "A simple request for clients to check their response handling.",
        args: [
            {
                name: "message",
                type: TYPE_STRING,
                description: "The message to response"
            }
        ],
        errors: [
            STATE_ERROR_NOT_LOGGED_IN
        ],
        responseData: [
            {
                name: "pong",
                type: TYPE_STRING,
                description: "The given message in the request"
            }
        ]
    },
    {
        name: "getImage",
        description: "Get an image",
        args: [
            {
                name: "hash",
                type: TYPE_IMAGE_HASH,
                description: "The hash of the image"
            }
        ],
        errors: [
            STATE_ERROR_NOT_LOGGED_IN
        ],
        responseData: [
            {
                name: "found",
                type: TYPE_BOOLEAN,
                description: "Is there is an image for the given hash"
            },
            {
                name: "image",
                type: TYPE_IMAGE,
                required: false,
                description: "The requested image. If there is no image for given hash, this field is not present"
            }
        ]
    }
];

const COMMAND_SCOPES = [
    {
        name: "server",
        commands: COMMANDS_SERVER
    },
    {
        name: "game",
        commands: COMMANDS_GAME
    },
    {
        name: "data",
        commands: COMMANDS_DATA
    }
];
