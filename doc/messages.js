const MESSAGES = [
    {
        type: "error",
        description: "A client command could not be parsed or does not match the general requirements",
        data: [
            {
                name: "list",
                type: list(TYPE_STRING),
                description: "A list of all found errors"
            }
        ]
    },
    {
        type: "commandResponse",
        description: "The immediate response for a command, if it could be parsed and fulfil the basic criteria",
        data: [
            {
                name: "nonce",
                type: TYPE_STRING,
                description: "The nonce, that the client gave in its command"
            },
            {
                name: "result",
                type: TYPE_STRING,
                description:
                    `The command result. One of
                    <ul>
                    <li><b>"OK"</b> - Command can be executed</li>
                    <li><b>"UNKNOWN_SCOPE"</b> - The given scope is not known</li>
                    <li><b>"UNKNOWN_COMMAND"</b> - The command is not available in the given scope</li>
                    <li><b>"FORMAL_ERROR"</b> - The arguments object does not fit the requirements</li>
                    <li><b>"STATE_ERROR"</b> - The command cannot be executed at the moment</li>
                    </ul>`
            },
            {
                name: "errors",
                type: list(TYPE_STRING),
                required: false,
                description:
                    `If result is \"STATE_ERROR\", all command specific state errors, that prevented the server of executing the command. See Commands for details<br>
                     If result is \"FORMAL_ERROR\", all found errors on the command structure. These should not be presented to the user and have the only purpose of helping to help client development<br>`
            },
            {
                name: "data",
                type: TYPE_OBJECT,
                required: false,
                description: "If the command has a direct response, the data goes here"
            }
        ]
    },
    {
        type: "playerData",
        description: `Data about your own player. This message will be sent after successful <a href="#command_server_login">login</a> to the server`,
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The id of your player"
            },
            {
                name: "name",
                type: TYPE_STRING,
                description: "The name of your player. Note: It may differ from the player name you gave when login in to the server."
            },
            {
                name: "token",
                type: TYPE_STRING,
                description: `This token can be used to <a href="#command_server_reconnect">reconnect</a> to the server, after your client has crashed. You should not share this to others, because others could use it to take control of your player`
            }
        ]
    },
    {
        type: "playerList",
        description: "A list of all players on the server. Clients should sync their player list to the given one",
        data: [
            {
                name: "list",
                type: list(TYPE_OBJECT),
                description: "The player list",
                subParameter: [
                    {
                        name: "id",
                        type: TYPE_STRING,
                        description: "The id of a player"
                    },
                    {
                        name: "name",
                        type: TYPE_STRING,
                        description: "The name of a player"
                    },
                    {
                        name: "connected",
                        type: TYPE_BOOLEAN,
                        description: "Is the player currently connected"
                    },
                    {
                        name: "avatar",
                        type: TYPE_IMAGE_HASH,
                        required: false,
                        description: "The players' avatar"
                    }
                ]
            }
        ]
    },
    {
        type: "gameTemplateList",
        description: "Information about possible games on this server",
        data: [
            {
                name: "list",
                type: list(TYPE_OBJECT),
                description: "The game template list",
                subParameter: [
                    {
                        name: "id",
                        type: TYPE_STRING,
                        description: `A unique identifier for the game type. Used to <a href="#command_server_createGame">create new games</a>`
                    },
                    {
                        name: "name",
                        type: TYPE_STRING,
                        description: "The name of this game"
                    },
                    {
                        name: "languages",
                        type: list(TYPE_LANGUAGE_CODE),
                        description: "All supported languages"
                    },
                    {
                        name: "defaultLanguage",
                        type: TYPE_LANGUAGE_CODE,
                        description: "The used language, if this game type does not support players language"
                    },
                    {
                        name: "description",
                        type: TYPE_STRING,
                        description: "A description of the game"
                    },
                    {
                        name: "minPlayers",
                        type: TYPE_INTEGER,
                        description: "The minimum amount of players needed to play"
                    },
                    {
                        name: "maxPlayers",
                        type: TYPE_INTEGER,
                        description: "The maximum number of players for a game of this type"
                    }
                ]
            }
        ]
    },
    {
        type: "gameList",
        description: "Information about all games on this server",
        data: [
            {
                name: "list",
                type: list(TYPE_OBJECT),
                description: "The game list",
                subParameter: [
                    {
                        name: "id",
                        type: TYPE_STRING,
                        description: "The id of the game"
                    },
                    {
                        name: "type",
                        type: TYPE_STRING,
                        description: "The id of the template used to create the game"
                    },
                    {
                        name: "owner",
                        type: TYPE_STRING,
                        description: "The id of games owner"
                    },
                    {
                        name: "players",
                        type: list(TYPE_STRING),
                        description: "The players' ids that are currently in that game"
                    },
                    {
                        name: "state",
                        type: enumeration("gameState"),
                        description: "The games state"
                    },
                    {
                        name: "turnState",
                        type: enumeration("turnState"),
                        description: "The games current turn state"
                    }
                ]
            }
        ]
    },
    {
        type: "newPlayer",
        description: "A new player has logged in to the server",
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The players id"
            },
            {
                name: "name",
                type: TYPE_STRING,
                description: "The name of the player"
            },
            {
                name: "avatar",
                type: TYPE_IMAGE_HASH,
                required: false,
                description: "The players' avatar"
            }
        ]
    },
    {
        type: "newGame",
        description: "A new game was created",
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The games id"
            },
            {
                name: "type",
                type: TYPE_STRING,
                description: "The id of the template that was used to create the gamer"
            },
            {
                name: "owner",
                type: TYPE_STRING,
                description: "The current owner of the game. Note: The owner should be added to the new created game"
            }
        ]
    },
    {
        type: "playerUpdate",
        description: "A player has updated his data.<br>Note: Only listed properties should be updated",
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The players' id"
            },
            {
                name: "connected",
                type: TYPE_BOOLEAN,
                required: false,
                description: "Is the player connected"
            },
            {
                name: "avatar",
                type: TYPE_STRING,
                required: false,
                description: "His avatar"
            },
            {
                name: "name",
                type: TYPE_STRING,
                required: false,
                description: "The new player name"
            }
        ]
    },
    {
        type: "gameUpdate",
        description: "A game has updates its data.<br>Note: Only given data should be updated",
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The games id"
            },
            {
                name: "owner",
                type: TYPE_STRING,
                required: false,
                description: "The id of the games owner"
            },
            {
                name: "players",
                type: list(TYPE_STRING),
                required: false,
                description: "The players' ids that are currently in that game"
            },
            {
                name: "state",
                type: enumeration("gameState"),
                required: false,
                description: "The games state"
            },
            {
                name: "turnState",
                type: enumeration("turnState"),
                required: false,
                description: "The games current turn state. Only relevant, if state is \"RUN\""
            },
            {
                name: "playerOrder",
                type: list(TYPE_STRING),
                required: false,
                description: "The players' ids in the order of the players get the turn"
            },
            {
                name: "turnPlayer",
                type: TYPE_STRING,
                required: false,
                description: "The players' id that is currently on turn"
            },
        ]
    },
    {
        type: "gameDelete",
        description: "A game has been deleted on the server",
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The games id"
            }
        ]
    },
    {
        type: "quit",
        description: "A player left the server.",
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The players' id"
            }
        ]
    },
    {
        type: "gameData",
        description: "Information about games estates and cards",
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The games id"
            },
            {
                name: "bgColor",
                type: TYPE_COLOR,
                description: "The background color of the game"
            },
            {
                name: "estateGroups",
                type: list(TYPE_OBJECT),
                description: "All estate groups of the game with",
                subParameter: [
                    {
                        name: "id",
                        type: TYPE_STRING,
                        description: "A game unique id of the estate group"
                    },
                    {
                        name: "name",
                        type: TYPE_STRING,
                        description: "The name of the estate group"
                    },
                    {
                        name: "housePrice",
                        type: TYPE_INTEGER,
                        description: "How much to pay to build a house"
                    },
                    {
                        name: "color",
                        type: TYPE_COLOR,
                        required: false,
                        description: "The color of the estate group"
                    }
                ]
            },
            {
                name: "estates",
                type: list(TYPE_OBJECT),
                description: "All estates of the game",
                subParameter: [
                    {
                        name: "id",
                        type: TYPE_STRING,
                        description: "A game unique id of the estate"
                    },
                    {
                        name: "name",
                        type: TYPE_STRING,
                        description: "The name of the estate"
                    },
                    {
                        name: "estateGroup",
                        type: TYPE_STRING,
                        required: false,
                        description: "If the estate belongs to an estate group, the id of that estate group"
                    },
                    {
                        name: "price",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "Buy price of the estate"
                    },
                    {
                        name: "rent0",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "The base rent of the estate"
                    },
                    {
                        name: "rent1",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "The rent with one house on it"
                    },
                    {
                        name: "rent2",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "The rent with two houses on it"
                    },
                    {
                        name: "rent3",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "The rent with three houses on it"
                    },
                    {
                        name: "rent4",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "The rent with four houses on it"
                    },
                    {
                        name: "rent5",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "The rent with a hotel on it"
                    },
                    {
                        name: "taxAmount",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "If a player lands on the field, the amount the player has to pay"
                    },
                    {
                        name: "taxPercent",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "If a player lands on the field, the percentage of the players worth of assets has to be payed"
                    },
                    {
                        name: "payTarget",
                        type: TYPE_STRING,
                        required: false,
                        description: "The id of the estate, where the money goes, if the player has to pay on this field. If no payTarget is given, the money simply goes back to the bank.",
                        notes: [
                            "The referenced estate might de declared at a later point in this list of estates"
                        ]
                    },
                    {
                        name: "owner",
                        type: TYPE_STRING,
                        required: false,
                        description: "The current owner of this estate. On new games this value is not set. Only relevant if a player joins a running game or rejoins from a disconnect."
                    },
                    {
                        name: "money",
                        type: TYPE_INTEGER,
                        required: false,
                        description: "The accumulated taxes that were payed to this estate. Only relevant if a player joins a running game or rejoins from a disconnect."
                    }
                ]
            },
        ]
    },
    {
        type: "chat",
        description: "A message from the chat",
        data: [
            {
                name: "sender",
                type: TYPE_STRING,
                description: "The players' id who sent the message"
            },
            {
                name: "target",
                type: TYPE_STRING,
                required: false,
                description: "The players' id which is the message was sent to. Only set for private messages"
            },
            {
                name: "message",
                type: TYPE_STRING,
                description: "The message"
            }
        ]
    },
    {
        type: "gamePlayerUpdate",
        description: "An update of a player in a game.<br>Note: Only listen properties should be updated",
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The players' id"
            },
            {
                name: "ready",
                type: TYPE_BOOLEAN,
                required: false,
                description: "Is the player ready to start the game"
            },
            {
                name: "money",
                type: TYPE_STRING,
                required: false,
                description: "The current players cash"
            },
            {
                name: "jailed",
                type: TYPE_INTEGER,
                required: false,
                description: "Is the player currently in jail?"
            },
            {
                name: "position",
                type: TYPE_INTEGER,
                required: false,
                description: "The players position on the board"
            },
            {
                name: "moved",
                type: TYPE_BOOLEAN,
                required: false,
                description: "Has the player completed the current movement animation"
            }
        ]
    },
    {
        type: "gamePlayerMove",
        description:
            `A game player is about to be moved around the board. Clients can react on this message to animate the players' movement. At the end of the movement client must send a <a href="#command_game_moveComplete">moveComplete</a> message to server
            to indicate, that the client has finished the movement animation. If a client does not send a <a href="#command_game_moveComplete">moveComplete</a> command within 5 seconds, the server will assume the client has completed the movement
            and will continue as it has sent the command. If a client does not want to animate a movement, it should directly send a <a href="#command_game_moveComplete">moveComplete</a> command.<br>
            Clients may update the players position directly based on the "to" value, when they like to.`,
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The id of the player to move"
            },
            {
                name: "from",
                type: TYPE_INTEGER,
                description: "The source position of the player movement"
            },
            {
                name: "to",
                type: TYPE_INTEGER,
                description: "The target position of the player movement"
            },
            {
                name: "mode",
                type: TYPE_STRING,
                description:
                    `The movement mode. On of 
                    <ul>
                        <li><b>FORWARD</b> - The animation should be in the normal movement direction</li>
                        <li><b>BACKWARD</b> - The animation should be in the opposite movement direction</li>
                    </ul>`
            }
        ]
    },
    {
        type: "estateUpdate",
        description: "An update of an estate in a game.<br>Note: Only listen properties should be updated",
        data: [
            {
                name: "id",
                type: TYPE_STRING,
                description: "The estates' id"
            },
            {
                name: "owner",
                type: TYPE_STRING,
                required: false,
                description: "The new owner of the estate. If empty, the estate has no owner"
            },
            {
                name: "money",
                type: TYPE_INTEGER,
                required: false,
                description: "The accumulated taxes that were payed to this estate"
            }
        ]
    },
    {
        type: "gameEvent",
        description: "An event in the clients current game. Clients should use these events only to display the games' story. No data given here should be interpreted in any other way.",
        data: [
            {
                name: "type",
                type: TYPE_STRING,
                description: `The type of the event of the game. See <a href="#chapter_gameEvents">Game Events</a> chapter for more`
            },
            {
                name: "data",
                type: "Any",
                description: "The event specific data"
            }
        ]
    }
];