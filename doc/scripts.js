// Toc Functions

function toc(types, enums, commands, messages, events) {
    const container = document.getElementById("toc");

    container.appendChild(typesToc(types, enums));
    container.appendChild(commandsToc(commands));
    container.appendChild(messagesToc(messages));
    container.appendChild(eventsToc(events));
}

function typesToc(types, enums) {
    const typesListItems = tocSubList("List", types.map(t => innerLink(typeId(t), t.name)));

    const typesList = document.createElement("ul");

    const toc = document.createElement("li");
    toc.append(innerLink("chapter_types", "Types"));
    toc.append(typesList);
    typesList.appendChild(typesListItems);
    typesList.appendChild(simpleElement("li", innerLink("type_desc_image", "Image")))
    typesList.appendChild(simpleElement("li", innerLink("type_desc_imageHash", "Image-Hash")))
    typesList.appendChild(simpleElement("li", innerLink("type_desc_color", "Color")))
    typesList.appendChild(enumsToc(enums));

    return toc;
}

function enumsToc(enums) {
    return tocSubList(innerLink("type_desc_enum", "Enumerations"), enums.map(e => innerLink(enumId(e), e.name)));
}

function commandsToc(commands) {
    const toc = document.createElement("li");
    toc.append(innerLink("chapter_clientToServer", "Client to server"));

    const scopeCommandsList = document.createElement("ul");

    commands.forEach(scope =>
        scopeCommandsList.appendChild(tocSubList(
                innerLink(commandScopeId(scope.name), scope.name),
                scope.commands.map(c => innerLink(commandId(scope.name, c), c.name))
            )
        ));

    toc.appendChild(scopeCommandsList);

    return toc;
}

function messagesToc(messages) {
    return tocSubList(innerLink("chapter_serverToClient", "Server to client"), messages.map(e => innerLink(messageId(e), e.type)));
}

function eventsToc(events) {
    return tocSubList(innerLink("chapter_gameEvents", "Game events"), events.map(e => innerLink(gameEventId(e), e.type)));
}

function tocSubList(name, items) {
    const subList = document.createElement("ul");
    items.forEach(l => subList.appendChild(simpleElement("li", l)));

    const toc = document.createElement("li");
    toc.append(name);
    toc.append(subList);

    return toc;
}

// Type Functions

function types(types) {
    const container = document.getElementById("types");

    for (let type of types) {
        const idCell = document.createElement("td");
        idCell.id = typeId(type);
        idCell.innerHTML = type.name + (type.generic ? " of [SomeType] " : "");

        const jsonTypeCell = simpleElement("td", type.jsonType);
        const descCell = simpleElement("td", type.description);

        const tableRow = document.createElement("tr");
        tableRow.appendChild(idCell);
        tableRow.appendChild(jsonTypeCell);
        tableRow.appendChild(descCell);

        container.appendChild(tableRow);
    }
}

function typeId(type) {
    return "type_" + type.id;
}

// Enum functions

function enums(enums) {
    const container = document.getElementById("enums");

    for (enumeration of enums) {
        enumBlock(enumeration, container);
    }
}

function enumBlock(enumeration, container) {
    const head = simpleElement("div", enumeration.name, "enumHead");
    head.id = enumId(enumeration);

    container.appendChild(head);
    container.appendChild(enumBody(enumeration));
}

function enumBody(enumeration) {
    const body = simpleElement("div", "", "enumBody");

    const table = document.createElement("table");

    table.appendChild(tableHeader(["Value", "Description"]));
    table.appendChild(enumTableBody(enumeration));

    body.appendChild(table);

    return body;
}

function enumTableBody(enumeration) {
    const body = document.createElement("tbody");

    for (const value of enumeration.values) {
        const row = document.createElement("tr");

        row.appendChild(simpleElement("td", value.name));
        row.appendChild(simpleElement("td", value.description));

        body.appendChild(row);
    }

    return body;
}

function enumId(enumeration) {
    return "enum_" + enumeration.name;
}

// Command functions

function commandScopes(scopes) {
    for (const scope of scopes) {
        commandScope(scope);
    }
}

function commandScope(scope) {
    const commandsElement = document.getElementById("commands");

    const scopeName = scope.name;
    const commands = scope.commands;

    const scopeContainer = document.createElement("div");
    scopeContainer.className = "commandScope";

    scopeContainer.appendChild(commandScopeHead(scopeName));
    scopeContainer.appendChild(commandScopeBody(scopeName, commands));

    commandsElement.appendChild(scopeContainer);
}

function commandScopeHead(name) {
    const head = simpleElement("header", "", "commandScopeHead");
    head.id = commandScopeId(name);

    head.appendChild(simpleElement("h2", "Scope: " + name));

    return head;
}

function commandScopeBody(scopeName, commands) {
    const body = document.createElement("div")
    body.className = "indent";

    for (const cmd of commands) {
        commandBlock(scopeName, cmd, body);
    }

    return body;
}

function commandScopeId(scopeName) {
    return "commands_" + scopeName;
}

function commandBlock(scope, command, container) {
    const head = simpleElement("div", command.name, "commandHead");
    head.id = commandId(scope, command);

    container.appendChild(head);
    container.appendChild(commandBody(command));
}

function commandBody(command) {
    const body = document.createElement("div");
    body.className = "commandBody";

    const descriptionElement = simpleElement("p", command.description);
    descriptionElement.className = "commandDescription";
    body.appendChild(descriptionElement);

    const args = command.args;
    const errors = command.errors;
    const responseData = command.responseData;

    if (args) {
        body.appendChild(simpleElement("h4", "Arguments"));
        body.appendChild(paramTable(args));
    }

    if (errors) {
        body.appendChild(simpleElement("h4", "State errors"));
        body.appendChild(commandStateErrors(errors));
    }

    if (responseData) {
        body.appendChild(simpleElement("h4", "Response data"));
        body.appendChild(paramTable(responseData));
    }

    return body;
}

function commandStateErrors(errors) {
    const list = document.createElement("ul");
    list.className = "commandErrors";

    for (const error of errors) {
        list.appendChild(simpleElement(
            "li",
            `<span class="stateError">` + error.name + `</span><br>` + error.reason
        ));
    }

    return list;
}

function commandId(scope, command) {
    return "command_" + scope + "_" + command.name;
}

// Message functions

function messages(messages) {
    const container = document.getElementById("messages");

    for (const message of messages) {
        messageBlock(message, container);
    }
}

function messageBlock(message, container) {
    const head = simpleElement("div", message.type, "messageHead");
    head.id = messageId(message);

    container.appendChild(head);
    container.appendChild(messageBody(message));
}

function messageBody(message) {
    const body = simpleElement("div", "", "messageBody");

    body.appendChild(simpleElement("p", message.description, "messageDescription"));

    const data = message.data;

    if (data) {
        body.appendChild(simpleElement("h4", "Data"));
        body.appendChild(paramTable(data));
    }

    return body;
}

function messageId(message) {
    return "message_" + message.type;
}

// Game event functions

function gameEvents(gameEvents) {
    const container = document.getElementById("gameEvents");

    for (const gameEvent of gameEvents) {
        gameEventBlock(gameEvent, container);
    }
}

function gameEventBlock(gameEvent, container) {
    const head = simpleElement("div", gameEvent.type, "gameEventHead");
    head.id = gameEventId(gameEvent);

    container.appendChild(head);
    container.appendChild(gameEventBody(gameEvent));
}

function gameEventBody(gameEvent) {
    const body = simpleElement("div", "", "gameEventBody");

    body.appendChild(simpleElement("p", gameEvent.description, "gameEventDescription"));

    const data = gameEvent.data;

    const typeElement = simpleElement("p", "Type: ");
    typeElement.appendChild(typeHtml(data.type))

    body.appendChild(simpleElement("h4", "Data"));
    body.appendChild(typeElement);

    if (data.description) {
        body.appendChild(simpleElement("div", data.description));
    }

    if (data.subData) {
        body.appendChild(paramTable(data.subData));
    }

    return body;
}

function gameEventId(gameEvent) {
    return "gameEvent_" + gameEvent.type;
}

// Common Functions

function paramTable(params) {
    const table = document.createElement("table");

    table.appendChild(tableHeader(["Name", "Type", "Required", "Description"]));
    table.appendChild(paramTableBody(params));

    return table;
}

function paramTableBody(params) {
    const body = document.createElement("tbody");

    for (const param of params) {
        body.appendChild(paramTableRow(param));
    }

    return body;
}

function paramTableRow(param) {
    const required = param.required ?? true;
    const row = document.createElement("tr");

    row.appendChild(simpleElement("td", param.name, "noWrap"));
    row.appendChild(simpleElement("td", typeHtml(param.type), "noWrap"));
    row.appendChild(simpleElement("td", required ? "✔ Yes" : "No"));
    row.appendChild(simpleElement("td", paramDescriptionHtml(param)));

    return row;
}

function paramDescriptionHtml(param) {
    const description = param.description;
    const notes = param.notes;
    const subParams = param.subParameter;

    let result = description;

    if (subParams) {
        result += paramTable(subParams).outerHTML;
    }

    if (notes) {
        result += "<p><b>Notes:</b><ul>";
        notes.forEach(note => result += "<li>" + note + "</li>");
        result += "</ul></p>";
    }

    return result;
}

function tableHeader(contents) {
    const head = document.createElement("thead");

    const headRow = document.createElement("tr");

    for (const content of contents) {
        headRow.appendChild(simpleElement("th", content));
    }

    head.appendChild(headRow);

    return head;
}

function simpleElement(tagName, htmlContent, classes) {
    const element = document.createElement(tagName);

    if (htmlContent instanceof Element) {
        element.appendChild(htmlContent);
    } else {
        element.innerHTML = htmlContent;
    }

    if (classes) {
        element.className = classes;
    }

    return element;
}

function typeHtml(type, parent) {
    if (typeof type === "string") {
        return typeLink(type, parent);
    }

    const name = type.name;
    const param = type.parameter;

    const typeElement = document.createElement("span");
    typeElement.append(
        typeLink(name),
        " of ",
        typeLink(param, name)
    );

    return typeElement;
}

function typeLink(type, parent) {
    if (parent === "enum") {
        return innerLink("enum_" + type, type);
    }

    const typeName = getTypeName(type);

    if (typeName === undefined) {
        return simpleElement("span", type);
    }

    return innerLink("type_" + type, getTypeName(type));
}

function innerLink(id, name) {
    const link = document.createElement("a");
    link.href = "#" + id;
    link.text = name;
    return link;
}

function getTypeName(typeId) {
    for (let type of TYPES) {
        if (type.id === typeId) {
            return type.name;
        }
    }

    return undefined;
}

// Definition utils

function list(type) {
    return {
        name: "list",
        parameter: type
    };
}

function enumeration(type) {
    return {
        name: "enum",
        parameter: type
    };
}
