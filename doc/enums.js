const ENUMS = [
    {
        name: "gameState",
        values: [
            {
                name: "CONFIG",
                description: "The game has not been started yet"
            },
            {
                name: "RUN",
                description: "The game is currently running"
            },
            {
                name: "END",
                description: "The game has ended"
            }
        ]
    },
    {
        name: "turnState",
        values: [
            {
                name: "NOTHING",
                description: "There is no current turn state"
            },
            {
                name: "ROLL",
                description: "Server is waiting for the turn player to roll the dice"
            },
            {
                name: "MOVE",
                description: "Clients are animating player movements"
            },
            {
                name: "TAX",
                description: "Player on turn has to pay taxes."
            },
            {
                name: "BUY",
                description: "Player on turn can buy estate"
            }
        ]
    }
];