const TYPE_STRING = "string";
const TYPE_INTEGER = "integer";
const TYPE_BOOLEAN = "boolean";
const TYPE_IMAGE = "image";
const TYPE_IMAGE_HASH = "imageHash";
const TYPE_COLOR = "color";
const TYPE_LANGUAGE_CODE = "languageCode";
const TYPE_OBJECT = "object";
const TYPE_NULL = "null";

const TYPES = [
    {
        id: TYPE_STRING,
        name: "String",
        jsonType: "String",
        description: "A sequence of characters"
    },
    {
        id: TYPE_INTEGER,
        name: "Integer",
        jsonType: "Integer",
        description: "An integer number"
    },
    {
        id: TYPE_BOOLEAN,
        name: "Boolean",
        jsonType: "Boolean",
        description: "<span class=\"code\">true</span> or <span class=\"code\">false</span>"
    },
    {
        id: TYPE_IMAGE,
        name: "Image",
        jsonType: "Object",
        description: "See subsection <a href=\"#type_desc_image\">Image</a>"
    },
    {
        id: TYPE_IMAGE_HASH,
        name: "Image-Hash",
        jsonType: "String",
        description: "The SHS-256 Hash of an image. See subsection <a href=\"#type_desc_imageHash\">Image-Hash</a>"
    },
    {
        id: TYPE_COLOR,
        name: "Color",
        jsonType: "String",
        description: "See subsection <a href=\"#type_desc_color\">Color</a>"
    },
    {
        id: TYPE_LANGUAGE_CODE,
        name: "Language-Code",
        jsonType: "String",
        description: "A <a href=\"https://iso639-3.sil.org/\">ISO-639-3</a> language code"
    },
    {
        id: "list",
        name: "List",
        jsonType: "Array",
        generic: true,
        description: "A list of items with of type [SomeType]"
    },
    {
        id: TYPE_OBJECT,
        name: "Object",
        jsonType: "Object",
        description: "Used, if a command argument or message data type does not fit into the types above. In such cases the actual structure is explained where this type is referenced."
    },
    {
        id: "enum",
        name: "Enum",
        jsonType: "String",
        generic: true,
        description: "The values are only one of a predefines list of possible values. See definition of respective [SomeType] for all possible values"
    }
];