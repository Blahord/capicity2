const GAME_EVENTS = [
    {
        type: "started",
        description: "The game has started",
        data: {
            type: TYPE_NULL
        }
    },
    {
        type: "turn",
        description: "A player got the turn",
        data: {
            type: TYPE_STRING,
            description: "The players' id"
        }
    },
    {
        type: "roll",
        description: "The player currently on turn rolled the dice",
        data: {
            type: TYPE_OBJECT,
            description: "The resulting values",
            subData: [
                {
                    name: "dice1",
                    type: TYPE_INTEGER,
                    description: "The result of the first (virtual) dice (The dice might be real, if a server implements a die roll that way)"
                },
                {
                    name: "dice2",
                    type: TYPE_INTEGER,
                    description: "The result of the second (virtual) dice (The dice might be real, if a server implements a die roll that way)"
                }
            ]
        }
    },
    {
        type: "land",
        description: "The player currently on turn has landed on given position during a die roll or movement",
        data: {
            type: TYPE_INTEGER,
            description: "The new players position"
        }
    },
    {
        type: "taxPayNeeded",
        description: "The player on turn, has to pay taxes, but cannot pay the directly or has to choose what to pay",
        data: {
            type: TYPE_OBJECT,
            description: "The possible options",
            subData: [
                {
                    name: "amount",
                    type: TYPE_INTEGER,
                    description: "The fixed mount. If this value is 0 or less, this option cannot be chosen"
                },
                {
                    name: "percent",
                    type: TYPE_INTEGER,
                    description: "The given percentage of the players assets. If this value is 0 or less, this option cannot be chosen"
                }
            ]
        }
    },
    {
        type: "taxPayed",
        description: "The player on turn, has payed the taxes",
        data: {
            type: TYPE_INTEGER,
            description: "The actual amount, the player has payed",
        }
    },
    {
        type: "canBuy",
        description: "The player on turn can buy the estate",
        data: {
            type: TYPE_NULL
        }
    },
    {
        type: "buy",
        description: "The player on turn has bought the estate",
        data: {
            type: TYPE_NULL
        }
    },
    {
        type: "skipBuy",
        description: "The player on turn chosen to not buy the estate",
        data: {
            type: TYPE_NULL
        }
    },
    {
        type: "end",
        description: "The game has ended and there is a winner",
        data: {
            type: TYPE_STRING,
            description: "The id of the winning player"
        }
    }
]