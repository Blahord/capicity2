#include "Capid.h"

#include "TcpClientConnection.h"
#include "CapidMessageUtils.h"
#include "command/data/DataScope.h"
#include "command/game/GameScope.h"
#include "command/server/ServerScope.h"
#include "json/JsonChecks.h"

Capid::Capid(const QString& gamesDir, const std::shared_ptr<RNG>& rng) {
    this->server = new QTcpServer();
    this->timer = new QTimer();
    this->serverManager = std::make_shared<ServerManager>();
    this->gameManager = std::make_shared<GameManager>(serverManager);
    this->gameStateListeners = std::make_unique<GameStateListeners>(rng);

    addScope(std::make_shared<ServerScope>(serverManager, gameManager, &imageRegistry, gamesDir));
    addScope(std::make_shared<GameScope>(serverManager, rng, gameManager));
    addScope(std::make_shared<DataScope>(serverManager, &imageRegistry));

    this->commandBaseCheck = JsonChecks::objectValue(
        {
            JsonChecks::requiredField("nonce", JsonChecks::stringValue()),
            JsonChecks::requiredField("scope", JsonChecks::stringValue()),
            JsonChecks::requiredField("command", JsonChecks::stringValue()),
            JsonChecks::optionalField("arguments", JsonChecks::objectValue())
        }
    );

    for (const auto& gameTemplate : CapidGameReader::getGames(gamesDir)) {
        serverManager->addGameTemplate(gameTemplate);
    }

    connect(timer, &QTimer::timeout, this, &Capid::tick);
    timer->start(1000);

    connect(server, &QTcpServer::newConnection, this, &Capid::acceptConnection);
    this->server->listen(QHostAddress::Any, 1234);
}

Capid::~Capid() {
    server->close();

    timer->deleteLater();
    timer = nullptr;

    server->deleteLater();
    server = nullptr;
}

std::shared_ptr<CapidPlayer> Capid::addConnection(ClientConnection* connection) {
    auto p = std::make_shared<CapidPlayer>();
    auto c = std::make_unique<CapidPlayerConnection>(p, connection);

    connect(c.get(), &CapidPlayerConnection::gotCommand, this, &Capid::playerCommand);
    connect(c.get(), &CapidPlayerConnection::disconnected, this, &Capid::handleDisconnect);

    serverManager->addConnectingPlayer(std::move(c));

    qDebug() << "New connection";
    return p;
}

void Capid::enableTestMode() {
    this->timer->stop();
    this->timer->start(50);
}

const QList<std::shared_ptr<CapidGame>>& Capid::getGames() {
    return serverManager->getGames();
}

void Capid::addScope(const std::shared_ptr<Scope>& scope) {
    this->scopes.insert(scope->getName(), scope);
}

void Capid::acceptConnection() {
    QTcpSocket* socket = server->nextPendingConnection();
    addConnection(new TcpClientConnection(socket));
}

void Capid::handleDisconnect(const std::shared_ptr<CapidPlayer>& player) {
    qDebug() << "Connection to player " + player->getId() + " lost";

    if (serverManager->isConnecting(player)) {
        serverManager->deleteConnectingPlayer(player);
    } else {
        QJsonObject const playerUpdate{
            {"id",        player->getId()},
            {"connected", false}
        };
        serverManager->broadcast("playerUpdate", playerUpdate, player);
    }
}

void Capid::playerCommand(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& command) {
    QList<QString> const baseErrors = CapidMessageUtils::validate(command, "", commandBaseCheck);
    if (!baseErrors.isEmpty()) {
        QJsonArray array;
        for (const QString& error : baseErrors) {
            array.append(error);
        }

        serverManager->sendToPlayer(
            player, "error",
            {
                {"list", array}
            }
        );

        return;
    }

    QString const nonce = command.value("nonce").toString();
    QString const commandScope = command.value("scope").toString();
    QString const commandName = command.value("command").toString();
    QJsonObject const commandArgs = command.value("arguments").toObject();

    if (player->isNonceUsed(nonce, serverManager->getTime(), 300)) {
        serverManager->sendToPlayer(
            player, "error",
            {
                {"list", QJsonArray{"Nonce already used"}}
            }
        );
        return;
    }

    player->addUsedNonce(nonce, serverManager->getTime());

    if (!scopes.contains(commandScope)) {
        serverManager->sendCommandResponse(player, nonce, "UNKNOWN_SCOPE", {}, {});
        return;
    }

    auto oldGame = serverManager->findGameByPlayer(player);

    scopes.value(commandScope)->executeCommand(player, nonce, commandName, commandArgs);

    auto newGame = serverManager->findGameByPlayer(player);

    if (oldGame) {
        gameStateListeners->run(oldGame, serverManager);
    }
    if (newGame && oldGame != newGame) {
        gameStateListeners->run(newGame, serverManager);
    }
}

void Capid::tick() {
    serverManager->incrTime();

    for (const auto& p : serverManager->getPlayers()) {
        p->clearOldNonce(serverManager->getTime(), 300);
    }

    for (const auto& g : serverManager->getGames()) {
        gameManager->tickGame(g);

        gameStateListeners->run(g, serverManager);
    }
}
