#ifndef CAPICITY2_CAPID_H
#define CAPICITY2_CAPID_H

#include <QObject>
#include <QTcpServer>
#include <QTimer>

#include "CapidPlayer.h"
#include "GameManager.h"
#include "RNG.h"
#include "ServerManager.h"
#include "game/CapidGameReader.h"
#include "command/Scope.h"
#include "statelistener/GameStateListeners.h"

class Capid : public QObject {

    public:
        explicit Capid(const QString& gamesDir, const std::shared_ptr<RNG>& rng);
        ~Capid() override;

        std::shared_ptr<CapidPlayer> addConnection(ClientConnection* connection);
        void enableTestMode();
        const QList<std::shared_ptr<CapidGame>>& getGames();

    private:
        void addScope(const std::shared_ptr<Scope>& scope);

        void acceptConnection();
        void handleDisconnect(const std::shared_ptr<CapidPlayer>& player);
        void playerCommand(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& command);
        void tick();

        std::shared_ptr<ServerManager> serverManager;
        std::shared_ptr<GameManager> gameManager;
        std::unique_ptr<GameStateListeners> gameStateListeners;
        ImageRegistry imageRegistry;

        QTcpServer* server;
        std::shared_ptr<JsonValueCheck> commandBaseCheck;
        QMap<QString, std::shared_ptr<Scope>> scopes;

        QTimer* timer;
};

#endif //CAPICITY2_CAPID_H
