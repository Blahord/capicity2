#ifndef CAPICITY2_GAMEMANAGER_H
#define CAPICITY2_GAMEMANAGER_H

#include "ServerManager.h"

class GameManager {

    public:
        explicit GameManager(const std::shared_ptr<ServerManager>& serverManager);
        virtual ~GameManager();

        void tickGame(const std::shared_ptr<CapidGame>& game);
        void endMovement(const std::shared_ptr<CapidGame>& game);
        void endTax(const std::shared_ptr<CapidGame>& game);

        void removePlayerFromGame(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapidPlayer>& player, bool sendMessagesToPlayer);
        void endTurn(const std::shared_ptr<CapidGame>& game);

        static int getPlayerAssets(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& player);
        void payTaxes(const std::shared_ptr<CapidGame>& game, const std::weak_ptr<CapidEstate>& target, int amount);

        static QJsonObject getGameInfoObject(const std::shared_ptr<CapidGame>& game);
        static QJsonObject getGameDataObject(const std::shared_ptr<CapidGame>& game, const QString& language);
        static QJsonObject getGameUpdateObject(const std::shared_ptr<CapidGame>& game);
        static QJsonObject getEstateGroupDataObject(const std::shared_ptr<CapidEstateGroup>& estateGroup, const QString& language);
        static QJsonObject getEstateDataObject(const std::shared_ptr<CapidEstate>& estate, const QString& language);

    private:
        static bool removePlayerFromPlayerOrder(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapidPlayer>& player);

        /**
         * Returns -1, if taxes cannot be payed directly,
         * Returns some number >= 0, if the player can pay the taxes directly with the number be the amount of taxes to pay
         */
        static int canPayTaxDirectly(const std::shared_ptr<CapidGame>& game);

        void broadcastTurnStateChange(const std::shared_ptr<CapidGame>& game);

        std::shared_ptr<ServerManager> serverManager;
};


#endif //CAPICITY2_GAMEMANAGER_H
