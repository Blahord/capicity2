#include "TcpClientConnection.h"

TcpClientConnection::TcpClientConnection(QTcpSocket* socket) : ClientConnection(nullptr) {
    this->capiConnection = new CapiConnection(socket, this);

    connect(capiConnection, &CapiConnection::gotMessage, this, &ClientConnection::gotCommand);
    connect(capiConnection, &CapiConnection::disconnected, this, &ClientConnection::disconnected);
}

TcpClientConnection::~TcpClientConnection() = default;

void TcpClientConnection::send(const QJsonObject& data) {
    capiConnection->send(data);
}

void TcpClientConnection::disconnect() {
    capiConnection->disconnect();
}
