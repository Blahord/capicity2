#ifndef CAPICITY2_GAMESTATELISTENER_H
#define CAPICITY2_GAMESTATELISTENER_H

#include "game/CapidGame.h"
#include "ServerManager.h"

class GameStateListener {

    public:
        explicit GameStateListener();
        virtual ~GameStateListener();

        virtual bool isApplied(const std::shared_ptr<CapidGame>& game) = 0;
        virtual void apply(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<ServerManager>& serverManager) = 0;
};

#endif //CAPICITY2_GAMESTATELISTENER_H
