#include <QJsonArray>
#include "AllReadyListener.h"

AllReadyListener::AllReadyListener(const std::shared_ptr<RNG>& rng) : GameStateListener() {
    this->rng = rng;
}

AllReadyListener::~AllReadyListener() = default;

bool AllReadyListener::isApplied(const std::shared_ptr<CapidGame>& game) {
    bool allReady = game->getState() == GameState::CONFIG &&
                    !game->getPlayers().isEmpty();

    for (const auto& player : game->getPlayers()) {
        allReady &= player->isReady();
    }

    return allReady;
}

void AllReadyListener::apply(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<ServerManager>& serverManager) {
    int const startPosition = game->getPositionOfStartEstate();

    for (const auto& gamePlayer : game->getPlayers()) {
        gamePlayer->setPosition(startPosition);
        gamePlayer->setMoney(game->getStartMoney());
        gamePlayer->setInJail(false);

        serverManager->broadcast(
            game,
            "gamePlayerUpdate",
            {
                {"id",       gamePlayer->getPlayer()->getId()},
                {"money",    gamePlayer->getMoney()},
                {"position", gamePlayer->getPosition()},
                {"inJail",   gamePlayer->isInJail()}
            }
        );
    }

    shufflePlayers(game);

    QJsonArray playersOrderArray;
    for (const auto& gamePlayer1 : game->getPlayerOrder()) {
        playersOrderArray.append(gamePlayer1->getPlayer()->getId());
    }

    game->setPlayerOnTurnIndex(0);
    game->setState(GameState::RUN);
    game->setTurnState(TurnState::ROLL);

    serverManager->broadcast(
        "gameUpdate",
        {
            {"id",    game->getId()},
            {"state", game->getStateName()},
        }
    );

    serverManager->broadcast(
        game,
        "gameUpdate",
        {
            {"id",          game->getId()},
            {"turnState",   game->getTurnStateName()},
            {"playerOrder", playersOrderArray},
            {"turnPlayer",  game->getPlayerOnTurn()->getPlayer()->getId()}
        }
    );

    serverManager->broadcastGameEvent(
        game,
        "started",
        QJsonValue::Null
    );

    serverManager->broadcastGameEvent(
        game,
        "turn",
        game->getPlayerOnTurn()->getPlayer()->getId()
    );
}

void AllReadyListener::shufflePlayers(const std::shared_ptr<CapidGame>& game) {
    QList<QString> playerNames;

    for (const auto& gamePlayer : game->getPlayers()) {
        playerNames.append(gamePlayer->getPlayer()->getId());
    }

    for (int i = 0; i < playerNames.size(); i++) {
        int const swapPos = rng->next(0, playerNames.size() - 1);
        playerNames.swapItemsAt(i, swapPos);
    }

    game->setPlayerOrder(playerNames);
}
