#include "EmptyGameListener.h"

EmptyGameListener::EmptyGameListener() : GameStateListener() {

}

EmptyGameListener::~EmptyGameListener() = default;


bool EmptyGameListener::isApplied(const std::shared_ptr<CapidGame>& game) {
    return game->getPlayers().isEmpty();
}

void EmptyGameListener::apply(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<ServerManager>& serverManager) {
    serverManager->broadcast("gameDelete", QJsonObject{{"id", game->getId()}});
    serverManager->deleteGame(game);
}
