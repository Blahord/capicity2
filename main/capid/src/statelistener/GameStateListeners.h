#ifndef CAPICITY2_GAMESTATELISTENERS_H
#define CAPICITY2_GAMESTATELISTENERS_H

#include "GameStateListener.h"
#include "RNG.h"

class GameStateListeners {

    public:
        explicit GameStateListeners(const std::shared_ptr<RNG>& rng);
        virtual ~GameStateListeners();

        void run(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<ServerManager>& serverManager);

    private:
        QList<GameStateListener*> listeners;
};


#endif //CAPICITY2_GAMESTATELISTENERS_H
