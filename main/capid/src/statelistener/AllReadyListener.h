#ifndef CAPICITY2_ALLREADYLISTENER_H
#define CAPICITY2_ALLREADYLISTENER_H

#include "GameStateListener.h"
#include "RNG.h"

class AllReadyListener : public GameStateListener {

    public:
        explicit AllReadyListener(const std::shared_ptr<RNG>& rng);
        ~AllReadyListener() override;

        bool isApplied(const std::shared_ptr<CapidGame>& game) override;
        void apply(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<ServerManager>& serverManager) override;

    private:
        void shufflePlayers(const std::shared_ptr<CapidGame>& game);

        std::shared_ptr<RNG> rng;
};


#endif //CAPICITY2_ALLREADYLISTENER_H
