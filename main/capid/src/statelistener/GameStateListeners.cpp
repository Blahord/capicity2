#include "GameStateListeners.h"

#include <QDebug>

#include "EmptyGameListener.h"
#include "AllReadyListener.h"

GameStateListeners::GameStateListeners(const std::shared_ptr<RNG>& rng) {
    listeners.append(new EmptyGameListener());
    listeners.append(new AllReadyListener(rng));
}

GameStateListeners::~GameStateListeners() {
    qDeleteAll(listeners);
}

void GameStateListeners::run(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<ServerManager>& serverManager) {
    QList<GameStateListener*> appliedListeners;

    for (auto listener : listeners) {
        if (listener->isApplied(game)) {
            appliedListeners.append(listener);
        }
    }

    for (auto listener : appliedListeners) {
        listener->apply(game, serverManager);
    }
}
