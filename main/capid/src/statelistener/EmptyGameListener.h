#ifndef CAPICITY2_EMPTYGAMELISTENER_H
#define CAPICITY2_EMPTYGAMELISTENER_H

#include "GameStateListener.h"

class EmptyGameListener : public GameStateListener {

    public:
        explicit EmptyGameListener();
        ~EmptyGameListener() override;

        bool isApplied(const std::shared_ptr<CapidGame>& game) override;
        void apply(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<ServerManager>& serverManager) override;
};


#endif //CAPICITY2_EMPTYGAMELISTENER_H
