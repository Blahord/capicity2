#include "CapidMessageUtils.h"

#include <QJsonArray>

QList<QString> CapidMessageUtils::validate(const QJsonObject& object, const QString& path, const std::shared_ptr<JsonValueCheck>& check) {
    QList<JsonError> errors;
    check->check(object, path, errors);

    if (errors.isEmpty()) {
        return {};
    }

    QList<QString> errorList;
    for (const auto& error : errors) {
        errorList.append(QString("%1 : %2").arg(error.getPath(), error.getMessage()));
    }

    return errorList;
}


