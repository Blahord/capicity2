#ifndef CAPICITY2_SERVERMANAGER_H
#define CAPICITY2_SERVERMANAGER_H

#include "CapidPlayer.h"
#include "game/CapidGame.h"
#include "image/ImageRegistry.h"
#include "CapidPlayerConnection.h"

class ServerManager {

    public:
        explicit ServerManager();
        virtual ~ServerManager();

        void broadcast(const QString& type, const QJsonObject& data, const std::shared_ptr<CapidPlayer>& exclude = nullptr);
        void broadcast(const std::shared_ptr<CapidGame>& game, const QString& type, const QJsonObject& data);
        void broadcastGameEvent(const std::shared_ptr<CapidGame>& game, const QString& eventType, const QJsonValue& data);

        void sendToPlayer(const std::shared_ptr<CapidPlayer>& player, const QString& type, const QJsonObject& data);
        void sendCommandResponse(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QString& result, const QList<QString>& errors, const QJsonObject& data);

        void addConnectingPlayer(std::unique_ptr<CapidPlayerConnection> connection);
        bool isConnecting(const std::shared_ptr<CapidPlayer>& player);
        void deleteConnectingPlayer(const std::shared_ptr<CapidPlayer>& player);
        void transferConnection(const std::shared_ptr<CapidPlayer>& from, const std::shared_ptr<CapidPlayer>& to);

        void addPlayer(const std::shared_ptr<CapidPlayer>& player);
        std::shared_ptr<CapidPlayer> findPlayer(const QString& id);
        std::shared_ptr<CapidPlayer> findPlayerByName(const QString& name);
        std::shared_ptr<CapidPlayer> findServerPlayerByToken(const QString& token);
        bool isLoggedIn(const std::shared_ptr<CapidPlayer>& player);
        const QList<std::shared_ptr<CapidPlayer>>& getPlayers();
        void deletePlayer(const std::shared_ptr<CapidPlayer>& player);

        void addGameTemplate(const CapidGameTemplate& gameTemplate);
        CapidGameTemplate findGameTemplate(const QString& id);
        const QList<CapidGameTemplate>& getGameTemplates();

        void addGame(const std::shared_ptr<CapidGame>& game);
        std::shared_ptr<CapidGame> findGameById(const QString& id);
        std::shared_ptr<CapidGame> findGameByPlayer(const std::shared_ptr<CapidPlayer>& player);
        const QList<std::shared_ptr<CapidGame>>& getGames();
        void deleteGame(const std::shared_ptr<CapidGame>& game);

        int getTime() const;
        void incrTime();

    private:
        static QJsonObject toMessageObject(const QString& type, const QJsonObject& data);
        std::shared_ptr<CapidPlayerConnection> findConnection(const std::shared_ptr<CapidPlayer>& player);

        int time = 0;

        QMap<std::shared_ptr<CapidPlayer>, std::shared_ptr<CapidPlayerConnection>> connections;

        QList<std::shared_ptr<CapidPlayer>> connectingPlayers;
        QList<std::shared_ptr<CapidPlayer>> players;

        QList<CapidGameTemplate> gameTemplates;
        QList<std::shared_ptr<CapidGame>> games;
};


#endif //CAPICITY2_SERVERMANAGER_H
