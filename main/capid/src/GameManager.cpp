#include "GameManager.h"

#include <QJsonArray>

GameManager::GameManager(const std::shared_ptr<ServerManager>& serverManager) : serverManager(serverManager) {
}

GameManager::~GameManager() = default;

void GameManager::endMovement(const std::shared_ptr<CapidGame>& game) {
    serverManager->broadcast(
        game,
        "gamePlayerUpdate",
        {
            {"id",       game->getPlayerOnTurn()->getPlayer()->getId()},
            {"position", game->getPlayerOnTurn()->getPosition()},
        }
    );

    serverManager->broadcastGameEvent(
        game,
        "land",
        game->getPlayerOnTurn()->getPosition()
    );

    auto estate = game->getEstates().at(game->getPlayerOnTurn()->getPosition());

    int const taxAmount = canPayTaxDirectly(game);
    if (taxAmount >= 0) {
        payTaxes(game, estate->getPayTarget(), taxAmount);
        endTax(game);
        return;
    }

    game->setTurnState(TurnState::TAX);
    broadcastTurnStateChange(game);
    serverManager->broadcastGameEvent(
        game, "taxPayNeeded",
        QJsonObject{
            {"amount",  estate->getTaxAmount()},
            {"percent", estate->getTaxPercent()}
        }
    );
}

void GameManager::endTax(const std::shared_ptr<CapidGame>& game) {
    auto estate = game->getEstates().at(game->getPlayerOnTurn()->getPosition());

    if (estate->getPrice() <= 0 || estate->getOwner()) {
        endTurn(game);
        return;
    }

    game->setTurnState(TurnState::BUY);
    broadcastTurnStateChange(game);

    serverManager->broadcastGameEvent(
        game,
        "canBuy",
        QJsonValue::Null
    );
}

void GameManager::tickGame(const std::shared_ptr<CapidGame>& game) {
    if (game->getState() == GameState::RUN && game->getTurnState() == TurnState::MOVE) {
        game->incrMovementTime();

        if (game->getMovementTime() > 5) {
            endMovement(game);
        }
    }
}

void GameManager::removePlayerFromGame(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapidPlayer>& player, bool sendMessagesToPlayer) {
    bool const playerTurnEnds = removePlayerFromPlayerOrder(game, player);

    game->removePlayer(player);
    bool const isOwner = game->getOwner() == player;
    if (isOwner) {
        if (game->getPlayers().isEmpty()) {
            game->setOwner(nullptr);
        } else {
            game->setOwner(game->getPlayers().at(0)->getPlayer());
        }
    }

    if (playerTurnEnds && game->getPlayerOrder().size() > 1) {
        game->setTurnState(TurnState::ROLL);
        serverManager->broadcastGameEvent(game, "turn", game->getPlayerOnTurn()->getPlayer()->getId());
    }
    if (game->getState() == GameState::RUN && game->getPlayerOrder().size() == 1) {
        game->setTurnState(TurnState::NOTHING);
        game->setState(GameState::END);
        serverManager->broadcastGameEvent(game, "end", game->getPlayerOrder().at(0)->getPlayer()->getId());
    }

    if (!game->getPlayers().isEmpty()) {
        serverManager->broadcast("gameUpdate", GameManager::getGameUpdateObject(game), sendMessagesToPlayer ? nullptr : player);
    }
}

void GameManager::endTurn(const std::shared_ptr<CapidGame>& game) {
    game->setPlayerOnTurnIndex((game->getPlayerOnTurnIndex() + 1) % game->getPlayerOrder().size());

    auto nextPlayer = game->getPlayerOnTurn();
    game->setTurnState(TurnState::ROLL);

    serverManager->broadcast(
        game,
        "gameUpdate",
        QJsonObject{
            {"id",         game->getId()},
            {"turnPlayer", nextPlayer->getPlayer()->getId()},
            {"turnState",  game->getTurnStateName()}
        }
    );

    serverManager->broadcastGameEvent(
        game,
        "turn",
        nextPlayer->getPlayer()->getId()
    );
}

int GameManager::getPlayerAssets(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& player) {
    int result = player->getMoney();

    for (const auto& estate : game->getEstates()) {
        if (estate->getOwner() == player) {
            result += estate->getPrice();
        }
    }
    return result;
}

QJsonObject GameManager::getGameInfoObject(const std::shared_ptr<CapidGame>& game) {
    QJsonArray playersArray;

    for (const auto& gamePlayer : game->getPlayers()) {
        playersArray.append(gamePlayer->getPlayer()->getId());
    }

    return {
        {"id",        game->getId()},
        {"type",      game->getTemplate().getId()},
        {"owner",     game->getOwner()->getId()},
        {"state",     game->getStateName()},
        {"turnState", game->getTurnStateName()},
        {"players",   playersArray}
    };
}

QJsonObject GameManager::getGameUpdateObject(const std::shared_ptr<CapidGame>& game) {
    QJsonArray playersArray;
    for (const auto& gamePlayer : game->getPlayers()) {
        playersArray.append(gamePlayer->getPlayer()->getId());
    }

    QJsonObject gameUpdateData = {
        {"id",        game->getId()},
        {"owner",     game->getOwner()->getId()},
        {"state",     game->getStateName()},
        {"turnState", game->getTurnStateName()},
        {"players",   playersArray}
    };

    if (game->getState() == GameState::RUN) {
        QJsonArray playerOrderArray;
        for (const auto& gamePlayer : game->getPlayerOrder()) {
            playerOrderArray.append(gamePlayer->getPlayer()->getId());
        }

        gameUpdateData.insert("playerOrder", playerOrderArray);
        gameUpdateData.insert("turnPlayer", game->getPlayerOnTurn()->getPlayer()->getId());
    }

    return gameUpdateData;
}

QJsonObject GameManager::getGameDataObject(const std::shared_ptr<CapidGame>& game, const QString& language) {
    QJsonObject dataObject;

    dataObject.insert("id", game->getId());
    dataObject.insert("bgColor", game->getBgColor());

    QJsonArray estateGroupsArray;
    for (const auto& group : game->getEstateGroups()) {
        estateGroupsArray.append(getEstateGroupDataObject(group, language));
    }
    dataObject.insert("estateGroups", estateGroupsArray);

    QJsonArray estatesArray;
    for (const auto& estate : game->getEstates()) {
        estatesArray.append(getEstateDataObject(estate, language));
    }
    dataObject.insert("estates", estatesArray);

    return dataObject;

}

QJsonObject GameManager::getEstateGroupDataObject(const std::shared_ptr<CapidEstateGroup>& estateGroup, const QString& language) {
    QJsonObject dataObject;

    dataObject.insert("id", estateGroup->getId());
    dataObject.insert("name", estateGroup->getName(language));
    dataObject.insert("housePrice", estateGroup->getHousePrice());

    if (!estateGroup->getColor().isEmpty()) {
        dataObject.insert("color", estateGroup->getColor());
    }

    return dataObject;
}

QJsonObject GameManager::getEstateDataObject(const std::shared_ptr<CapidEstate>& estate, const QString& language) {
    QJsonObject dataObject{
        {"id",         estate->getId()},
        {"name",       estate->getName(language)},
        {"price",      estate->getPrice()},
        {"rent0",      estate->getRent0()},
        {"rent1",      estate->getRent1()},
        {"rent2",      estate->getRent2()},
        {"rent3",      estate->getRent3()},
        {"rent4",      estate->getRent4()},
        {"rent5",      estate->getRent5()},
        {"taxAmount",  estate->getTaxAmount()},
        {"taxPercent", estate->getTaxPercent()},
        {"payTarget",  estate->getPayTargetId()}
    };

    if (estate->getEstateGroup()) {
        dataObject.insert("estateGroup", estate->getEstateGroup()->getId());
    }

    if (!estate->getImage().isNull()) {
        dataObject.insert("image", estate->getImage());
    }

    return dataObject;
}

bool GameManager::removePlayerFromPlayerOrder(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapidPlayer>& player) {
    if (game->getState() != GameState::RUN) {
        return false;
    }

    auto gamePlayer = game->findPlayer(player);
    int const playerTurnIndex = game->getPlayerOrder().indexOf(gamePlayer);
    bool const isPlayerOnTurn = playerTurnIndex == game->getPlayerOnTurnIndex();
    game->removeFromPlayerOrder(player);

    if (playerTurnIndex < game->getPlayerOnTurnIndex()) {
        game->setPlayerOnTurnIndex(game->getPlayerOnTurnIndex() - 1);
    }

    game->setPlayerOnTurnIndex(game->getPlayerOnTurnIndex() % game->getPlayerOrder().size());

    return isPlayerOnTurn;
}

void GameManager::payTaxes(const std::shared_ptr<CapidGame>& game, const std::weak_ptr<CapidEstate>& target, int amount) {
    if (amount <= 0) {
        return;
    }

    game->getPlayerOnTurn()->setMoney(game->getPlayerOnTurn()->getMoney() - amount);
    auto const payTarget = target.lock();
    if (payTarget) {
        payTarget->setMoney(payTarget->getMoney() + amount);
        serverManager->broadcast(
            game, "estateUpdate",
            {
                {"id",    payTarget->getId()},
                {"money", payTarget->getMoney()}
            }
        );
    }

    serverManager->broadcast(
        game, "gamePlayerUpdate",
        {
            {"id",    game->getPlayerOnTurn()->getPlayer()->getId()},
            {"money", game->getPlayerOnTurn()->getMoney()}
        }
    );

    serverManager->broadcastGameEvent(game, "taxPayed", amount);
}

int GameManager::canPayTaxDirectly(const std::shared_ptr<CapidGame>& game) {
    auto playerOnTurn = game->getPlayerOnTurn();
    auto estate = game->getEstates().at(playerOnTurn->getPosition());

    if (estate->getTaxPercent() <= 0 && playerOnTurn->getMoney() >= estate->getTaxAmount()) {
        return estate->getTaxAmount();
    }

    int const amount = (estate->getTaxPercent() * getPlayerAssets(game, game->getPlayerOnTurn())) / 100;
    if (estate->getTaxAmount() <= 0 && playerOnTurn->getMoney() >= amount) {
        return amount;
    }

    return -1;
}

void GameManager::broadcastTurnStateChange(const std::shared_ptr<CapidGame>& game) {
    serverManager->broadcast(
        game,
        "gameUpdate",
        {
            {"id",        game->getId()},
            {"turnState", game->getTurnStateName()}
        }
    );
}
