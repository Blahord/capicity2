#ifndef CAPICITY2_CAPIDMESSAGEUTILS_H
#define CAPICITY2_CAPIDMESSAGEUTILS_H

#include "json/checks/JsonValueCheck.h"
#include "CapidPlayer.h"

class CapidMessageUtils {

    public:
        static QList<QString> validate(const QJsonObject& object, const QString& path, const std::shared_ptr<JsonValueCheck>& check);
};


#endif //CAPICITY2_CAPIDMESSAGEUTILS_H
