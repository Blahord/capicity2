#ifndef CAPICITY2_RNG_H
#define CAPICITY2_RNG_H

class RNG {

    public:
        virtual int next() = 0;
        virtual int next(int min, int max) = 0;
};


#endif //CAPICITY2_RNG_H
