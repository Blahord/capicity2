#ifndef CAPICITY2_CAPIDPLAYERCONNECTION_H
#define CAPICITY2_CAPIDPLAYERCONNECTION_H

#include <QObject>
#include "CapidPlayer.h"

class CapidPlayerConnection : public QObject {
    Q_OBJECT

    public:
        explicit CapidPlayerConnection(const std::shared_ptr<CapidPlayer>& player, ClientConnection* connection, QObject* parent = nullptr);
        ~CapidPlayerConnection() override;

        void send(const QJsonObject& data);
        void disconnectFromClient();

        std::shared_ptr<CapidPlayer> getPlayer();

        ClientConnection* getConnection() const;
        void setConnection(ClientConnection* newConnection);

    private slots:
        void emitCommand(const QJsonObject& command);
        void emitDisconnected();

    signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
        void disconnected(const std::shared_ptr<CapidPlayer>& player);
        void gotCommand(const std::shared_ptr<CapidPlayer>& player, QJsonObject command);
#pragma clang diagnostic pop

    private:
        std::shared_ptr<CapidPlayer> player;
        ClientConnection* connection = nullptr;
};


#endif //CAPICITY2_CAPIDPLAYERCONNECTION_H
