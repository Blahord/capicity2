#ifndef CAPICITY2_TCPCLIENTCONNECTION_H
#define CAPICITY2_TCPCLIENTCONNECTION_H

#include <QTcpSocket>

#include "ClientConnection.h"

class TcpClientConnection : public ClientConnection {

    public:
        explicit TcpClientConnection(QTcpSocket* socket);
        ~TcpClientConnection() override;

        void send(const QJsonObject& data) override;
        void disconnect() override;

    private:
        CapiConnection* capiConnection = nullptr;
};


#endif //CAPICITY2_TCPCLIENTCONNECTION_H
