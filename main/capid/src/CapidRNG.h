#ifndef CAPICITY2_CAPIDRNG_H
#define CAPICITY2_CAPIDRNG_H

#include <QRandomGenerator>

#include "RNG.h"

class CapidRNG : public RNG {

    public:
        int next() override;
        int next(int min, int max) override;

    private:
        QRandomGenerator randomGenerator = QRandomGenerator::securelySeeded();
};


#endif //CAPICITY2_CAPIDRNG_H
