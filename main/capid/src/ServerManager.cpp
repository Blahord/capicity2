#include "ServerManager.h"

#include <QJsonArray>

ServerManager::ServerManager() = default;

ServerManager::~ServerManager() {
//    for (auto connection : connections.values()) {
//        delete connection;
//    }
}

void ServerManager::broadcast(const QString& type, const QJsonObject& data, const std::shared_ptr<CapidPlayer>& exclude) {
    for (const auto& player : players) {
        if (player == exclude) {
            continue;
        }

        sendToPlayer(player, type, data);
    }
}

void ServerManager::broadcast(const std::shared_ptr<CapidGame>& game, const QString& type, const QJsonObject& data) {
    for (const auto& gamePlayer : game->getPlayers()) {
        sendToPlayer(findPlayer(gamePlayer->getPlayer()->getId()), type, data);
    }
}

void ServerManager::broadcastGameEvent(const std::shared_ptr<CapidGame>& game, const QString& eventType, const QJsonValue& data) {
    broadcast(
        game,
        "gameEvent",
        {
            {"type", eventType},
            {"data", data}
        }
    );
}

void ServerManager::sendToPlayer(const std::shared_ptr<CapidPlayer>& player, const QString& type, const QJsonObject& data) {
    findConnection(player)->send(toMessageObject(type, data));
}

void ServerManager::sendCommandResponse(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QString& result, const QList<QString>& errors, const QJsonObject& data) {
    QJsonObject dataObject = {
        {"nonce",  nonce},
        {"result", result},
    };

    if (!errors.isEmpty()) {
        QJsonArray array;
        for (const auto& error : errors) {
            array.append(error);
        }

        dataObject.insert("errors", array);
    }

    if (!data.isEmpty()) {
        dataObject.insert("data", data);
    }

    findConnection(player)->send(
        {
            {"type", "commandResponse"},
            {"data", dataObject}
        }
    );
}

void ServerManager::addConnectingPlayer(std::unique_ptr<CapidPlayerConnection> connection) {
    std::shared_ptr<CapidPlayerConnection> conn = std::move(connection);

    connections.insert(conn->getPlayer(), conn);
    connectingPlayers.append(conn->getPlayer());
}

bool ServerManager::isConnecting(const std::shared_ptr<CapidPlayer>& player) {
    return connectingPlayers.contains(player);
}

void ServerManager::deleteConnectingPlayer(const std::shared_ptr<CapidPlayer>& player) {
    connectingPlayers.removeOne(player);
    auto connection = findConnection(player);
    connections.remove(player);
    connection->deleteLater();
}

void ServerManager::transferConnection(const std::shared_ptr<CapidPlayer>& from, const std::shared_ptr<CapidPlayer>& to) {
    auto connFrom = findConnection(from);
    auto connTo = findConnection(to);

    connTo->setConnection(connFrom->getConnection());
    connFrom->setConnection(nullptr);
}

void ServerManager::addPlayer(const std::shared_ptr<CapidPlayer>& player) {
    connectingPlayers.removeOne(player);
    players.append(player);
}

std::shared_ptr<CapidPlayer> ServerManager::findPlayer(const QString& id) {
    for (auto player : players) {
        if (player->getId() == id) {
            return player;
        }
    }

    return nullptr;
}

std::shared_ptr<CapidPlayer> ServerManager::findPlayerByName(const QString& name) {
    for (const auto& player : players) {
        if (player->getName() == name) {
            return player;
        }
    }

    return nullptr;
}

std::shared_ptr<CapidPlayer> ServerManager::findServerPlayerByToken(const QString& token) {
    for (const auto& player : players) {
        if (player->getToken() == token) {
            return player;
        }
    }

    return nullptr;
}

bool ServerManager::isLoggedIn(const std::shared_ptr<CapidPlayer>& player) {
    return findPlayer(player->getId()).operator bool();
}

const QList<std::shared_ptr<CapidPlayer>>& ServerManager::getPlayers() {
    return players;
}

void ServerManager::deletePlayer(const std::shared_ptr<CapidPlayer>& player) {
    players.removeOne(player);
    connectingPlayers.removeOne(player);

    auto connection = findConnection(player);

    connection->disconnectFromClient();
    connection->deleteLater();

    connections.remove(player);
}

void ServerManager::addGameTemplate(const CapidGameTemplate& gameTemplate) {
    gameTemplates.append(gameTemplate);
}

CapidGameTemplate ServerManager::findGameTemplate(const QString& id) {
    for (const auto& gameTemplate : gameTemplates) {
        if (gameTemplate.getId() == id) {
            return gameTemplate;
        }
    }

    return CapidGameTemplate("");
}

const QList<CapidGameTemplate>& ServerManager::getGameTemplates() {
    return gameTemplates;
}

void ServerManager::addGame(const std::shared_ptr<CapidGame>& game) {
    games.append(game);
}

std::shared_ptr<CapidGame> ServerManager::findGameById(const QString& id) {
    for (const auto& game : games) {
        if (game->getId() == id) {
            return game;
        }
    }

    return nullptr;
}

std::shared_ptr<CapidGame> ServerManager::findGameByPlayer(const std::shared_ptr<CapidPlayer>& player) {
    for (const auto& game : games) {
        bool const inGame = game->containsPlayer(player);
        if (inGame) {
            return game;
        }
    }

    return nullptr;
}

const QList<std::shared_ptr<CapidGame>>& ServerManager::getGames() {
    return games;
}

void ServerManager::deleteGame(const std::shared_ptr<CapidGame>& game) {
    games.removeOne(game);
}

int ServerManager::getTime() const {
    return time;
}

void ServerManager::incrTime() {
    time++;
}

QJsonObject ServerManager::toMessageObject(const QString& type, const QJsonObject& data) {
    return {
        {"type", type},
        {"data", data}
    };
}

std::shared_ptr<CapidPlayerConnection> ServerManager::findConnection(const std::shared_ptr<CapidPlayer>& player) {
    return connections.value(player, nullptr);
}
