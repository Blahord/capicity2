#ifndef CAPICITY2_CAPIDPLAYER_H
#define CAPICITY2_CAPIDPLAYER_H

#include <QJsonObject>

#include "ClientConnection.h"
#include "CapiPlayer.h"

class CapidPlayer : public CapiPlayer {
    public:
        explicit CapidPlayer();
        ~CapidPlayer() override;

        QJsonObject getDataObject();

        QString getToken() const;

        const QString& getLanguage() const;
        void setLanguage(const QString& lang);

        void clearOldNonce(long serverTime, long maxAge);
        bool isNonceUsed(const QString& nonce, long serverTime, long maxAge);
        void addUsedNonce(const QString& nonce, long serverTime);

    private:
        QString token;
        QString language = "eng";

        QMap<QString, long> nonceTimes;
};

#endif //CAPICITY2_CAPIDPLAYER_H
