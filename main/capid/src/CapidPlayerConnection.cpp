#include "CapidPlayerConnection.h"

CapidPlayerConnection::CapidPlayerConnection(const std::shared_ptr<CapidPlayer>& player, ClientConnection* connection, QObject* parent)
    : QObject(parent) {
    this->player = player;
    setConnection(connection);
}

CapidPlayerConnection::~CapidPlayerConnection() {
    if (connection) {
        disconnect(connection, &ClientConnection::gotCommand, this, &CapidPlayerConnection::emitCommand);
        disconnect(connection, &ClientConnection::disconnected, this, &CapidPlayerConnection::emitDisconnected);

        connection->disconnect();
        connection->deleteLater();

        connection = nullptr;
    }
}

void CapidPlayerConnection::send(const QJsonObject& data) {
    if (connection) {
        connection->send(data);
    }
}

void CapidPlayerConnection::disconnectFromClient() {
    if (connection) {
        connection->disconnect();
    }
}

std::shared_ptr<CapidPlayer> CapidPlayerConnection::getPlayer() {
    return player;
}

ClientConnection* CapidPlayerConnection::getConnection() const {
    return connection;
}

void CapidPlayerConnection::setConnection(ClientConnection* newConnection) {
    if (this->connection) {
        disconnect(this->connection, &ClientConnection::gotCommand, this, &CapidPlayerConnection::emitCommand);
        disconnect(this->connection, &ClientConnection::disconnected, this, &CapidPlayerConnection::emitDisconnected);
    }

    this->connection = newConnection;

    if (newConnection) {
        connect(newConnection, &ClientConnection::gotCommand, this, &CapidPlayerConnection::emitCommand);
        connect(newConnection, &ClientConnection::disconnected, this, &CapidPlayerConnection::emitDisconnected);
    }
}


void CapidPlayerConnection::emitCommand(const QJsonObject& command) {
    emit(gotCommand(player, command));
}

void CapidPlayerConnection::emitDisconnected() {
    player->setConnected(false);

    emit(disconnected(player));

    connection->deleteLater();
    connection = nullptr;
}

