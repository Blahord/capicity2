#ifndef CAPICITY2_SCOPE_H
#define CAPICITY2_SCOPE_H

#include "Command.h"

class Scope {

    public:
        explicit Scope(const std::shared_ptr<ServerManager>& serverManager);
        virtual ~Scope();

        virtual QString getName() = 0;

        void executeCommand(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QString& commandName, const QJsonObject& arguments);

    protected:
        void addCommand(const std::shared_ptr<Command>& command);

    private:
        void sendStateError(const std::shared_ptr<CapidPlayer>&, const QString& nonce, const QString& stateError);
        void sendOk(const std::shared_ptr<CapidPlayer>&, const QString& nonce, const QJsonObject& data);

        bool validateCommandArguments(const std::shared_ptr<CapidPlayer>&, const QString& nonce, const QJsonObject& argumentsObject, const std::shared_ptr<JsonValueCheck>& check);

        QMap<QString, std::shared_ptr<Command>> commands;
        std::shared_ptr<ServerManager> serverManager;
};


#endif //CAPICITY2_SCOPE_H
