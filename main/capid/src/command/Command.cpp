#include "Command.h"

Command::Command(const std::shared_ptr<ServerManager>& serverManager) {
    this->serverManager = serverManager;
}

Command::~Command() = default;

std::shared_ptr<JsonValueCheck> Command::getValidation() {
    return validation;
}

QJsonObject Command::getOkResponseObject(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    return {};
}

std::shared_ptr<ServerManager> Command::getServerManager() {
    return serverManager;
}

void Command::setValidation(const std::shared_ptr<JsonValueCheck>& check) {
    this->validation = check;
}
