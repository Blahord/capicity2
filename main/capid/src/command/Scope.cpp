#include "Scope.h"

#include <QJsonArray>

#include "../CapidMessageUtils.h"

Scope::Scope(const std::shared_ptr<ServerManager>& serverManager) {
    this->serverManager = serverManager;
}

Scope::~Scope() = default;

void Scope::executeCommand(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QString& commandName, const QJsonObject& arguments) {
    if (!commands.contains(commandName)) {
        serverManager->sendCommandResponse(player, nonce, "UNKNOWN_COMMAND", {}, {});
        return;
    }

    auto command = commands.value(commandName);
    if (!validateCommandArguments(player, nonce, arguments, command->getValidation())) {
        return;
    }

    auto stateError = command->validateState(player, arguments);
    if (!stateError.isEmpty()) {
        sendStateError(player, nonce, stateError);
        return;
    }

    auto responseData = command->getOkResponseObject(player, nonce, arguments);
    sendOk(player, nonce, responseData);

    command->execute(player, nonce, arguments);
}

void Scope::addCommand(const std::shared_ptr<Command>& command) {
    commands.insert(command->getName(), command);
}

void Scope::sendStateError(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QString& stateError) {
    serverManager->sendCommandResponse(player, nonce, "STATE_ERROR", {stateError}, {});
}

void Scope::sendOk(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& data) {
    serverManager->sendCommandResponse(player, nonce, "OK", {}, data);
}

bool Scope::validateCommandArguments(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& argumentsObject, const std::shared_ptr<JsonValueCheck>& check) {
    if (!check) {
        return true;
    }

    QList<QString> const errors = CapidMessageUtils::validate(argumentsObject, "/arguments", check);

    if (!errors.isEmpty()) {
        QJsonArray array;
        for (const auto& error : errors) {
            array.append(error);
        }

        serverManager->sendToPlayer(
            player,
            "commandResponse",
            QJsonObject{
                {"nonce",  nonce},
                {"result", "FORMAL_ERROR"},
                {"errors", array}
            }
        );

        return false;
    }

    return true;
}
