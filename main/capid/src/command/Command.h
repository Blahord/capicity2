#ifndef CAPICITY2_COMMAND_H
#define CAPICITY2_COMMAND_H

#include <QString>
#include <QJsonObject>

#include "json/checks/JsonValueCheck.h"
#include "../CapidPlayer.h"
#include "../ServerManager.h"

class Command {

    public:
        explicit Command(const std::shared_ptr<ServerManager>& serverManager);
        virtual ~Command();

        virtual QString getName() = 0;
        std::shared_ptr<JsonValueCheck> getValidation();
        virtual QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) = 0;
        virtual QJsonObject getOkResponseObject(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments);
        virtual void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) = 0;

    protected:
        std::shared_ptr<ServerManager> getServerManager();
        void setValidation(const std::shared_ptr<JsonValueCheck>& check);

    private:
        std::shared_ptr<ServerManager> serverManager;
        std::shared_ptr<JsonValueCheck> validation;
};

#endif //CAPICITY2_COMMAND_H
