#include "GetImageCommand.h"

#include "json/JsonChecks.h"

GetImageCommand::GetImageCommand(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry) : Command(serverManager) {
    this->imageRegistry = imageRegistry;

    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("hash", JsonChecks::regexp("^[0-9a-f]+$"))
        )
    );
}

GetImageCommand::~GetImageCommand() = default;

QString GetImageCommand::getName() {
    return "getImage";
}

QString GetImageCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    return {};
}

QJsonObject GetImageCommand::getOkResponseObject(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    QString hash = arguments.value("hash").toString();

    if (!imageRegistry->containsImage(hash)) {
        return {{"found", false}};
    }

    Image image = imageRegistry->getImage(hash);
    return {
        {"found", true},
        {"image", image.toJsonObject()}
    };
}

void GetImageCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    //Nothing to do
}
