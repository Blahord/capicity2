#ifndef CAPICITY2_GETIMAGECOMMAND_H
#define CAPICITY2_GETIMAGECOMMAND_H

#include "../Command.h"
#include "../../image/ImageRegistry.h"

class GetImageCommand : public Command {

    public:
        explicit GetImageCommand(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry);
        ~GetImageCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        QJsonObject getOkResponseObject(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;

    private:
        ImageRegistry* imageRegistry;
};


#endif //CAPICITY2_GETIMAGECOMMAND_H
