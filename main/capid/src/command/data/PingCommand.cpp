#include "PingCommand.h"

#include "json/JsonChecks.h"

PingCommand::PingCommand(const std::shared_ptr<ServerManager>& serverManager) : Command(serverManager) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("message", JsonChecks::stringValue())
        )
    );
}

PingCommand::~PingCommand() = default;

QString PingCommand::getName() {
    return "ping";
}

QString PingCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    return {};
}

QJsonObject PingCommand::getOkResponseObject(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    QString message = arguments.value("message").toString();
    return {{"pong", message}};
}

void PingCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    // Nothing to do
}
