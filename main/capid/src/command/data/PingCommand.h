#ifndef CAPICITY2_PINGCOMMAND_H
#define CAPICITY2_PINGCOMMAND_H

#include "../Command.h"

class PingCommand : public Command {

    public:
        explicit PingCommand(const std::shared_ptr<ServerManager>& serverManager);
        ~PingCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        QJsonObject getOkResponseObject(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_PINGCOMMAND_H
