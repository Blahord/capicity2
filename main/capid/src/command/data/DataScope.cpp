#include "DataScope.h"

#include "GetImageCommand.h"
#include "PingCommand.h"

DataScope::DataScope(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry) : Scope(serverManager) {
    addCommand(std::make_shared<GetImageCommand>(serverManager, imageRegistry));
    addCommand(std::make_shared<PingCommand>(serverManager));
}

DataScope::~DataScope() = default;

QString DataScope::getName() {
    return "data";
}


