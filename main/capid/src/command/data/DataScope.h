#ifndef CAPICITY2_DATASCOPE_H
#define CAPICITY2_DATASCOPE_H

#include "../Scope.h"

class DataScope : public Scope {

    public:
        explicit DataScope(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry);
        ~DataScope() override;

        QString getName() override;
};


#endif //CAPICITY2_DATASCOPE_H
