#include "ClearAvatarCommand.h"

ClearAvatarCommand::ClearAvatarCommand(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry) : ServerCommand(serverManager) {
    this->imageRegistry = imageRegistry;
}

ClearAvatarCommand::~ClearAvatarCommand() = default;

QString ClearAvatarCommand::getName() {
    return "clearAvatar";
}

QString ClearAvatarCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    return {};
}

void ClearAvatarCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    imageRegistry->removeUsage(player->getAvatarData(), QString("avatar/%1").arg(player->getName()));
    player->setAvatarData("");

    getServerManager()->broadcast(
        "playerUpdate",
        {
            {"id",     player->getId()},
            {"avatar", ""}
        }
    );
}
