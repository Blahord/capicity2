#ifndef CAPICITY2_RECONNECTCOMMAND_H
#define CAPICITY2_RECONNECTCOMMAND_H

#include "ServerCommand.h"

class ReconnectCommand : public ServerCommand {

    public:
        explicit ReconnectCommand(const std::shared_ptr<ServerManager>& serverManager);
        ~ReconnectCommand();

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_RECONNECTCOMMAND_H
