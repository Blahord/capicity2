#ifndef CAPICITY2_SERVERCOMMAND_H
#define CAPICITY2_SERVERCOMMAND_H

#include <QJsonArray>

#include "../Command.h"
#include "../../ServerManager.h"

class ServerCommand : public Command {

    public:
        explicit ServerCommand(const std::shared_ptr<ServerManager>& serverManager);
        ~ServerCommand() override;

    protected:
        QJsonArray collectGameTemplates(const std::shared_ptr<CapidPlayer>& player);
        QJsonArray collectGamesData();
        QJsonArray collectPlayersData();

        QString getFreePlayerName(QString baseName);
};


#endif //CAPICITY2_SERVERCOMMAND_H
