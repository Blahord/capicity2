#include "ServerScope.h"

#include "ChatCommand.h"
#include "ClearAvatarCommand.h"
#include "CreateGameCommand.h"
#include "JoinGameCommand.h"
#include "LeaveGameCommand.h"
#include "LoginCommand.h"
#include "QuitCommand.h"
#include "ReconnectCommand.h"
#include "SetAvatarCommand.h"
#include "SetNameCommand.h"

ServerScope::ServerScope(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<GameManager>& gameManager, ImageRegistry* imageRegistry, const QString& gamesDir) : Scope(serverManager) {
    addCommand(std::make_shared<ChatCommand>(serverManager));
    addCommand(std::make_shared<ClearAvatarCommand>(serverManager, imageRegistry));
    addCommand(std::make_shared<CreateGameCommand>(serverManager, imageRegistry, gamesDir));
    addCommand(std::make_shared<JoinGameCommand>(serverManager));
    addCommand(std::make_shared<LeaveGameCommand>(serverManager, gameManager));
    addCommand(std::make_shared<LoginCommand>(serverManager));
    addCommand(std::make_shared<QuitCommand>(serverManager, gameManager));
    addCommand(std::make_shared<ReconnectCommand>(serverManager));
    addCommand(std::make_shared<SetAvatarCommand>(serverManager, imageRegistry));
    addCommand(std::make_shared<SetNameCommand>(serverManager));
}

ServerScope::~ServerScope() = default;

QString ServerScope::getName() {
    return "server";
}
