#include "ReconnectCommand.h"

#include "json/JsonChecks.h"
#include "../../GameManager.h"

ReconnectCommand::ReconnectCommand(const std::shared_ptr<ServerManager>& serverManager) : ServerCommand(serverManager) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("token", JsonChecks::stringValue())
        )
    );
}

ReconnectCommand::~ReconnectCommand() = default;

QString ReconnectCommand::getName() {
    return "reconnect";
}

QString ReconnectCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (getServerManager()->isLoggedIn(player)) {
        return "ALREADY_LOGGED_IN";
    }

    QString const token = arguments.value("token").toString();
    auto existingPlayer = getServerManager()->findServerPlayerByToken(token);

    if (!existingPlayer) {
        return "INVALID_TOKEN";
    }

    return {};
}

void ReconnectCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    QString token = arguments.value("token").toString();
    auto existingPlayer = getServerManager()->findServerPlayerByToken(token);

    getServerManager()->transferConnection(player, existingPlayer);
    player->setConnected(true);

    QJsonObject const connectionData{
        {"token", token},
        {"id",    existingPlayer->getId()},
        {"name",  existingPlayer->getName()}
    };

    getServerManager()->sendToPlayer(existingPlayer, "playerData", connectionData);
    getServerManager()->sendToPlayer(existingPlayer, "playerList", {{"list", collectPlayersData()}});
    getServerManager()->sendToPlayer(existingPlayer, "gameTemplateList", {{"list", collectGameTemplates(player)}});
    getServerManager()->sendToPlayer(existingPlayer, "gameList", {{"list", collectGamesData()}});

    auto playersGame = getServerManager()->findGameByPlayer(existingPlayer);
    if (playersGame) {
        getServerManager()->sendToPlayer(existingPlayer, "gameData", GameManager::getGameDataObject(playersGame, existingPlayer->getLanguage()));
        getServerManager()->sendToPlayer(existingPlayer, "gameUpdate", GameManager::getGameUpdateObject(playersGame));

        for (const auto& gamePlayer : playersGame->getPlayers()) {
            getServerManager()->sendToPlayer(
                existingPlayer, "gamePlayerUpdate",
                {
                    {"id",       gamePlayer->getPlayer()->getId()},
                    {"ready",    gamePlayer->isReady()},
                    {"money",    gamePlayer->getMoney()},
                    {"jailed",   gamePlayer->isInJail()},
                    {"position", gamePlayer->getPosition()}
                }
            );
        }
    }

    QJsonObject const playerUpdate{
        {"id",        existingPlayer->getId()},
        {"connected", true}
    };

    getServerManager()->broadcast("playerUpdate", playerUpdate, player);
    getServerManager()->deleteConnectingPlayer(player);
}
