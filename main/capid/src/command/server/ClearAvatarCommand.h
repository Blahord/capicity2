#ifndef CAPICITY2_CLEARAVATARCOMMAND_H
#define CAPICITY2_CLEARAVATARCOMMAND_H

#include "ServerCommand.h"

class ClearAvatarCommand : public ServerCommand {

    public:
        explicit ClearAvatarCommand(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry);
        ~ClearAvatarCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;

    private:
        ImageRegistry* imageRegistry;
};


#endif //CAPICITY2_CLEARAVATARCOMMAND_H
