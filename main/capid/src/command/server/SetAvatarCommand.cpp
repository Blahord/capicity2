#include "SetAvatarCommand.h"

#include "json/JsonChecks.h"

SetAvatarCommand::SetAvatarCommand(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry) : ServerCommand(serverManager) {
    this->imageRegistry = imageRegistry;

    setValidation(JsonChecks::imageValue());
}

SetAvatarCommand::~SetAvatarCommand() = default;

QString SetAvatarCommand::getName() {
    return "setAvatar";
}

QString SetAvatarCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    return {};
}

void SetAvatarCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    int width = arguments.value("width").toInt(0);
    int height = arguments.value("height").toInt(0);
    QString data = arguments.value("data").toString();

    Image image(width, height, data);
    if (imageRegistry->containsImage(player->getAvatarData())) {
        imageRegistry->removeUsage(player->getAvatarData(), QString("avatar/%1").arg(player->getName()));
    }

    imageRegistry->addImage(image, QString("avatar/%1").arg(player->getName()));
    player->setAvatarData(image.hash());

    getServerManager()->broadcast(
        "playerUpdate",
        {
            {"id",     player->getId()},
            {"avatar", player->getAvatarData()}
        }
    );
}
