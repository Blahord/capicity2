#include "LeaveGameCommand.h"

#include "../../GameManager.h"

LeaveGameCommand::LeaveGameCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<GameManager>& gameManager) : ServerCommand(serverManager) {
    this->gameManager = gameManager;
}

LeaveGameCommand::~LeaveGameCommand() = default;

QString LeaveGameCommand::getName() {
    return "leaveGame";
}

QString LeaveGameCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    auto playersGame = getServerManager()->findGameByPlayer(player);
    if (!playersGame) {
        return "NOT_IN_GAME";
    }

    return {};
}

void LeaveGameCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    auto playersGame = getServerManager()->findGameByPlayer(player);

    gameManager->removePlayerFromGame(playersGame, player, true);
}
