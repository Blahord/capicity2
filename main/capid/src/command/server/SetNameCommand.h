#ifndef CAPICITY2_SETNAMECOMMAND_H
#define CAPICITY2_SETNAMECOMMAND_H

#include "ServerCommand.h"

class SetNameCommand : public ServerCommand {

    public:
        explicit SetNameCommand(const std::shared_ptr<ServerManager>& serverManager);
        ~SetNameCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_SETNAMECOMMAND_H
