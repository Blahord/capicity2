#ifndef CAPICITY2_CREATEGAMECOMMAND_H
#define CAPICITY2_CREATEGAMECOMMAND_H

#include "ServerCommand.h"

class CreateGameCommand : public ServerCommand {

    public:
        explicit CreateGameCommand(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry, const QString& gamesDir);
        ~CreateGameCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;

    private:
        ImageRegistry* imageRegistry;
        QString gamesDir;
};


#endif //CAPICITY2_CREATEGAMECOMMAND_H
