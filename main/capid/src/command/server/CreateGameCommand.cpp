#include "CreateGameCommand.h"

#include "json/JsonChecks.h"
#include "../../GameManager.h"
#include "../../game/CapidGameReader.h"

CreateGameCommand::CreateGameCommand(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry, const QString& gamesDir) : ServerCommand(serverManager) {
    this->imageRegistry = imageRegistry;
    this->gamesDir = gamesDir;

    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("type", JsonChecks::stringValue())
        )
    );
}

CreateGameCommand::~CreateGameCommand() = default;

QString CreateGameCommand::getName() {
    return "createGame";
}

QString CreateGameCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    if (getServerManager()->findGameByPlayer(player)) {
        return "ALREADY_IN_GAME";
    }

    QString const type = arguments.value("type").toString();
    auto gameTemplate = getServerManager()->findGameTemplate(type);
    if (gameTemplate.getId().isEmpty()) {
        return "UNKNOWN_TYPE";
    }

    return {};
}

void CreateGameCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {

    QString const type = arguments.value("type").toString();

    std::shared_ptr<CapidGame> const game = CapidGameReader::readGame(gamesDir, type, imageRegistry);
    getServerManager()->addGame(game);

    game->addPlayer(player);
    game->setOwner(player);

    getServerManager()->broadcast("newGame", GameManager::getGameInfoObject(game));
    getServerManager()->sendToPlayer(player, "gameData", GameManager::getGameDataObject(game, player->getLanguage()));
}
