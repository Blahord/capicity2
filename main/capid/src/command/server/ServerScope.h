#ifndef CAPICITY2_SERVERSCOPE_H
#define CAPICITY2_SERVERSCOPE_H

#include "../Scope.h"
#include "GameManager.h"

class ServerScope : public Scope {

    public:
        explicit ServerScope(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<GameManager>& gameManager, ImageRegistry* imageRegistry, const QString& gamesDir);
        ~ServerScope() override;

        QString getName() override;
};


#endif //CAPICITY2_SERVERSCOPE_H
