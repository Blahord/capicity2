#include "QuitCommand.h"

QuitCommand::QuitCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<GameManager>& gameManager) : ServerCommand(serverManager) {
    this->gameManager = gameManager;
}

QuitCommand::~QuitCommand() = default;

QString QuitCommand::getName() {
    return "quit";
}

QString QuitCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    return {};
}

void QuitCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    auto playersGame = getServerManager()->findGameByPlayer(player);
    if (playersGame) {
        gameManager->removePlayerFromGame(playersGame, player, false);
    }

    getServerManager()->broadcast("quit", QJsonObject{{"id", player->getId()}}, player);
    getServerManager()->deletePlayer(player);
}
