#ifndef CAPICITY2_LEAVEGAMECOMMAND_H
#define CAPICITY2_LEAVEGAMECOMMAND_H

#include "ServerCommand.h"
#include "GameManager.h"

class LeaveGameCommand : public ServerCommand {

    public:
        explicit LeaveGameCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<GameManager>& gameManager);
        ~LeaveGameCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;

    private:
        std::shared_ptr<GameManager> gameManager;
};


#endif //CAPICITY2_LEAVEGAMECOMMAND_H
