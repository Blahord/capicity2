#include "LoginCommand.h"

#include "json/JsonChecks.h"

LoginCommand::LoginCommand(const std::shared_ptr<ServerManager>& serverManager) : ServerCommand(serverManager) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("name", JsonChecks::stringValue()),
            JsonChecks::optionalField("language", JsonChecks::regexp("[a-z]{3}"))
        )
    );
}

LoginCommand::~LoginCommand() = default;

QString LoginCommand::getName() {
    return "login";
}

QString LoginCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (getServerManager()->isLoggedIn(player)) {
        return "ALREADY_LOGGED_IN";
    }

    return {};
}

void LoginCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    QString const playerName = getFreePlayerName(arguments.value("name").toString());

    player->setName(playerName);
    getServerManager()->addPlayer(player);

    if (arguments.contains("language")) {
        player->setLanguage(arguments.value("language").toString());
    }

    qDebug() << "Player " + player->getName() + " successfully connected";

    QJsonObject const connectionData{
        {"id",    player->getId()},
        {"token", player->getToken()},
        {"name",  player->getName()}
    };

    getServerManager()->sendToPlayer(player, "playerData", connectionData);
    getServerManager()->sendToPlayer(player, "playerList", {{"list", collectPlayersData()}});
    getServerManager()->sendToPlayer(player, "gameTemplateList", {{"list", collectGameTemplates(player)}});
    getServerManager()->sendToPlayer(player, "gameList", {{"list", collectGamesData()}});

    getServerManager()->broadcast("newPlayer", player->getDataObject(), player);
}
