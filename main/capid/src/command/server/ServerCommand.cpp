#include "ServerCommand.h"

#include "../../GameManager.h"

ServerCommand::ServerCommand(const std::shared_ptr<ServerManager>& serverManager) : Command(serverManager) {
}

ServerCommand::~ServerCommand() = default;

QJsonArray ServerCommand::collectGameTemplates(const std::shared_ptr<CapidPlayer>& player) {
    QJsonArray gameInfoArray;

    for (CapidGameTemplate gameTemplate : getServerManager()->getGameTemplates()) {
        gameInfoArray.append(gameTemplate.getDataObject(player->getLanguage()));
    }

    return gameInfoArray;
}

QJsonArray ServerCommand::collectGamesData() {
    QJsonArray gamesInfoArray;

    for (const auto& game : getServerManager()->getGames()) {
        gamesInfoArray.append(GameManager::getGameInfoObject(game));
    }

    return gamesInfoArray;
}

QJsonArray ServerCommand::collectPlayersData() {
    QJsonArray playerList;

    for (const auto& player : getServerManager()->getPlayers()) {
        QJsonObject const playerData = player->getDataObject();
        playerList.append(playerData);
    }

    return playerList;
}

QString ServerCommand::getFreePlayerName(QString baseName) {
    if (getServerManager()->findPlayerByName(baseName)) {
        int suffix = 2;
        while (getServerManager()->findPlayerByName(QString(baseName + "%1").arg(suffix))) {
            suffix++;
        }

        return baseName + QString::number(suffix);
    }

    return baseName;
}

