#include "ChatCommand.h"

#include "json/JsonChecks.h"

ChatCommand::ChatCommand(const std::shared_ptr<ServerManager>& serverManager) : ServerCommand(serverManager) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::optionalField("target", JsonChecks::stringValue()),
            JsonChecks::requiredField("message", JsonChecks::stringValue())
        )
    );
}

ChatCommand::~ChatCommand() = default;

QString ChatCommand::getName() {
    return "chat";
}

QString ChatCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    if (arguments.contains("target")) {
        QString targetId = arguments.value("target").toString();
        if (!getServerManager()->findPlayer(targetId)) {
            return "UNKNOWN_TARGET";
        }
    }

    return {};
}

void ChatCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    std::shared_ptr<CapidPlayer> target;
    if (arguments.contains("target")) {
        QString targetId = arguments.value("target").toString();
        target = getServerManager()->findPlayer(targetId);
    }

    QJsonObject dataObject{
        {"message", arguments.value("message").toString()},
        {"sender",  player->getId()},
    };

    if (target) {
        dataObject.insert("target", target->getId());
        getServerManager()->sendToPlayer(player, "chat", dataObject);
        getServerManager()->sendToPlayer(target, "chat", dataObject);
        return;
    }

    auto playersGame = getServerManager()->findGameByPlayer(player);

    for (auto p : getServerManager()->getPlayers()) {
        if (getServerManager()->findGameByPlayer(p) == playersGame) {
            getServerManager()->sendToPlayer(p, "chat", dataObject);
        }
    }
}
