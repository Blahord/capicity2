#include "SetNameCommand.h"

#include "json/JsonChecks.h"

SetNameCommand::SetNameCommand(const std::shared_ptr<ServerManager>& serverManager) : ServerCommand(serverManager) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("name", JsonChecks::stringValue())
        )
    );
}

SetNameCommand::~SetNameCommand() = default;

QString SetNameCommand::getName() {
    return "setName";
}

QString SetNameCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    return {};
}

void SetNameCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    QString baseName = arguments.value("name").toString();
    QString oldName = player->getName();

    if (oldName == baseName) {
        return;
    }

    QString newName = getFreePlayerName(baseName);
    player->setName(newName);

    getServerManager()->broadcast(
        "playerUpdate",
        {
            {"id",   player->getId()},
            {"name", player->getName()}
        }
    );
}
