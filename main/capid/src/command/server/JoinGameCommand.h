#ifndef CAPICITY2_JOINGAMECOMMAND_H
#define CAPICITY2_JOINGAMECOMMAND_H

#include "ServerCommand.h"

class JoinGameCommand : public ServerCommand {

    public:
        explicit JoinGameCommand(const std::shared_ptr<ServerManager>& serverManager);
        ~JoinGameCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_JOINGAMECOMMAND_H
