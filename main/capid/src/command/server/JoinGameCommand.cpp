#include "JoinGameCommand.h"

#include "json/JsonChecks.h"
#include "../../GameManager.h"

JoinGameCommand::JoinGameCommand(const std::shared_ptr<ServerManager>& serverManager) : ServerCommand(serverManager) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("id", JsonChecks::stringValue())
        )
    );
}

JoinGameCommand::~JoinGameCommand() = default;

QString JoinGameCommand::getName() {
    return "joinGame";
}

QString JoinGameCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    if (getServerManager()->findGameByPlayer(player)) {
        return "ALREADY_IN_GAME";
    }

    QString const gameId = arguments.value("id").toString();
    std::shared_ptr<CapidGame> game = getServerManager()->findGameById(gameId);
    if (!game) {
        return "UNKNOWN_GAME";
    }

    if (game->getPlayers().size() >= game->getTemplate().getMaxPlayers()) {
        return "GAME_FULL";
    }

    return {};
}

void JoinGameCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    QString const gameId = arguments.value("id").toString();
    auto game = getServerManager()->findGameById(gameId);

    game->addPlayer(player);
    getServerManager()->sendToPlayer(player, "gameData", GameManager::getGameDataObject(game, player->getLanguage()));
    getServerManager()->broadcast("gameUpdate", GameManager::getGameUpdateObject(game));
}
