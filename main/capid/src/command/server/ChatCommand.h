#ifndef CAPICITY2_CHATCOMMAND_H
#define CAPICITY2_CHATCOMMAND_H

#include "ServerCommand.h"

class ChatCommand : public ServerCommand {

    public:
        explicit ChatCommand(const std::shared_ptr<ServerManager>& serverManager);
        ~ChatCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_CHATCOMMAND_H
