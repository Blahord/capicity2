#ifndef CAPICITY2_LOGINCOMMAND_H
#define CAPICITY2_LOGINCOMMAND_H

#include "ServerCommand.h"

class LoginCommand : public ServerCommand {

    public:
        explicit LoginCommand(const std::shared_ptr<ServerManager>& serverManager);
        ~LoginCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_LOGINCOMMAND_H
