#ifndef CAPICITY2_QUITCOMMAND_H
#define CAPICITY2_QUITCOMMAND_H

#include "ServerCommand.h"
#include "GameManager.h"

class QuitCommand : public ServerCommand {

    public:
        explicit QuitCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<GameManager>& gameManager);
        ~QuitCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;

    private:
        std::shared_ptr<GameManager> gameManager;
};


#endif //CAPICITY2_QUITCOMMAND_H
