#ifndef CAPICITY2_SETAVATARCOMMAND_H
#define CAPICITY2_SETAVATARCOMMAND_H

#include "ServerCommand.h"

class SetAvatarCommand : public ServerCommand {

    public:
        explicit SetAvatarCommand(const std::shared_ptr<ServerManager>& serverManager, ImageRegistry* imageRegistry);
        ~SetAvatarCommand() override;

        QString getName() override;
        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) override;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) override;

    private:
        ImageRegistry* imageRegistry;
};


#endif //CAPICITY2_SETAVATARCOMMAND_H
