#ifndef CAPICITY2_GAMECOMMAND_H
#define CAPICITY2_GAMECOMMAND_H

#include "game/CapiGamePlayer.h"
#include "../Command.h"
#include "../../RNG.h"
#include "GameManager.h"

class GameCommand : public Command {

    public:
        explicit GameCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager);
        ~GameCommand() override;

        QString validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) final;
        void execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) final;

    protected:
        virtual QString validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) = 0;
        virtual void executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) = 0;

        std::shared_ptr<RNG> getRng();
        std::shared_ptr<GameManager> getGameManager();

    private:
        std::shared_ptr<RNG> rng;
        std::shared_ptr<GameManager> gameManager;
};


#endif //CAPICITY2_GAMECOMMAND_H
