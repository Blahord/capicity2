#ifndef CAPICITY2_GAMESCOPE_H
#define CAPICITY2_GAMESCOPE_H

#include "../Scope.h"
#include "../../RNG.h"
#include "../../GameManager.h"

class GameScope : public Scope {

    public:
        explicit GameScope(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>&);
        ~GameScope() override;

        QString getName() override;
};


#endif //CAPICITY2_GAMESCOPE_H
