#ifndef CAPICITY2_SETREADYCOMMAND_H
#define CAPICITY2_SETREADYCOMMAND_H

#include "GameCommand.h"

class SetReadyCommand : public GameCommand {

    public:
        explicit SetReadyCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager);
        ~SetReadyCommand() override;

        QString getName() override;

    protected:
        QString validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;
        void executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_SETREADYCOMMAND_H
