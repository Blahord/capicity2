#include "GameCommand.h"

GameCommand::GameCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager)
    : Command(serverManager) {
    this->rng = rng;
    this->gameManager = gameManager;
}

GameCommand::~GameCommand() = default;

QString GameCommand::validateState(const std::shared_ptr<CapidPlayer>& player, const QJsonObject& arguments) {
    if (!getServerManager()->isLoggedIn(player)) {
        return "NOT_LOGGED_IN";
    }

    auto game = getServerManager()->findGameByPlayer(player);
    if (!game) {
        return "NOT_IN_GAME";
    }

    auto gamePlayer = game->findPlayer(player);

    return validateStateInternal(game, gamePlayer, arguments);
}

void GameCommand::execute(const std::shared_ptr<CapidPlayer>& player, const QString& nonce, const QJsonObject& arguments) {
    auto game = getServerManager()->findGameByPlayer(player);
    auto gamePlayer = game->findPlayer(player);

    executeInternal(game, gamePlayer, arguments);
}

std::shared_ptr<RNG> GameCommand::getRng() {
    return rng;
}

std::shared_ptr<GameManager> GameCommand::getGameManager() {
    return gameManager;
}
