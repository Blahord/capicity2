#ifndef CAPICITY2_MOVECOMPLETECOMMAND_H
#define CAPICITY2_MOVECOMPLETECOMMAND_H

#include "GameCommand.h"
#include "../../GameManager.h"

class MoveCompleteCommand : public GameCommand {

    public:
        explicit MoveCompleteCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>&);
        ~MoveCompleteCommand() override;

        QString getName() override;

    protected:
        QString validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;
        void executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_MOVECOMPLETECOMMAND_H
