#include "SetReadyCommand.h"

#include "json/JsonChecks.h"

SetReadyCommand::SetReadyCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager)
    : GameCommand(serverManager, rng, gameManager) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("ready", JsonChecks::booleanValue())
        )
    );
}

SetReadyCommand::~SetReadyCommand() = default;

QString SetReadyCommand::getName() {
    return "setReady";
}

QString SetReadyCommand::validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    if (game->getPlayers().size() < game->getTemplate().getMinPlayers()) {
        return "TOO_FEW_PLAYERS";
    }

    if (game->getState() != GameState::CONFIG) {
        return "GAME_RUNNING";
    }

    return {};
}

void SetReadyCommand::executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    bool ready = arguments.value("ready").toBool();
    gamePlayer->setReady(ready);

    getServerManager()->broadcast(
        game,
        "gamePlayerUpdate",
        {
            {"id",    gamePlayer->getPlayer()->getId()},
            {"ready", ready}
        }
    );
}
