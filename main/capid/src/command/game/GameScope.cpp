#include "GameScope.h"

#include "MoveCompleteCommand.h"
#include "RollCommand.h"
#include "SetReadyCommand.h"
#include "BuyCommand.h"
#include "SkipBuyCommand.h"
#include "PayTaxCommand.h"

GameScope::GameScope(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager) : Scope(serverManager) {
    addCommand(std::make_shared<MoveCompleteCommand>(serverManager, rng, gameManager));
    addCommand(std::make_shared<RollCommand>(serverManager, rng, gameManager));
    addCommand(std::make_shared<SetReadyCommand>(serverManager, rng, gameManager));
    addCommand(std::make_shared<BuyCommand>(serverManager, rng, gameManager));
    addCommand(std::make_shared<SkipBuyCommand>(serverManager, rng, gameManager));
    addCommand(std::make_shared<PayTaxCommand>(serverManager, rng, gameManager));
}

GameScope::~GameScope() = default;

QString GameScope::getName() {
    return "game";
}
