#ifndef CAPICITY2_PAYTAXCOMMAND_H
#define CAPICITY2_PAYTAXCOMMAND_H

#include "GameCommand.h"
#include "GameManager.h"

class PayTaxCommand : public GameCommand {

    public:
        explicit PayTaxCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager);
        ~PayTaxCommand() override;

        QString getName() override;

    protected:
        QString validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;
        void executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;

    private:
        int getTaxAmount(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QString& preferredMode);
        static QString getPayMode(std::shared_ptr<CapidEstate>& estate, const QString& preferredPayMode);
};

#endif //CAPICITY2_PAYTAXCOMMAND_H
