#ifndef CAPICITY2_ROLLCOMMAND_H
#define CAPICITY2_ROLLCOMMAND_H

#include "GameCommand.h"

class RollCommand : public GameCommand {

    public:
        explicit RollCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager);
        ~RollCommand() override;

        QString getName() override;

    protected:
        QString validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;
        void executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_ROLLCOMMAND_H
