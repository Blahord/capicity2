#ifndef CAPICITY2_BUYCOMMAND_H
#define CAPICITY2_BUYCOMMAND_H

#include "GameCommand.h"
#include "GameManager.h"

class BuyCommand : public GameCommand {

    public:
        explicit BuyCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager);
        ~BuyCommand() override;

        QString getName() override;

    protected:
        QString validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;
        void executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) override;
};


#endif //CAPICITY2_BUYCOMMAND_H
