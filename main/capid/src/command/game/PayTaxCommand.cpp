#include "PayTaxCommand.h"
#include "json/JsonChecks.h"

PayTaxCommand::PayTaxCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager)
    : GameCommand(serverManager, rng, gameManager) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::optionalField("mode", JsonChecks::regexp("amount|percent"))
        )
    );
}

PayTaxCommand::~PayTaxCommand() = default;

QString PayTaxCommand::getName() {
    return "payTax";
}

QString PayTaxCommand::validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    if (game->getPlayerOnTurn() != gamePlayer) {
        return "NOT_ON_TURN";
    }

    if (game->getTurnState() != TurnState::TAX) {
        return "WRONG_TURN_STATE";
    }

    int const amount = getTaxAmount(game, gamePlayer, arguments.value("mode").toString("amount"));

    if (amount > gamePlayer->getMoney()) {
        return "NOT_ENOUGH_MONEY";
    }

    return {};
}

void PayTaxCommand::executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    int const amount = getTaxAmount(game, gamePlayer, arguments.value("mode").toString());
    auto estate = game->getEstates().at(gamePlayer->getPosition());

    getGameManager()->payTaxes(game, estate->getPayTarget(), amount);

    getGameManager()->endTax(game);
}

int PayTaxCommand::getTaxAmount(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QString& preferredMode) {
    auto estate = game->getEstates().at(gamePlayer->getPosition());
    QString const mode = getPayMode(estate, preferredMode);

    return mode == "amount"
           ? estate->getTaxAmount()
           : (estate->getTaxPercent() * getGameManager()->getPlayerAssets(game, gamePlayer)) / 100;
}

QString PayTaxCommand::getPayMode(std::shared_ptr<CapidEstate>& estate, const QString& preferredPayMode) {
    if (estate->getTaxPercent() <= 0) {
        return "amount";
    }

    if (estate->getTaxAmount() <= 0) {
        return "percent";
    }

    return preferredPayMode;
}

