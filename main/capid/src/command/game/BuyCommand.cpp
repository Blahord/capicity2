#include "BuyCommand.h"

#include <QJsonObject>

BuyCommand::BuyCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager)
    : GameCommand(serverManager, rng, gameManager) {}

BuyCommand::~BuyCommand() = default;

QString BuyCommand::getName() {
    return "buy";
}

QString BuyCommand::validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    if (game->getPlayerOnTurn() != gamePlayer) {
        return "NOT_ON_TURN";
    }

    if (game->getTurnState() != TurnState::BUY) {
        return "WRONG_TURN_STATE";
    }

    auto estate = game->getEstates().at(gamePlayer->getPosition());

    if (estate->getPrice() > gamePlayer->getMoney()) {
        return "NOT_ENOUGH_MONEY";
    }

    return {};
}

void BuyCommand::executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    auto estate = game->getEstates().at(gamePlayer->getPosition());
    estate->setOwner(gamePlayer);
    gamePlayer->setMoney(gamePlayer->getMoney() - estate->getPrice());

    getServerManager()->broadcast(
        game,
        "estateUpdate",
        {
            {"id",    estate->getId()},
            {"owner", estate->getOwner()->getPlayer()->getId()}
        }
    );

    getServerManager()->broadcast(
        game,
        "gamePlayerUpdate",
        {
            {"id",    gamePlayer->getPlayer()->getId()},
            {"money", gamePlayer->getMoney()}
        }
    );

    getServerManager()->broadcastGameEvent(
        game,
        "buy",
        QJsonValue::Null
    );

    getGameManager()->endTurn(game);
}
