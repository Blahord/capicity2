#include "SkipBuyCommand.h"

SkipBuyCommand::SkipBuyCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager)
    : GameCommand(serverManager, rng, gameManager) {}

SkipBuyCommand::~SkipBuyCommand() = default;

QString SkipBuyCommand::getName() {
    return "skipBuy";
}

QString SkipBuyCommand::validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    if (game->getPlayerOnTurn() != gamePlayer) {
        return "NOT_ON_TURN";
    }

    if (game->getTurnState() != TurnState::BUY) {
        return "WRONG_TURN_STATE";
    }

    return {};
}

void SkipBuyCommand::executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    getServerManager()->broadcastGameEvent(
        game,
        "skipBuy",
        QJsonValue::Null
    );

    getGameManager()->endTurn(game);
}
