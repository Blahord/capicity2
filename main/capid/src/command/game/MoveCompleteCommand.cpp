#include "MoveCompleteCommand.h"

#include "game/TurnState.h"

MoveCompleteCommand::MoveCompleteCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager)
    : GameCommand(serverManager, rng, gameManager) {}

MoveCompleteCommand::~MoveCompleteCommand() = default;

QString MoveCompleteCommand::getName() {
    return "moveComplete";
}

QString MoveCompleteCommand::validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    if (game->getTurnState() != TurnState::MOVE) {
        return "WRONG_TURN_STATE";
    }

    return {};
}

void MoveCompleteCommand::executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    gamePlayer->setMoved(true);

    getServerManager()->broadcast(
        game,
        "gamePlayerUpdate",
        {
            {"id",    gamePlayer->getPlayer()->getId()},
            {"moved", gamePlayer->isMoved()}
        }
    );

    auto players = game->getPlayers();
    if (std::all_of(players.begin(), players.end(), [](const std::shared_ptr<CapiGamePlayer>& gp) { return gp->isMoved(); })) {
        getGameManager()->endMovement(game);
    }
}

