#include "RollCommand.h"

RollCommand::RollCommand(const std::shared_ptr<ServerManager>& serverManager, const std::shared_ptr<RNG>& rng, const std::shared_ptr<GameManager>& gameManager)
    : GameCommand(serverManager, rng, gameManager) {

}

RollCommand::~RollCommand() = default;

QString RollCommand::getName() {
    return "roll";
}

QString RollCommand::validateStateInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    if (game->getPlayerOnTurn() != gamePlayer) {
        return "NOT_ON_TURN";
    }

    if (game->getTurnState() != TurnState::ROLL) {
        return "WRONG_TURN_STATE";
    }

    return {};
}

void RollCommand::executeInternal(const std::shared_ptr<CapidGame>& game, const std::shared_ptr<CapiGamePlayer>& gamePlayer, const QJsonObject& arguments) {
    int dice1 = getRng()->next(1, 6);
    int dice2 = getRng()->next(1, 6);

    int oldPos = gamePlayer->getPosition();
    gamePlayer->setPosition((gamePlayer->getPosition() + dice1 + dice2) % game->getEstates().size());

    for (auto gamePlayer : game->getPlayers()) {
        gamePlayer->setMoved(false);
    }

    getServerManager()->broadcastGameEvent(
        game,
        "roll",
        QJsonObject{
            {"dice1", dice1},
            {"dice2", dice2}
        }
    );

    for (auto gamePlayer : game->getPlayers()) {
        getServerManager()->broadcast(
            game,
            "gamePlayerUpdate",
            {
                {"id",    gamePlayer->getPlayer()->getId()},
                {"moved", gamePlayer->isMoved()}
            }
        );
    }

    game->resetMovementTime();
    game->setTurnState(TurnState::MOVE);

    getServerManager()->broadcast(
        game,
        "gameUpdate",
        {
            {"id",        game->getId()},
            {"turnState", game->getTurnStateName()}
        }
    );

    getServerManager()->broadcast(
        game,
        "gamePlayerMove",
        {
            {"id",   gamePlayer->getPlayer()->getId()},
            {"from", oldPos},
            {"to",   gamePlayer->getPosition()},
            {"mode", "FORWARD"}
        }
    );
}

