#include "CapidPlayer.h"

#include <QUuid>

CapidPlayer::CapidPlayer() : CapiPlayer(QUuid::createUuid().toString(QUuid::Id128)) {
    token = QUuid::createUuid().toString(QUuid::Id128) + "-" + QUuid::createUuid().toString(QUuid::Id128);
}

CapidPlayer::~CapidPlayer() = default;

QJsonObject CapidPlayer::getDataObject() {
    return {
        {"id",        getId()},
        {"name",      getName()},
        {"avatar",    getAvatarData()},
        {"connected", isConnected()}
    };
}

QString CapidPlayer::getToken() const {
    return token;
}

const QString& CapidPlayer::getLanguage() const {
    return language;
}

void CapidPlayer::setLanguage(const QString& lang) {
    this->language = lang;
}

void CapidPlayer::clearOldNonce(long serverTime, long maxAge) {
    long minNonceTime = serverTime - maxAge;

    QList<QString> toDelete;
    for (auto nonceEntry = nonceTimes.keyValueBegin(); nonceEntry != nonceTimes.keyValueEnd(); nonceEntry++) {
        if ((*nonceEntry).second < minNonceTime) {
            toDelete.append((*nonceEntry).first);
        }
    }


    for (const auto& nonce : toDelete) {
        nonceTimes.remove(nonce);
    }
}

bool CapidPlayer::isNonceUsed(const QString& nonce, long serverTime, long maxAge) {
    return nonceTimes.contains(nonce) &&
           nonceTimes.value(nonce) >= serverTime - maxAge;
}

void CapidPlayer::addUsedNonce(const QString& nonce, long serverTime) {
    nonceTimes.insert(nonce, serverTime);
}
