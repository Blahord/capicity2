#include "CapidRNG.h"

int CapidRNG::next() {
    return randomGenerator.generate();
}

int CapidRNG::next(int min, int max) {
    return randomGenerator.bounded(min, max + 1);
}
