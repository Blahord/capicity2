#ifndef CAPICITY2_CLIENTCONNECTION_H
#define CAPICITY2_CLIENTCONNECTION_H

#include <QObject>
#include <QJsonObject>

#include "CapiConnection.h"

class ClientConnection : public QObject {
    Q_OBJECT

    public:
        explicit ClientConnection(QObject* parent = nullptr);
        ~ClientConnection() override;

        virtual void send(const QJsonObject& data) = 0;
        virtual void disconnect() = 0;

    signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
        void disconnected();
        void gotCommand(const QJsonObject& command);
#pragma clang diagnostic pop
};


#endif //CAPICITY2_CLIENTCONNECTION_H
