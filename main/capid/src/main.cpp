#include <QCoreApplication>

#include "Capid.h"
#include "CapidRNG.h"

int main(int argc, char** args) {
    QCoreApplication a(argc, args);

    new Capid("resources/games", std::make_shared<CapidRNG>());

    return QCoreApplication::exec();
}