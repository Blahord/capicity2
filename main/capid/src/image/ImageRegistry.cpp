#include "ImageRegistry.h"

ImageRegistry::~ImageRegistry() = default;

bool ImageRegistry::containsImage(const QString& hash) {
    return images.contains(hash);
}

Image ImageRegistry::getImage(const QString& hash) {
    return images.value(hash);
}

void ImageRegistry::addImage(Image image, const QString& usage) {
    QString const hash = image.hash();
    images.insert(hash, image);

    if (!imageUsages.contains(hash)) {
        imageUsages.insert(hash, std::make_shared<QSet<QString>>());
    }

    imageUsages.value(hash)->insert(usage);
}

void ImageRegistry::removeUsage(const QString& hash, const QString& usage) {
    if (!imageUsages.contains(hash)) {
        return;
    }

    imageUsages.value(hash)->remove(usage);
    if (imageUsages.value(hash)->isEmpty()) {
        imageUsages.remove(hash);
        images.remove(hash);
    }
}
