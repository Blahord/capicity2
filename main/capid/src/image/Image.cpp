#include "Image.h"

#include <utility>
#include <QCryptographicHash>

Image::Image(int width, int height, QString data) : nullImage(false), width(width), height(height), data(std::move(data)) {}

Image::Image() : nullImage(true), width(0), height(0), data("") {}

Image::~Image() = default;

bool Image::isNull() const {
    return nullImage;
}

QString Image::hash() {
    if (data.isEmpty()) {
        return "";
    }

    QString widthStr = QString::number(width, 16).leftJustified(4, '0');
    QString heightStr = QString::number(height, 16).leftJustified(4, '0');

    QCryptographicHash hash(QCryptographicHash::Algorithm::Sha256);
    hash.addData(widthStr.toUtf8().data(), 4);
    hash.addData(heightStr.toUtf8().data(), 4);
    hash.addData(data.toUtf8().data(), data.size());

    return hash.result().toHex();
}

QJsonObject Image::toJsonObject() {
    return {
        {"width",  width},
        {"height", height},
        {"data",   data}
    };
}
