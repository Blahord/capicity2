#ifndef CAPICITY2_IMAGEREGISTRY_H
#define CAPICITY2_IMAGEREGISTRY_H

#include <QMap>
#include <QSet>

#include "Image.h"

class ImageRegistry {

    public:
        virtual ~ImageRegistry();

        bool containsImage(const QString& hash);
        Image getImage(const QString& hash);

        void addImage(Image image, const QString& usage);
        void removeUsage(const QString& hash, const QString& usage);

    private:
        QMap<QString, Image> images;
        QMap<QString, std::shared_ptr<QSet<QString>>> imageUsages;
};


#endif //CAPICITY2_IMAGEREGISTRY_H
