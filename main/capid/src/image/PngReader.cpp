#include "PngReader.h"

#include <png.h>

Image PngReader::readFile(const QString& filePath) {

    FILE* file = fopen(filePath.toUtf8().data(), "rb");
    if (!file ||
        !isPngFile(file)) {
        return Image();
    }

    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    png_infop info = png_create_info_struct(png);
    setjmp(png_jmpbuf(png));

    png_init_io(png, file);
    png_set_sig_bytes(png, 8);
    png_read_info(png, info);

    int imageWidth = png_get_image_width(png, info);
    int imageHeight = png_get_image_height(png, info);
    png_byte colorType = png_get_color_type(png, info);

    if (imageWidth > 256 || imageHeight > 256) {
        return Image();
    }

    png_read_update_info(png, info);

    setjmp(png_jmpbuf(png));

    auto imageData = (png_bytep*) calloc(imageHeight, sizeof(png_bytep));

    for (int row = 0; row < imageHeight; row++) {
        imageData[row] = (png_byte*) malloc(png_get_rowbytes(png, info));
    }

    png_read_image(png, imageData);

    QString data = "";
    for (int row = 0; row < imageHeight; row++) {
        png_byte* rowData = imageData[row];

        for (int col = 0; col < imageWidth; col++) {
            switch (colorType) {
                case PNG_COLOR_TYPE_RGB:
                    data += "ff"; //No alpha channel, so set alpha to full
                    data += toHex(rowData[col * 3]);
                    data += toHex(rowData[col * 3 + 1]);
                    data += toHex(rowData[col * 3 + 2]);
                    break;
                case PNG_COLOR_TYPE_RGBA: // NOLINT(hicpp-signed-bitwise)
                    data += toHex(rowData[col * 4 + 3]);
                    data += toHex(rowData[col * 4]);
                    data += toHex(rowData[col * 4 + 1]);
                    data += toHex(rowData[col * 4 + 2]);
                    break;
                default:
                    data += "00000000";
            }
        }
        free(rowData);
    }

    free(imageData);
    png_read_end(png, nullptr);
    png_destroy_read_struct(&png, &info, nullptr);
    fclose(file);

    return Image(imageWidth, imageHeight, data);
}

bool PngReader::isPngFile(FILE* file) {
    char header[8];

    fread(header, 1, 8, file);
    return !png_sig_cmp(reinterpret_cast<png_const_bytep>(header), 0, 8);
}

QString PngReader::toHex(png_byte byte) {
    return QString::number(byte, 16).leftJustified(2, '0');
}
