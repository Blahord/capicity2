#ifndef CAPICITY2_IMAGE_H
#define CAPICITY2_IMAGE_H

#include <QString>
#include <QJsonObject>

class Image {

    public:
        explicit Image(int width, int height, QString data);
        Image();
        virtual ~Image();

        bool isNull() const;
        QString hash();
        QJsonObject toJsonObject();

    private:
        bool nullImage = false;
        int width = 0;
        int height = 0;
        QString data;
};


#endif //CAPICITY2_IMAGE_H
