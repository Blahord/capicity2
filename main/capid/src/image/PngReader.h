#ifndef CAPICITY2_PNGREADER_H
#define CAPICITY2_PNGREADER_H

#include <QString>
#include <png.h>

#include "Image.h"

class PngReader {

    public:
        static Image readFile(const QString& filePath);

    private:
        static bool isPngFile(FILE* file);
        static QString toHex(png_byte byte);
};


#endif //CAPICITY2_PNGREADER_H
