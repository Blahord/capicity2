#include "CapidGameReader.h"

#include <QDomDocument>
#include <QFile>
#include <QDir>

#include "../image/PngReader.h"

std::unique_ptr<CapidGame> CapidGameReader::readGame(const QString& gamesDir, const QString& templateId, ImageRegistry* imageRegistry) {
    QFile gameFile(QString("%1/%2/game.xml").arg(gamesDir, templateId));

    QDomDocument document;
    document.setContent(&gameFile);

    QDomElement const gameElement = document.documentElement();
    CapidGameTemplate const gameInfo = readGameTemplate(templateId, gameElement.firstChildElement("info"));

    QDomElement const generalElement = gameElement.firstChildElement("general");
    QDomElement const configElement = gameElement.firstChildElement("config");

    auto game = std::make_unique<CapidGame>(gameInfo);

    game->setBgColor(generalElement.attribute("bgColor"));
    game->setStartEstate(generalElement.attribute("startEstate"));

    game->setStartMoney(configElement.attribute("startMoney").toInt());

    for (QDomElement groupElement = gameElement.firstChildElement("estateGroup");
         !groupElement.isNull();
         groupElement = groupElement.nextSiblingElement("estateGroup")) {

        game->addEstateGroup(readEstateGroup(groupElement, gameInfo.getLanguages()));
    }

    QList<std::shared_ptr<CapidEstateGroup>> const estateGroups = game->getEstateGroups();
    for (QDomElement estateElement = gameElement.firstChildElement("estate");
         !estateElement.isNull();
         estateElement = estateElement.nextSiblingElement("estate")) {

        game->addEstate(readEstate(estateElement, estateGroups, gameInfo.getLanguages(), gamesDir, templateId, imageRegistry));
    }

    for (const auto& estate : game->getEstates()) {
        if (estate->getPayTargetId().isNull()) {
            continue;
        }

        estate->setPayTarget(game->findEstate(estate->getPayTargetId()));
    }

    return game;
}

QList<CapidGameTemplate> CapidGameReader::getGames(const QString& gamesDirPath) {
    QDir const gamesDir(gamesDirPath);

    QList<CapidGameTemplate> gameTemplates;
    for (const QString& id : gamesDir.entryList(QDir::Dirs, QDir::Name)) {
        if (id.startsWith('.')) continue;

        QFile gameFile(QString("%1/%2/game.xml").arg(gamesDirPath, id));

        QDomDocument document;
        document.setContent(&gameFile);

        QDomElement const rootElement = document.documentElement();
        QDomElement const infoElement = rootElement.firstChildElement("info");

        gameTemplates.append(readGameTemplate(id, infoElement));
    }

    return gameTemplates;
}

CapidGameTemplate CapidGameReader::readGameTemplate(const QString& id, const QDomElement& infoElement) {
    CapidGameTemplate gameTemplate(id);

    gameTemplate.setMinPlayers(readNumberFromAttribute(infoElement, "minPlayers", 2));
    gameTemplate.setMaxPlayers(readNumberFromAttribute(infoElement, "maxPlayers", 6));

    QList<QString> allLanguages;
    QDomElement const languagesElement = infoElement.firstChildElement("languages");
    for (QDomElement languageElement = languagesElement.firstChildElement("language"); !languageElement.isNull(); languageElement = languageElement.nextSiblingElement("language")) {
        QString const language = languageElement.text();
        gameTemplate.addLanguage(language);
        allLanguages.append(language);
    }
    gameTemplate.setDefaultLanguage(languagesElement.attribute("default"));

    QMap<QString, QString> const description = readLocalized(infoElement, "description", allLanguages);
    for (const QString& lang : description.keys()) {
        gameTemplate.setDescription(lang, description.value(lang));
    }

    QMap<QString, QString> const name = readLocalized(infoElement, "name", allLanguages);
    for (const QString& lang : name.keys()) {
        gameTemplate.setName(lang, name.value(lang));
    }

    return gameTemplate;
}

std::unique_ptr<CapidEstateGroup> CapidGameReader::readEstateGroup(const QDomElement& groupElement, const QList<QString>& allLanguages) {
    QString const id = groupElement.attribute("id");
    QString const color = groupElement.attribute("color");
    int const housePrice = readNumberFromAttribute(groupElement, "housePrice", 0);
    auto estateGroup = std::make_unique<CapidEstateGroup>(id, color, housePrice);

    estateGroup->setRentMath(groupElement.firstChildElement("rentMath").text());

    QMap<QString, QString> const name = readLocalized(groupElement, "name", allLanguages);
    for (const QString& lang : name.keys()) {
        estateGroup->setName(lang, name.value(lang));
    }

    return estateGroup;
}

std::unique_ptr<CapidEstate> CapidGameReader::readEstate(const QDomElement& estateElement, const QList<std::shared_ptr<CapidEstateGroup>>& estateGroups,
                                                         const QList<QString>& allLanguages,
                                                         const QString& gamesDir, const QString& templateId, ImageRegistry* imageRegistry) {
    QDomElement const rentElement = estateElement.firstChildElement("rent");
    QDomElement const taxElement = estateElement.firstChildElement("tax");
    QString const id = estateElement.attribute("id");
    auto estate = std::make_unique<CapidEstate>(
        id,
        readNumberFromAttribute(estateElement, "price", 0),
        readNumberFromAttribute(rentElement, "base", 0),
        readNumberFromAttribute(rentElement, "house1", 0),
        readNumberFromAttribute(rentElement, "house2", 0),
        readNumberFromAttribute(rentElement, "house3", 0),
        readNumberFromAttribute(rentElement, "house4", 0),
        readNumberFromAttribute(rentElement, "hotel", 0),
        readNumberFromAttribute(taxElement, "amount", 0),
        readNumberFromAttribute(taxElement, "percent", 0),
        taxElement.attribute("target", QString())
    );

    QString const groupId = estateElement.attribute("group", QString());
    if (!groupId.isNull()) {
        for (const auto& group : estateGroups) {
            if (group->getId() == groupId) {
                estate->setEstateGroup(group);
                break;
            }
        }
    }

    if (estateElement.hasAttribute("image")) {
        Image image = PngReader::readFile(QString("%1/%2/%3").arg(gamesDir, templateId, estateElement.attribute("image")));
        if (!image.isNull()) {
            imageRegistry->addImage(image, "game");
            estate->setImage(image.hash());
        }
    }

    QMap<QString, QString> const name = readLocalized(estateElement, "name", allLanguages);
    for (const QString& lang : name.keys()) {
        estate->setName(lang, name.value(lang));
    }

    return estate;
}

QMap<QString, QString> CapidGameReader::readLocalized(const QDomElement& parentElement, const QString& tagName, const QList<QString>& allLanguages) {
    QMap<QString, QString> translations;

    for (QDomElement locElement = parentElement.firstChildElement(tagName); !locElement.isNull(); locElement = locElement.nextSiblingElement(tagName)) {
        QString const language = locElement.attribute("language", "");
        QString const description = locElement.text();

        if (language == "") {
            for (const QString& lang : allLanguages) {
                translations.insert(lang, description);
            }
        } else {
            translations.insert(language, description);
        }
    }

    return translations;
}

int CapidGameReader::readNumberFromAttribute(const QDomElement& element, const QString& attribute, int defaultValue) {
    bool ok;

    QString const valueString = element.attribute(attribute, "");
    int const number = valueString.toInt(&ok);

    return ok ? number : defaultValue;
}
