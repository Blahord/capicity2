#ifndef CAPICITY2_CAPIDESTATE_H
#define CAPICITY2_CAPIDESTATE_H

#include <QString>

#include "game/CapiGamePlayer.h"
#include "game/CapiEstate.h"
#include "CapidEstateGroup.h"

class CapidEstate : public CapiEstate {

    public:
        explicit CapidEstate(const QString& id,
                             int price,
                             int rent0, int rent1, int rent2,
                             int rent3, int rent4, int rent5,
                             int taxAmount, int taxPercent, const QString& payTargetId);

        ~CapidEstate() override;

        QString getName(const QString& language);
        void setName(const QString& language, const QString& estateName);

        std::shared_ptr<CapidEstateGroup> getEstateGroup();
        void setEstateGroup(const std::shared_ptr<CapidEstateGroup>& group);

        std::weak_ptr<CapidEstate> getPayTarget();
        void setPayTarget(const std::shared_ptr<CapidEstate>& payTarget);

    private:
        QMap<QString, QString> name;
        std::shared_ptr<CapidEstateGroup> estateGroup;
        std::weak_ptr<CapidEstate> payTarget;
};


#endif //CAPICITY2_CAPIDESTATE_H
