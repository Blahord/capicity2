#ifndef CAPICITY2_CAPIDGAME_H
#define CAPICITY2_CAPIDGAME_H

#include <QList>

#include "GameState.h"
#include "game/CapiGamePlayer.h"
#include "game/CapiGame.h"
#include "../CapidPlayer.h"
#include "CapidEstate.h"
#include "CapidEstateGroup.h"
#include "CapidGameTemplate.h"

class CapidGame : public CapiGame {

    public:
        explicit CapidGame(const CapidGameTemplate& gameTemplate);
        ~CapidGame() override;

        const CapidGameTemplate& getTemplate();

        const QList<std::shared_ptr<CapidEstate>>& getEstates();
        std::shared_ptr<CapidEstate> findEstate(const QString& id);
        void addEstate(const std::shared_ptr<CapidEstate>& estate);

        const QList<std::shared_ptr<CapidEstateGroup>>& getEstateGroups() const;
        void addEstateGroup(const std::shared_ptr<CapidEstateGroup>& estateGroup);

        int getPositionOfStartEstate();
        void setStartEstate(const QString& startEstate);

        //State
        QString getStateName() const;
        QString getTurnStateName() const;

        int getPlayerOnTurnIndex() const;
        void setPlayerOnTurnIndex(int playerOnTurnIndex);

        std::shared_ptr<CapiGamePlayer> getPlayerOnTurn();

        int getMovementTime() const;
        void incrMovementTime();
        void resetMovementTime();

    private:

        CapidGameTemplate gameTemplate;

        QMap<QString, std::shared_ptr<CapidEstate>> estatesById;
        QList<std::shared_ptr<CapidEstate>> estates;
        QList<std::shared_ptr<CapidEstateGroup>> estateGroups;

        QString startEstate;

        //State
        int playerOnTurnIndex = 0;
        int movementTime = 0;
};

#endif //CAPICITY2_CAPIDGAME_H
