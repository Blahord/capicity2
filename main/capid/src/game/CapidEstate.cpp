#include "CapidEstate.h"

CapidEstate::CapidEstate(const QString& id,
                         int price,
                         int rent0, int rent1, int rent2,
                         int rent3, int rent4, int rent5,
                         int taxAmount, int taxPercent, const QString& payTargetId)
    : CapiEstate(id,
                 price,
                 rent0, rent1, rent2,
                 rent3, rent4, rent5,
                 taxAmount, taxPercent, payTargetId) {}

CapidEstate::~CapidEstate() = default;

QString CapidEstate::getName(const QString& language) {
    return name.value(language);
}

void CapidEstate::setName(const QString& language, const QString& estateName) {
    this->name.insert(language, estateName);
}

std::shared_ptr<CapidEstateGroup> CapidEstate::getEstateGroup() {
    return estateGroup;
}

void CapidEstate::setEstateGroup(const std::shared_ptr<CapidEstateGroup>& group) {
    this->estateGroup = group;
}

std::weak_ptr<CapidEstate> CapidEstate::getPayTarget() {
    return payTarget;
}

void CapidEstate::setPayTarget(const std::shared_ptr<CapidEstate>& payTarget) {
    this->payTarget = payTarget;
}
