#include "CapidGameTemplate.h"

#include <QJsonArray>

CapidGameTemplate::CapidGameTemplate(const QString& id) {
    this->id = id;
}

CapidGameTemplate::~CapidGameTemplate() = default;

const QString& CapidGameTemplate::getId() const {
    return id;
}

const QList<QString>& CapidGameTemplate::getLanguages() const {
    return languages;
}

QJsonObject CapidGameTemplate::getDataObject(const QString& language) {
    QString usedLanguage = languages.contains(language) ? language : defaultLanguage;

    QJsonObject dataObject;
    dataObject.insert("name", name.value(usedLanguage));

    QJsonArray languagesArray;
    for (const QString& lang : languages) {
        languagesArray.append(lang);
    }

    dataObject.insert("id", id);
    dataObject.insert("languages", languagesArray);
    dataObject.insert("defaultLanguage", defaultLanguage);
    dataObject.insert("description", description.value(usedLanguage));
    dataObject.insert("minPlayers", minPlayers);
    dataObject.insert("maxPlayers", maxPlayers);

    return dataObject;
}

void CapidGameTemplate::setName(const QString& language, const QString& gameName) {
    this->name.insert(language, gameName);
}

void CapidGameTemplate::setDescription(const QString& language, const QString& langDescription) {
    this->description.insert(language, langDescription);
}

void CapidGameTemplate::setMinPlayers(int gameMinPlayers) {
    this->minPlayers = gameMinPlayers;
}

void CapidGameTemplate::setMaxPlayers(int gameMaxPlayers) {
    this->maxPlayers = gameMaxPlayers;
}

void CapidGameTemplate::addLanguage(const QString& language) {
    this->languages.append(language);
}

void CapidGameTemplate::setDefaultLanguage(const QString& gameDefaultLanguage) {
    CapidGameTemplate::defaultLanguage = gameDefaultLanguage;
}

int CapidGameTemplate::getMinPlayers() const {
    return minPlayers;
}

int CapidGameTemplate::getMaxPlayers() const {
    return maxPlayers;
}
