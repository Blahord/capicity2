#include "CapidEstateGroup.h"

CapidEstateGroup::CapidEstateGroup(const QString& id, const QString& color, int housePrice) : CapiEstateGroup(id, color, housePrice) {
}

CapidEstateGroup::~CapidEstateGroup() = default;

QString CapidEstateGroup::getName(const QString& language) {
    return name.value(language);
}

void CapidEstateGroup::setName(const QString& language, const QString& name) {
    this->name.insert(language, name);
}

const QString& CapidEstateGroup::getRentMath() const {
    return rentMath;
}

void CapidEstateGroup::setRentMath(const QString& rentMath) {
    this->rentMath = rentMath;
}
