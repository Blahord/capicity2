#ifndef CAPICITY2_CAPIDGAMEREADER_H
#define CAPICITY2_CAPIDGAMEREADER_H

#include <QDomElement>

#include "CapidGame.h"
#include "CapidGameTemplate.h"
#include "CapidEstateGroup.h"
#include "../image/ImageRegistry.h"

class CapidGameReader {

    public:
        static std::unique_ptr<CapidGame> readGame(const QString& gamesDir, const QString& templateId, ImageRegistry* imageRegistry);
        static QList<CapidGameTemplate> getGames(const QString& gamesDirPath);

    private:
        static CapidGameTemplate readGameTemplate(const QString& id, const QDomElement& infoElement);

        static std::unique_ptr<CapidEstateGroup> readEstateGroup(const QDomElement& groupElement, const QList<QString>& allLanguages);
        static std::unique_ptr<CapidEstate> readEstate(const QDomElement& estateElement, const QList<std::shared_ptr<CapidEstateGroup>>& estateGroups,
                                       const QList<QString>& allLanguages,
                                       const QString& gamesDir, const QString& templateId, ImageRegistry* imageRegistry);

        static QMap<QString, QString> readLocalized(const QDomElement& parentElement, const QString& tagName, const QList<QString>& allLanguages);

        static int readNumberFromAttribute(const QDomElement& element, const QString& attribute, int defaultValue);
};


#endif //CAPICITY2_CAPIDGAMEREADER_H
