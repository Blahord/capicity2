#include "CapidGame.h"

#include <QUuid>

CapidGame::CapidGame(const CapidGameTemplate& gameTemplate)
    : CapiGame(QUuid::createUuid().toString(QUuid::Id128)), gameTemplate(gameTemplate) {
    setState(GameState::CONFIG);
}

CapidGame::~CapidGame() = default;

const CapidGameTemplate& CapidGame::getTemplate() {
    return gameTemplate;
}

const QList<std::shared_ptr<CapidEstate>>& CapidGame::getEstates() {
    return estates;
}

void CapidGame::addEstate(const std::shared_ptr<CapidEstate>& estate) {
    estates.append(estate);
    estatesById.insert(estate->getId(), estate);
}

std::shared_ptr<CapidEstate> CapidGame::findEstate(const QString& id) {
    return estatesById.value(id, nullptr);
}

const QList<std::shared_ptr<CapidEstateGroup>>& CapidGame::getEstateGroups() const {
    return estateGroups;
}

void CapidGame::addEstateGroup(const std::shared_ptr<CapidEstateGroup>& estateGroup) {
    estateGroups.append(estateGroup);
}

int CapidGame::getPositionOfStartEstate() {
    for (int i = 0; i < estates.size(); i++) {
        if (estates.at(i)->getId() == startEstate) {
            return i;
        }
    }

    return 0;
}

void CapidGame::setStartEstate(const QString& startEstate) {
    this->startEstate = startEstate;
}

QString CapidGame::getStateName() const {
    switch (getState()) {
        case GameState::CONFIG:
            return "CONFIG";
        case GameState::RUN:
            return "RUN";
        case GameState::END:
            return "END";
        default:
            return "";
    };
}

QString CapidGame::getTurnStateName() const {
    switch (getTurnState()) {
        case TurnState::ROLL:
            return "ROLL";
        case TurnState::MOVE:
            return "MOVE";
        case TurnState::TAX:
            return "TAX";
        case TurnState::BUY:
            return "BUY";
        default:
            return "NOTHING";
    };
}

int CapidGame::getPlayerOnTurnIndex() const {
    return playerOnTurnIndex;
}

void CapidGame::setPlayerOnTurnIndex(int playerOnTurnIndex) {
    CapidGame::playerOnTurnIndex = playerOnTurnIndex;
}

std::shared_ptr<CapiGamePlayer> CapidGame::getPlayerOnTurn() {
    if (getState() != GameState::RUN) {
        return nullptr;
    }

    return getPlayerOrder().at(playerOnTurnIndex);
}

int CapidGame::getMovementTime() const {
    return movementTime;
}

void CapidGame::incrMovementTime() {
    movementTime++;
}

void CapidGame::resetMovementTime() {
    movementTime = 0;
}
