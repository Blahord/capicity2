#ifndef CAPICITY2_CAPIDGAMETEMPLATE_H
#define CAPICITY2_CAPIDGAMETEMPLATE_H

#include <QString>
#include <QMap>
#include <QJsonObject>

class CapidGameTemplate {

    public:
        explicit CapidGameTemplate(const QString& id);
        virtual ~CapidGameTemplate();

        const QString& getId() const;
        const QList<QString>& getLanguages() const;
        QJsonObject getDataObject(const QString& language);

        void setName(const QString& language, const QString& gameName);
        void setDescription(const QString& language, const QString& langDescription);
        void setMinPlayers(int minPlayers);
        void setMaxPlayers(int gameMaxPlayers);
        void addLanguage(const QString& language);
        void setDefaultLanguage(const QString& gameDefaultLanguage);

        int getMinPlayers() const;
        int getMaxPlayers() const;

    private:
        QString id;
        QMap<QString, QString> name;
        QMap<QString, QString> description;
        int minPlayers = 0;
        int maxPlayers = 1;
        QList<QString> languages;
        QString defaultLanguage = "en";
};

#endif //CAPICITY2_CAPIDGAMETEMPLATE_H