#ifndef CAPICITY2_CAPIDESTATEGROUP_H
#define CAPICITY2_CAPIDESTATEGROUP_H

#include <QMap>

#include "game/CapiEstateGroup.h"

class CapidEstate;

class CapidEstateGroup : public CapiEstateGroup {

    public:
        explicit CapidEstateGroup(const QString& id, const QString& color, int housePrice);
        ~CapidEstateGroup() override;

        QString getName(const QString& language);
        void setName(const QString& language, const QString& name);

        const QString& getRentMath() const;
        void setRentMath(const QString& rentMath);

    private:
        QMap<QString, QString> name;
        QString rentMath;
};


#endif //CAPICITY2_CAPIDESTATEGROUP_H
