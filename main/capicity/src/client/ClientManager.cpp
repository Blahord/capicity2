#include "ClientManager.h"

ClientManager::ClientManager() = default;

ClientManager::~ClientManager() = default;

std::shared_ptr<CapiPlayer> ClientManager::getOwnPlayer() {
    return ownPlayer;
}

std::shared_ptr<CapiCityGame> ClientManager::getOwnGame() {
    return findGame(ownPlayer);
}

const QList<std::shared_ptr<CapiPlayer>>& ClientManager::getPlayers() {
    return players;
}

const QList<std::shared_ptr<CapiCityGameTemplate>>& ClientManager::getGameTemplates() {
    return gameTemplates;
}

const QList<std::shared_ptr<CapiCityGame>>& ClientManager::getGames() {
    return games;
}

std::shared_ptr<CapiPlayer> ClientManager::findPlayer(const QString& id) {
    for (const auto& player : players) {
        if (player->getId() == id) {
            return player;
        }
    }

    return nullptr;
}

std::shared_ptr<CapiCityGameTemplate> ClientManager::findGameTemplate(const QString& id) {
    for (const auto& gameTemplate : gameTemplates) {
        if (gameTemplate->getId() == id) {
            return gameTemplate;
        }
    }

    return nullptr;
}

std::shared_ptr<CapiCityGame> ClientManager::findGame(const std::shared_ptr<CapiPlayer>& player) {
    for (const auto& game : games) {
        if (game->containsPlayer(player)) {
            return game;
        }
    }

    return nullptr;
}

std::shared_ptr<CapiCityGame> ClientManager::findGame(const QString& id) {
    for (const auto& game : games)
        if (game->getId() == id) {
            return game;
        }


    return nullptr;
}

void ClientManager::setOwnPlayer(const std::shared_ptr<CapiPlayer>& player) {
    this->ownPlayer = player;
}

std::shared_ptr<CapiPlayer> ClientManager::createNewPlayer(const QString& id) {
    auto player = std::make_shared<CapiPlayer>(id);
    players.append(player);
    return player;
}

std::shared_ptr<CapiPlayer> ClientManager::findOrCreatePlayer(const QString& id) {
    auto player = findPlayer(id);
    if (player != nullptr)
        return player;

    return createNewPlayer(id);
}

void ClientManager::removePlayer(const std::shared_ptr<CapiPlayer>& player) {
    players.removeAll(player);
}

std::shared_ptr<CapiCityGameTemplate> ClientManager::createNewGameTemplate(const QString& id) {
    auto gameTemplate = std::make_shared<CapiCityGameTemplate>(id);
    gameTemplates.append(gameTemplate);
    return gameTemplate;
}

std::shared_ptr<CapiCityGameTemplate> ClientManager::findOrCreateGameTemplate(const QString& id) {
    auto gameTemplate = findGameTemplate(id);
    if (gameTemplate) {
        return gameTemplate;
    }

    return createNewGameTemplate(id);
}

void ClientManager::removeGameTemplate(const std::shared_ptr<CapiCityGameTemplate>& gameTemplate) {
    gameTemplates.removeAll(gameTemplate);
}

std::shared_ptr<CapiCityGame> ClientManager::createNewGame(const QString& id, const std::shared_ptr<CapiCityGameTemplate>& gameTemplate, const std::shared_ptr<CapiPlayer>& owner) {
    auto game = std::make_shared<CapiCityGame>(id, gameTemplate, owner);
    games.append(game);
    return game;
}

std::shared_ptr<CapiCityGame> ClientManager::findOrCreateGame(const QString& id, const std::shared_ptr<CapiCityGameTemplate>& gameTemplate, const std::shared_ptr<CapiPlayer>& owner) {
    auto game = findGame(id);
    if (game) {
        return game;
    }

    return createNewGame(id, gameTemplate, owner);
}

void ClientManager::removeGame(const std::shared_ptr<CapiCityGame>& game) {
    games.removeAll(game);
}
