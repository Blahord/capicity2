#include "ServerConnection.h"
#include "ClientUtils.h"

ServerConnection::ServerConnection(CapiConnection* connection) {
    this->connection = connection;

    connect(connection, &CapiConnection::gotMessage, this, &ServerConnection::newMessage);
}

ServerConnection::~ServerConnection() {

}

void ServerConnection::sendCommand(const QString& scope, const QString& name, const QJsonObject& arguments) {
    QString nonce = ClientUtils::createNonce();

    sendCommand(nonce, scope, name, arguments);
}

void ServerConnection::sendCommand(const QString& nonce, const QString& scope, const QString& name, const QJsonObject& arguments) {
    connection->send(
        {
            {"nonce",     nonce},
            {"scope",     scope},
            {"command",   name},
            {"arguments", arguments}
        }
    );
}
