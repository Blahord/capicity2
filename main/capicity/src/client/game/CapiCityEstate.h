#ifndef CAPICITY2_CAPICITYESTATE_H
#define CAPICITY2_CAPICITYESTATE_H

#include <QString>

#include "game/CapiEstate.h"
#include "CapiCityEstateGroup.h"

class CapiCityEstate : public CapiEstate {

    public:
        explicit CapiCityEstate(const QString& id,
                                const std::shared_ptr<CapiCityEstateGroup>& group, QString name,
                                int price,
                                int rent0, int rent1, int rent2,
                                int rent3, int rent4, int rent5,
                                int taxAmount, int taxPercent, const QString& payTargetId);


        ~CapiCityEstate() override = default;

        std::shared_ptr<CapiCityEstateGroup> getGroup() const;
        const QString& getName() const;

        std::weak_ptr<CapiCityEstate> getPayTarget();
        void setPayTarget(const std::shared_ptr<CapiCityEstate>& payTarget);

    private:
        std::shared_ptr<CapiCityEstateGroup> group;
        std::weak_ptr<CapiCityEstate> payTarget;
        QString name;

};


#endif //CAPICITY2_CAPICITYESTATE_H
