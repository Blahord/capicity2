#ifndef CAPICITY2_CAPICITYGAMETEMPLATE_H
#define CAPICITY2_CAPICITYGAMETEMPLATE_H

#include <QString>
#include <QList>

#include "game/CapiGameTemplate.h"

class CapiCityGameTemplate : public CapiGameTemplate {

    public:
        explicit CapiCityGameTemplate(const QString& id);
        ~CapiCityGameTemplate() override = default;

        const QString& getName() const;
        void setName(const QString& name);

        const QString& getDescription() const;
        void setDescription(const QString& description);

    private:
        QString name;
        QString description;
};


#endif //CAPICITY2_CAPICITYGAMETEMPLATE_H
