#ifndef CAPICITY2_CAPICITYGAME_H
#define CAPICITY2_CAPICITYGAME_H

#include <QMap>

#include "CapiPlayer.h"
#include "GameState.h"
#include "game/CapiGame.h"
#include "game/CapiGamePlayer.h"
#include "CapiCityGameTemplate.h"
#include "CapiCityEstateGroup.h"
#include "CapiCityEstate.h"

class CapiCityGame : public CapiGame {

    public:
        explicit CapiCityGame(QString id, const std::shared_ptr<CapiCityGameTemplate>& gameTemplate, const std::shared_ptr<CapiPlayer>& owner);
        ~CapiCityGame() override;

        std::shared_ptr<CapiCityGameTemplate> getGameTemplate() const;

        std::shared_ptr<CapiGamePlayer> getTurnPlayer() const;
        void setTurnPlayer(const std::shared_ptr<CapiGamePlayer>& turnPlayer);

        const QList<std::shared_ptr<CapiCityEstateGroup>>& getEstateGroups() const;
        std::shared_ptr<CapiCityEstateGroup> findEstateGroup(const QString& id) const;
        void addEstateGroup(const std::shared_ptr<CapiCityEstateGroup>& estateGroup);

        const QList<std::shared_ptr<CapiCityEstate>>& getEstates() const;
        std::shared_ptr<CapiCityEstate> findEstate(const QString& id) const;
        void addEstate(const std::shared_ptr<CapiCityEstate>& estate);

        void clear();

    private:
        std::shared_ptr<CapiCityGameTemplate> gameTemplate;

        std::shared_ptr<CapiGamePlayer> turnPlayer;

        QList<std::shared_ptr<CapiCityEstateGroup>> estateGroups;

        QMap<QString, std::shared_ptr<CapiCityEstate>> estatesById;
        QList<std::shared_ptr<CapiCityEstate>> estates;
};


#endif //CAPICITY2_CAPICITYGAME_H
