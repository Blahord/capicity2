#include "CapiCityEstate.h"

#include <utility>

CapiCityEstate::CapiCityEstate(const QString& id,
                               const std::shared_ptr<CapiCityEstateGroup>& group, QString name,
                               int price,
                               int rent0, int rent1, int rent2,
                               int rent3, int rent4, int rent5,
                               int taxAmount, int taxPercent, const QString& payTargetId)
    : CapiEstate(id,
                 price,
                 rent0, rent1, rent2,
                 rent3, rent4, rent5,
                 taxAmount, taxPercent, payTargetId),
      group(group), name(std::move(name)) {}

std::shared_ptr<CapiCityEstateGroup> CapiCityEstate::getGroup() const {
    return group;
}

const QString& CapiCityEstate::getName() const {
    return name;
}

std::weak_ptr<CapiCityEstate> CapiCityEstate::getPayTarget() {
    return payTarget;
}

void CapiCityEstate::setPayTarget(const std::shared_ptr<CapiCityEstate>& payTarget) {
    this->payTarget = payTarget;
}
