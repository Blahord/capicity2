#include "CapiCityEstateGroup.h"

#include <utility>

CapiCityEstateGroup::CapiCityEstateGroup(const QString& id, const QString& color, int housePrice, QString name)
    : CapiEstateGroup(id, color, housePrice), name(std::move(name)) {
}

const QString& CapiCityEstateGroup::getName() const {
    return name;
}
