#include "CapiCityGameTemplate.h"

CapiCityGameTemplate::CapiCityGameTemplate(const QString& id)
    : CapiGameTemplate(id) {
}

const QString& CapiCityGameTemplate::getName() const {
    return name;
}

void CapiCityGameTemplate::setName(const QString& name) {
    CapiCityGameTemplate::name = name;
}

const QString& CapiCityGameTemplate::getDescription() const {
    return description;
}

void CapiCityGameTemplate::setDescription(const QString& description) {
    CapiCityGameTemplate::description = description;
}
