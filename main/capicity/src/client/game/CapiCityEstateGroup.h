#ifndef CAPICITY2_CAPICITYESTATEGROUP_H
#define CAPICITY2_CAPICITYESTATEGROUP_H

#include <QString>

#include "game/CapiEstateGroup.h"

class CapiCityEstateGroup : public CapiEstateGroup {

    public:
        explicit CapiCityEstateGroup(const QString& id, const QString& color, int housePrice, QString name);

        ~CapiCityEstateGroup() override = default;

        const QString& getName() const;

    private:
        QString name;
};


#endif //CAPICITY2_CAPICITYESTATEGROUP_H
