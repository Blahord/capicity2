#include "CapiCityGame.h"

#include <utility>

#include "game/CapiGame.h"

CapiCityGame::CapiCityGame(QString id, const std::shared_ptr<CapiCityGameTemplate>& gameTemplate, const std::shared_ptr<CapiPlayer>& owner) :
    CapiGame(std::move(id)), gameTemplate(gameTemplate) {
    setOwner(owner);
}

CapiCityGame::~CapiCityGame() {
    clear();
}

std::shared_ptr<CapiCityGameTemplate> CapiCityGame::getGameTemplate() const {
    return gameTemplate;
}

std::shared_ptr<CapiGamePlayer> CapiCityGame::getTurnPlayer() const {
    return turnPlayer;
}

void CapiCityGame::setTurnPlayer(const std::shared_ptr<CapiGamePlayer>& turnPlayer) {
    CapiCityGame::turnPlayer = turnPlayer;
}

const QList<std::shared_ptr<CapiCityEstateGroup>>& CapiCityGame::getEstateGroups() const {
    return estateGroups;
}

std::shared_ptr<CapiCityEstateGroup> CapiCityGame::findEstateGroup(const QString& id) const {
    for (auto group : estateGroups) {
        if (group->getId() == id) {
            return group;
        }
    }

    return nullptr;
}

void CapiCityGame::addEstateGroup(const std::shared_ptr<CapiCityEstateGroup>& estateGroup) {
    estateGroups.append(estateGroup);
}

const QList<std::shared_ptr<CapiCityEstate>>& CapiCityGame::getEstates() const {
    return estates;
}

std::shared_ptr<CapiCityEstate> CapiCityGame::findEstate(const QString& id) const {
    return estatesById.value(id, nullptr);
}

void CapiCityGame::addEstate(const std::shared_ptr<CapiCityEstate>& estate) {
    estates.append(estate);
    estatesById.insert(estate->getId(), estate);
}

void CapiCityGame::clear() {
    estatesById.clear();
    estates.clear();
    estateGroups.clear();
}
