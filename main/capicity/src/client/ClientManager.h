#ifndef CAPICITY2_CLIENTMANAGER_H
#define CAPICITY2_CLIENTMANAGER_H

#include <QObject>
#include "CapiPlayer.h"
#include "game/CapiCityGameTemplate.h"
#include "game/CapiCityGame.h"

class ClientManager {

    public:
        explicit ClientManager();
        virtual ~ClientManager();

        std::shared_ptr<CapiPlayer> getOwnPlayer();
        std::shared_ptr<CapiCityGame> getOwnGame();

        const QList<std::shared_ptr<CapiPlayer>>& getPlayers();
        const QList<std::shared_ptr<CapiCityGameTemplate>>& getGameTemplates();
        const QList<std::shared_ptr<CapiCityGame>>& getGames();

        std::shared_ptr<CapiPlayer> findPlayer(const QString& id);
        std::shared_ptr<CapiCityGameTemplate> findGameTemplate(const QString& id);
        std::shared_ptr<CapiCityGame> findGame(const std::shared_ptr<CapiPlayer>& player);
        std::shared_ptr<CapiCityGame> findGame(const QString& id);

        void setOwnPlayer(const std::shared_ptr<CapiPlayer>& player);

        std::shared_ptr<CapiPlayer> createNewPlayer(const QString& id);
        std::shared_ptr<CapiPlayer> findOrCreatePlayer(const QString& id);
        void removePlayer(const std::shared_ptr<CapiPlayer>& player);

        std::shared_ptr<CapiCityGameTemplate> createNewGameTemplate(const QString& id);
        std::shared_ptr<CapiCityGameTemplate> findOrCreateGameTemplate(const QString& id);
        void removeGameTemplate(const std::shared_ptr<CapiCityGameTemplate>& gameTemplate);

        std::shared_ptr<CapiCityGame> createNewGame(const QString& id, const std::shared_ptr<CapiCityGameTemplate>& gameTemplate, const std::shared_ptr<CapiPlayer>& owner);
        std::shared_ptr<CapiCityGame> findOrCreateGame(const QString& id, const std::shared_ptr<CapiCityGameTemplate>& gameTemplate, const std::shared_ptr<CapiPlayer>& owner);
        void removeGame(const std::shared_ptr<CapiCityGame>& game);

    private:
        std::shared_ptr<CapiPlayer> ownPlayer;

        QList<std::shared_ptr<CapiPlayer>> players;
        QList<std::shared_ptr<CapiCityGameTemplate>> gameTemplates;
        QList<std::shared_ptr<CapiCityGame>> games;

};


#endif //CAPICITY2_CLIENTMANAGER_H
