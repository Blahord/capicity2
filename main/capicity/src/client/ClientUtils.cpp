#include "ClientUtils.h"

#include <QDebug>
#include <QRandomGenerator>

#include "../CapiCitySettings.h"

bool ClientUtils::validate(const QJsonValue& value, const QString& path, const std::shared_ptr<JsonValueCheck>& check) {
    QList<JsonError> errors;

    check->check(value, path, errors);
    if (!errors.isEmpty()) {
        qDebug() << "Ignoring message";
        qDebug() << value;
        qDebug() << "reason(s):";

        for (const auto& error : errors) {
            qDebug() << error.getPath() << ": " << error.getMessage();
        }
        return false;
    }

    return true;
}

QString ClientUtils::createNonce() {
    QString nonce = "";
    for (int i = 0; i < 5; i++) {
        nonce += QString::number(QRandomGenerator::global()->generate(), 16);
    }

    return nonce;
}

void ClientUtils::setOwnAvatar(ServerConnection* serverConnection) {
    CapiCitySettings settings;

    if (settings.isUseAvatar()) {
        serverConnection->sendCommand(
            "server", "setAvatar",
            getAvatarData()
        );
    } else {
        serverConnection->sendCommand("server", "clearAvatar", {});
    }
}

QJsonObject ClientUtils::getAvatarData() {
    CapiCitySettings settings;

    QImage avatarPixMap = settings.getAvatarImage();

    QString avatarData = "";

    for (int y = 0; y < avatarPixMap.height(); y++) {
        for (int x = 0; x < avatarPixMap.width(); x++) {
            QColor color = avatarPixMap.pixelColor(x, y);
            avatarData.append(color.name(QColor::HexArgb).right(8));
        }
    }

    return {
        {"width",  avatarPixMap.width()},
        {"height", avatarPixMap.height()},
        {"data",   avatarData}
    };
}
