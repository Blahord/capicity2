#ifndef CAPICITY2_CHATHANDLER_H
#define CAPICITY2_CHATHANDLER_H

#include "MessageHandler.h"

class ChatHandler : public MessageHandler {

    public:
        explicit ChatHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~ChatHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_CHATHANDLER_H
