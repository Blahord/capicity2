#include "GamePlayerUpdateHandler.h"

#include "json/JsonChecks.h"

GamePlayerUpdateHandler::GamePlayerUpdateHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            {
                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                JsonChecks::optionalField("ready", JsonChecks::booleanValue()),
                JsonChecks::optionalField("money", JsonChecks::integerValue()),
                JsonChecks::optionalField("jailed", JsonChecks::booleanValue()),
                JsonChecks::optionalField("position", JsonChecks::integerValue())
            }
        )
    );
}

GamePlayerUpdateHandler::~GamePlayerUpdateHandler() = default;

QString GamePlayerUpdateHandler::getName() {
    return "gamePlayerUpdate";
}

void GamePlayerUpdateHandler::handle(const QJsonValue& messageData) {
    QJsonObject gamePlayerDataObject = messageData.toObject();
    QString id = gamePlayerDataObject.value("id").toString();

    auto player = getClientManager()->findPlayer(id);
    if (!player) {
        return;
    }

    auto ownGame = getClientManager()->getOwnGame();
    if (!ownGame) {
        return;
    }

    auto gamePlayer = ownGame->findPlayer(player);
    if (!gamePlayer) {
        return;
    }

    if (gamePlayerDataObject.contains("ready")) {
        gamePlayer->setReady(gamePlayerDataObject.value("ready").toBool(false));
    }
    if (gamePlayerDataObject.contains("position")) {
        gamePlayer->setPosition(gamePlayerDataObject.value("position").toInt(0));
    }
    if (gamePlayerDataObject.contains("money")) {
        gamePlayer->setMoney(gamePlayerDataObject.value("money").toInt(0));
    }
    if (gamePlayerDataObject.contains("jailed")) {
        gamePlayer->setInJail(gamePlayerDataObject.value("jailed").toBool(false));
    }
    if (gamePlayerDataObject.contains("moved")) {
        gamePlayer->setMoved(gamePlayerDataObject.value("moved").toBool(false));
    }

    emit(gamePlayerUpdate(gamePlayer));
}
