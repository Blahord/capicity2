#ifndef CAPICITY2_PLAYERDATAHANDLER_H
#define CAPICITY2_PLAYERDATAHANDLER_H

#include "MessageHandler.h"

class PlayerDataHandler : public MessageHandler {

    public:
        explicit PlayerDataHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~PlayerDataHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;

    private:
        CapiCitySettings settings;
};


#endif //CAPICITY2_PLAYERDATAHANDLER_H
