#include "GameDeleteHandler.h"

#include "json/JsonChecks.h"

GameDeleteHandler::GameDeleteHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("id", JsonChecks::stringValue())
        )
    );
}

GameDeleteHandler::~GameDeleteHandler() = default;

QString GameDeleteHandler::getName() {
    return "gameDelete";
}

void GameDeleteHandler::handle(const QJsonValue& messageData) {
    auto gameObject = messageData.toObject();
    QString id = gameObject.value("id").toString();

    auto game = getClientManager()->findGame(id);
    if (!game) {
        return;
    }

    if (game == getClientManager()->getOwnGame()) {
        emit(gameLeft());
    }

    getClientManager()->removeGame(game);

    emit(newGameList());
    emit(newPlayerList());
}
