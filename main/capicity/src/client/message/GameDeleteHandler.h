#ifndef CAPICITY2_GAMEDELETEHANDLER_H
#define CAPICITY2_GAMEDELETEHANDLER_H

#include "MessageHandler.h"

class GameDeleteHandler : public MessageHandler {

    public:
        explicit GameDeleteHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~GameDeleteHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_GAMEDELETEHANDLER_H
