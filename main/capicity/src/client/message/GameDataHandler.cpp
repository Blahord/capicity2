#include "GameDataHandler.h"

#include "json/JsonChecks.h"

GameDataHandler::GameDataHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            {
                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                JsonChecks::requiredField("bgColor", JsonChecks::regexp("^[0-9a-f]{6}$")),
                JsonChecks::requiredField(
                    "estateGroups",
                    JsonChecks::arrayValue(
                        JsonChecks::forAll(JsonChecks::objectValue(
                            {
                                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                                JsonChecks::requiredField("name", JsonChecks::stringValue()),
                                JsonChecks::optionalField("color", JsonChecks::regexp("^[0-9a-f]{6}$")),
                            }
                        ))
                    )
                ),
                JsonChecks::requiredField(
                    "estates",
                    JsonChecks::arrayValue(
                        JsonChecks::forAll(JsonChecks::objectValue(
                            {
                                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                                JsonChecks::requiredField("name", JsonChecks::stringValue()),
                                JsonChecks::optionalField("estateGroup", JsonChecks::stringValue()),
                                JsonChecks::optionalField("image", JsonChecks::regexp("^[0-9a-f]{64}$")),
                                JsonChecks::optionalField("price", JsonChecks::integerValue()),
                                JsonChecks::optionalField("rent0", JsonChecks::integerValue()),
                                JsonChecks::optionalField("rent1", JsonChecks::integerValue()),
                                JsonChecks::optionalField("rent2", JsonChecks::integerValue()),
                                JsonChecks::optionalField("rent3", JsonChecks::integerValue()),
                                JsonChecks::optionalField("rent4", JsonChecks::integerValue()),
                                JsonChecks::optionalField("rent5", JsonChecks::integerValue()),
                                JsonChecks::optionalField("taxAmount", JsonChecks::integerValue()),
                                JsonChecks::optionalField("taxPercent", JsonChecks::integerValue()),
                                JsonChecks::optionalField("payTarget", JsonChecks::stringValue()),
                                JsonChecks::optionalField("owner", JsonChecks::stringValue()),
                                JsonChecks::optionalField("money", JsonChecks::integerValue())
                            }
                        ))
                    )
                )
            }
        )
    );
}

GameDataHandler::~GameDataHandler() = default;

QString GameDataHandler::getName() {
    return "gameData";
}

void GameDataHandler::handle(const QJsonValue& messageData) {
    auto gameDataObj = messageData.toObject();
    QString const id = gameDataObj.value("id").toString();

    auto game = getClientManager()->findGame(id);
    if (!game) {
        return;
    }

    game->setBgColor(gameDataObj.value("bgColor").toString());
    game->clear();

    QMap<QString, std::shared_ptr<CapiCityEstateGroup>> groups;
    for (auto estateGroupValue : gameDataObj.value("estateGroups").toArray()) {
        auto estateGroupObj = estateGroupValue.toObject();
        QString const gid = estateGroupObj.value("id").toString();
        QJsonValue const colorValue = estateGroupObj.value("color");
        QJsonValue const housePriceValue = estateGroupObj.value("housePrice");
        QString const name = estateGroupObj.value("name").toString();

        auto estateGroup = std::make_shared<CapiCityEstateGroup>(gid, colorValue.isString() ? colorValue.toString() : "", housePriceValue.toInt(0), name);
        groups.insert(gid, estateGroup);
        game->addEstateGroup(estateGroup);
    }

    for (auto estateValue : gameDataObj.value("estates").toArray()) {
        auto estateObj = estateValue.toObject();
        QString const eid = estateObj.value("id").toString();
        auto group = groups.value(estateObj.value("estateGroup").toString(), nullptr);
        QString const name = estateObj.value("name").toString();
        QJsonValue const imageValue = estateObj.value("image");
        int const price = estateObj.value("price").toInt(0);
        int const rent0 = estateObj.value("rent0").toInt(0);
        int const rent1 = estateObj.value("rent1").toInt(0);
        int const rent2 = estateObj.value("rent2").toInt(0);
        int const rent3 = estateObj.value("rent3").toInt(0);
        int const rent4 = estateObj.value("rent4").toInt(0);
        int const rent5 = estateObj.value("rent5").toInt(0);
        int const taxAmount = estateObj.value("taxAmount").toInt(0);
        int const taxPercent = estateObj.value("taxPercent").toInt(0);
        QString const payTargetId = estateObj.value("payTargetId").toString(QString());
        QString const ownerId = estateObj.value("owner").toString(QString());
        int const money = estateObj.value("money").toInt(0);

        QString image = "";
        if (imageValue.isString()) {
            image = imageValue.toString();
            emit(imageHash(image));
        }

        auto estate = std::make_shared<CapiCityEstate>(
            eid,
            group, name,
            price,
            rent0, rent1, rent2,
            rent3, rent4, rent5,
            taxAmount, taxPercent, payTargetId
        );
        estate->setImage(image);
        estate->setMoney(money);
        estate->setOwner(game->findPlayer(ownerId));

        game->addEstate(estate);
    }

    for (const auto& estate : game->getEstates()) {
        if (estate->getPayTargetId().isNull()) {
            continue;
        }

        estate->setPayTarget(game->findEstate(estate->getPayTargetId()));
    }

    emit(newGameData());
}
