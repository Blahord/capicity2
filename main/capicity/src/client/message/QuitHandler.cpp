#include "QuitHandler.h"

#include "json/JsonChecks.h"

QuitHandler::QuitHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("id", JsonChecks::stringValue())
        )
    );
}

QuitHandler::~QuitHandler() = default;

QString QuitHandler::getName() {
    return "quit";
}

void QuitHandler::handle(const QJsonValue& messageData) {
    auto gameDataObj = messageData.toObject();
    QString const id = gameDataObj.value("id").toString();

    auto player = getClientManager()->findPlayer(id);
    if (!player || player == getClientManager()->getOwnPlayer()) {
        return;
    }

    auto playersGame = getClientManager()->findGame(player);
    if (playersGame) {
        playersGame->removePlayer(player);
        playersGame->removeFromPlayerOrder(player);
    }

    getClientManager()->removePlayer(player);

    emit(newPlayerList());
}
