#ifndef CAPICITY2_GAMEEVENTCHECKS_H
#define CAPICITY2_GAMEEVENTCHECKS_H

#include <QString>
#include <QMap>

#include "json/checks/JsonValueCheck.h"

class GameEventChecks {

    public:
        explicit GameEventChecks();
        virtual ~GameEventChecks();

        std::shared_ptr<JsonValueCheck> get(const QString& type);

    private:
        QMap<QString, std::shared_ptr<JsonValueCheck>> checks;
};


#endif //CAPICITY2_GAMEEVENTCHECKS_H
