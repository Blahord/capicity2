#include "PlayerUpdateHandler.h"

#include "json/JsonChecks.h"

PlayerUpdateHandler::PlayerUpdateHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            {
                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                JsonChecks::optionalField("connected", JsonChecks::booleanValue()),
                JsonChecks::optionalField("avatar", JsonChecks::stringValue()),
                JsonChecks::optionalField("name", JsonChecks::stringValue()),
                JsonChecks::optionalField("moved", JsonChecks::booleanValue())
            }
        )
    );
}

PlayerUpdateHandler::~PlayerUpdateHandler() = default;

QString PlayerUpdateHandler::getName() {
    return "playerUpdate";
}

void PlayerUpdateHandler::handle(const QJsonValue& messageData) {
    QJsonObject const playerDataObject = messageData.toObject();
    QString const id = playerDataObject.value("id").toString();
    QJsonValue const connectedValue = playerDataObject.value("connected");
    QJsonValue const avatarValue = playerDataObject.value("avatar");
    QJsonValue const nameValue = playerDataObject.value("newName");

    auto player = getClientManager()->findPlayer(id);
    if (player == nullptr) {
        return;
    }

    if (connectedValue.isBool()) {
        bool const oldConnected = player->isConnected();
        bool const newConnected = connectedValue.toBool(true);
        player->setConnected(newConnected);

        if (getClientManager()->findGame(player) == getClientManager()->getOwnGame()) {
            if (oldConnected && !newConnected) {
                emit(playerConnectionLost(player));
            }
            if (!oldConnected && newConnected) {
                emit(playerReconnected(player));
            }
        }
    }

    if (avatarValue.isString()) {
        QString const avatarHash = avatarValue.toString();
        player->setAvatarData(avatarHash);
        emit(imageHash(avatarHash));
    }

    if (nameValue.isString()) {
        player->setName(nameValue.toString());
    }

    emit(playerUpdated(player));
    emit(newPlayerList());
}
