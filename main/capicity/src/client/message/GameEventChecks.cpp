#include "GameEventChecks.h"

#include "json/JsonChecks.h"

GameEventChecks::GameEventChecks() {
    checks.insert(
        "turn",
        JsonChecks::stringValue()
    );

    checks.insert(
        "roll",
        JsonChecks::objectValue(
            JsonChecks::requiredField("dice1", JsonChecks::integerValue()),
            JsonChecks::requiredField("dice2", JsonChecks::integerValue())
        )
    );

    checks.insert(
        "land",
        JsonChecks::integerValue()
    );

    checks.insert(
        "canBuy",
        JsonChecks::nullValue()
    );

    checks.insert(
        "buy",
        JsonChecks::nullValue()
    );

    checks.insert(
        "taxPayNeeded",
        JsonChecks::objectValue(
            JsonChecks::requiredField("amount", JsonChecks::integerValue()),
            JsonChecks::requiredField("percent", JsonChecks::integerValue())
        )
    );

    checks.insert(
        "taxPayed",
        JsonChecks::integerValue()
    );

    checks.insert(
        "skipBuy",
        JsonChecks::nullValue()
    );

    checks.insert(
        "end",
        JsonChecks::stringValue()
    );
}

GameEventChecks::~GameEventChecks() = default;

std::shared_ptr<JsonValueCheck> GameEventChecks::get(const QString& type) {
    return checks.value(type, nullptr);
}
