#ifndef CAPICITY2_ESTATEUPDATEHANDLER_H
#define CAPICITY2_ESTATEUPDATEHANDLER_H

#include "MessageHandler.h"

class EstateUpdateHandler : public MessageHandler {

    public:
        explicit EstateUpdateHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~EstateUpdateHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_ESTATEUPDATEHANDLER_H
