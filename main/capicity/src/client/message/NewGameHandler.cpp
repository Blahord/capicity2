#include "NewGameHandler.h"

#include "json/JsonChecks.h"

NewGameHandler::NewGameHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            {
                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                JsonChecks::requiredField("owner", JsonChecks::stringValue()),
                JsonChecks::requiredField("type", JsonChecks::stringValue())
            }
        )
    );
}

NewGameHandler::~NewGameHandler() = default;

QString NewGameHandler::getName() {
    return "newGame";
}

void NewGameHandler::handle(const QJsonValue& messageData) {
    QJsonObject gameDataObj = messageData.toObject();
    QString id = gameDataObj.value("id").toString();
    QString typeId = gameDataObj.value("type").toString();
    QString ownerId = gameDataObj.value("owner").toString();

    auto owner = getClientManager()->findPlayer(ownerId);
    auto gameTemplate = getClientManager()->findGameTemplate(typeId);

    if (!owner || !gameTemplate) {
        return;
    }

    auto game = getClientManager()->createNewGame(id, gameTemplate, owner);
    game->addPlayer(owner);

    emit(newGameList());

    if (owner == getClientManager()->getOwnPlayer()) {
        emit(gameEntered());
    }
}
