#include "MessageHandler.h"

MessageHandler::MessageHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) {
    this->clientManager = clientManager;
    this->serverConnection = serverConnection;
}

MessageHandler::~MessageHandler() = default;

std::shared_ptr<JsonValueCheck> MessageHandler::getValidation() {
    return validation;
}

void MessageHandler::setValidation(const std::shared_ptr<JsonValueCheck>& check) {
    this->validation = check;
}

std::shared_ptr<ClientManager> MessageHandler::getClientManager() {
    return clientManager;
}

ServerConnection* MessageHandler::getServerConnection() {
    return serverConnection;
}

void MessageHandler::updateGamePlayersList(const std::shared_ptr<CapiCityGame>& game, const QJsonArray& playerList) {
    QList<std::shared_ptr<CapiPlayer>> gamePlayersFromServer;
    for (auto playerValue : playerList) {
        QString const playerId = playerValue.toString();
        auto player = clientManager->findPlayer(playerId);
        if (!player) {
            continue;
        }

        if (!game->containsPlayer(player)) {
            game->addPlayer(player);
        }

        gamePlayersFromServer.append(player);
    }

    QList<std::shared_ptr<CapiPlayer>> playersToRemove;
    for (const auto& player : game->getPlayers()) {
        if (!gamePlayersFromServer.contains(player->getPlayer())) {
            playersToRemove.append(player->getPlayer());
        }
    }

    for (auto player : playersToRemove) {
        game->removePlayer(player);
    }
}
