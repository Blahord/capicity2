#include "PlayerListHandler.h"

#include "json/JsonChecks.h"

PlayerListHandler::PlayerListHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField(
                "list",
                JsonChecks::arrayValue(
                    JsonChecks::forAll(
                        JsonChecks::objectValue(
                            {
                                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                                JsonChecks::requiredField("name", JsonChecks::stringValue()),
                                JsonChecks::requiredField("connected", JsonChecks::booleanValue()),
                                JsonChecks::optionalField("avatar", JsonChecks::stringValue())
                            }
                        )
                    )
                )
            )
        )
    );
}

PlayerListHandler::~PlayerListHandler() = default;

QString PlayerListHandler::getName() {
    return "playerList";
}

void PlayerListHandler::handle(const QJsonValue& messageData) {
    QList<std::shared_ptr<CapiPlayer>> playerListFromServer;

    QJsonArray playersArray = messageData.toObject().value("list").toArray();
    for (QJsonValue playerInfo : playersArray) {
        QJsonObject playerObject = playerInfo.toObject();

        QString id = playerObject.value("id").toString();
        QString name = playerObject.value("name").toString();
        QJsonValue avatarValue = playerObject.value("avatar");
        QJsonValue connectedValue = playerObject.value("connected");

        auto player = getClientManager()->findOrCreatePlayer(id);
        player->setName(name);
        playerListFromServer.append(player);

        if (connectedValue.isBool()) {
            player->setConnected(connectedValue.toBool(true));
        }

        if (avatarValue.isString()) {
            const QString& avatarHash = avatarValue.toString();
            player->setAvatarData(avatarHash);
            emit(imageHash(avatarHash));
        }
    }

    for (auto player : QList(getClientManager()->getPlayers())) {
        if (!playerListFromServer.contains(player)) {
            getClientManager()->removePlayer(player);
        }
    }

    emit(newPlayerList());
}
