#ifndef CAPICITY2_GAMEUPDATEHANDLER_H
#define CAPICITY2_GAMEUPDATEHANDLER_H

#include "MessageHandler.h"

class GameUpdateHandler : public MessageHandler {

    public:
        explicit GameUpdateHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~GameUpdateHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};

#endif //CAPICITY2_GAMEUPDATEHANDLER_H
