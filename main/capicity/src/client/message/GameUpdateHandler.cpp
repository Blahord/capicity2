#include "GameUpdateHandler.h"

#include "json/JsonChecks.h"

GameUpdateHandler::GameUpdateHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            {
                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                JsonChecks::optionalField("owner", JsonChecks::stringValue()),
                JsonChecks::optionalField("players", JsonChecks::arrayValue(JsonChecks::forAll(JsonChecks::stringValue()))),
                JsonChecks::optionalField("state", JsonChecks::regexp("CONFIG|RUN|END")),
                JsonChecks::optionalField("turnState", JsonChecks::regexp("NOTHING|ROLL|MOVE|TAX|BUY")),
                JsonChecks::optionalField("playerOrder", JsonChecks::arrayValue(JsonChecks::forAll(JsonChecks::stringValue()))),
                JsonChecks::optionalField("turnPlayer", JsonChecks::stringValue())
            }
        )
    );
}

GameUpdateHandler::~GameUpdateHandler() = default;

QString GameUpdateHandler::getName() {
    return "gameUpdate";
}

void GameUpdateHandler::handle(const QJsonValue& messageData) {
    auto gameObject = messageData.toObject();

    QString const id = gameObject.value("id").toString();

    auto game = getClientManager()->findGame(id);
    if (!game) {
        return;
    }

    auto oldCurrentGame = getClientManager()->getOwnGame();

    if (gameObject.contains("owner")) {
        QString const ownerId = gameObject.value("owner").toString();
        auto newOwner = getClientManager()->findPlayer(ownerId);
        if (newOwner) {
            game->setOwner(newOwner);
        }
    }

    if (gameObject.contains("players")) {
        QJsonArray const playersList = gameObject.value("players").toArray();
        updateGamePlayersList(game, playersList);
    }

    if (gameObject.contains("state")) {
        QString const state = gameObject.value("state").toString();

        if (state == "CONFIG") {
            game->setState(GameState::CONFIG);
        } else if (state == "RUN") {
            game->setState(GameState::RUN);
        } else if (state == "END") {
            game->setState(GameState::END);
        }
    }

    if (gameObject.contains("turnState")) {
        QString const turnState = gameObject.value("turnState").toString();

        if (turnState == "NOTHING") {
            game->setTurnState(TurnState::NOTHING);
        } else if (turnState == "ROLL") {
            game->setTurnState(TurnState::ROLL);
        } else if (turnState == "MOVE") {
            game->setTurnState(TurnState::MOVE);
        } else if (turnState == "TAX") {
            game->setTurnState(TurnState::TAX);
        } else if (turnState == "BUY") {
            game->setTurnState(TurnState::BUY);
        }
    }

    if (gameObject.contains("playerOrder")) {
        QJsonArray const playerOrder = gameObject.value("playerOrder").toArray();
        QList<QString> playersInOrder;

        for (QJsonValue const playerValue : playerOrder) {
            QString const playerId = playerValue.toString();
            playersInOrder.append(playerId);
        }

        game->setPlayerOrder(playersInOrder);
    }

    if (gameObject.contains("turnPlayer")) {
        QString const playerId = gameObject.value("turnPlayer").toString();
        auto gamePlayer = game->findPlayer(playerId);
        if (gamePlayer) {
            game->setTurnPlayer(gamePlayer);
        }
    }

    auto newCurrentGame = getClientManager()->getOwnGame();

    emit(gameUpdate(game));
    emit(newGameList());
    emit(newPlayerList());
    if (!oldCurrentGame && newCurrentGame) {
        emit(gameEntered());
    } else if (oldCurrentGame && !newCurrentGame) {
        emit(gameLeft());
    }
}
