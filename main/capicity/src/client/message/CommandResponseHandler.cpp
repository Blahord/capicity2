#include "CommandResponseHandler.h"

#include "json/JsonChecks.h"
#include "response/DataResponseDataChecks.h"
#include "../ClientUtils.h"

CommandResponseHandler::CommandResponseHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            {
                JsonChecks::requiredField("nonce", JsonChecks::stringValue()),
                JsonChecks::requiredField("result", JsonChecks::regexp("OK|UNKNOWN_SCOPE|UNKNOWN_COMMAND|FORMAL_ERROR|STATE_ERROR")),
                JsonChecks::optionalField("errors", JsonChecks::forAll(JsonChecks::stringValue())),
                JsonChecks::optionalField("data", JsonChecks::objectValue())
            }
        )
    );

    responseDataChecks.insert("data", new DataResponseDataChecks());
}

CommandResponseHandler::~CommandResponseHandler() {
    qDeleteAll(responseDataChecks);
}

void CommandResponseHandler::addCommand(const QString& scope, const QString& name, const QString& nonce, const std::function<void(const QJsonObject&)>& callback) {
    qDebug() << "add command: " << nonce;

    commands.insert(
        nonce,
        new Command(
            {
                .scope = scope,
                .name = name,
                .callback = callback
            }
        )
    );
}

QString CommandResponseHandler::getName() {
    return "commandResponse";
}

void CommandResponseHandler::handle(const QJsonValue& messageData) {
    QJsonObject responseObject = messageData.toObject();
    QString nonce = responseObject.value("nonce").toString();

    qDebug() << "nonce: " << nonce;

    if (!commands.contains(nonce)) {
        return;
    }

    QString result = responseObject.value("result").toString();
    if (result != "OK") {
        return; //Ignore failed commands for now
    }

    Command* command = commands.value(nonce);

    const QJsonObject& responseDataObject = responseObject.value("data").toObject();

    if (responseDataChecks.contains(command->scope)) {
        auto check = responseDataChecks.value(command->scope)->get(command->name);

        if (!check || !ClientUtils::validate(responseDataObject, "/data/data", check)) {
            return;
        }
    }

    commands.remove(nonce);
    command->callback(responseDataObject);

    delete command;
}
