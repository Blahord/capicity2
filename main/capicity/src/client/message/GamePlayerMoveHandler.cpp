#include "GamePlayerMoveHandler.h"

#include "json/JsonChecks.h"

GamePlayerMoveHandler::GamePlayerMoveHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            {
                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                JsonChecks::requiredField("from", JsonChecks::integerValue()),
                JsonChecks::requiredField("to", JsonChecks::integerValue()),
                JsonChecks::requiredField("mode", JsonChecks::regexp("FORWARD|BACKWARD"))
            }
        )
    );
}

GamePlayerMoveHandler::~GamePlayerMoveHandler() = default;

QString GamePlayerMoveHandler::getName() {
    return "gamePlayerMove";
}

void GamePlayerMoveHandler::handle(const QJsonValue& messageData) {
    QJsonObject gamePlayerMoveDataObject = messageData.toObject();
    QString id = gamePlayerMoveDataObject.value("id").toString();
    int from = gamePlayerMoveDataObject.value("from").toInt(0);
    int to = gamePlayerMoveDataObject.value("to").toInt(0);
    bool forward = gamePlayerMoveDataObject.value("mode").toString() == "FORWARD";

    if (!getClientManager()->getOwnGame()) {
        return;
    }

    auto gamePlayer = getClientManager()->getOwnGame()->findPlayer(id);
    if (!gamePlayer) {
        return;
    }

    if (settings.isAnimationEnabled()) {
        emit(gamePlayerMovement(gamePlayer, from, to, forward));
    } else {
        gamePlayer->setPosition(to);
        getServerConnection()->sendCommand("game", "moveComplete", {});
        emit(gameUpdate(getClientManager()->findGame(gamePlayer->getPlayer())));
    }
}
