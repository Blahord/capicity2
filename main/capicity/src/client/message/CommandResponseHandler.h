#ifndef CAPICITY2_COMMANDRESPONSEHANDLER_H
#define CAPICITY2_COMMANDRESPONSEHANDLER_H

#include <QMap>

#include "MessageHandler.h"
#include "response/ResponseDataChecks.h"

class CommandResponseHandler : public MessageHandler {

    public:
        explicit CommandResponseHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~CommandResponseHandler() override;

        void addCommand(const QString& scope, const QString& name, const QString& nonce, const std::function<void(const QJsonObject&)>& callback);

        QString getName() override;
        void handle(const QJsonValue& messageData) override;

    private:
        struct Command {
            QString scope;
            QString name;
            std::function<void(const QJsonObject&)> callback;
        } __attribute__((aligned(64)));

        QMap<QString, Command*> commands;

        QMap<QString, ResponseDataChecks*> responseDataChecks;
};


#endif //CAPICITY2_COMMANDRESPONSEHANDLER_H
