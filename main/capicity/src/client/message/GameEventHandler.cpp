#include "GameEventHandler.h"

#include "json/JsonChecks.h"
#include "../ClientUtils.h"

GameEventHandler::GameEventHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            {
                JsonChecks::requiredField("type", JsonChecks::stringValue()),
                JsonChecks::requiredField("data")
            }
        )
    );
}

GameEventHandler::~GameEventHandler() = default;

QString GameEventHandler::getName() {
    return "gameEvent";
}

void GameEventHandler::handle(const QJsonValue& messageData) {
    QJsonObject eventObject = messageData.toObject();
    QString type = eventObject.value("type").toString();
    QJsonValue data = eventObject.value("data");

    auto eventDataCheck = gameEventChecks.get(type);

    if (eventDataCheck && !ClientUtils::validate(data, "", eventDataCheck)) {
        return;
    }

    emit(gameEvent(type, data));
}
