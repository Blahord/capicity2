#ifndef CAPICITY2_GAMEPLAYERUPDATEHANDLER_H
#define CAPICITY2_GAMEPLAYERUPDATEHANDLER_H

#include "MessageHandler.h"

class GamePlayerUpdateHandler : public MessageHandler {

    public:
        explicit GamePlayerUpdateHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~GamePlayerUpdateHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_GAMEPLAYERUPDATEHANDLER_H
