#include "GameTemplateListHandler.h"

#include "json/JsonChecks.h"

GameTemplateListHandler::GameTemplateListHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField(
                "list",
                JsonChecks::arrayValue(
                    JsonChecks::forAll(
                        JsonChecks::objectValue(
                            {
                                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                                JsonChecks::requiredField("name", JsonChecks::stringValue()),
                                JsonChecks::requiredField("languages", JsonChecks::arrayValue(JsonChecks::forAll(JsonChecks::stringValue()))),
                                JsonChecks::requiredField("defaultLanguage", JsonChecks::stringValue()),
                                JsonChecks::requiredField("description", JsonChecks::stringValue()),
                                JsonChecks::requiredField("minPlayers", JsonChecks::integerValue()),
                                JsonChecks::requiredField("maxPlayers", JsonChecks::integerValue())
                            }
                        )
                    )
                )
            )
        )
    );
}

GameTemplateListHandler::~GameTemplateListHandler() = default;

QString GameTemplateListHandler::getName() {
    return "gameTemplateList";
}

void GameTemplateListHandler::handle(const QJsonValue& messageData) {
    QJsonArray const gameTemplatesArray = messageData.toObject().value("list").toArray();
    QList<std::shared_ptr<CapiCityGameTemplate>> gameTemplatesFromServer;
    for (QJsonValue const gameTemplateValue : gameTemplatesArray) {
        QJsonObject const gameTemplateObject = gameTemplateValue.toObject();
        QString const id = gameTemplateObject.value("id").toString();
        QString const name = gameTemplateObject.value("name").toString();
        QString const description = gameTemplateObject.value("description").toString();
        QJsonArray const languagesArray = gameTemplateObject.value("languages").toArray();
        QString const defaultLanguage = gameTemplateObject.value("defaultLanguage").toString();
        int const minPlayersValue = gameTemplateObject.value("minPlayers").toInt(2);
        int const maxPlayersValue = gameTemplateObject.value("maxPlayers").toInt(2);

        QList<QString> languages;
        for (QJsonValue const languageValue : languagesArray) {
            languages.append(languageValue.toString());
        }

        auto gameTemplate = getClientManager()->findOrCreateGameTemplate(id);
        gameTemplate->setName(name);
        gameTemplate->setDescription(description);
        gameTemplate->setLanguages(languages);
        gameTemplate->setDefaultLanguage(defaultLanguage);
        gameTemplate->setMinPlayers(minPlayersValue);
        gameTemplate->setMaxPlayers(maxPlayersValue);

        gameTemplatesFromServer.append(gameTemplate);
    }

    for (const auto& gameTemplate : QList(getClientManager()->getGameTemplates())) {
        if (!gameTemplatesFromServer.contains(gameTemplate)) {
            getClientManager()->removeGameTemplate(gameTemplate);
        }
    }

    emit(newGameTemplateList());
}
