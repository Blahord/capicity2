#ifndef CAPICITY2_PLAYERUPDATEHANDLER_H
#define CAPICITY2_PLAYERUPDATEHANDLER_H

#include "MessageHandler.h"

class PlayerUpdateHandler : public MessageHandler {

    public:
        explicit PlayerUpdateHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~PlayerUpdateHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};

#endif //CAPICITY2_PLAYERUPDATEHANDLER_H
