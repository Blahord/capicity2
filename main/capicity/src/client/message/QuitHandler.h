#ifndef CAPICITY2_QUITHANDLER_H
#define CAPICITY2_QUITHANDLER_H

#include "MessageHandler.h"

class QuitHandler : public MessageHandler {

    public:
        explicit QuitHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~QuitHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_QUITHANDLER_H
