#ifndef CAPICITY2_PLAYERLISTHANDLER_H
#define CAPICITY2_PLAYERLISTHANDLER_H

#include "MessageHandler.h"

class PlayerListHandler : public MessageHandler {

    public:
        explicit PlayerListHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~PlayerListHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_PLAYERLISTHANDLER_H
