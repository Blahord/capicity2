#ifndef CAPICITY2_GAMEPLAYERMOVEHANDLER_H
#define CAPICITY2_GAMEPLAYERMOVEHANDLER_H

#include "MessageHandler.h"

class GamePlayerMoveHandler : public MessageHandler {

    public:
        explicit GamePlayerMoveHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~GamePlayerMoveHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;

    private:
        CapiCitySettings settings;
};


#endif //CAPICITY2_GAMEPLAYERMOVEHANDLER_H
