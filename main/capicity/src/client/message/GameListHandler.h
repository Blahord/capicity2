#ifndef CAPICITY2_GAMELISTHANDLER_H
#define CAPICITY2_GAMELISTHANDLER_H

#include "MessageHandler.h"

class GameListHandler : public MessageHandler {

    public:
        explicit GameListHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~GameListHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_GAMELISTHANDLER_H
