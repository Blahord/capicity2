#ifndef CAPICITY2_GAMETEMPLATELISTHANDLER_H
#define CAPICITY2_GAMETEMPLATELISTHANDLER_H

#include "MessageHandler.h"

class GameTemplateListHandler : public MessageHandler {

    public:
        explicit GameTemplateListHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~GameTemplateListHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_GAMETEMPLATELISTHANDLER_H
