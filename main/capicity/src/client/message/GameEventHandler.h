#ifndef CAPICITY2_GAMEEVENTHANDLER_H
#define CAPICITY2_GAMEEVENTHANDLER_H

#include "MessageHandler.h"
#include "GameEventChecks.h"

class GameEventHandler : public MessageHandler {

    public:
        explicit GameEventHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~GameEventHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;

    private:
        GameEventChecks gameEventChecks;
};


#endif //CAPICITY2_GAMEEVENTHANDLER_H
