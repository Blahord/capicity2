#include "ChatHandler.h"

#include "json/JsonChecks.h"

ChatHandler::ChatHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            {
                JsonChecks::requiredField("message", JsonChecks::stringValue()),
                JsonChecks::requiredField("sender", JsonChecks::stringValue()),
                JsonChecks::optionalField("target", JsonChecks::stringValue())
            }
        )
    );
}

ChatHandler::~ChatHandler() = default;

QString ChatHandler::getName() {
    return "chat";
}

void ChatHandler::handle(const QJsonValue& messageData) {
    QJsonObject messageDataObject = messageData.toObject();
    QString message = messageDataObject.value("message").toString();
    QString senderId = messageDataObject.value("sender").toString();

    auto sender = getClientManager()->findPlayer(senderId);
    if (sender == nullptr) {
        return;
    }

    emit(newChatMessage(sender, message));
}
