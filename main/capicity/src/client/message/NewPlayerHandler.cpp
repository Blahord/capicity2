#include "NewPlayerHandler.h"

#include "json/JsonChecks.h"

NewPlayerHandler::NewPlayerHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("id", JsonChecks::stringValue()),
            JsonChecks::requiredField("name", JsonChecks::stringValue()),
            JsonChecks::requiredField("avatar", JsonChecks::stringValue())
        )
    );
}

NewPlayerHandler::~NewPlayerHandler() = default;

QString NewPlayerHandler::getName() {
    return "newPlayer";
}

void NewPlayerHandler::handle(const QJsonValue& messageData) {
    QJsonObject playerDataObject = messageData.toObject();
    QString id = playerDataObject.value("id").toString();
    QString name = playerDataObject.value("name").toString();
    QJsonValue avatarValue = playerDataObject.value("avatar");

    if (getClientManager()->findPlayer(id)) {
        return;
    }

    auto newPlayer = getClientManager()->createNewPlayer(id);
    newPlayer->setName(name);

    if (!avatarValue.isUndefined()) {
        const QString& avatarHash = avatarValue.toString();
        newPlayer->setAvatarData(avatarHash);
        emit(imageHash(avatarHash));
    }

    emit(newPlayerList());
}
