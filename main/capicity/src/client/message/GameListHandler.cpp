#include "GameListHandler.h"

#include "json/JsonChecks.h"

GameListHandler::GameListHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField(
                "list",
                JsonChecks::arrayValue(
                    JsonChecks::forAll(
                        JsonChecks::objectValue(
                            {
                                JsonChecks::requiredField("id", JsonChecks::stringValue()),
                                JsonChecks::requiredField("type", JsonChecks::stringValue()),
                                JsonChecks::requiredField("owner", JsonChecks::stringValue()),
                                JsonChecks::requiredField("players", JsonChecks::arrayValue(JsonChecks::forAll(JsonChecks::stringValue()))),
                                JsonChecks::optionalField("state", JsonChecks::regexp("CONFIG|RUN|END"))
                            }
                        )
                    )
                )
            )
        )
    );
}

GameListHandler::~GameListHandler() = default;

QString GameListHandler::getName() {
    return "gameList";
}

void GameListHandler::handle(const QJsonValue& messageData) {
    auto oldCurrentGame = getClientManager()->getOwnGame();

    QList<std::shared_ptr<CapiCityGame>> gamesFromServer;

    QJsonArray const gamesArray = messageData.toObject().value("list").toArray();
    for (auto gameValue : gamesArray) {
        QJsonObject const gameObject = gameValue.toObject();

        QString const id = gameObject.value("id").toString();
        QString const type = gameObject.value("type").toString();
        QString const ownerId = gameObject.value("owner").toString();
        QJsonArray const playersList = gameObject.value("players").toArray();
        QString const state = gameObject.value("state").toString();

        auto gameTemplate = getClientManager()->findGameTemplate(type);
        auto owner = getClientManager()->findPlayer(ownerId);

        if (!gameTemplate || !owner) {
            continue;
        }

        auto game = getClientManager()->findOrCreateGame(id, gameTemplate, owner);
        if (state == "CONFIG") {
            game->setState(GameState::CONFIG);
        } else if (state == "RUN") {
            game->setState(GameState::RUN);
        } else if (state == "END") {
            game->setState(GameState::END);
        }

        updateGamePlayersList(game, playersList);

        gamesFromServer.append(game);
    }

    for (const auto& game : QList(getClientManager()->getGames())) {
        if (!gamesFromServer.contains(game)) {
            getClientManager()->removeGame(game);
        }
    }

    auto newCurrentGame = getClientManager()->getOwnGame();

    emit(newGameList());
    if (!oldCurrentGame && newCurrentGame) {
        emit(gameEntered());
    } else if (oldCurrentGame && !newCurrentGame) {
        emit(gameLeft());
    }
}
