#ifndef CAPICITY2_NEWGAMEHANDLER_H
#define CAPICITY2_NEWGAMEHANDLER_H

#include "MessageHandler.h"

class NewGameHandler : public MessageHandler {

    public:
        explicit NewGameHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~NewGameHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_NEWGAMEHANDLER_H
