#ifndef CAPICITY2_NEWPLAYERHANDLER_H
#define CAPICITY2_NEWPLAYERHANDLER_H

#include "MessageHandler.h"

class NewPlayerHandler : public MessageHandler {

    public:
        explicit NewPlayerHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~NewPlayerHandler() override;

        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_NEWPLAYERHANDLER_H
