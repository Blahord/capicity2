#ifndef CAPICITY2_GAMEDATAHANDLER_H
#define CAPICITY2_GAMEDATAHANDLER_H

#include "MessageHandler.h"

class GameDataHandler : public MessageHandler {

    public:
        explicit GameDataHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~GameDataHandler();


        QString getName() override;
        void handle(const QJsonValue& messageData) override;
};


#endif //CAPICITY2_GAMEDATAHANDLER_H
