#ifndef CAPICITY2_DATARESPONSEDATACHECKS_H
#define CAPICITY2_DATARESPONSEDATACHECKS_H

#include "ResponseDataChecks.h"

class DataResponseDataChecks : public ResponseDataChecks {

    public:
        explicit DataResponseDataChecks();
        ~DataResponseDataChecks() override;

};


#endif //CAPICITY2_DATARESPONSEDATACHECKS_H
