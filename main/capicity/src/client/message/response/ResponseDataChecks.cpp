#include "ResponseDataChecks.h"

ResponseDataChecks::ResponseDataChecks() = default;

ResponseDataChecks::~ResponseDataChecks() = default;

std::shared_ptr<JsonValueCheck> ResponseDataChecks::get(const QString& name) {
    return checks.value(name, nullptr);
}

void ResponseDataChecks::addCheck(const QString& name, const std::shared_ptr<JsonValueCheck>& check) {
    checks.insert(name, check);
}
