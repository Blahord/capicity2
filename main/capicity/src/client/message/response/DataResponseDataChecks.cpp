#include "DataResponseDataChecks.h"

#include "json/JsonChecks.h"

DataResponseDataChecks::DataResponseDataChecks() {
    addCheck(
        "ping",
        JsonChecks::objectValue(
            JsonChecks::requiredField("pong", JsonChecks::stringValue())
        )
    );

    addCheck(
        "getImage",
        JsonChecks::objectValue(
            JsonChecks::requiredField("found", JsonChecks::booleanValue()),
            JsonChecks::optionalField("image", JsonChecks::imageValue())
        )
    );
}

DataResponseDataChecks::~DataResponseDataChecks() = default;
