#ifndef CAPICITY2_RESPONSEDATACHECKS_H
#define CAPICITY2_RESPONSEDATACHECKS_H

#include <QString>
#include <QMap>

#include "json/checks/JsonValueCheck.h"

class ResponseDataChecks {

    public:
        explicit ResponseDataChecks();
        virtual ~ResponseDataChecks();

        std::shared_ptr<JsonValueCheck> get(const QString& name);

    protected:
        void addCheck(const QString& name, const std::shared_ptr<JsonValueCheck>& check);

    private:
        QMap<QString, std::shared_ptr<JsonValueCheck>> checks;

};


#endif //CAPICITY2_RESPONSEDATACHECKS_H
