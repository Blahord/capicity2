#include "EstateUpdateHandler.h"

#include "json/JsonChecks.h"

EstateUpdateHandler::EstateUpdateHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("id", JsonChecks::stringValue()),
            JsonChecks::optionalField("owner", JsonChecks::stringValue()),
            JsonChecks::optionalField("money", JsonChecks::integerValue())
        )
    );
}

EstateUpdateHandler::~EstateUpdateHandler() = default;

QString EstateUpdateHandler::getName() {
    return "estateUpdate";
}

void EstateUpdateHandler::handle(const QJsonValue& messageData) {
    auto dataObj = messageData.toObject();
    QString const id = dataObj.value("id").toString();

    auto game = getClientManager()->getOwnGame();
    if (!game) {
        return;
    }

    auto estate = game->findEstate(id);
    if (!estate) {
        return;
    }

    if (dataObj.contains("owner")) {
        QString const ownerId = dataObj.value("owner").toString();
        if (ownerId == "") {
            estate->setOwner(nullptr);
        } else {
            estate->setOwner(game->findPlayer(ownerId));
        }
    }

    if (dataObj.contains("money")) {
        estate->setMoney(dataObj.value("money").toInt(0));
    }

    emit(gameUpdate(game));
}
