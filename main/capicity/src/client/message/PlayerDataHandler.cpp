#include "PlayerDataHandler.h"

#include "json/JsonChecks.h"
#include "../ClientUtils.h"

PlayerDataHandler::PlayerDataHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection) : MessageHandler(clientManager, serverConnection) {
    setValidation(
        JsonChecks::objectValue(
            JsonChecks::requiredField("id", JsonChecks::stringValue()),
            JsonChecks::requiredField("name", JsonChecks::stringValue()),
            JsonChecks::requiredField("token", JsonChecks::stringValue())
        )
    );
}

PlayerDataHandler::~PlayerDataHandler() = default;

QString PlayerDataHandler::getName() {
    return "playerData";
}

void PlayerDataHandler::handle(const QJsonValue& messageData) {
    QJsonObject playerData = messageData.toObject();

    QString playerId = playerData.value("id").toString();
    QString playerName = playerData.value("name").toString();
    QString token = playerData.value("token").toString();

    settings.setReconnectToken(token);

    auto knownOwnPlayer = getClientManager()->findPlayer(playerId);
    if (knownOwnPlayer != nullptr) {
        getClientManager()->setOwnPlayer(knownOwnPlayer);
    } else {
        getClientManager()->setOwnPlayer(getClientManager()->createNewPlayer(playerId));
        getClientManager()->getOwnPlayer()->setName(playerName);
    }

    emit(connected());

    if (settings.isUseAvatar()) {
        ClientUtils::setOwnAvatar(getServerConnection());
    }
}
