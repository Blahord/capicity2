#ifndef CAPICITY2_MESSAGEHANDLER_H
#define CAPICITY2_MESSAGEHANDLER_H

#include <QObject>
#include <QJsonArray>

#include "CapiPlayer.h"
#include "json/checks/JsonValueCheck.h"
#include "../game/CapiCityGame.h"
#include "../ClientManager.h"
#include "../ServerConnection.h"
#include "../../CapiCitySettings.h"

class MessageHandler : public QObject {
    Q_OBJECT // NOLINT(altera-struct-pack-align)

    public:
        explicit MessageHandler(const std::shared_ptr<ClientManager>& clientManager, ServerConnection* serverConnection);
        ~MessageHandler() override;

        virtual QString getName() = 0;
        std::shared_ptr<JsonValueCheck> getValidation();
        virtual void handle(const QJsonValue& messageData) = 0;

    protected:
        void setValidation(const std::shared_ptr<JsonValueCheck>& check);

        std::shared_ptr<ClientManager> getClientManager();
        ServerConnection* getServerConnection();

        void updateGamePlayersList(const std::shared_ptr<CapiCityGame>& game, const QJsonArray& playerList);

    signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
        void connected();
        void imageHash(const QString& hash);
        void newPlayerList();
        void newGameTemplateList();
        void newGameList();
        void newChatMessage(const std::shared_ptr<CapiPlayer>& sender, const QString& message);
        void newGameData();
        void gameEntered();
        void gameLeft();
        void gameUpdate(const std::shared_ptr<CapiCityGame>& game);
        void playerUpdated(const std::shared_ptr<CapiPlayer>& player);
        void gamePlayerUpdate(const std::shared_ptr<CapiGamePlayer>& gamePlayer);
        void gamePlayerMovement(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int from, int to, bool forward);
        void gameEvent(const QString& type, const QJsonValue& data);

        void playerConnectionLost(const std::shared_ptr<CapiPlayer>& player);
        void playerReconnected(const std::shared_ptr<CapiPlayer>& player);
#pragma clang diagnostic pop

    private:
        std::shared_ptr<ClientManager> clientManager;
        ServerConnection* serverConnection;

        std::shared_ptr<JsonValueCheck> validation;
};


#endif //CAPICITY2_MESSAGEHANDLER_H
