#ifndef CAPICITY2_CLIENTUTILS_H
#define CAPICITY2_CLIENTUTILS_H

#include <QJsonValue>

#include "json/checks/JsonValueCheck.h"
#include "ServerConnection.h"

class ClientUtils {

    public:
        static bool validate(const QJsonValue& value, const QString& path, const std::shared_ptr<JsonValueCheck>& check);

        static QString createNonce();

        static void setOwnAvatar(ServerConnection* serverConnection);

    private:
        static QJsonObject getAvatarData();
};


#endif //CAPICITY2_CLIENTUTILS_H
