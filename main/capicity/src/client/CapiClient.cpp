#include "CapiClient.h"

#include "constants.h"
#include "json/JsonError.h"
#include "message/PlayerListHandler.h"
#include "message/GameTemplateListHandler.h"
#include "message/GameListHandler.h"
#include "message/NewPlayerHandler.h"
#include "message/NewGameHandler.h"
#include "message/PlayerUpdateHandler.h"
#include "message/GameUpdateHandler.h"
#include "message/QuitHandler.h"
#include "message/GameDeleteHandler.h"
#include "message/GameDataHandler.h"
#include "message/GamePlayerUpdateHandler.h"
#include "message/GamePlayerMoveHandler.h"
#include "message/ChatHandler.h"
#include "message/GameEventHandler.h"
#include "message/PlayerDataHandler.h"
#include "ClientUtils.h"
#include "message/EstateUpdateHandler.h"

CapiClient::CapiClient(QObject* parent) : QObject(parent) {
    this->clientManager = std::make_shared<ClientManager>();

    this->connection = new QTcpSocket();
    this->capiConnection = new CapiConnection(connection, this);
    this->serverConnection = new ServerConnection(capiConnection);

    this->commandResponseHandler = new CommandResponseHandler(clientManager, serverConnection);

    addMessageHandler(new ChatHandler(clientManager, serverConnection));
    addMessageHandler(commandResponseHandler);
    addMessageHandler(new GameDataHandler(clientManager, serverConnection));
    addMessageHandler(new GameDeleteHandler(clientManager, serverConnection));
    addMessageHandler(new GameEventHandler(clientManager, serverConnection));
    addMessageHandler(new GameListHandler(clientManager, serverConnection));
    addMessageHandler(new GamePlayerMoveHandler(clientManager, serverConnection));
    addMessageHandler(new GamePlayerUpdateHandler(clientManager, serverConnection));
    addMessageHandler(new GameTemplateListHandler(clientManager, serverConnection));
    addMessageHandler(new GameUpdateHandler(clientManager, serverConnection));
    addMessageHandler(new EstateUpdateHandler(clientManager, serverConnection));
    addMessageHandler(new NewGameHandler(clientManager, serverConnection));
    addMessageHandler(new NewPlayerHandler(clientManager, serverConnection));
    addMessageHandler(new PlayerDataHandler(clientManager, serverConnection));
    addMessageHandler(new PlayerListHandler(clientManager, serverConnection));
    addMessageHandler(new PlayerUpdateHandler(clientManager, serverConnection));
    addMessageHandler(new QuitHandler(clientManager, serverConnection));

    messageBaseCheck = JsonChecks::objectValue(
        JsonChecks::requiredField("type", JsonChecks::stringValue()),
        JsonChecks::requiredField("data", JsonChecks::objectValue())
    );

    connect(capiConnection, &CapiConnection::gotMessage, this, &CapiClient::handleMessage);
}

CapiClient::~CapiClient() {
    delete serverConnection;
    delete capiConnection;

    qDeleteAll(messageHandlers);
}

void CapiClient::connectToServer(const QString& host, quint16 port) {
    connection->connectToHost(host, port);
    connection->waitForConnected(2000);

    if (connection->state() == QAbstractSocket::ConnectedState) {
        settings.setLastServer(host);
        settings.setLastServerPort(port);
        login();
    }
}

void CapiClient::reconnectToLastServer() {
    QString lastServer = settings.getLastServer();
    uint lastPort = settings.getLastServerPort();
    QString token = settings.getPlayerToken();

    connection->connectToHost(lastServer, quint16(lastPort));
    connection->waitForConnected(2000);

    if (connection->state() == QAbstractSocket::ConnectedState) {
        reconnect(token);
    }
}

void CapiClient::sendCommand(const QString& scope, const QString& command, const QJsonObject& arguments) {
    serverConnection->sendCommand(scope, command, arguments);
}

void CapiClient::sendRequest(const QString& type, const QJsonObject& arguments, const std::function<void(const QJsonObject&)>& callback) {
    QString nonce = ClientUtils::createNonce();

    commandResponseHandler->addCommand(
        "data", type,
        nonce,
        callback
    );

    serverConnection->sendCommand(nonce, "data", type, arguments);
}

void CapiClient::sendSetName() {
    sendCommand(
        "server", "setName",
        {
            {"name", settings.getPlayerName()}
        }
    );
}

void CapiClient::sendSetAvatar() {
    ClientUtils::setOwnAvatar(serverConnection);
}

void CapiClient::sendGetImageRequest(const QString& hash) {
    sendRequest(
        "getImage",
        {{"hash", hash}},
        [this](const QJsonObject& data) {
            bool found = data.value("found").toBool();
            if (found) {
                this->storeImage(data.value("image"));
            }
        }
    );
}

std::shared_ptr<ClientManager> CapiClient::getClientManager() {
    return clientManager;
}

void CapiClient::addMessageHandler(MessageHandler* messageHandler) {
    messageHandlers.insert(messageHandler->getName(), messageHandler);

    connect(messageHandler, &MessageHandler::connected, this, &CapiClient::connected);
    connect(messageHandler, &MessageHandler::imageHash, this, &CapiClient::loadImage);
    connect(messageHandler, &MessageHandler::newPlayerList, this, &CapiClient::newPlayerList);
    connect(messageHandler, &MessageHandler::newGameTemplateList, this, &CapiClient::newGameTemplateList);
    connect(messageHandler, &MessageHandler::newGameList, this, &CapiClient::newGameList);
    connect(messageHandler, &MessageHandler::gameEntered, this, &CapiClient::gameEntered);
    connect(messageHandler, &MessageHandler::gameLeft, this, &CapiClient::gameLeft);
    connect(messageHandler, &MessageHandler::playerUpdated, this, &CapiClient::playerUpdated);
    connect(messageHandler, &MessageHandler::gameUpdate, this, &CapiClient::gameUpdate);
    connect(messageHandler, &MessageHandler::gamePlayerUpdate, this, &CapiClient::gamePlayerUpdate);
    connect(messageHandler, &MessageHandler::playerConnectionLost, this, &CapiClient::playerConnectionLost);
    connect(messageHandler, &MessageHandler::playerReconnected, this, &CapiClient::playerReconnected);
    connect(messageHandler, &MessageHandler::newGameData, this, &CapiClient::newGameData);
    connect(messageHandler, &MessageHandler::newChatMessage, this, &CapiClient::newChatMessage);
    connect(messageHandler, &MessageHandler::gameEvent, this, &CapiClient::gameEvent);
    connect(messageHandler, &MessageHandler::gamePlayerMovement, this, &CapiClient::gamePlayerMovement);
}

void CapiClient::loadImage(const QString& hash) {
    if (hash.isEmpty() || imageStorage.containsImage(hash)) {
        return;
    }

    sendGetImageRequest(hash);
}

void CapiClient::login() {
    QString ownName = settings.getPlayerName();

    sendCommand(
        "server", "login",
        {{"name", ownName}}
    );
}

void CapiClient::reconnect(const QString& token) {
    QJsonObject reconnectArguments;
    reconnectArguments.insert("token", token);

    sendCommand("server", "reconnect", reconnectArguments);
}

void CapiClient::handleMessage(const QJsonObject& message) {
    if (!ClientUtils::validate(message, "", messageBaseCheck)) {
        return;
    }

    QString type = message.value("type").toString();
    QJsonValue data = message.value("data");

    auto handler = messageHandlers.value(type);
    if (!handler) {
        return;
    }

    if (!ClientUtils::validate(data, "/data", handler->getValidation())) {
        return;
    }

    handler->handle(data);
}

void CapiClient::storeImage(const QJsonValue& image) {
    auto imageObject = image.toObject();

    int width = imageObject.value("width").toInt();
    int height = imageObject.value("height").toInt();
    QString data = imageObject.value("data").toString();

    QImage tmp(width, height, QImage::Format_ARGB32);
    for (int pos = 0; pos < width * height; pos++) {
        QString colorStr = data.mid(8 * pos, 8);
        uint color = colorStr.toUInt(nullptr, 16);
        tmp.setPixel(pos % width, pos / width, color);
    }

    QString hash = imageStorage.storeImage(tmp);
    emit(newImageData(hash));
}
