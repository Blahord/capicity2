#ifndef CAPICITY2_SERVERCONNECTION_H
#define CAPICITY2_SERVERCONNECTION_H

#include <QObject>

#include "CapiConnection.h"

class ServerConnection : public QObject {
    Q_OBJECT // NOLINT(altera-struct-pack-align)

    public:
        explicit ServerConnection(CapiConnection* connection);
        ~ServerConnection() override;

        void sendCommand(const QString& scope, const QString& name, const QJsonObject& arguments);
        void sendCommand(const QString& nonce, const QString& scope, const QString& name, const QJsonObject& arguments);

    signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
        void newMessage();
#pragma clang diagnostic pop

    private:
        CapiConnection* connection;
};


#endif //CAPICITY2_SERVERCONNECTION_H
