#ifndef CAPICITY2_CAPICLIENT_H
#define CAPICITY2_CAPICLIENT_H

#include <QObject>
#include <QSettings>
#include <QTcpSocket>
#include "json/checks/JsonValueCheck.h"
#include "json/JsonChecks.h"
#include "CapiConnection.h"
#include "CapiPlayer.h"
#include "game/CapiCityGame.h"
#include "game/CapiCityGameTemplate.h"
#include "../ImageStorage.h"
#include "../CapiCitySettings.h"
#include "ClientManager.h"
#include "ServerConnection.h"
#include "message/CommandResponseHandler.h"

class CapiClient : public QObject {
    Q_OBJECT // NOLINT(altera-struct-pack-align)

    public:
        explicit CapiClient(QObject* parent = nullptr);
        ~CapiClient() override;

        void connectToServer(const QString& host, quint16 port);
        void reconnectToLastServer();

        void sendCommand(const QString& scope, const QString& command, const QJsonObject& arguments);
        void sendRequest(const QString& type, const QJsonObject& arguments, const std::function<void(const QJsonObject&)>& callback);

        void sendSetName();
        void sendSetAvatar();
        void sendGetImageRequest(const QString& hash);

        std::shared_ptr<ClientManager> getClientManager();

    signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
        void connected();
        void newPlayerList();
        void newGameTemplateList();
        void newGameList();
        void newChatMessage(const std::shared_ptr<CapiPlayer>& sender, const QString& message);
        void newGameData();
        void gameEntered();
        void gameLeft();
        void gameUpdate(const std::shared_ptr<CapiCityGame>& game);
        void playerUpdated(const std::shared_ptr<CapiPlayer>& player);
        void gamePlayerUpdate(const std::shared_ptr<CapiGamePlayer>& gamePlayer);
        void gamePlayerMovement(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int from, int to, bool forward);
        void newImageData(const QString& hash);
        void gameEvent(const QString& type, const QJsonValue& data);

        void playerConnectionLost(const std::shared_ptr<CapiPlayer>& player);
        void playerReconnected(const std::shared_ptr<CapiPlayer>& player);
#pragma clang diagnostic pop

    private:
        void addMessageHandler(MessageHandler* messageHandler);
        void loadImage(const QString& hash);

        void login();
        void reconnect(const QString& token);

        void handleMessage(const QJsonObject& message);

        void storeImage(const QJsonValue& image);

        CapiCitySettings settings;
        ImageStorage imageStorage;

        std::shared_ptr<ClientManager> clientManager;

        QTcpSocket* connection = nullptr;
        CapiConnection* capiConnection = nullptr;
        CommandResponseHandler* commandResponseHandler;
        QMap<QString, MessageHandler*> messageHandlers;
        std::shared_ptr<JsonValueCheck> messageBaseCheck;

        ServerConnection* serverConnection;
};

#endif //CAPICITY2_CAPICLIENT_H
