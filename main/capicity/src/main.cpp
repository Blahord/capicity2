#include <QApplication>
#include <QMessageBox>

#include "CapiCitySettings.h"
#include "client/CapiClient.h"
#include "gui/CapiCityWindow.h"

int main(int argc, char** args) {
    QApplication a(argc, args);

    QApplication::setApplicationName("Capi City 2");

    CapiClient capiClient(nullptr);
    CapiCityWindow mainWindow(&capiClient, nullptr);

    mainWindow.show();

    CapiCitySettings settings;
    if (settings.getPlayerToken() != 0) {
        QMessageBox::StandardButton result = QMessageBox::question(
            &mainWindow, QObject::tr("Reconnect?"),
            QObject::tr("Reconnect to server?")
        );

        if (result == QMessageBox::Yes) {
            mainWindow.reconnect();
        }
    }

    return QApplication::exec();
}