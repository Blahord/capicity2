#include "ImageStorage.h"

#include <QStandardPaths>
#include <QDir>
#include <QCryptographicHash>
#include <QPainter>

ImageStorage::ImageStorage() {
    imagePath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir(imagePath).mkpath("images");
    QDir(imagePath).mkpath("shadows");
}

ImageStorage::~ImageStorage() = default;

bool ImageStorage::containsImage(const QString& hash) {
    QFile imageFile(getFilePath(hash));
    return imageFile.exists();
}

bool ImageStorage::containsShadow(const QString& hash) {
    QFile imageFile(getShadowPath(hash));
    return imageFile.exists();
}

QPixmap ImageStorage::getImage(const QString& hash) {
    return {getFilePath(hash)};
}

QPixmap ImageStorage::getShadow(const QString& hash) {
    if (!containsShadow(hash)) {
        createShadow(hash);
    }

    return {getShadowPath(hash)};
}

QPixmap ImageStorage::createDefaultAvatar(const QString& playerName) {
    QColor bgColor = getDefaultAvatarBackgroundColor(playerName);

    QPixmap img(32, 32);
    img.fill(QColor(0, 0, 0, 0));

    QPainter p(&img);
    p.setBrush(bgColor);
    p.setPen(bgColor);

    p.drawEllipse(0, 0, 20, 20);
    p.drawEllipse(11, 0, 20, 20);
    p.drawEllipse(0, 11, 20, 20);
    p.drawEllipse(11, 11, 20, 20);

    p.fillRect(10, 0, 12, 32, bgColor);
    p.fillRect(0, 10, 32, 12, bgColor);

    QFont font = p.font();
    font.setPixelSize(28);

    p.setPen(Qt::white);
    p.setFont(font);

    p.drawText(0, 2, 32, 28, Qt::AlignVCenter | Qt::AlignHCenter, playerName.left(1));

    p.end();

    return img;
}

QString ImageStorage::storeImage(const QImage& image) {
    QString imageHash = hash(image);
    image.save(getFilePath(imageHash));
    createShadow(imageHash);

    return imageHash;
}

QPixmap ImageStorage::scalePixmap(const QPixmap& pixmap, int size) {
    QPixmap result(size, size);
    result.fill(QColor(0, 0, 0, 0));

    if (pixmap.width() > size || pixmap.height() > size) {
        QPixmap tmp = pixmap.scaled(size, size, Qt::KeepAspectRatio);
        QPainter p(&result);
        p.drawPixmap((size - tmp.width()) / 2, (size - tmp.height()) / 2, tmp);
        p.end();
        return result;
    }
    if (pixmap.width() < size && pixmap.height() < size) {
        QPixmap tmp = pixmap.scaled(size, size, Qt::KeepAspectRatioByExpanding);
        QPainter p(&result);
        p.drawPixmap((size - tmp.width()) / 2, (size - tmp.height()) / 2, tmp);
        p.end();
        return result;
    }

    return pixmap;
}

QString ImageStorage::getFilePath(const QString& hash) {
    return QString("%1/images/%2.png").arg(imagePath, hash);
}

QString ImageStorage::getShadowPath(const QString& hash) {
    return QString("%1/shadows/%2.png").arg(imagePath, hash);
}

QString ImageStorage::hash(const QImage& image) {
    QCryptographicHash hash(QCryptographicHash::Sha256);

    QString width = QString::number(image.width(), 16).leftJustified(4, '0');
    QString height = QString::number(image.height(), 16).leftJustified(4, '0');

    hash.addData(width.toUtf8().data(), 4);
    hash.addData(height.toUtf8().data(), 4);

    for (int y = 0; y < image.height(); y++) {
        for (int x = 0; x < image.width(); x++) {
            hash.addData(image.pixelColor(x, y).name(QColor::HexArgb).right(8).toUtf8().data(), 8);
        }
    }

    return hash.result().toHex();
}

void ImageStorage::createShadow(const QString& hash) {
    QImage image = getImage(hash).toImage();
    QImage alpha = QImage(image.width() + 8, image.height() + 8, QImage::Format_ARGB32);

    for (int x = -4; x < image.width() + 4; x++) {
        for (int y = -4; y < image.height() + 4; y++) {

            int alphaSum = 0;
            for (int sx = -4; sx <= 4; sx++) {
                for (int sy = -4; sy <= 4; sy++) {
                    if (sx * sx + sy * sy > 16) {
                        continue;
                    }

                    int ux = x + sx;
                    int uy = y + sy;
                    if (ux < 0 || ux >= image.width() ||
                        uy < 0 || uy >= image.height()) {
                        continue;
                    }
                    alphaSum += image.pixelColor(ux, uy).alpha();
                }
            }
            alpha.setPixelColor(x + 4, y + 4, QColor(0, 0, 0, alphaSum / 162));
        }
    }

    alpha.save(getShadowPath(hash));
}

QColor ImageStorage::getDefaultAvatarBackgroundColor(const QString& playerName) {
    QCryptographicHash hash(QCryptographicHash::Sha256);
    hash.addData(playerName.toUtf8());

    QByteArray result = hash.result();

    int h = ((result.at(0) + 128) * 256 + result.at(1) + 128) % 360;
    int s = ((result.at(2) + 128) % 128) + 128;
    int v = ((result.at(3) + 128) % 128) + 128;

    return QColor::fromHsv(h, s, v);
}
