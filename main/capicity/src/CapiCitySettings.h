#ifndef CAPICITY2_CAPICITYSETTINGS_H
#define CAPICITY2_CAPICITYSETTINGS_H

#include <QString>
#include <QPixmap>
#include <QStandardPaths>
#include <QSettings>

class CapiCitySettings {

    public:
        QString getPlayerName();
        bool isUseAvatar();
        QPixmap getAvatarPixmap();
        QImage getAvatarImage();

        bool isAnimationEnabled();
        int getAnimationSpeed();

        QString getLastServer();
        quint16 getLastServerPort();
        QString getPlayerToken();

        void setPlayerName(const QString& name);
        void setUseAvatar(bool avatarActive);
        void storeAvatar(const QPixmap& avatar);
        void setAnimationEnabled(bool value);
        void setAnimationSpeed(int value);

        void setLastServer(const QString& host);
        void setLastServerPort(quint16 port);
        void setReconnectToken(const QString& token);

    private:
        static QString KEY_PLAYER_NAME;
        static QString KEY_PLAYER_USE_AVATAR;
        static QString KEY_ANIMATION_ACTIVE;
        static QString KEY_ANIMATION_SPEED;

        static QString KEY_CLIENT_LAST_SERVER;
        static QString KEY_CLIENT_LAST_PORT;
        static QString KEY_CLIENT_RECONNECT_TOKEN;

        QString dataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        QSettings settings;
};


#endif //CAPICITY2_CAPICITYSETTINGS_H
