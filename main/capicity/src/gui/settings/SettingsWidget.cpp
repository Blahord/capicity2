#include "SettingsWidget.h"

#include <QVBoxLayout>
#include <QFormLayout>

SettingsWidget::SettingsWidget(CapiClient* client, QWidget* parent) : QWidget(parent) {
    this->client = client;
    this->nameInput = new QLineEdit(settings.getPlayerName(), this);
    this->avatarConfig = new AvatarConfig(this);
    this->animateCheckBox = new QCheckBox(tr("Animate"), this);
    this->animationSpeedSlider = new QSlider(Qt::Horizontal, this);
    animationSpeedSlider->setMinimum(0);
    animationSpeedSlider->setMaximum(3000);

    animateCheckBox->setChecked(settings.isAnimationEnabled());
    animationSpeedSlider->setValue(settings.getAnimationSpeed());
    animationSpeedSlider->setEnabled(animateCheckBox->isEnabled());

    auto formLayout = new QFormLayout();
    formLayout->addRow(tr("Name"), nameInput);
    formLayout->addRow(tr("Avatar"), avatarConfig);
    formLayout->addRow(newHLine());

    formLayout->addRow(tr("Player movement"), animateCheckBox);

    auto speedLayout = new QHBoxLayout();
    speedLayout->addWidget(new QLabel("Speed"));
    speedLayout->addWidget(animationSpeedSlider);
    formLayout->addRow("", speedLayout);

    setLayout(formLayout);

    setMinimumSize(300, 0);

    connect(animateCheckBox, &QCheckBox::clicked, animationSpeedSlider, &QSlider::setEnabled);
}

SettingsWidget::~SettingsWidget() = default;

void SettingsWidget::save() {
    bool avatarActive = avatarConfig->isAvatarActive();
    bool wasAvatarActive = settings.isUseAvatar();

    QString oldName = settings.getPlayerName();
    settings.setPlayerName(nameInput->text());
    settings.setUseAvatar(avatarActive);

    if (avatarActive) {
        QPixmap avatar;

        if (avatarConfig->getSelectedAvatar().isNull()) {
            avatar = settings.getAvatarPixmap();
        } else {
            avatar = QPixmap(avatarConfig->getSelectedAvatar());
        }

        settings.storeAvatar(avatar);
    }

    if (client->getClientManager()->getOwnPlayer()) {
        if ((wasAvatarActive != avatarActive) || !avatarConfig->getSelectedAvatar().isNull()) {
            client->sendSetAvatar();
        }

        if (oldName != settings.getPlayerName()) {
            client->sendSetName();
        }
    }

    settings.setAnimationEnabled(animateCheckBox->isChecked());
    settings.setAnimationSpeed(animationSpeedSlider->value());
}

QWidget* SettingsWidget::newHLine() {
    auto line = new QFrame(this);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    return line;
}
