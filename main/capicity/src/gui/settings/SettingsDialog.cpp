#include "SettingsDialog.h"

#include <QVBoxLayout>

SettingsDialog::SettingsDialog(CapiClient* client, QWidget* parent) : QDialog(parent) {
    this->buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);
    this->settingsWidget = new SettingsWidget(client, this);

    auto settingsLayout = new QHBoxLayout();
    settingsLayout->addWidget(settingsWidget, 1, Qt::AlignHCenter);

    setLayout(new QVBoxLayout());
    layout()->addItem(settingsLayout);
    layout()->addWidget(buttons);

    setModal(true);
    setWindowTitle(tr("Settings"));

    connect(buttons, &QDialogButtonBox::accepted, this, &SettingsDialog::save);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

SettingsDialog::~SettingsDialog() = default;

void SettingsDialog::save() {
    settingsWidget->save();

    accept();
}
