#ifndef CAPICITY2_SETTINGSDIALOG_H
#define CAPICITY2_SETTINGSDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QLabel>

#include "AvatarConfig.h"
#include "SettingsWidget.h"
#include "../../client/CapiClient.h"

class SettingsDialog : public QDialog {

    public:
        explicit SettingsDialog(CapiClient* client, QWidget* parent = nullptr);
        ~SettingsDialog() override;

    private slots:
        void save();

    private:
        SettingsWidget* settingsWidget;
        QDialogButtonBox* buttons;
};


#endif //CAPICITY2_SETTINGSDIALOG_H
