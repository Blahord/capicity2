#ifndef CAPICITY2_AVATARCONFIG_H
#define CAPICITY2_AVATARCONFIG_H

#include <QWidget>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QSettings>

#include "../widgets/AvatarLabel.h"
#include "../../CapiCitySettings.h"

class AvatarConfig : public QWidget {

    public:
        explicit AvatarConfig(QWidget* parent = nullptr);
        ~AvatarConfig() override;

        bool isAvatarActive() const;
        QString getSelectedAvatar();

    private slots:
        void updateAvatarConfig(bool enabled);
        void selectAvatar();

    private:
        QPixmap getCurrentAvatar();

        CapiCitySettings settings;

        QCheckBox* activeCheckBox = nullptr;
        AvatarLabel* avatarLabel = nullptr;
        QPushButton* selectImageButton = nullptr;

        bool avatarActive = false;
        QString selectedAvatar;
};


#endif //CAPICITY2_AVATARCONFIG_H
