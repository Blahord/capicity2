#include "AvatarConfig.h"

#include <QVBoxLayout>
#include <QStandardPaths>
#include <QFileDialog>

AvatarConfig::AvatarConfig(QWidget* parent) : QWidget(parent) {
    this->activeCheckBox = new QCheckBox(tr("Use avatar"), this);
    this->avatarLabel = new AvatarLabel(this);
    this->selectImageButton = new QPushButton(QIcon::fromTheme("document-open"), tr("Select ..."), this);
    this->avatarActive = settings.isUseAvatar();

    activeCheckBox->setChecked(avatarActive);
    avatarLabel->setEnabled(avatarActive);
    avatarLabel->setAvatar(getCurrentAvatar());
    selectImageButton->setEnabled(avatarActive);

    auto avatarLayout = new QHBoxLayout();
    avatarLayout->addWidget(avatarLabel);
    avatarLayout->addWidget(selectImageButton);

    auto layout = new QVBoxLayout(this);
    layout->addWidget(activeCheckBox);
    layout->addLayout(avatarLayout);

    connect(activeCheckBox, &QCheckBox::clicked, this, &AvatarConfig::updateAvatarConfig);
    connect(selectImageButton, &QPushButton::clicked, this, &AvatarConfig::selectAvatar);
}

AvatarConfig::~AvatarConfig() = default;

bool AvatarConfig::isAvatarActive() const {
    return avatarActive;
}

QString AvatarConfig::getSelectedAvatar() {
    return selectedAvatar;
}

void AvatarConfig::updateAvatarConfig(bool enabled) {
    avatarActive = enabled;
    avatarLabel->setEnabled(enabled);
    selectImageButton->setEnabled(enabled);
}

void AvatarConfig::selectAvatar() {
    QString fileName = QFileDialog::getOpenFileName(
        this, tr("select image"),
        QStandardPaths::writableLocation(QStandardPaths::PicturesLocation),
        tr("Images (*.png *.xpm *.jpg)")
    );

    if (fileName.isNull()) {
        return;
    }

    selectedAvatar = fileName;

    avatarLabel->setAvatar(QPixmap(selectedAvatar));
}

QPixmap AvatarConfig::getCurrentAvatar() {
    return settings.getAvatarPixmap();
}
