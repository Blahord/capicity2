#ifndef CAPICITY2_SETTINGSWIDGET_H
#define CAPICITY2_SETTINGSWIDGET_H

#include <QDialogButtonBox>
#include <QLineEdit>
#include <QSlider>

#include "AvatarConfig.h"
#include "../../client/CapiClient.h"

class SettingsWidget : public QWidget {

    public:
        explicit SettingsWidget(CapiClient* client, QWidget* parent = nullptr);
        ~SettingsWidget() override;

        void save();

    private:
        QWidget* newHLine();

        CapiCitySettings settings;

        CapiClient* client;
        QLineEdit* nameInput;
        AvatarConfig* avatarConfig;
        QCheckBox* animateCheckBox;
        QSlider* animationSpeedSlider;
};


#endif //CAPICITY2_SETTINGSWIDGET_H
