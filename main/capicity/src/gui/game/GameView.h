#ifndef CAPICITY2_GAMEVIEW_H
#define CAPICITY2_GAMEVIEW_H

#include <QWidget>
#include <QTabWidget>
#include <QToolButton>

#include "GameRun.h"
#include "GameConfiguration.h"
#include "../../client/game/CapiCityGame.h"
#include "../../client/CapiClient.h"
#include "../../ImageStorage.h"

class GameView : public QWidget {
    Q_OBJECT

        Q_PROPERTY(int animVal MEMBER animationValue)

    public:
        explicit GameView(CapiClient* client, QWidget* parent = nullptr);
        ~GameView() override;

        void reset();

    protected:
        void paintEvent(QPaintEvent* event) override;

    private:
        struct RenderProperties {
            int countEstatesPerSide = 0;
            int renderOffsetX = 0;
            int renderOffsetY = 0;
            int dynamicWidth = 0;
            int dynamicHeight = 0;
            int textSize = 0;
            int iconSize = 0;
            QColor bgColor;
            QPainter* painter = nullptr;
        } __attribute__((aligned(64)));

        void createLayout();

        void leaveGame();

        void startMoveAnimation(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int from, int to, bool forward);
        void renderAnimationState(const QVariant& value);
        void endMoveAnimation();

        void rerenderGame(const std::shared_ptr<CapiCityGame>& game);
        void rerenderOwnGame();
        void rerenderGameImage(const QString& hash);

        void renderSouthEstate(const std::shared_ptr<CapiCityEstate>& estate, int sideNumber, const RenderProperties& renderProperties);
        void renderWestEstate(const std::shared_ptr<CapiCityEstate>& estate, int sideNumber, const RenderProperties& renderProperties);
        void renderNorthEstate(const std::shared_ptr<CapiCityEstate>& estate, int sideNumber, const RenderProperties& renderProperties);
        void renderEastEstate(const std::shared_ptr<CapiCityEstate>& estate, int sideNumber, const RenderProperties& renderProperties);
        void renderEstate(const std::shared_ptr<CapiCityEstate>& estate, QRect estateRect, QRect badgeRect, QRect textRect, const QColor& bgColor, QPainter* painter);

        void renderPlayerOnBoard(const std::shared_ptr<CapiGamePlayer>& gamePlayer, const RenderProperties& renderProperties);
        void renderSouthPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int sideNumber, int offset, const RenderProperties& renderProperties);
        void renderWestPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int sideNumber, int offset, const RenderProperties& renderProperties);
        void renderNorthPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int sideNumber, int offset, const RenderProperties& renderProperties);
        void renderEastPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int sideNumber, int offset, const RenderProperties& renderProperties);
        void renderPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int x, int y, int size, QPainter* painter);

        int estateSize = 100;
        int groupBadgeSize = 20;
        int textMargin = 2;

        //Animation properties

        int animationSteps = 100;

        int animationTo = 0; //We sill set the players position to this value, after animation is completed
        QString animationPlayer;
        int animationValue = 0;

        CapiClient* client = nullptr;
        QTabWidget* centerWidget = nullptr;
        GameConfiguration* gameConfiguration = nullptr;
        GameRun* gameRun = nullptr;
        QToolButton* leaveButton = nullptr;

        CapiCitySettings settings;
        ImageStorage imageStorage;
};


#endif //CAPICITY2_GAMEVIEW_H
