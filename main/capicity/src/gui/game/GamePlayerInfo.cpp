#include "GamePlayerInfo.h"

GamePlayerInfo::GamePlayerInfo(const std::shared_ptr<CapiGamePlayer>& player, CapiClient* client, QWidget* parent) : QFrame(parent) {
    this->client = client;
    this->ownPlayer = player;

    this->avatarLabel = new PlayerAvatarLabel(player->getPlayer(), client, this);
    this->playerNameLabel = new QLabel("", this);
    this->moneyLabel = new QLabel("", this);

    auto upperLayout = new QHBoxLayout();
    upperLayout->addWidget(playerNameLabel);
    upperLayout->addStretch(1);
    upperLayout->addWidget(moneyLabel);

    auto infoLayout = new QVBoxLayout();
    infoLayout->addItem(upperLayout);

    setLayout(new QHBoxLayout());
    layout()->addWidget(avatarLabel);
    layout()->addItem(infoLayout);

    setFrameShape(QFrame::StyledPanel);
    setFrameShadow(QFrame::Raised);

    updateGamePlayer(player);
    updatePlayer(player->getPlayer());
    updateGame(client->getClientManager()->getOwnGame());

    connect(client, &CapiClient::gamePlayerUpdate, this, &GamePlayerInfo::updateGamePlayer);
    connect(client, &CapiClient::playerUpdated, this, &GamePlayerInfo::updatePlayer);
    connect(client, &CapiClient::gameUpdate, this, &GamePlayerInfo::updateGame);
}

GamePlayerInfo::~GamePlayerInfo() = default;

void GamePlayerInfo::tick(long tickCount) {
    avatarLabel->tick(tickCount);
}

void GamePlayerInfo::updateGamePlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer) {
    if (ownPlayer != gamePlayer) {
        return;
    }

    if (gamePlayer->isInJail()) {
        avatarLabel->setOverlayImage(QPixmap(":avatars/jailed.png"));
    } else {
        avatarLabel->setOverlayImage(QPixmap());
    }

    moneyLabel->setText(QString::number(gamePlayer->getMoney()));
    updatePlayerNameStyle();
}

void GamePlayerInfo::updatePlayer(const std::shared_ptr<CapiPlayer>& player) {
    if (ownPlayer->getPlayer() != player) {
        return;
    }

    playerNameLabel->setText(player->getName());
}

void GamePlayerInfo::updateGame(const std::shared_ptr<CapiCityGame>& game) {
    if (game != client->getClientManager()->getOwnGame()) {
        return;
    }

    moneyLabel->setVisible(game->getState() == GameState::RUN);
    updatePlayerNameStyle();
}

void GamePlayerInfo::updatePlayerNameStyle() {
    QFont font = playerNameLabel->font();

    font.setBold(client->getClientManager()->getOwnGame()->getTurnPlayer() == ownPlayer);
    font.setItalic(client->getClientManager()->getOwnGame()->getState() == GameState::RUN && !ownPlayer->isMoved());

    playerNameLabel->setFont(font);
}
