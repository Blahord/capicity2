#include "GameRun.h"

#include <QVBoxLayout>

GameRun::GameRun(CapiClient* client, QWidget* parent) : QWidget(parent) {
    this->client = client;

    this->eventView = new QTextBrowser();
    this->rollButton = new QPushButton(tr("Roll"), this);
    this->buyButton = new QPushButton(tr("Buy"), this);
    this->skipBuyButton = new QPushButton(tr("Skip buy"), this);
    this->payAmountButton = new QPushButton("", this);
    this->payPercentButton = new QPushButton("", this);

    auto buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(rollButton);
    buttonsLayout->addWidget(buyButton);
    buttonsLayout->addWidget(payAmountButton);
    buttonsLayout->addWidget(payPercentButton);
    buttonsLayout->addWidget(skipBuyButton);

    setLayout(new QVBoxLayout());
    layout()->addWidget(eventView);
    layout()->addItem(buttonsLayout);

    connect(client, &CapiClient::gameEvent, this, &GameRun::addEvent);
    connect(client, &CapiClient::gameUpdate, this, &GameRun::updateButtonVisibilities);

    connect(rollButton, &QPushButton::clicked, this, [this]() { this->sendCommand("roll"); });
    connect(buyButton, &QPushButton::clicked, this, [this]() { this->sendCommand("buy"); });
    connect(payAmountButton, &QPushButton::clicked, this, [this]() { this->sendCommand("payTax", {{"mode", "amount"}}); });
    connect(payPercentButton, &QPushButton::clicked, this, [this]() { this->sendCommand("payTax", {{"mode", "percent"}}); });
    connect(skipBuyButton, &QPushButton::clicked, this, [this]() { this->sendCommand("skipBuy"); });
}

GameRun::~GameRun() = default;

void GameRun::reset() {
    eventView->clear();
}

void GameRun::addEvent(const QString& type, const QJsonValue& data) {
    auto const clientManager = client->getClientManager();

    auto const game = clientManager->getOwnGame();
    auto const player = game->getTurnPlayer();
    auto const estate = game->getEstates().at(player->getPosition());

    if (type == "started") {
        eventView->append("The game has started");
    } else if (type == "turn") {
        auto const gamePlayer = clientManager->getOwnGame()->findPlayer(data.toString());
        if (!gamePlayer) {
            return;
        }

        eventView->append(QString("%1 is on turn").arg(gamePlayer->getPlayer()->getName()));
    } else if (type == "roll") {
        QJsonObject const rollObject = data.toObject();
        auto const turnPlayer = clientManager->getOwnGame()->getTurnPlayer();
        if (!turnPlayer) {
            return;
        }

        eventView->append(
            QString("%1 rolls %2 and %3").arg(turnPlayer->getPlayer()->getName())
                .arg(rollObject.value("dice1").toInt())
                .arg(rollObject.value("dice2").toInt())
        );
    } else if (type == "land") {
        int const position = data.toInt();
        if (position < 0 || position >= clientManager->getOwnGame()->getEstates().size()) {
            return;
        }

        eventView->append(
            QString("%1 lands on %2")
                .arg(
                    clientManager->getOwnGame()->getTurnPlayer()->getPlayer()->getName(),
                    clientManager->getOwnGame()->getEstates().at(position)->getName()
                )
        );
    } else if (type == "canBuy") {
        eventView->append(
            QString("%1 can buy %2")
                .arg(
                    player->getPlayer()->getName(),
                    estate->getName()
                )
        );
    } else if (type == "taxPayNeeded") {
        auto const amount = data.toObject().value("amount").toInt(0);
        auto const percent = data.toObject().value("percent").toInt(0);

        if (amount <= 0) {
            eventView->append(
                QString("%1 has to pay %2 of his/her assets")
                    .arg(player->getPlayer()->getName())
                    .arg(percent)
            );
        } else if (percent <= 0) {
            eventView->append(
                QString("%1 has to pay %2")
                    .arg(player->getPlayer()->getName())
                    .arg(amount)
            );
        } else {
            eventView->append(
                QString("%1 has to pay %2 or %3 of his/her assets")
                    .arg(player->getPlayer()->getName())
                    .arg(amount)
                    .arg(percent)
            );
        }
    } else if (type == "taxPayed") {
        auto const amount = data.toInt(0);

        eventView->append(
            QString("%1 has payed %2 of taxes")
                .arg(player->getPlayer()->getName())
                .arg(amount)
        );
    } else if (type == "buy") {
        eventView->append(
            QString("%1 has bought %2")
                .arg(
                    player->getPlayer()->getName(),
                    estate->getName()
                )
        );
    } else if (type == "skipBuy") {
        eventView->append(
            QString("%1 has not bought %2")
                .arg(
                    player->getPlayer()->getName(),
                    estate->getName()
                )
        );
    } else if (type == "end") {
        QString const winnerId = data.toString();
        auto const winner = clientManager->getOwnGame()->findPlayer(winnerId);

        eventView->append(
            QString("The game has ended. %1 has won!")
                .arg(
                    winner->getPlayer()->getName()
                )
        );
    }
}

void GameRun::sendCommand(const QString& command) {
    sendCommand(command, QJsonObject());
}

void GameRun::sendCommand(const QString& command, const QJsonObject& arguments) {
    client->sendCommand(
        "game", command, arguments
    );
}

void GameRun::updateButtonVisibilities() {
    auto const game = client->getClientManager()->getOwnGame();

    if (!game || !game->getTurnPlayer()) {
        rollButton->setVisible(false);
        buyButton->setVisible(false);
        payAmountButton->setVisible(false);
        payPercentButton->setVisible(false);
        skipBuyButton->setVisible(false);
        return;
    }

    bool const onTurn = game->getTurnPlayer()->getPlayer() == client->getClientManager()->getOwnPlayer();
    auto const playerOnTurnEstate = game->getEstates().at(game->getTurnPlayer()->getPosition());

    payAmountButton->setText(tr("Pay %1").arg(playerOnTurnEstate->getTaxAmount()));
    payPercentButton->setText(tr("Pay %1 %").arg(playerOnTurnEstate->getTaxPercent()));

    rollButton->setVisible(
        game->getState() == GameState::RUN &&
        game->getTurnState() == TurnState::ROLL &&
        onTurn
    );

    buyButton->setVisible(
        game->getState() == GameState::RUN &&
        game->getTurnState() == TurnState::BUY &&
        onTurn &&
        playerOnTurnEstate->getPrice() > 0 &&
        playerOnTurnEstate->getPrice() <= game->getTurnPlayer()->getMoney() &&
        !playerOnTurnEstate->getOwner()
    );

    payAmountButton->setVisible(
        game->getState() == GameState::RUN &&
        game->getTurnState() == TurnState::TAX &&
        onTurn &&
        playerOnTurnEstate->getTaxAmount() > 0
    );

    payPercentButton->setVisible(
        game->getState() == GameState::RUN &&
        game->getTurnState() == TurnState::TAX &&
        onTurn &&
        playerOnTurnEstate->getTaxPercent() > 0
    );

    skipBuyButton->setVisible(
        game->getState() == GameState::RUN &&
        game->getTurnState() == TurnState::BUY &&
        onTurn
    );
}
