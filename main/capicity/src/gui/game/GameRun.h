#ifndef CAPICITY2_GAMERUN_H
#define CAPICITY2_GAMERUN_H

#include <QWidget>
#include <QTextBrowser>
#include <QPushButton>

#include "../../client/CapiClient.h"

class GameRun : public QWidget {

    public:
        explicit GameRun(CapiClient* client, QWidget* parent = nullptr);
        ~GameRun() override;

        void reset();

    private:
        void addEvent(const QString& type, const QJsonValue& data);

        void sendCommand(const QString& command);
        void sendCommand(const QString& command, const QJsonObject& arguments);

        void updateButtonVisibilities();

        CapiClient* client = nullptr;
        QTextBrowser* eventView;

        QPushButton* rollButton;
        QPushButton* buyButton;
        QPushButton* payAmountButton;
        QPushButton* payPercentButton;
        QPushButton* skipBuyButton;
};


#endif //CAPICITY2_GAMERUN_H
