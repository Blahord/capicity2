#ifndef CAPICITY2_GAMEPLAYERINFO_H
#define CAPICITY2_GAMEPLAYERINFO_H

#include <QFrame>

#include "../widgets/PlayerAvatarLabel.h"
#include "../PlayerInfo.h"
#include "../../client/CapiClient.h"

class GamePlayerInfo : public QFrame, public PlayerInfo {

    public:
        explicit GamePlayerInfo(const std::shared_ptr<CapiGamePlayer>& player, CapiClient* client, QWidget* parent = nullptr);
        ~GamePlayerInfo() override;

        void tick(long tickCount) override;

    private:
        void updateGamePlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer);
        void updatePlayer(const std::shared_ptr<CapiPlayer>& player);
        void updateGame(const std::shared_ptr<CapiCityGame>& game);
        void updatePlayerNameStyle();

        CapiClient* client;
        std::shared_ptr<CapiGamePlayer> ownPlayer;
        PlayerAvatarLabel* avatarLabel;
        QLabel* playerNameLabel;
        QLabel* moneyLabel;

        ImageStorage imageStorage;
};


#endif //CAPICITY2_GAMEPLAYERINFO_H
