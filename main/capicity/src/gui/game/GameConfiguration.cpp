#include "GameConfiguration.h"

#include <QVBoxLayout>

GameConfiguration::GameConfiguration(CapiClient* client, QWidget* parent) : QWidget(parent) {
    this->client = client;

    this->setReadyText1 = tr("I am ready!!");
    this->setReadyText2 = tr("Lets go! Snacks are ready!");
    this->unsetReadyText = tr("Stop!! I need to get snacks!");

    this->configurationWidget = new QLabel("Game configuration goes here");
    this->playerReadyList = new PlayerReadyList(client, this);
    this->readyButton = new QPushButton(setReadyText1, this);

    createLayout();

    connect(readyButton, &QPushButton::clicked, this, &GameConfiguration::toggleReady);
    connect(client, &CapiClient::gameUpdate, this, &GameConfiguration::updateWidgets);
    connect(client, &CapiClient::gamePlayerUpdate, this, &GameConfiguration::updateOwnPlayer);
}

GameConfiguration::~GameConfiguration() = default;

void GameConfiguration::reset() {
    playerReadyList->reset();
    playerReadyList->setVisible(true);
    readyButton->setVisible(true);
    configurationWidget->setVisible(true);

    readyButton->setText(setReadyText1);
}

void GameConfiguration::createLayout() {
    auto* boxLayout = new QVBoxLayout();
    boxLayout->addWidget(configurationWidget);
    boxLayout->addStretch(1);
    boxLayout->addWidget(playerReadyList);
    boxLayout->addWidget(readyButton);

    this->setLayout(boxLayout);
}

void GameConfiguration::toggleReady() {
    auto clientManager = client->getClientManager();
    bool const ownGamePlayerReady = clientManager->getOwnGame()->findPlayer(clientManager->getOwnPlayer())->isReady();
    client->sendCommand(
        "game", "setReady",
        {{"ready", !ownGamePlayerReady}}
    );
}

void GameConfiguration::updateWidgets(const std::shared_ptr<CapiCityGame>& game) {
    if (game != client->getClientManager()->getOwnGame()) {
        return;
    }

    readyButton->setVisible(game->getState() == GameState::CONFIG);
    playerReadyList->setVisible(game->getState() == GameState::CONFIG);
    configurationWidget->setEnabled(game->getState() == GameState::CONFIG);
}

void GameConfiguration::updateOwnPlayer(const std::shared_ptr<CapiGamePlayer>& player) {
    if (player->getPlayer() != client->getClientManager()->getOwnPlayer()) {
        return;
    }

    if (player->isReady()) {
        readyButton->setText(unsetReadyText);
    } else {
        readyButton->setText(setReadyText2);
    }
}
