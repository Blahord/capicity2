#include "PlayerReadyList.h"

#include <QHBoxLayout>
#include <QSet>

#include "../widgets/PlayerAvatarLabel.h"

PlayerReadyList::PlayerReadyList(CapiClient* client, QWidget* parent) : QWidget(parent) {
    this->client = client;

    setLayout(new QHBoxLayout());

    connect(client, &CapiClient::newPlayerList, this, &PlayerReadyList::updateGamePlayerList);
    connect(client, &CapiClient::gamePlayerUpdate, this, &PlayerReadyList::updateGamePlayerReady);
    connect(client, &CapiClient::gameEntered, this, &PlayerReadyList::updateGamePlayerList);
}

PlayerReadyList::~PlayerReadyList() = default;

void PlayerReadyList::reset() {
    qDeleteAll(playersReadyWidgets);
    playersReadyWidgets.clear();
}

void PlayerReadyList::updateGamePlayerList() {
    auto clientManager = client->getClientManager();
    QList<QString> playerWidgetsToDelete = QList(playersReadyWidgets.keys());
    QList<std::shared_ptr<CapiGamePlayer>> playersWidgetsToCreate;

    if (!clientManager->getOwnGame()) {
        return;
    }

    for (const auto& gamePlayer : clientManager->getOwnGame()->getPlayers()) {
        QString const name = gamePlayer->getPlayer()->getName();

        playerWidgetsToDelete.removeAll(name);
        if (!playersReadyWidgets.contains(name)) {
            playersWidgetsToCreate.append(gamePlayer);
        }
    }

    for (const auto& name : playerWidgetsToDelete) {
        auto widget = playersReadyWidgets.value(name);
        playersReadyWidgets.remove(name);
        delete widget;
    }

    for (auto gamePlayer : playersWidgetsToCreate) {
        auto name = gamePlayer->getPlayer()->getName();

        auto playerAvatarLabel = new PlayerAvatarLabel(gamePlayer->getPlayer(), client, this);
        playerAvatarLabel->setEnabled(gamePlayer->isReady());

        playersReadyWidgets.insert(name, playerAvatarLabel);
        this->layout()->addWidget(playerAvatarLabel);
    }
}

void PlayerReadyList::updateGamePlayerReady(const std::shared_ptr<CapiGamePlayer>& gamePlayer) {
    auto widget = playersReadyWidgets.value(gamePlayer->getPlayer()->getName());
    widget->setEnabled(gamePlayer->isReady());
}
