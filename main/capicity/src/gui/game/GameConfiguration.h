#ifndef CAPICITY2_GAMECONFIGURATION_H
#define CAPICITY2_GAMECONFIGURATION_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>

#include "PlayerReadyList.h"
#include "../../client/CapiClient.h"

class GameConfiguration : public QWidget {

    public:
        GameConfiguration(CapiClient* client, QWidget* parent);
        ~GameConfiguration() override;

        void reset();

    private:
        void createLayout();

        void toggleReady();
        void updateWidgets(const std::shared_ptr<CapiCityGame>& game);
        void updateOwnPlayer(const std::shared_ptr<CapiGamePlayer>& player);

        CapiClient* client = nullptr;

        QLabel* configurationWidget;
        PlayerReadyList* playerReadyList;
        QPushButton* readyButton;

        QString setReadyText1;
        QString setReadyText2;
        QString unsetReadyText;
};


#endif //CAPICITY2_GAMECONFIGURATION_H
