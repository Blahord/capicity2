#ifndef CAPICITY2_PLAYERREADYLIST_H
#define CAPICITY2_PLAYERREADYLIST_H

#include <QWidget>

#include "../../client/CapiClient.h"

class PlayerReadyList : public QWidget {

    public:
        explicit PlayerReadyList(CapiClient* client, QWidget* parent);
        ~PlayerReadyList() override;

        void reset();

    private:
        void updateGamePlayerList();
        void updateGamePlayerReady(const std::shared_ptr<CapiGamePlayer>& gamePlayer);

        QMap<QString, QWidget*> playersReadyWidgets;

        CapiClient* client = nullptr;
};


#endif //CAPICITY2_PLAYERREADYLIST_H
