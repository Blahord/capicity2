#include "GameView.h"

#include <QPainter>
#include <QPaintEvent>
#include <QVBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPropertyAnimation>
#include <QtMath>

#include "GameConfiguration.h"
#include "GameRun.h"

GameView::GameView(CapiClient* client, QWidget* parent) : QWidget(parent) {
    this->client = client;

    this->gameConfiguration = new GameConfiguration(client, this);
    this->gameRun = new GameRun(client, this);

    this->centerWidget = new QTabWidget(this);
    this->leaveButton = new QToolButton(this);

    this->leaveButton->setIcon(QIcon::fromTheme("window-close"));
    this->leaveButton->setToolTip(tr("Leave game"));

    this->centerWidget->setCornerWidget(leaveButton, Qt::TopRightCorner);
    this->centerWidget->setTabBarAutoHide(false);
    this->centerWidget->addTab(gameConfiguration, tr("Configuration"));
    this->centerWidget->addTab(gameRun, tr("Game run"));

    createLayout();

    connect(client, &CapiClient::newImageData, this, &GameView::rerenderGameImage);
    connect(client, &CapiClient::newGameData, this, &GameView::rerenderOwnGame);
    connect(client, &CapiClient::gameUpdate, this, &GameView::rerenderGame);
    connect(client, &CapiClient::gameUpdate, this, [this](const std::shared_ptr<CapiCityGame>& g) { if (g->getState() == GameState::RUN) { this->centerWidget->setCurrentIndex(1); }});
    connect(client, &CapiClient::gamePlayerMovement, this, &GameView::startMoveAnimation);
    connect(client, &CapiClient::playerUpdated, this, &GameView::rerenderOwnGame);
    connect(leaveButton, &QToolButton::clicked, this, &GameView::leaveGame);
}

GameView::~GameView() = default;

void GameView::reset() {
    this->centerWidget->setCurrentIndex(0);
    this->gameRun->reset();
    this->gameConfiguration->reset();
}

void GameView::paintEvent(QPaintEvent* event) {
    auto clientManager = client->getClientManager();

    auto game = clientManager->getOwnGame();
    if (!game) {
        return;
    }

    int countEstatesPerSide = game->getEstates().size() / 4;

    int unusedPixelsX = (width() - 1 - 2 * estateSize) % (countEstatesPerSide - 1);
    int renderOffsetX = unusedPixelsX / 2;
    int dynamicWidth = (width() - 1 - 2 * estateSize) / (countEstatesPerSide - 1);

    int unusedPixelsY = (height() - 1 - 2 * estateSize) % (countEstatesPerSide - 1);
    int renderOffsetY = unusedPixelsY / 2;
    int dynamicHeight = (height() - 1 - 2 * estateSize) / (countEstatesPerSide - 1);

    QPainter p(this);
    p.setPen(Qt::black);

    RenderProperties renderProperties{
        .countEstatesPerSide = countEstatesPerSide,
        .renderOffsetX = renderOffsetX,
        .renderOffsetY = renderOffsetY,
        .dynamicWidth = dynamicWidth,
        .dynamicHeight = dynamicHeight,
        .textSize = estateSize / 2,
        .iconSize = qMin(dynamicWidth, dynamicHeight) / 2,
        .bgColor = QColor("#" + game->getBgColor()),
        .painter = &p,
    };

    p.translate(renderOffsetX, renderOffsetY);

    for (int i = 0; i < game->getEstates().size(); i++) {
        auto estate = game->getEstates().at(i);

        int side = i / countEstatesPerSide;
        int sideNumber = i % countEstatesPerSide;

        switch (side) {
            case 0:
                renderSouthEstate(estate, sideNumber, renderProperties);
                break;
            case 1:
                renderWestEstate(estate, sideNumber, renderProperties);
                break;
            case 2:
                renderNorthEstate(estate, sideNumber, renderProperties);
                break;
            case 3:
                renderEastEstate(estate, sideNumber, renderProperties);
                break;
            default:
                p.fillRect(estateSize, estateSize, estateSize / 2, estateSize / 2, Qt::black);
                p.fillRect(estateSize + estateSize / 2, estateSize, estateSize / 2, estateSize / 2, QColor(255, 0, 255));
                p.fillRect(estateSize + estateSize, estateSize + estateSize / 2, estateSize / 2, estateSize / 2, QColor(255, 0, 255));
                p.fillRect(estateSize + estateSize / 2, estateSize + estateSize / 2, estateSize / 2, estateSize / 2, Qt::black);
        }
    }

    if (game->getState() == GameState::RUN) {
        for (auto gamePlayer : game->getPlayers()) {
            if (gamePlayer->getPlayer() == clientManager->getOwnPlayer()) {
                continue;
            }

            renderPlayerOnBoard(gamePlayer, renderProperties);
        }

        for (auto gamePlayer : game->getPlayers()) {
            if (gamePlayer->getPlayer() != clientManager->getOwnPlayer()) {
                continue;
            }

            renderPlayerOnBoard(gamePlayer, renderProperties);
        }
    }

    p.end();
    event->accept();
}

void GameView::createLayout() {
    this->setLayout(new QVBoxLayout());
    this->layout()->addWidget(this->centerWidget);
    this->layout()->setMargin((int) (1.1 * estateSize));
}

void GameView::leaveGame() {
    QMessageBox::StandardButton result = QMessageBox::question(
        this, QObject::tr("Leave game?"),
        QObject::tr("Are you sure you want to leave the game?")
    );

    if (result == QMessageBox::Yes) {
        this->client->sendCommand("server", "leaveGame", {});
    }
}

void GameView::startMoveAnimation(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int from, int to, bool forward) {
    auto clientManager = client->getClientManager();

    animationPlayer = gamePlayer->getPlayer()->getId();
    animationTo = to;

    int animationLength;
    if (forward) {
        animationLength = (to > from)
                          ? to - from
                          : to + clientManager->getOwnGame()->getEstates().size() - from;
    } else {
        animationLength = (from > to)
                          ? to - from
                          : to - clientManager->getOwnGame()->getEstates().size() - from;
    }

    auto animation = new QPropertyAnimation(this, "animVal");
    animation->setDuration(4000 - settings.getAnimationSpeed());
    animation->setStartValue(0);
    animation->setEndValue(animationLength * animationSteps);
    animation->setEasingCurve(QEasingCurve::InOutQuad);

    connect(animation, &QPropertyAnimation::valueChanged, this, &GameView::renderAnimationState);
    connect(animation, &QPropertyAnimation::finished, this, &GameView::endMoveAnimation);
    connect(animation, &QPropertyAnimation::finished, animation, &QObject::deleteLater);

    animation->start();
}

void GameView::renderAnimationState(const QVariant& value) {
    animationValue = value.toInt();
    rerenderOwnGame();
}

void GameView::endMoveAnimation() {
    client->getClientManager()->getOwnGame()->findPlayer(animationPlayer)->setPosition(animationTo);
    animationPlayer = "";
    animationTo = 0;
    animationValue = 0;

    client->sendCommand("game", "moveComplete", {});
    rerenderOwnGame();
}

void GameView::rerenderGame(const std::shared_ptr<CapiCityGame>& game) {
    if (game == client->getClientManager()->getOwnGame()) {
        rerenderOwnGame();
    }
}

void GameView::rerenderOwnGame() {
    repaint();
}

void GameView::rerenderGameImage(const QString& hash) {
    auto game = client->getClientManager()->getOwnGame();
    if (!game) {
        return;
    }

    for (auto estate : game->getEstates()) {
        if (estate->getImage() == hash) {
            repaint();
            return;
        }
    }

    for (auto player : game->getPlayers()) {
        if (player->getPlayer()->getAvatarData() == hash) {
            repaint();
            return;
        }
    }
}

void GameView::renderSouthEstate(const std::shared_ptr<CapiCityEstate>& estate, int sideNumber, const RenderProperties& renderProperties) {
    auto painter = renderProperties.painter;
    int estateWidth = sideNumber ? renderProperties.dynamicWidth : estateSize;

    QRect estateRect(0, 0, estateWidth, estateSize);
    QRect badgeRect(0, 0, estateWidth, groupBadgeSize);
    QRect textRect(textMargin, groupBadgeSize + ((estateSize - groupBadgeSize) - renderProperties.textSize) / 2, estateWidth - 2 * textMargin, renderProperties.textSize);

    painter->save();

    painter->translate(
        this->estateSize + (renderProperties.countEstatesPerSide - 1 - sideNumber) * renderProperties.dynamicWidth,
        (renderProperties.countEstatesPerSide - 1) * renderProperties.dynamicHeight + estateSize
    );

    renderEstate(estate, estateRect, badgeRect, textRect, renderProperties.bgColor, painter);

    painter->restore();
}

void GameView::renderWestEstate(const std::shared_ptr<CapiCityEstate>& estate, int sideNumber, const RenderProperties& renderProperties) {
    auto painter = renderProperties.painter;
    int estateHeight = sideNumber ? renderProperties.dynamicHeight : estateSize;

    QRect estateRect(0, 0, estateSize, estateHeight);
    QRect badgeRect(estateSize - groupBadgeSize, 0, groupBadgeSize, estateHeight);
    QRect textRect(((estateSize - groupBadgeSize) - renderProperties.textSize) / 2, 2, estateSize / 2, estateHeight - 2 * textMargin);

    painter->save();

    painter->translate(
        0,
        (renderProperties.countEstatesPerSide - 1 - sideNumber) * renderProperties.dynamicHeight + estateSize
    );

    renderEstate(estate, estateRect, badgeRect, textRect, renderProperties.bgColor, painter);

    painter->restore();
}

void GameView::renderNorthEstate(const std::shared_ptr<CapiCityEstate>& estate, int sideNumber, const RenderProperties& renderProperties) {
    auto painter = renderProperties.painter;
    int estateWidth = sideNumber ? renderProperties.dynamicWidth : estateSize;

    QRect estateRect(0, 0, estateWidth, estateSize);
    QRect badgeRect(0, estateSize - groupBadgeSize, estateWidth, groupBadgeSize);
    QRect textRect(textMargin, ((estateSize - groupBadgeSize) - renderProperties.textSize) / 2, estateWidth - 2 * textMargin, renderProperties.textSize);

    painter->save();

    painter->translate(
        (sideNumber > 0 ? (estateSize + renderProperties.dynamicWidth * (sideNumber - 1)) : 0),
        0
    );

    renderEstate(estate, estateRect, badgeRect, textRect, renderProperties.bgColor, painter);

    painter->restore();
}

void GameView::renderEastEstate(const std::shared_ptr<CapiCityEstate>& estate, int sideNumber, const RenderProperties& renderProperties) {
    auto painter = renderProperties.painter;
    int estateHeight = sideNumber ? renderProperties.dynamicHeight : estateSize;

    QRect estateRect(0, 0, estateSize, estateHeight);
    QRect badgeRect(0, 0, groupBadgeSize, estateHeight);
    QRect textRect(groupBadgeSize + ((estateSize - groupBadgeSize) - (estateSize / 2)) / 2, textMargin, estateSize / 2, estateHeight - 2 * textMargin);

    painter->save();

    painter->translate(
        estateSize + (renderProperties.countEstatesPerSide - 1) * renderProperties.dynamicWidth,
        (sideNumber > 0 ? (estateSize + renderProperties.dynamicHeight * (sideNumber - 1)) : 0)
    );

    renderEstate(estate, estateRect, badgeRect, textRect, renderProperties.bgColor, painter);

    painter->restore();
}

void GameView::renderEstate(const std::shared_ptr<CapiCityEstate>& estate, QRect estateRect, QRect badgeRect, QRect textRect, const QColor& bgColor, QPainter* painter) {
    painter->fillRect(estateRect, bgColor);

    if (estate->getGroup() && !estate->getGroup()->getColor().isEmpty()) {
        painter->fillRect(badgeRect, QColor("#" + estate->getGroup()->getColor()));
        painter->drawRect(badgeRect);
    }

    if (estate->getOwner()) {
        auto owner = estate->getOwner()->getPlayer();
        QImage image = (
            owner->getAvatarData().isEmpty()
            ? ImageStorage::createDefaultAvatar(owner->getName())
            : imageStorage.getImage(owner->getAvatarData()).scaled(textRect.width(), textRect.height(), Qt::KeepAspectRatio)
        ).scaled(textRect.width(), textRect.height(), Qt::KeepAspectRatio).toImage();

        for (int x = 0; x < image.width(); x++) {
            for (int y = 0; y < image.height(); y++) {
                auto color = image.pixelColor(x, y);
                color.setAlpha((int) (color.alpha() / 4));

                image.setPixelColor(x, y, color);
            }
        }

        int renderPosX = textRect.x() + (textRect.width() - image.width()) / 2;
        int renderPosY = textRect.y() + (textRect.height() - image.height()) / 2;
        painter->drawImage(renderPosX, renderPosY, image);
    }

    if (!estate->getImage().isEmpty() && imageStorage.containsImage(estate->getImage())) {
        QPixmap image = imageStorage.getImage(estate->getImage()).scaled(textRect.width(), textRect.height(), Qt::KeepAspectRatio);

        int renderPosX = textRect.x() + (textRect.width() - image.width()) / 2;
        int renderPosY = textRect.y() + (textRect.height() - image.height()) / 2;
        painter->drawPixmap(renderPosX, renderPosY, image);
    } else {
        painter->drawText(textRect, Qt::AlignCenter | Qt::TextWrapAnywhere, estate->getName());
    }

    painter->drawRect(estateRect);
}

void GameView::renderPlayerOnBoard(const std::shared_ptr<CapiGamePlayer>& gamePlayer, const GameView::RenderProperties& renderProperties) {
    int actualPlayerPosition = gamePlayer->getPosition();
    int renderPosition = (actualPlayerPosition + qFloor(animationValue * 1.0 / animationSteps)) % (renderProperties.countEstatesPerSide * 4);
    if (renderPosition < 0) {
        renderPosition += renderProperties.countEstatesPerSide * 4;
    }

    int offset = animationValue % animationSteps;
    if (offset < 0) {
        offset += animationSteps;
    }

    if (gamePlayer->getPlayer()->getId() != animationPlayer) {
        renderPosition = gamePlayer->getPosition();
        offset = 0;
    }

    int side = renderPosition / renderProperties.countEstatesPerSide;
    int sideNumber = renderPosition % renderProperties.countEstatesPerSide;

    switch (side) {
        case 0:
            renderSouthPlayer(gamePlayer, sideNumber, offset, renderProperties);
            break;
        case 1:
            renderWestPlayer(gamePlayer, sideNumber, offset, renderProperties);
            break;
        case 2:
            renderNorthPlayer(gamePlayer, sideNumber, offset, renderProperties);
            break;
        case 3:
            renderEastPlayer(gamePlayer, sideNumber, offset, renderProperties);
            break;
        default:
            //Nothing to render
            break;
    }
}

void GameView::renderSouthPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int sideNumber, int offset, const GameView::RenderProperties& renderProperties) {
    auto painter = renderProperties.painter;
    int estateWidth = sideNumber ? renderProperties.dynamicWidth : (estateSize - groupBadgeSize);
    int estateHeight = estateSize - groupBadgeSize;
    int renderX = (estateWidth - renderProperties.iconSize) / 2;
    int renderY = (estateHeight - renderProperties.iconSize) / 2;

    if (offset) {
        int animationEstateSize = estateWidth;
        if (!sideNumber || sideNumber == renderProperties.countEstatesPerSide - 1) {
            animationEstateSize = (estateSize - groupBadgeSize + renderProperties.dynamicWidth) / 2 + groupBadgeSize;
        }

        renderX -= (offset * animationEstateSize) / animationSteps;
    }

    painter->save();

    painter->translate(
        estateSize + (renderProperties.countEstatesPerSide - 1 - sideNumber) * renderProperties.dynamicWidth,
        (renderProperties.countEstatesPerSide - 1) * renderProperties.dynamicHeight + estateSize + groupBadgeSize
    );

    if (!sideNumber) {
        painter->translate(groupBadgeSize, 0);
    }

    renderPlayer(gamePlayer, renderX, renderY, renderProperties.iconSize, painter);

    painter->restore();
}

void GameView::renderWestPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int sideNumber, int offset, const GameView::RenderProperties& renderProperties) {
    auto painter = renderProperties.painter;
    int estateWidth = estateSize - groupBadgeSize;
    int estateHeight = sideNumber ? renderProperties.dynamicHeight : (estateSize - groupBadgeSize);
    int renderX = (estateWidth - renderProperties.iconSize) / 2;
    int renderY = (estateHeight - renderProperties.iconSize) / 2;

    if (offset) {
        int animationEstateSize = estateHeight;
        if (!sideNumber || sideNumber == renderProperties.countEstatesPerSide - 1) {
            animationEstateSize = (estateSize - groupBadgeSize + renderProperties.dynamicHeight) / 2 + groupBadgeSize;
        }

        renderY -= (offset * animationEstateSize) / animationSteps;
    }

    painter->save();

    painter->translate(
        0,
        (renderProperties.countEstatesPerSide - 1 - sideNumber) * renderProperties.dynamicHeight + estateSize
    );
    if (!sideNumber) {
        painter->translate(0, groupBadgeSize);
    }

    renderPlayer(gamePlayer, renderX, renderY, renderProperties.iconSize, painter);

    painter->restore();
}

void GameView::renderNorthPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int sideNumber, int offset, const GameView::RenderProperties& renderProperties) {
    auto painter = renderProperties.painter;
    int estateWidth = sideNumber ? renderProperties.dynamicWidth : (estateSize - groupBadgeSize);
    int estateHeight = estateSize - groupBadgeSize;
    int renderX = (estateWidth - renderProperties.iconSize) / 2;
    int renderY = (estateHeight - renderProperties.iconSize) / 2;

    if (offset) {
        int animationEstateSize = estateWidth;
        if (!sideNumber || sideNumber == renderProperties.countEstatesPerSide - 1) {
            animationEstateSize = (estateSize - groupBadgeSize + renderProperties.dynamicWidth) / 2 + groupBadgeSize;
        }

        renderX += (offset * animationEstateSize) / animationSteps;
    }

    painter->save();

    painter->translate(
        (sideNumber ? (estateSize + renderProperties.dynamicWidth * (sideNumber - 1)) : 0),
        0
    );

    renderPlayer(gamePlayer, renderX, renderY, renderProperties.iconSize, painter);

    painter->restore();
}

void GameView::renderEastPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int sideNumber, int offset, const GameView::RenderProperties& renderProperties) {
    auto painter = renderProperties.painter;
    int estateWidth = estateSize - groupBadgeSize;
    int estateHeight = sideNumber ? renderProperties.dynamicHeight : (estateSize - groupBadgeSize);
    int renderX = (estateWidth - renderProperties.iconSize) / 2;
    int renderY = (estateHeight - renderProperties.iconSize) / 2;

    if (offset) {
        int animationEstateSize = estateHeight;
        if (!sideNumber || sideNumber == renderProperties.countEstatesPerSide - 1) {
            animationEstateSize = (estateSize - groupBadgeSize + renderProperties.dynamicHeight) / 2 + groupBadgeSize;
        }

        renderY += (offset * animationEstateSize) / animationSteps;
    }

    painter->save();

    painter->translate(
        estateSize + groupBadgeSize + (renderProperties.countEstatesPerSide - 1) * renderProperties.dynamicWidth,
        (sideNumber > 0 ? (estateSize + renderProperties.dynamicHeight * (sideNumber - 1)) : 0)
    );

    renderPlayer(gamePlayer, renderX, renderY, renderProperties.iconSize, painter);

    painter->restore();
}

void GameView::renderPlayer(const std::shared_ptr<CapiGamePlayer>& gamePlayer, int x, int y, int size, QPainter* painter) {
    QPixmap avatar;
    QPixmap shadow;
    if (imageStorage.containsImage(gamePlayer->getPlayer()->getAvatarData())) {
        avatar = imageStorage.getImage(gamePlayer->getPlayer()->getAvatarData());
        shadow = imageStorage.getShadow(gamePlayer->getPlayer()->getAvatarData());
    } else {
        avatar = ImageStorage::createDefaultAvatar(gamePlayer->getPlayer()->getName());
        shadow = QPixmap(":shadows/default.png");
    }

    shadow = ImageStorage::scalePixmap(shadow, size);
    avatar = ImageStorage::scalePixmap(avatar, size);

    painter->drawPixmap(x + size / 8, y + size / 8, shadow);
    painter->drawPixmap(x, y, avatar);
}

