#ifndef CAPICITY2_PLAYERINFO_H
#define CAPICITY2_PLAYERINFO_H

class PlayerInfo {

    public:
        virtual ~PlayerInfo() = default;

        virtual void tick(long tickCount) = 0;
};

#endif //CAPICITY2_PLAYERINFO_H
