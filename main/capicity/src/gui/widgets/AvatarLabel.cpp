#include "AvatarLabel.h"

#include <QPainter>

AvatarLabel::AvatarLabel(QWidget* parent) : QLabel(parent) {
    setFixedSize(32, 32);
}

AvatarLabel::~AvatarLabel() = default;

void AvatarLabel::setAvatar(const QPixmap& pixmap) {
    this->avatar = ImageStorage::scalePixmap(pixmap, 32);

    paintOverlay();
}

void AvatarLabel::setOverlay(const QPixmap& overlay) {
    if (overlay.isNull()) {
        this->overlay = overlay;
    } else {
        this->overlay = overlay.scaled(32, 32);
    }

    paintOverlay();
}

void AvatarLabel::paintOverlay() {
    QPixmap overlayed = QPixmap(avatar);

    if (!overlay.isNull()) {
        QPainter p(&overlayed);
        p.drawPixmap(0, 0, overlay);
        p.end();
    }

    setPixmap(overlayed);
}
