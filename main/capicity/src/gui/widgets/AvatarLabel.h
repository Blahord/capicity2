#ifndef CAPICITY2_AVATARLABEL_H
#define CAPICITY2_AVATARLABEL_H

#include <QLabel>

#include "../../ImageStorage.h"

class AvatarLabel : public QLabel {

    public:
        explicit AvatarLabel(QWidget* parent = nullptr);
        ~AvatarLabel() override;

        void setAvatar(const QPixmap& pixmap);
        void setOverlay(const QPixmap& overlay);

    private:
        void paintOverlay();

        ImageStorage imageStorage;
        QPixmap avatar;
        QPixmap overlay;
};


#endif //CAPICITY2_AVATARLABEL_H
