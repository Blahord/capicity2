#ifndef CAPICITY2_PLAYERAVATARLABEL_H
#define CAPICITY2_PLAYERAVATARLABEL_H

#include <QWidget>
#include <QStackedLayout>

#include "CapiPlayer.h"
#include "AvatarLabel.h"
#include "../../ImageStorage.h"
#include "../../client/CapiClient.h"

class PlayerAvatarLabel : public QWidget {

    public:
        explicit PlayerAvatarLabel(const std::shared_ptr<CapiPlayer>& player, CapiClient* client, QWidget* parent = nullptr);
        ~PlayerAvatarLabel() override;

        void enableToolTip();
        void setOverlayImage(const QPixmap& overlay);

        void tick(long tickCount);

    private:
        void updateAvatar(const QString& hash);
        void updatePlayer(const std::shared_ptr<CapiPlayer>& player);

        QPixmap createDisplayImage();

        ImageStorage imageStorage;

        std::shared_ptr<CapiPlayer> player;

        QStackedLayout* avatarLayout;
        AvatarLabel* avatarLabel;
        QLabel* disconnectedLabel;
};


#endif //CAPICITY2_PLAYERAVATARLABEL_H
