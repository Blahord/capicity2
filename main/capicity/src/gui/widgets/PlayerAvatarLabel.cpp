#include "PlayerAvatarLabel.h"

#include <QIcon>
#include <QVBoxLayout>
#include <QPainter>
#include <QCryptographicHash>
#include <QStackedLayout>

PlayerAvatarLabel::PlayerAvatarLabel(const std::shared_ptr<CapiPlayer>& player, CapiClient* client, QWidget* parent) : QWidget(parent) {
    this->player = player;
    this->avatarLabel = new AvatarLabel(this);
    this->avatarLabel->setAvatar(createDisplayImage());
    this->disconnectedLabel = new QLabel();
    this->disconnectedLabel->setPixmap(QIcon::fromTheme("network-offline").pixmap(32, 32));

    this->avatarLayout = new QStackedLayout();
    avatarLayout->setContentsMargins(0, 0, 0, 0);
    avatarLayout->addWidget(avatarLabel);
    avatarLayout->addWidget(disconnectedLabel);
    avatarLayout->setCurrentWidget(avatarLabel);

    setLayout(avatarLayout);
    setFixedSize(avatarLabel->size());

    connect(client, &CapiClient::newImageData, this, &PlayerAvatarLabel::updateAvatar);
    connect(client, &CapiClient::playerUpdated, this, &PlayerAvatarLabel::updatePlayer);
}

PlayerAvatarLabel::~PlayerAvatarLabel() = default;

void PlayerAvatarLabel::enableToolTip() {
    setToolTip(player->getName());
}

void PlayerAvatarLabel::setOverlayImage(const QPixmap& overlay) {
    avatarLabel->setOverlay(overlay);
}

void PlayerAvatarLabel::tick(long tickCount) {
    if ((tickCount % 2) && !player->isConnected() && avatarLayout->currentWidget() == avatarLabel) {
        avatarLayout->setCurrentWidget(disconnectedLabel);
    } else {
        avatarLayout->setCurrentWidget(avatarLabel);
    }
}

void PlayerAvatarLabel::updateAvatar(const QString& hash) {
    if (player->getAvatarData() != hash) {
        return;
    }

    avatarLabel->setAvatar(imageStorage.getImage(hash));
    avatarLabel->repaint(0, 0, width(), height());
}

void PlayerAvatarLabel::updatePlayer(const std::shared_ptr<CapiPlayer>& player) {
    if (this->player != player) {
        return;
    }

    avatarLabel->setAvatar(createDisplayImage());
    avatarLabel->repaint(0, 0, width(), height());
}

QPixmap PlayerAvatarLabel::createDisplayImage() {
    if (player->getAvatarData().isEmpty()) {
        return ImageStorage::createDefaultAvatar(player->getName());
    }

    if (imageStorage.containsImage(player->getAvatarData())) {
        return imageStorage.getImage(player->getAvatarData());
    }

    return ImageStorage::createDefaultAvatar(player->getName());
}
