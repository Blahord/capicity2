#include "CapiCityServer.h"

#include <utility>

CapiCityServer::CapiCityServer(QString uuid, QString name, QString host, quint16 port)
    : uuid(std::move(uuid)),
      name(std::move(name)),
      host(std::move(host)),
      port(port) {
}

CapiCityServer::~CapiCityServer() = default;

const QString& CapiCityServer::getUuid() const {
    return uuid;
}

const QString& CapiCityServer::getName() const {
    return name;
}

const QString& CapiCityServer::getHost() const {
    return host;
}

quint16 CapiCityServer::getPort() const {
    return port;
}
