#include "AddServerDialog.h"

#include <QFormLayout>

#include "constants.h"

AddServerDialog::AddServerDialog(ServerStorage* serverStorage, QWidget* parent) : QDialog(parent) {
    this->serverStorage = serverStorage;

    this->nameInput = new QLineEdit(tr("New server"), this);
    this->hostInput = new QLineEdit(this);
    this->portInput = new QSpinBox(this);

    portInput->setMinimum(0);
    portInput->setMaximum(65536);
    portInput->setValue(DEFAULT_SERVER_PORT);

    this->buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);

    auto formLayout = new QFormLayout();
    formLayout->addRow(tr("Name"), nameInput);
    formLayout->addRow(tr("Host"), hostInput);
    formLayout->addRow(tr("Port"), portInput);

    auto layout = new QVBoxLayout(this);
    layout->addLayout(formLayout);
    layout->addWidget(buttons);

    setModal(true);
    setWindowTitle(tr("Add server"));

    connect(buttons, &QDialogButtonBox::accepted, this, &AddServerDialog::save);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);
}

AddServerDialog::~AddServerDialog() = default;

void AddServerDialog::save() {
    serverStorage->addNewServer(
        nameInput->text(),
        hostInput->text(),
        portInput->value()
    );

    accept();
}
