#ifndef CAPICITY2_CAPISERVERCHOOSER_H
#define CAPICITY2_CAPISERVERCHOOSER_H

#include <QWidget>
#include <QTableView>
#include <QPushButton>
#include <QTableWidget>
#include <QScrollArea>

#include "ServerStorage.h"
#include "CustomServerEditor.h"
#include "CapiServerInfo.h"
#include "../../client/CapiClient.h"

class CapiServerChooser : public QWidget {
    Q_OBJECT

    public:
        explicit CapiServerChooser(CapiClient* client, QWidget* parent = nullptr);
        ~CapiServerChooser() override;

    private slots:
        void addServer();
        void deleteServer(const QString& uuid);

    private:
        void createLayout();
        void refreshServerList();

        ServerStorage serverStorage;

        CapiClient* client = nullptr;

        QScrollArea* serverList = nullptr;
        QWidget* serverListContent = nullptr;
        QPushButton* addServerButton = nullptr;
        CustomServerEditor* customServerEditor = nullptr;

        QList<CapiServerInfo*> serverInfos;
};


#endif //CAPICITY2_CAPISERVERCHOOSER_H
