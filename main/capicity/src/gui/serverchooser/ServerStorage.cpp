#include "ServerStorage.h"

#include <QUuid>
#include <QFile>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>
#include <QDir>

ServerStorage::ServerStorage() {
    readServers();
}

ServerStorage::~ServerStorage() {
    for (auto server : servers) {
        delete server;
    }
}

QList<CapiCityServer*> ServerStorage::getServers() {
    return servers;
}

void ServerStorage::addNewServer(const QString& name, const QString& host, quint16 port) {
    auto server = new CapiCityServer(QUuid::createUuid().toString(), name, host, port);
    servers.append(server);
    writeServers();
}

void ServerStorage::deleteServer(const QString& uuid) {
    QList<CapiCityServer*> serversToDelete;
    auto deletedServers = std::find_if(servers.begin(), servers.end(), [uuid](CapiCityServer* s) { return s->getUuid() == uuid; });
    while (deletedServers != servers.end()) {
        serversToDelete.append(*deletedServers);
        deletedServers++;
    }

    for (auto server : serversToDelete) {
        if (server->getUuid() == uuid) {
            servers.removeOne(server);
            delete server;
        }
    }

    writeServers();
}

void ServerStorage::readServers() {
    QDir serverDir(dataPath);
    serverDir.mkpath(".");

    QDir(dataPath).mkpath("server");

    servers.clear();
    QFile serverListFile(dataPath + "/servers.json");
    if (!serverListFile.exists()) {
        return;
    }

    serverListFile.open(QFile::ReadOnly);
    QByteArray content = serverListFile.readAll();

    QJsonParseError error{};
    QJsonDocument document = QJsonDocument::fromJson(content, &error);
    if (error.error != QJsonParseError::NoError) {
        return;
    }

    QJsonArray serverArray = document.array();

    for (auto serverValue : serverArray) {
        auto serverObject = serverValue.toObject();

        QString uuid = serverObject.value("uuid").toString();
        QString name = serverObject.value("name").toString();
        QString host = serverObject.value("host").toString();
        quint16 port = serverObject.value("port").toInt(0);

        servers.append(new CapiCityServer(uuid, name, host, port));
    }
}

void ServerStorage::writeServers() {
    QFile serverListFile(dataPath + "/servers.json");
    if (serverListFile.exists()) {
        serverListFile.remove();
    }

    serverListFile.open(QFile::WriteOnly);
    QJsonArray serverArray;

    for (auto server : servers) {
        QJsonObject serverObj = {
            {"uuid", server->getUuid()},
            {"name", server->getName()},
            {"host", server->getHost()},
            {"port", server->getPort()}
        };

        serverArray.append(serverObj);
    }

    QJsonDocument resultDoc(serverArray);
    QString resultString(resultDoc.toJson(QJsonDocument::Indented));
    serverListFile.write(resultString.toUtf8());
    serverListFile.flush();
    serverListFile.close();
}
