#ifndef CAPICITY2_ADDSERVERDIALOG_H
#define CAPICITY2_ADDSERVERDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QSpinBox>
#include <QDialogButtonBox>

#include "ServerStorage.h"

class AddServerDialog : public QDialog {

    public:
        explicit AddServerDialog(ServerStorage* serverStorage, QWidget* parent = nullptr);
        ~AddServerDialog() override;

    private slots:
        void save();

    private:
        ServerStorage* serverStorage = nullptr;

        QDialogButtonBox* buttons = nullptr;
        QLineEdit* nameInput = nullptr;
        QLineEdit* hostInput = nullptr;
        QSpinBox* portInput = nullptr;
};


#endif //CAPICITY2_ADDSERVERDIALOG_H
