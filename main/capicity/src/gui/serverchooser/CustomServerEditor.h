#ifndef CAPICITY2_CUSTOMSERVEREDITOR_H
#define CAPICITY2_CUSTOMSERVEREDITOR_H

#include <QFrame>
#include <QLineEdit>
#include <QSpinBox>

class CustomServerEditor : public QFrame {
    Q_OBJECT

    public:
        explicit CustomServerEditor(QWidget* parent = nullptr);
        ~CustomServerEditor() override;

    signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
        void connectionRequested(const QString& address, quint16 port);
#pragma clang diagnostic pop

    private:
        void createLayout();
        void emitConnectRequest();

        QLineEdit* addressEditor = nullptr;
        QSpinBox* portEditor = nullptr;

        QToolButton* connectButton = nullptr;

};


#endif //CAPICITY2_CUSTOMSERVEREDITOR_H
