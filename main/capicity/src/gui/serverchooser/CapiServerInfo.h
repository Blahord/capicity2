#ifndef CAPICITY2_CAPISERVERINFO_H
#define CAPICITY2_CAPISERVERINFO_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QToolButton>

#include "CapiCityServer.h"

class CapiServerInfo : public QFrame {
    Q_OBJECT

    public:
        explicit CapiServerInfo(CapiCityServer* server, QWidget* parent = nullptr);
        ~CapiServerInfo() override;

    signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
        void connectionRequested(QString address, quint16 port);
        void deleted(const QString& uuid);
#pragma clang diagnostic pop

    private slots:
        void askDelete();
        void emitConnectionRequest();

    private:
        CapiCityServer* server;

        QLabel* nameLabel = nullptr;
        QLabel* addressLabel = nullptr;
        QLabel* portLabel = nullptr;

        QToolButton* connectButton = nullptr;
        QToolButton* deleteButton = nullptr;
};


#endif //CAPICITY2_CAPISERVERINFO_H
