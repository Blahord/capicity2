#include "CapiServerInfo.h"

#include <QHBoxLayout>
#include <QMessageBox>

CapiServerInfo::CapiServerInfo(CapiCityServer* server, QWidget* parent) : QFrame(parent) {
    this->server = server;

    this->nameLabel = new QLabel(server->getName(), this);
    this->addressLabel = new QLabel(server->getHost(), this);
    this->portLabel = new QLabel(QString::number(server->getPort()), this);

    this->connectButton = new QToolButton(this);
    this->deleteButton = new QToolButton(this);

    connectButton->setIcon(QIcon::fromTheme("media-playback-start"));
    connectButton->setToolTip(tr("Connect to this server"));

    deleteButton->setIcon(QIcon::fromTheme("edit-delete"));
    deleteButton->setToolTip(tr("Delete this server"));

    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

    auto hostPortLayout = new QHBoxLayout();
    hostPortLayout->addWidget(addressLabel);
    hostPortLayout->addStretch(1);
    hostPortLayout->addWidget(portLabel);

    auto serverInfoLayout = new QVBoxLayout();
    serverInfoLayout->addWidget(nameLabel);
    serverInfoLayout->addLayout(hostPortLayout);

    auto boxLayout = new QHBoxLayout();
    boxLayout->addLayout(serverInfoLayout);
    boxLayout->addStretch(1);
    boxLayout->addWidget(connectButton);
    boxLayout->addWidget(deleteButton);

    setLayout(boxLayout);

    setFrameShadow(QFrame::Raised);
    setFrameShape(QFrame::Panel);

    connect(connectButton, &QToolButton::clicked, this, &CapiServerInfo::emitConnectionRequest);
    connect(deleteButton, &QToolButton::clicked, this, &CapiServerInfo::askDelete);
}

CapiServerInfo::~CapiServerInfo() = default;

void CapiServerInfo::askDelete() {
    auto result = QMessageBox::question(
        this, tr("Delete"), tr("Really delete this server?")
    );

    if (result == QMessageBox::Yes) {
        emit(deleted(server->getUuid()));
    }
}

void CapiServerInfo::emitConnectionRequest() {
    emit(connectionRequested(server->getHost(), server->getPort()));
}
