#include "CapiServerChooser.h"

#include <QVBoxLayout>

#include "CapiServerInfo.h"
#include "AddServerDialog.h"

CapiServerChooser::CapiServerChooser(CapiClient* client, QWidget* parent) : QWidget(parent) {
    this->client = client;

    this->serverList = new QScrollArea(this);
    this->serverListContent = new QWidget(this);
    this->addServerButton = new QPushButton(QIcon::fromTheme("document-new"), tr("Add server ..."), this);
    this->customServerEditor = new CustomServerEditor(this);

    this->serverList->setWidget(serverListContent);
    this->serverList->setWidgetResizable(true);

    refreshServerList();
    createLayout();

    connect(addServerButton, &QPushButton::clicked, this, &CapiServerChooser::addServer);
}

CapiServerChooser::~CapiServerChooser() = default;

void CapiServerChooser::addServer() {
    auto dialog = new AddServerDialog(&serverStorage, this);
    dialog->open();

    connect(dialog, &QDialog::accepted, this, &CapiServerChooser::refreshServerList);
    connect(dialog, &QDialog::finished, dialog, &QDialog::deleteLater);
}

void CapiServerChooser::deleteServer(const QString& uuid) {
    serverStorage.deleteServer(uuid);
    refreshServerList();
}

void CapiServerChooser::createLayout() {
    auto boxLayout = new QVBoxLayout();
    boxLayout->addWidget(new QLabel(tr("Saved servers:"), this));
    boxLayout->addWidget(serverList);
    boxLayout->addWidget(customServerEditor);

    setLayout(boxLayout);
}

void CapiServerChooser::refreshServerList() {
    for (auto serverInfo : serverInfos) {
        delete serverInfo;
    }
    serverInfos.clear();

    if (serverListContent->layout()) {
        delete (serverListContent->layout());
    }
    auto boxLayout = new QVBoxLayout();
    this->serverListContent->setLayout(boxLayout);

    boxLayout->addWidget(addServerButton);

    for (auto server : serverStorage.getServers()) {
        auto serverInfo = new CapiServerInfo(server, this);
        serverInfos.append(serverInfo);
        boxLayout->addWidget(serverInfo);

        connect(serverInfo, &CapiServerInfo::connectionRequested, client, &CapiClient::connectToServer);
        connect(serverInfo, &CapiServerInfo::deleted, this, &CapiServerChooser::deleteServer);
    }

    boxLayout->addStretch(1);
}
