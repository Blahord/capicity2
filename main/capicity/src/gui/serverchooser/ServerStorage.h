#ifndef CAPICITY2_SERVERSTORAGE_H
#define CAPICITY2_SERVERSTORAGE_H

#include <QStandardPaths>

#include "CapiCityServer.h"

class ServerStorage {

    public:
        explicit ServerStorage();
        virtual ~ServerStorage();

        QList<CapiCityServer*> getServers();
        void addNewServer(const QString& name, const QString& host, quint16 port);
        void deleteServer(const QString& uuid);

    private:
        void readServers();
        void writeServers();

        QList<CapiCityServer*> servers;
        QString dataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
};


#endif //CAPICITY2_SERVERSTORAGE_H
