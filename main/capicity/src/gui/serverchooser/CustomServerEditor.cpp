#include "CustomServerEditor.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QToolButton>

#include "constants.h"

CustomServerEditor::CustomServerEditor(QWidget* parent) : QFrame(parent) {
    this->addressEditor = new QLineEdit(this);
    this->portEditor = new QSpinBox(this);
    this->connectButton = new QToolButton(this);
    this->connectButton->setIcon(QIcon::fromTheme("media-playback-start"));

    portEditor->setMinimum(0);
    portEditor->setMaximum(65536);
    portEditor->setValue(DEFAULT_SERVER_PORT);

    setFrameShape(QFrame::Panel);
    setFrameShadow(QFrame::Raised);

    createLayout();

    connect(connectButton, &QToolButton::clicked, this, &CustomServerEditor::emitConnectRequest);
}

CustomServerEditor::~CustomServerEditor() = default;

void CustomServerEditor::createLayout() {
    auto inputLayout = new QHBoxLayout();
    inputLayout->addWidget(new QLabel(tr("Address:"), this));
    inputLayout->addWidget(addressEditor);
    inputLayout->addWidget(new QLabel(tr("Port:"), this));
    inputLayout->addWidget(portEditor);
    inputLayout->addWidget(connectButton);

    auto layout = new QVBoxLayout();
    layout->addWidget(new QLabel(tr("Quick connect:"), this));
    layout->addLayout(inputLayout);

    setLayout(layout);
}

void CustomServerEditor::emitConnectRequest() {
    emit(connectionRequested(addressEditor->text(), (quint16) portEditor->value()));
}
