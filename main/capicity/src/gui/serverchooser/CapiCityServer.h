#ifndef CAPICITY2_CAPICITYSERVER_H
#define CAPICITY2_CAPICITYSERVER_H

#include <QString>

class CapiCityServer {

    public:
        CapiCityServer(QString uuid, QString name, QString host, quint16 port);
        virtual ~CapiCityServer();

        const QString& getUuid() const;
        const QString& getName() const;
        const QString& getHost() const;
        quint16 getPort() const;

    private:
        QString uuid;
        QString name;
        QString host;
        quint16 port = 1234;
};


#endif //CAPICITY2_CAPICITYSERVER_H
