#include "CapiCityWindow.h"

#include <QApplication>
#include <QMenuBar>
#include <QCloseEvent>
#include "settings/SettingsDialog.h"
#include "dock/PlayersDock.h"
#include "dock/ChatDock.h"

CapiCityWindow::CapiCityWindow(CapiClient* capiClient, QWidget* parent) : QMainWindow(parent) {
    this->client = capiClient;

    this->lobbyPlayerListDock = new PlayersDock(capiClient, this);
    this->chatListDock = new ChatDock(capiClient, this);

    this->serverChooser = new CapiServerChooser(client, this);
    this->lobby = new Lobby(client, this);
    this->gameView = new GameView(client, this);

    auto gameMenu = menuBar()->addMenu(tr("Capi City 2"));
    gameMenu->addAction(QIcon::fromTheme("configure", QIcon::fromTheme("preferences-other")), tr("Settings ..."), this, &CapiCityWindow::openSettings);
    gameMenu->addAction(QIcon::fromTheme("application-exit"), tr("Quit"), this, &CapiCityWindow::exit);

    createLayout();

    connect(client, &CapiClient::connected, this, &CapiCityWindow::showLobby);
    connect(client, &CapiClient::gameEntered, this, &CapiCityWindow::showGame);
    connect(client, &CapiClient::gameLeft, this, &CapiCityWindow::showLobby);
}

CapiCityWindow::~CapiCityWindow() = default;

void CapiCityWindow::reconnect() {
    client->reconnectToLastServer();
}

void CapiCityWindow::closeEvent(QCloseEvent* event) {
    event->accept();
    exit();
}

void CapiCityWindow::createLayout() {
    this->viewLayout = new QStackedLayout();
    viewLayout->addWidget(serverChooser);
    viewLayout->addWidget(lobby);
    viewLayout->addWidget(gameView);
    viewLayout->setCurrentWidget(serverChooser);

    auto centralWidget = new QWidget(this);
    centralWidget->setLayout(viewLayout);

    addDockWidget(Qt::LeftDockWidgetArea, lobbyPlayerListDock);
    addDockWidget(Qt::RightDockWidgetArea, chatListDock);

    setCentralWidget(centralWidget);
}

void CapiCityWindow::showLobby() {
    viewLayout->setCurrentWidget(lobby);
    gameView->reset();
}

void CapiCityWindow::showGame() {
    viewLayout->setCurrentWidget(gameView);
}

void CapiCityWindow::openSettings() {
    auto dialog = new SettingsDialog(client, this);
    dialog->open();

    connect(dialog, &QDialog::finished, dialog, &QDialog::deleteLater);
}

void CapiCityWindow::exit() {
    if (client->getClientManager()->getOwnPlayer()) {
        client->sendCommand("server", "quit", {});
    }

    settings.setReconnectToken("");

    QApplication::quit();
}
