#include "PlayersDock.h"

#include <QVBoxLayout>

#include "../game/GamePlayerInfo.h"
#include "../lobby/LobbyPlayerInfo.h"

PlayersDock::PlayersDock(CapiClient* client, QWidget* parent) : CapiDockWidget(tr("Players"), client, parent) {
    this->playersContent = new QWidget();

    this->playerList = new QScrollArea(this);
    this->playerList->setWidget(playersContent);
    this->playerList->setWidgetResizable(true);
    this->playerList->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

    auto layout = new QVBoxLayout();
    layout->addWidget(playerList);
    setDockLayout(layout);

    timer = new QTimer();
    timer->start(750);

    connect(client, &CapiClient::newPlayerList, this, &PlayersDock::updatePlayerList);
    connect(client, &CapiClient::gameUpdate, this, &PlayersDock::updatePlayerList);
    connect(client, &CapiClient::gameEntered, this, &PlayersDock::updatePlayerList);
    connect(client, &CapiClient::gameLeft, this, &PlayersDock::updatePlayerList);
    connect(timer, &QTimer::timeout, this, &PlayersDock::tickPlayerInfos);
}

PlayersDock::~PlayersDock() = default;

void PlayersDock::updatePlayerList() {
    for (PlayerInfo* playerInfo : playerInfos) {
        delete (playerInfo);
    }
    playerInfos.clear();

    if (playerList->layout()) {
        delete (playerList->layout());
    }
    auto boxLayout = new QVBoxLayout();
    playerList->setLayout(boxLayout);

    auto ownGame = getClient()->getClientManager()->getOwnGame();

    if (ownGame) {
        createGamePlayersList(boxLayout);
    } else {
        createLobbyPlayersList(boxLayout);
    }

    boxLayout->addStretch(1);
}

void PlayersDock::createLobbyPlayersList(QLayout* layout) {
    auto clientManager = getClient()->getClientManager();
    auto ownPlayerInfo = new LobbyPlayerInfo(clientManager->getOwnPlayer(), getClient(), this);
    playerInfos.append(ownPlayerInfo);
    layout->addWidget(ownPlayerInfo);

    for (auto player : clientManager->getPlayers()) {
        if (player == clientManager->getOwnPlayer() || clientManager->findGame(player)) {
            continue;
        }

        auto playerInfo = new LobbyPlayerInfo(player, getClient(), this);
        playerInfos.append(playerInfo);
        layout->addWidget(playerInfo);
    }
}

void PlayersDock::createGamePlayersList(QLayout* layout) {
    for (auto gamePlayer : getClient()->getClientManager()->getOwnGame()->getPlayers()) {
        auto playerInfo = new GamePlayerInfo(gamePlayer, getClient(), this);
        playerInfos.append(playerInfo);
        layout->addWidget(playerInfo);
    }
}

void PlayersDock::tickPlayerInfos() {
    for (auto playerInfo : playerInfos) {
        playerInfo->tick(tickCount);
    }

    tickCount++;
}
