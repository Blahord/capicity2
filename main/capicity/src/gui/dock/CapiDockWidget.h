#ifndef CAPICITY2_CAPIDOCKWIDGET_H
#define CAPICITY2_CAPIDOCKWIDGET_H

#include <QDockWidget>

#include "../../client/CapiClient.h"

class CapiDockWidget : public QDockWidget {

    public:
        explicit CapiDockWidget(const QString& title, CapiClient* client, QWidget* parent = nullptr);
        ~CapiDockWidget() override;

        void setTitleVisible(bool visible);

    protected:
        void setDockLayout(QLayout* layout);
        CapiClient* getClient() const;

    private:
        CapiClient* client = nullptr;

        QWidget* dummyTitle = nullptr;
        QWidget* dockWidget = nullptr;
};


#endif //CAPICITY2_CAPIDOCKWIDGET_H
