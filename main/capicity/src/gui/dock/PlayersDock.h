#ifndef CAPICITY2_PLAYERSDOCK_H
#define CAPICITY2_PLAYERSDOCK_H

#include <QScrollArea>
#include <QTimer>

#include "CapiDockWidget.h"
#include "../PlayerInfo.h"
#include "../../client/CapiClient.h"

class PlayersDock : public CapiDockWidget {

    public:
        explicit PlayersDock(CapiClient* client, QWidget* parent = nullptr);
        ~PlayersDock() override;

    private:
        void updatePlayerList();
        void createLobbyPlayersList(QLayout* layout);
        void createGamePlayersList(QLayout* layout);

        void tickPlayerInfos();

        QScrollArea* playerList;
        QWidget* playersContent = nullptr;

        QList<PlayerInfo*> playerInfos;

        QTimer* timer = nullptr;
        long tickCount = 0;
};


#endif //CAPICITY2_PLAYERSDOCK_H
