#include "CapiDockWidget.h"

CapiDockWidget::CapiDockWidget(const QString& title, CapiClient* client, QWidget* parent) : QDockWidget(parent) {
    this->client = client;

    this->dockWidget = new QWidget(this);
    this->dummyTitle = new QWidget(this);

    setWindowTitle(title);

    setWidget(dockWidget);

    setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
}

CapiDockWidget::~CapiDockWidget() = default;

void CapiDockWidget::setTitleVisible(bool visible) {
    setTitleBarWidget(visible ? nullptr : dummyTitle);
}

void CapiDockWidget::setDockLayout(QLayout* layout) {
    dockWidget->setLayout(layout);
}

CapiClient* CapiDockWidget::getClient() const {
    return client;
}
