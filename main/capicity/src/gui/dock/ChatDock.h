#ifndef CAPICITY2_CHATDOCK_H
#define CAPICITY2_CHATDOCK_H

#include <QTextBrowser>
#include <QLineEdit>
#include <QPushButton>

#include "CapiDockWidget.h"

class ChatDock : public CapiDockWidget {

    public:
        explicit ChatDock(CapiClient* client, QWidget* parent);
        ~ChatDock() override;

    private:
        void createLayout();

        void sendChatMessage();
        void appendChat(const std::shared_ptr<CapiPlayer>& sender, const QString& message);

        void appendConnectionLost(const std::shared_ptr<CapiPlayer>& player);
        void appendReconnected(const std::shared_ptr<CapiPlayer>& player);

        QTextBrowser* chatDisplay = nullptr;
        QLineEdit* inputLine = nullptr;
        QPushButton* sendButton = nullptr;
};


#endif //CAPICITY2_CHATDOCK_H
