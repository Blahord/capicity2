#include "ChatDock.h"

#include <QHBoxLayout>

ChatDock::ChatDock(CapiClient* client, QWidget* parent) : CapiDockWidget(tr("Chat"), client, parent) {
    this->chatDisplay = new QTextBrowser(this);
    this->inputLine = new QLineEdit(this);
    this->sendButton = new QPushButton(QWidget::tr("Send"), this);

    this->chatDisplay->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    this->inputLine->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

    createLayout();

    connect(client, &CapiClient::newChatMessage, this, &ChatDock::appendChat);
    connect(client, &CapiClient::playerConnectionLost, this, &ChatDock::appendConnectionLost);
    connect(client, &CapiClient::playerReconnected, this, &ChatDock::appendReconnected);
    connect(sendButton, &QPushButton::clicked, this, &ChatDock::sendChatMessage);
}

ChatDock::~ChatDock() = default;

void ChatDock::createLayout() {
    QLayout* sendLineLayout = new QHBoxLayout();
    sendLineLayout->addWidget(inputLine);
    sendLineLayout->addWidget(sendButton);

    auto layout = new QVBoxLayout();
    layout->addWidget(chatDisplay);
    layout->addLayout(sendLineLayout);

    setDockLayout(layout);
}

void ChatDock::sendChatMessage() {
    getClient()->sendCommand(
        "server", "chat",
        {{"message", inputLine->text()}}
    );

    inputLine->clear();
}

void ChatDock::appendChat(const std::shared_ptr<CapiPlayer>& sender, const QString& message) {
    chatDisplay->append(sender->getName() + ": " + message);
}

void ChatDock::appendConnectionLost(const std::shared_ptr<CapiPlayer>& player) {
    chatDisplay->append(tr("Connection to player '%1' lost").arg(player->getName()));
}

void ChatDock::appendReconnected(const std::shared_ptr<CapiPlayer>& player) {
    chatDisplay->append(tr("Player '%1' has reconnected").arg(player->getName()));
}
