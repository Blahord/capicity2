#ifndef CAPICITY2_CAPICITYWINDOW_H
#define CAPICITY2_CAPICITYWINDOW_H

#include <QMainWindow>
#include <QStackedLayout>

#include "game/GameView.h"
#include "serverchooser/CapiServerChooser.h"
#include "lobby/Lobby.h"
#include "../client/CapiClient.h"

class CapiCityWindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit CapiCityWindow(CapiClient* capiClient, QWidget* parent = nullptr);
        ~CapiCityWindow() override;

        void reconnect();

    protected:
        void closeEvent(QCloseEvent* event) override;

    private:
        void createLayout();

        void showLobby();
        void showGame();

        void openSettings();
        void exit();

        CapiCitySettings settings;

        CapiClient* client = nullptr;

        QDockWidget* lobbyPlayerListDock = nullptr;
        QDockWidget* chatListDock = nullptr;

        QStackedLayout* viewLayout = nullptr;
        CapiServerChooser* serverChooser = nullptr;
        Lobby* lobby = nullptr;
        GameView* gameView = nullptr;
};


#endif //CAPICITY2_CAPICITYWINDOW_H
