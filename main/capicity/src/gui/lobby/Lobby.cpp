#include "Lobby.h"

#include <QHBoxLayout>

Lobby::Lobby(CapiClient* client, QWidget* parent) : QWidget(parent) {
    this->client = client;

    this->gamesList = new QScrollArea(this);
    this->gameListContent = new QWidget(this);

    this->gamesList->setWidget(gameListContent);
    gamesList->setWidgetResizable(true);

    this->gamesList->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

    createLayout();

    connect(client, &CapiClient::newPlayerList, this, &Lobby::updateGameList);
    connect(client, &CapiClient::newGameTemplateList, this, &Lobby::updateGameList);
    connect(client, &CapiClient::newGameList, this, &Lobby::updateGameList);
}

Lobby::~Lobby() = default;


void Lobby::createLayout() {
    setLayout(new QHBoxLayout());

    layout()->addWidget(gamesList);
}

void Lobby::updateGameList() {
    for (auto templateInfo : gameTemplateInfos) {
        delete templateInfo;
    }
    gameTemplateInfos.clear();

    for (auto gameInfo : gameInfos) {
        delete gameInfo;
    }
    gameInfos.clear();

    if (gameListContent->layout()) {
        delete gameListContent->layout();
    }

    auto clientManager = client->getClientManager();
    auto layout = new QVBoxLayout();
    for (auto gameTemplate : clientManager->getGameTemplates()) {
        auto templateInfo = new GameTemplateInfo(gameTemplate, client, this);
        gameTemplateInfos.append(templateInfo);
        layout->addWidget(templateInfo);
    }
    for (auto game : clientManager->getGames()) {
        auto gameInfo = new GameInfo(game, client, this);
        gameInfos.append(gameInfo);
        layout->addWidget(gameInfo);
    }

    layout->addStretch(1);

    gameListContent->setLayout(layout);
}
