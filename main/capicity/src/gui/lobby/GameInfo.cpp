#include "GameInfo.h"

#include <QHBoxLayout>

#include "../widgets/PlayerAvatarLabel.h"

GameInfo::GameInfo(const std::shared_ptr<CapiCityGame>& game, CapiClient* client, QWidget* parent) : QFrame(parent) {
    this->game = game;
    this->client = client;

    this->titleLabel = new QLabel(tr("%1 by %2").arg(game->getGameTemplate()->getName(), game->getOwner()->getName()), this);
    this->joinButton = new QToolButton(this);
    this->joinButton->setIcon(QIcon::fromTheme("media-playback-start"));
    this->joinButton->setToolTip(tr("Join"));

    this->setFrameShape(QFrame::Panel);
    this->setFrameShadow(QFrame::Raised);

    createLayout();

    connect(joinButton, &QToolButton::clicked, this, &GameInfo::joinGame);
}

GameInfo::~GameInfo() = default;

void GameInfo::createLayout() {
    auto infoLineLayout = new QHBoxLayout();
    infoLineLayout->addWidget(titleLabel);
    infoLineLayout->addStretch(1);
    infoLineLayout->addWidget(joinButton);

    auto playersLayout = new QHBoxLayout();
    playersLayout->addWidget(new QLabel(tr("Players: "), this));
    for (const auto& gamePlayer : game->getPlayers()) {
        auto playerLabel = new PlayerAvatarLabel(gamePlayer->getPlayer(), client, this);
        playerLabel->enableToolTip();
        playersLayout->addWidget(playerLabel);
    }

    for (int i = game->getPlayers().size(); i < game->getGameTemplate()->getMaxPlayers(); i++) {
        auto emptySlotLabel = new AvatarLabel(this);
        emptySlotLabel->setAvatar(QPixmap(":avatars/empty.png"));
        emptySlotLabel->setEnabled(false);
        playersLayout->addWidget(emptySlotLabel);
    }

    playersLayout->addStretch(1);

    auto layout = new QVBoxLayout();
    layout->addLayout(infoLineLayout);
    layout->addLayout(playersLayout);
    setLayout(layout);
}

void GameInfo::joinGame() {
    client->sendCommand("server", "joinGame", {{"id", game->getId()}});
}
