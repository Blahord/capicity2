#include "GameTemplateInfo.h"

#include <QVBoxLayout>

GameTemplateInfo::GameTemplateInfo(const std::shared_ptr<CapiCityGameTemplate>& gameTemplate, CapiClient* client, QWidget* parent) : QFrame(parent) {
    setFrameShape(QFrame::Panel);
    setFrameShadow(QFrame::Raised);

    this->gameTemplate = gameTemplate;
    this->client = client;

    this->nameLabel = new QLabel(gameTemplate->getName(), this);
    this->descriptionLabel = new QLabel(gameTemplate->getDescription(), this);
    this->numPlayersRangeLabel = new QLabel(
        QString("%1 - %2")
            .arg(gameTemplate->getMinPlayers())
            .arg(gameTemplate->getMaxPlayers()),
        this
    );

    this->createButton = new QToolButton();
    createButton->setIcon(QIcon::fromTheme("document-new"));
    createButton->setToolTip(tr("Create game of this type"));

    createLayout();

    connect(createButton, &QToolButton::clicked, this, &GameTemplateInfo::createGame);
}

GameTemplateInfo::~GameTemplateInfo() = default;

void GameTemplateInfo::createLayout() {
    auto nameAndDescriptionLayout = new QVBoxLayout();
    nameAndDescriptionLayout->addWidget(nameLabel);
    nameAndDescriptionLayout->addWidget(descriptionLabel);

    auto mainLayout = new QHBoxLayout();
    mainLayout->addLayout(nameAndDescriptionLayout);
    mainLayout->addStretch(1);
    mainLayout->addWidget(numPlayersRangeLabel);
    mainLayout->addWidget(createButton);

    setLayout(mainLayout);
}

void GameTemplateInfo::createGame() {
    client->sendCommand(
        "server", "createGame",
        {
            {"type", gameTemplate->getId()}
        }
    );
}
