#ifndef CAPICITY2_LOBBY_H
#define CAPICITY2_LOBBY_H

#include <QWidget>
#include <QScrollArea>
#include <QTableWidget>

#include "LobbyPlayerInfo.h"
#include "GameTemplateInfo.h"
#include "GameInfo.h"
#include "../../client/CapiClient.h"

class Lobby : public QWidget {
    Q_OBJECT

    public:
        explicit Lobby(CapiClient* client, QWidget* parent = nullptr);
        ~Lobby() override;

    private:
        void createLayout();
        void updateGameList();

        CapiClient* client = nullptr;

        QScrollArea* gamesList = nullptr;
        QWidget* gameListContent = nullptr;

        QList<GameTemplateInfo*> gameTemplateInfos;
        QList<GameInfo*> gameInfos;
};


#endif //CAPICITY2_LOBBY_H
