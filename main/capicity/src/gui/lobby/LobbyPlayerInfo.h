#ifndef CAPICITY2_LOBBYPLAYERINFO_H
#define CAPICITY2_LOBBYPLAYERINFO_H

#include <QFrame>
#include <QLabel>

#include "CapiPlayer.h"
#include "../widgets/PlayerAvatarLabel.h"
#include "../PlayerInfo.h"
#include "../../client/CapiClient.h"

class LobbyPlayerInfo : public QFrame, public PlayerInfo {
    Q_OBJECT // NOLINT(altera-struct-pack-align)

    public:
        explicit LobbyPlayerInfo(const std::shared_ptr<CapiPlayer>& player, CapiClient* client, QWidget* parent = nullptr);
        ~LobbyPlayerInfo() override;

        void tick(long tickCount) override;

    private:
        PlayerAvatarLabel* avatarLabel = nullptr;

        ImageStorage imageStorage;
};


#endif //CAPICITY2_LOBBYPLAYERINFO_H
