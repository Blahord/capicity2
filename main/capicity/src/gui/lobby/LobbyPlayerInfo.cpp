#include "LobbyPlayerInfo.h"

#include <QHBoxLayout>
#include <QPainter>
#include <QCryptographicHash>

LobbyPlayerInfo::LobbyPlayerInfo(const std::shared_ptr<CapiPlayer>& player, CapiClient* client, QWidget* parent) : QFrame(parent) {
    this->avatarLabel = new PlayerAvatarLabel(player, client, this);

    auto playerNameLabel = new QLabel(player->getName(), this);

    setLayout(new QHBoxLayout());
    layout()->addWidget(avatarLabel);
    layout()->addWidget(playerNameLabel);

    setFrameShape(QFrame::StyledPanel);
    setFrameShadow(QFrame::Raised);
}

LobbyPlayerInfo::~LobbyPlayerInfo() = default;

void LobbyPlayerInfo::tick(long tickCount) {
    avatarLabel->tick(tickCount);
}
