#ifndef CAPICITY2_GAMETEMPLATEINFO_H
#define CAPICITY2_GAMETEMPLATEINFO_H

#include <QFrame>
#include <QLabel>
#include <QToolButton>

#include "../../client/CapiClient.h"

class GameTemplateInfo : public QFrame {

    public:
        explicit GameTemplateInfo(const std::shared_ptr<CapiCityGameTemplate>& gameTemplate, CapiClient* client, QWidget* parent = nullptr);
        ~GameTemplateInfo() override;

    private:
        void createLayout();

        void createGame();

        std::shared_ptr<CapiCityGameTemplate> gameTemplate;
        CapiClient* client = nullptr;

        QLabel* nameLabel = nullptr;
        QLabel* descriptionLabel = nullptr;
        QLabel* numPlayersRangeLabel = nullptr;
        QToolButton* createButton = nullptr;
};


#endif //CAPICITY2_GAMETEMPLATEINFO_H
