#ifndef CAPICITY2_GAMEINFO_H
#define CAPICITY2_GAMEINFO_H

#include <QFrame>
#include <QLabel>
#include <QToolButton>

#include "../../client/CapiClient.h"

class GameInfo : public QFrame {

    public:
        explicit GameInfo(const std::shared_ptr<CapiCityGame>& game, CapiClient* client, QWidget* parent = nullptr);
        ~GameInfo() override;

    private:
        void createLayout();
        void joinGame();

        CapiClient* client = nullptr;
        std::shared_ptr<CapiCityGame> game;

        QLabel* titleLabel = nullptr;
        QToolButton* joinButton = nullptr;
};


#endif //CAPICITY2_GAMEINFO_H
