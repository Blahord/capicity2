#ifndef CAPICITY2_IMAGESTORAGE_H
#define CAPICITY2_IMAGESTORAGE_H

#include <QString>
#include <QPixmap>

#include "CapiPlayer.h"

class ImageStorage {

    public:
        ImageStorage();
        virtual ~ImageStorage();

        bool containsImage(const QString& hash);
        bool containsShadow(const QString& hash);

        QPixmap getImage(const QString& hash);
        QPixmap getShadow(const QString& hash);

        static QPixmap createDefaultAvatar(const QString& playerName);

        QString storeImage(const QImage& image);

        static QPixmap scalePixmap(const QPixmap& pixmap, int size);

    private:
        QString getFilePath(const QString& hash);
        QString getShadowPath(const QString& hash);
        static QString hash(const QImage& image);

        void createShadow(const QString& hash);

        static QColor getDefaultAvatarBackgroundColor(const QString& playerName);

        QString imagePath;
};


#endif //CAPICITY2_IMAGESTORAGE_H
