#include "CapiCitySettings.h"

QString CapiCitySettings::KEY_PLAYER_NAME = "player/name"; // NOLINT(cert-err58-cpp)
QString CapiCitySettings::KEY_PLAYER_USE_AVATAR = "player/useAvatar"; // NOLINT(cert-err58-cpp)
QString CapiCitySettings::KEY_ANIMATION_ACTIVE = "animation/enabled"; // NOLINT(cert-err58-cpp)
QString CapiCitySettings::KEY_ANIMATION_SPEED = "animation/speed"; // NOLINT(cert-err58-cpp)
QString CapiCitySettings::KEY_CLIENT_LAST_SERVER = "client/lastServer"; // NOLINT(cert-err58-cpp)
QString CapiCitySettings::KEY_CLIENT_LAST_PORT = "client/lastPort"; // NOLINT(cert-err58-cpp)
QString CapiCitySettings::KEY_CLIENT_RECONNECT_TOKEN = "client/reconnectToken"; // NOLINT(cert-err58-cpp)

// Getter

QString CapiCitySettings::getPlayerName() {
    return settings.value(KEY_PLAYER_NAME, "Capi City Player").toString();
}

bool CapiCitySettings::isUseAvatar() {
    return settings.value(KEY_PLAYER_USE_AVATAR, false).toBool();
}

bool CapiCitySettings::isAnimationEnabled() {
    return settings.value(KEY_ANIMATION_ACTIVE, true).toBool();
}

int CapiCitySettings::getAnimationSpeed() {
    return settings.value(KEY_ANIMATION_SPEED, 2000).toInt();
}

QPixmap CapiCitySettings::getAvatarPixmap() {
    QPixmap avatar(dataPath + "/avatar.png");
    if (avatar.isNull()) {
        return {":avatars/default.png"};
    }

    return avatar;
}

QImage CapiCitySettings::getAvatarImage() {
    QImage avatar(dataPath + "/avatar.png");
    if (avatar.isNull()) {
        return QImage(":avatars/default.png");
    }

    return avatar;
}

QString CapiCitySettings::getLastServer() {
    return settings.value(KEY_CLIENT_LAST_SERVER, "").toString();
}

quint16 CapiCitySettings::getLastServerPort() {
    return settings.value(KEY_CLIENT_LAST_PORT).toUInt();
}

QString CapiCitySettings::getPlayerToken() {
    return settings.value(KEY_CLIENT_RECONNECT_TOKEN, "").toString();
}

// Setter

void CapiCitySettings::setPlayerName(const QString& name) {
    settings.setValue(KEY_PLAYER_NAME, name);
}

void CapiCitySettings::setUseAvatar(bool avatarActive) {
    settings.setValue(KEY_PLAYER_USE_AVATAR, avatarActive);
}

void CapiCitySettings::storeAvatar(const QPixmap& avatar) {
    QPixmap stored = avatar;
    if (avatar.width() > 256 || avatar.height() > 256) {
        stored = avatar.scaled(256, 256, Qt::KeepAspectRatio);
    }
    if (avatar.width() < 16 || avatar.height() < 16) {
        stored = avatar.scaled(16, 16, Qt::KeepAspectRatioByExpanding);
    }

    stored.save(dataPath + "/avatar.png");
}

void CapiCitySettings::setAnimationEnabled(bool value) {
    settings.setValue(KEY_ANIMATION_ACTIVE, value);
}

void CapiCitySettings::setAnimationSpeed(int value) {
    settings.setValue(KEY_ANIMATION_SPEED, value);
}

void CapiCitySettings::setLastServer(const QString& host) {
    settings.setValue(KEY_CLIENT_LAST_SERVER, host);
}

void CapiCitySettings::setLastServerPort(quint16 port) {
    settings.setValue(KEY_CLIENT_LAST_PORT, port);
}

void CapiCitySettings::setReconnectToken(const QString& token) {
    settings.setValue(KEY_CLIENT_RECONNECT_TOKEN, token);
}
