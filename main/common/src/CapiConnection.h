#ifndef CAPICITY2_CAPICONNECTION_H
#define CAPICITY2_CAPICONNECTION_H

#include <QObject>
#include <QAbstractSocket>
#include <QJsonObject>

using std::unique_ptr;

class CapiConnection : public QObject {
    Q_OBJECT

    public:
        explicit CapiConnection(QAbstractSocket* socket, int maxBufferSize, QObject* parent = nullptr);
        explicit CapiConnection(QAbstractSocket* socket, QObject* parent = nullptr);
        ~CapiConnection() override;

        void send(const QJsonObject& data);

    signals:
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NotImplementedFunctions"
        void disconnected();
        void gotMessage(const QJsonObject& command);
#pragma clang diagnostic pop

    private slots:
        void readData();

    private:
        void readCharacters(const QString& characters);
        void emitGotMessage();
        void resetMessageRead();

        QAbstractSocket* socket;
        unique_ptr<QTextDecoder> decoder;

        //reading data
        int maxBufferSize = 1024 * 1024; //1MiB

        int jsonBracesOpen = 0;
        bool charMasked = false;
        bool inTextLiteral = false;
        QString lastData;
};


#endif //CAPICITY2_CAPICONNECTION_H
