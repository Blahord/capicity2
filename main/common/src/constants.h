#ifndef CAPICITY2_CONSTANTS_H
#define CAPICITY2_CONSTANTS_H

#include <QtGlobal>
#include <QList>
#include <QRegularExpression>

const quint16 DEFAULT_SERVER_PORT = 1234;

const QList<int> ALLOWED_AVATAR_SIZES({32, 64, 128});

const QRegularExpression AVATAR_DATA_REGEXP("^[0-9a-f]+$");

#endif //CAPICITY2_CONSTANTS_H
