#include "CapiGameTemplate.h"

CapiGameTemplate::CapiGameTemplate(const QString& id) {
    this->id = id;
}

CapiGameTemplate::~CapiGameTemplate() = default;

const QString& CapiGameTemplate::getId() const {
    return id;
}

int CapiGameTemplate::getMinPlayers() const {
    return minPlayers;
}

void CapiGameTemplate::setMinPlayers(int minPlayers) {
    CapiGameTemplate::minPlayers = minPlayers;
}

int CapiGameTemplate::getMaxPlayers() const {
    return maxPlayers;
}

void CapiGameTemplate::setMaxPlayers(int maxPlayers) {
    CapiGameTemplate::maxPlayers = maxPlayers;
}

const QList<QString>& CapiGameTemplate::getLanguages() const {
    return languages;
}

void CapiGameTemplate::setLanguages(const QList<QString>& languages) {
    CapiGameTemplate::languages = languages;
}

const QString& CapiGameTemplate::getDefaultLanguage() const {
    return defaultLanguage;
}

void CapiGameTemplate::setDefaultLanguage(const QString& defaultLanguage) {
    CapiGameTemplate::defaultLanguage = defaultLanguage;
}
