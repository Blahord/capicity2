#ifndef CAPICITY2_CAPIGAME_H
#define CAPICITY2_CAPIGAME_H

#include <QString>
#include <QList>

#include "CapiGamePlayer.h"
#include "CapiGameTemplate.h"
#include "TurnState.h"
#include "../GameState.h"

class CapiGame {

    public:
        explicit CapiGame(QString id);
        virtual ~CapiGame();

        //Data

        const QString& getId() const;

        const QString& getBgColor() const;
        void setBgColor(const QString& bgColor);

        //Players

        const QList<std::shared_ptr<CapiGamePlayer>>& getPlayers() const;
        bool containsPlayer(const std::shared_ptr<CapiPlayer>& player) const;
        std::shared_ptr<CapiGamePlayer> findPlayer(const std::shared_ptr<CapiPlayer>& player) const;
        std::shared_ptr<CapiGamePlayer> findPlayer(const QString& id) const;
        void addPlayer(const std::shared_ptr<CapiPlayer>& player);
        void removePlayer(const std::shared_ptr<CapiPlayer>& player);

        std::shared_ptr<CapiPlayer> getOwner() const;
        void setOwner(const std::shared_ptr<CapiPlayer>& owner);

        //Configuration

        int getMaxHouses() const;
        void setMaxHouses(int maxHouses);

        int getMaxHotels() const;
        void setMaxHotels(int maxHotels);

        int getStartMoney() const;
        void setStartMoney(int startMoney);

        bool isCollectFine() const;
        void setCollectFine(bool collectFine);

        bool isShuffleCards() const;
        void setShuffleCards(bool shuffleCards);

        bool isAuctions() const;
        void setAuctions(bool auctions);

        bool isDoubleMoneyOnExactLanding() const;
        void setDoubleMoneyOnExactLanding(bool doubleMoneyOnExactLanding);

        bool isUnlimitedHouses() const;
        void setUnlimitedHouses(bool unlimitedHouses);

        bool isJailNoRent() const;
        void setJailNoRent(bool jailNoRent);

        bool isAutoTax() const;
        void setAutoTax(bool autoTax);

        //State

        GameState getState() const;
        void setState(GameState state);

        TurnState getTurnState() const;
        void setTurnState(TurnState turnState);

        const QList<std::shared_ptr<CapiGamePlayer>>& getPlayerOrder() const;
        void setPlayerOrder(const QList<QString>& playerOrder);
        void removeFromPlayerOrder(const std::shared_ptr<CapiPlayer>& player);

    private:
        //Data

        QString id;

        QString bgColor;

        //Players

        QList<std::shared_ptr<CapiGamePlayer>> players;
        std::shared_ptr<CapiPlayer> owner;

        //Configuration

        int maxHouses = 0;
        int maxHotels = 0;
        int startMoney = 0;
        bool collectFine = false;
        bool shuffleCards = false;
        bool auctions = false;
        bool doubleMoneyOnExactLanding = false;
        bool unlimitedHouses = false;
        bool jailNoRent = false;
        bool autoTax = false;

        //State

        GameState state = GameState::CONFIG;
        TurnState turnState = TurnState::NOTHING;
        QList<std::shared_ptr<CapiGamePlayer>> playerOrder;
};

#endif //CAPICITY2_CAPIGAME_H
