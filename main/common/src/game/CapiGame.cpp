#include "CapiGame.h"

#include <utility>

CapiGame::CapiGame(QString id) :
    id(std::move(id)) {
}

CapiGame::~CapiGame() = default;

const QString& CapiGame::getId() const {
    return id;
}

const QString& CapiGame::getBgColor() const {
    return bgColor;
}

void CapiGame::setBgColor(const QString& bgColor) {
    this->bgColor = bgColor;
}

const QList<std::shared_ptr<CapiGamePlayer>>& CapiGame::getPlayers() const {
    return players;
}

bool CapiGame::containsPlayer(const std::shared_ptr<CapiPlayer>& player) const {
    return std::any_of(players.begin(), players.end(), [player](const std::shared_ptr<CapiGamePlayer>& gp) { return gp->getPlayer() == player; });
}

std::shared_ptr<CapiGamePlayer> CapiGame::findPlayer(const std::shared_ptr<CapiPlayer>& player) const {
    for (auto gamePlayer : players) {
        if (gamePlayer->getPlayer() == player) {
            return gamePlayer;
        }
    }

    return nullptr;
}

std::shared_ptr<CapiGamePlayer> CapiGame::findPlayer(const QString& id) const {
    for (auto gamePlayer : players) {
        if (gamePlayer->getPlayer()->getId() == id) {
            return gamePlayer;
        }
    }

    return nullptr;
}

void CapiGame::addPlayer(const std::shared_ptr<CapiPlayer>& player) {
    if (containsPlayer(player)) {
        return;
    }

    players.append(std::make_shared<CapiGamePlayer>(player));
}

void CapiGame::removePlayer(const std::shared_ptr<CapiPlayer>& player) {
    std::shared_ptr<CapiGamePlayer> const gamePlayer = findPlayer(player);

    if (gamePlayer) {
        players.removeAll(gamePlayer);
    }
}

std::shared_ptr<CapiPlayer> CapiGame::getOwner() const {
    return owner;
}

void CapiGame::setOwner(const std::shared_ptr<CapiPlayer>& owner) {
    CapiGame::owner = owner;
}

int CapiGame::getMaxHouses() const {
    return maxHouses;
}

void CapiGame::setMaxHouses(int maxHouses) {
    CapiGame::maxHouses = maxHouses;
}

int CapiGame::getMaxHotels() const {
    return maxHotels;
}

void CapiGame::setMaxHotels(int maxHotels) {
    CapiGame::maxHotels = maxHotels;
}

int CapiGame::getStartMoney() const {
    return startMoney;
}

void CapiGame::setStartMoney(int startMoney) {
    CapiGame::startMoney = startMoney;
}

bool CapiGame::isCollectFine() const {
    return collectFine;
}

void CapiGame::setCollectFine(bool collectFine) {
    CapiGame::collectFine = collectFine;
}

bool CapiGame::isShuffleCards() const {
    return shuffleCards;
}

void CapiGame::setShuffleCards(bool shuffleCards) {
    CapiGame::shuffleCards = shuffleCards;
}

bool CapiGame::isAuctions() const {
    return auctions;
}

void CapiGame::setAuctions(bool auctions) {
    CapiGame::auctions = auctions;
}

bool CapiGame::isDoubleMoneyOnExactLanding() const {
    return doubleMoneyOnExactLanding;
}

void CapiGame::setDoubleMoneyOnExactLanding(bool doubleMoneyOnExactLanding) {
    CapiGame::doubleMoneyOnExactLanding = doubleMoneyOnExactLanding;
}

bool CapiGame::isUnlimitedHouses() const {
    return unlimitedHouses;
}

void CapiGame::setUnlimitedHouses(bool unlimitedHouses) {
    CapiGame::unlimitedHouses = unlimitedHouses;
}

bool CapiGame::isJailNoRent() const {
    return jailNoRent;
}

void CapiGame::setJailNoRent(bool jailNoRent) {
    CapiGame::jailNoRent = jailNoRent;
}

bool CapiGame::isAutoTax() const {
    return autoTax;
}

void CapiGame::setAutoTax(bool autoTax) {
    CapiGame::autoTax = autoTax;
}

GameState CapiGame::getState() const {
    return state;
}

void CapiGame::setState(GameState state) {
    this->state = state;
}

TurnState CapiGame::getTurnState() const {
    return turnState;
}

void CapiGame::setTurnState(TurnState turnState) {
    CapiGame::turnState = turnState;
}

const QList<std::shared_ptr<CapiGamePlayer>>& CapiGame::getPlayerOrder() const {
    return playerOrder;
}

void CapiGame::setPlayerOrder(const QList<QString>& playerOrder) {
    QList<std::shared_ptr<CapiGamePlayer>> newPlayerList;

    for (const QString& playerId : playerOrder) {
        auto gamePlayer = findPlayer(playerId);
        if (gamePlayer) {
            newPlayerList.append(gamePlayer);
        }
    }

    this->playerOrder = newPlayerList;
}

void CapiGame::removeFromPlayerOrder(const std::shared_ptr<CapiPlayer>& player) {
    playerOrder.removeAll(findPlayer(player));
}

