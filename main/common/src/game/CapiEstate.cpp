#include "CapiEstate.h"

#include <utility>

CapiEstate::CapiEstate(QString id,
                       int price,
                       int rent0, int rent1, int rent2,
                       int rent3, int rent4, int rent5,
                       int taxAmount, int taxPercent, QString payTargetId) :
    id(std::move(id)),
    price(price),
    rent0(rent0), rent1(rent1), rent2(rent2),
    rent3(rent3), rent4(rent4), rent5(rent5),
    taxAmount(taxAmount), taxPercent(taxPercent),
    payTargetId(std::move(payTargetId)) {}

CapiEstate::~CapiEstate() = default;

const QString& CapiEstate::getId() const {
    return id;
}

int CapiEstate::getPrice() const {
    return price;
}

int CapiEstate::getRent0() const {
    return rent0;
}

int CapiEstate::getRent1() const {
    return rent1;
}

int CapiEstate::getRent2() const {
    return rent2;
}

int CapiEstate::getRent3() const {
    return rent3;
}

int CapiEstate::getRent4() const {
    return rent4;
}

int CapiEstate::getRent5() const {
    return rent5;
}

int CapiEstate::getTaxAmount() const {
    return taxAmount;
}

int CapiEstate::getTaxPercent() const {
    return taxPercent;
}

const QString& CapiEstate::getPayTargetId() const {
    return payTargetId;
}

const QString& CapiEstate::getImage() const {
    return image;
}

void CapiEstate::setImage(const QString& image) {
    CapiEstate::image = image;
}

std::shared_ptr<CapiGamePlayer> CapiEstate::getOwner() const {
    return owner;
}

void CapiEstate::setOwner(const std::shared_ptr<CapiGamePlayer>& owner) {
    CapiEstate::owner = owner;
}

int CapiEstate::getMoney() const {
    return money;
}

void CapiEstate::setMoney(int money) {
    CapiEstate::money = money;
}
