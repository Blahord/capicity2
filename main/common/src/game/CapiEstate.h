#ifndef CAPICITY2_CAPIESTATE_H
#define CAPICITY2_CAPIESTATE_H

#include <QString>

#include "CapiEstateGroup.h"
#include "CapiGamePlayer.h"

class CapiEstate {

    public:
        CapiEstate(QString id,
                   int price,
                   int rent0, int rent1, int rent2,
                   int rent3, int rent4, int rent5,
                   int taxAmount, int taxPercent, QString payTargetId);

        virtual ~CapiEstate();

        const QString& getId() const;
        int getPrice() const;
        int getRent0() const;
        int getRent1() const;
        int getRent2() const;
        int getRent3() const;
        int getRent4() const;
        int getRent5() const;
        int getTaxAmount() const;
        int getTaxPercent() const;
        const QString& getPayTargetId() const;

        const QString& getImage() const;
        void setImage(const QString& image);

        std::shared_ptr<CapiGamePlayer> getOwner() const;
        void setOwner(const std::shared_ptr<CapiGamePlayer>& owner);

        int getMoney() const;
        void setMoney(int money);

    private:
        QString id;
        int price;
        int rent0;
        int rent1;
        int rent2;
        int rent3;
        int rent4;
        int rent5;

        int taxAmount;
        int taxPercent;
        QString payTargetId;

        QString image;

        std::shared_ptr<CapiGamePlayer> owner;
        int money = 0;
};

#endif //CAPICITY2_CAPIESTATE_H
