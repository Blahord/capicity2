#include "CapiGamePlayer.h"

CapiGamePlayer::CapiGamePlayer(const std::shared_ptr<CapiPlayer>& player) {
    this->player = player;
}

std::shared_ptr<CapiPlayer> CapiGamePlayer::getPlayer() const {
    return player;
}

bool CapiGamePlayer::isReady() const {
    return ready;
}

void CapiGamePlayer::setReady(bool ready) {
    this->ready = ready;
}

int CapiGamePlayer::getPosition() const {
    return position;
}

void CapiGamePlayer::setPosition(int position) {
    this->position = position;
}

int CapiGamePlayer::getMoney() const {
    return money;
}

void CapiGamePlayer::setMoney(int money) {
    this->money = money;
}

bool CapiGamePlayer::isInJail() const {
    return inJail;
}

void CapiGamePlayer::setInJail(bool inJail) {
    this->inJail = inJail;
}

bool CapiGamePlayer::isMoved() const {
    return moved;
}

void CapiGamePlayer::setMoved(bool moved) {
    CapiGamePlayer::moved = moved;
}
