#ifndef CAPICITY2_CAPIGAMETEMPLATE_H
#define CAPICITY2_CAPIGAMETEMPLATE_H

#include <QString>
#include <QList>

class CapiGameTemplate {

    public:
        explicit CapiGameTemplate(const QString& id);
        virtual ~CapiGameTemplate();

        const QString& getId() const;

        int getMinPlayers() const;
        void setMinPlayers(int minPlayers);

        int getMaxPlayers() const;
        void setMaxPlayers(int maxPlayers);

        const QList<QString>& getLanguages() const;
        void setLanguages(const QList<QString>& languages);

        const QString& getDefaultLanguage() const;
        void setDefaultLanguage(const QString& defaultLanguage);

    private:
        QString id;
        int minPlayers = 2;
        int maxPlayers = 2;
        QList<QString> languages;
        QString defaultLanguage = "eng";
};

#endif //CAPICITY2_CAPIGAMETEMPLATE_H
