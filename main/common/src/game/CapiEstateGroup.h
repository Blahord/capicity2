#ifndef CAPICITY2_CAPIESTATEGROUP_H
#define CAPICITY2_CAPIESTATEGROUP_H

#include <QString>

class CapiEstateGroup {

    public:
        explicit CapiEstateGroup(const QString& id, const QString& color, int housePrice);
        virtual ~CapiEstateGroup();

        const QString& getId() const;
        const QString& getColor() const;
        int getHousePrice() const;

    private:
        QString id;
        QString color;

        int housePrice = 0;
};

#endif //CAPICITY2_CAPIESTATEGROUP_H
