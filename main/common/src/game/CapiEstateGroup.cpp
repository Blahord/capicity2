#include "CapiEstateGroup.h"

CapiEstateGroup::CapiEstateGroup(const QString& id, const QString& color, int housePrice) {
    this->id = id;
    this->color = color;
    this->housePrice = housePrice;
}

CapiEstateGroup::~CapiEstateGroup() = default;

const QString& CapiEstateGroup::getId() const {
    return id;
}

const QString& CapiEstateGroup::getColor() const {
    return color;
}

int CapiEstateGroup::getHousePrice() const {
    return housePrice;
}
