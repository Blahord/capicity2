#ifndef CAPICITY2_TURNSTATE_H
#define CAPICITY2_TURNSTATE_H

enum class TurnState {

    NOTHING, // Used to indicate that the game has currently no turn state
    ROLL,    // Wait for player on turn to roll
    MOVE,    // Wait for clients to finish movement animation
    TAX,     // Wait for player on turn to decide, to pay tax based on absolute or percentage
    BUY      // Player on turn can buy the estate
};

#endif //CAPICITY2_TURNSTATE_H
