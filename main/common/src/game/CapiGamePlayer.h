#ifndef CAPICITY2_CAPIGAMEPLAYER_H
#define CAPICITY2_CAPIGAMEPLAYER_H

#include <memory>
#include "../CapiPlayer.h"

class CapiGamePlayer {

    public:
        explicit CapiGamePlayer(const std::shared_ptr<CapiPlayer>& player);
        virtual ~CapiGamePlayer() = default;

        std::shared_ptr<CapiPlayer> getPlayer() const;

        bool isReady() const;
        void setReady(bool ready);

        int getPosition() const;
        void setPosition(int position);

        int getMoney() const;
        void setMoney(int money);

        bool isInJail() const;
        void setInJail(bool inJail);

        bool isMoved() const;
        void setMoved(bool moved);

    private:
        std::shared_ptr<CapiPlayer> player;

        bool ready = false;
        int money = 0;
        bool inJail = false;
        int position = 0;

        bool moved = false;
};

#endif //CAPICITY2_CAPIGAMEPLAYER_H
