#include "CapiPlayer.h"

#include <utility>

CapiPlayer::CapiPlayer(QString id) : id(std::move(id)) {
}

CapiPlayer::~CapiPlayer() = default;

const QString& CapiPlayer::getId() const {
    return id;
}

const QString& CapiPlayer::getName() const {
    return name;
}

void CapiPlayer::setName(const QString& name) {
    this->name = name;
}

const QString& CapiPlayer::getAvatarData() const {
    return avatarData;
}

void CapiPlayer::setAvatarData(const QString& avatarData) {
    CapiPlayer::avatarData = avatarData;
}

bool CapiPlayer::isConnected() const {
    return connected;
}

void CapiPlayer::setConnected(bool connected) {
    CapiPlayer::connected = connected;
}
