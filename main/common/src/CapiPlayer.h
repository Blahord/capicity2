#ifndef CAPICITY2_CAPIPLAYER_H
#define CAPICITY2_CAPIPLAYER_H

#include <QString>

class CapiPlayer {

    public:
        explicit CapiPlayer(QString id);
        virtual ~CapiPlayer();

        const QString& getId() const;

        const QString& getName() const;
        void setName(const QString& name);

        const QString& getAvatarData() const;
        void setAvatarData(const QString& avatarData);

        bool isConnected() const;
        void setConnected(bool connected);

    private:
        const QString id = "";
        QString name = "";
        QString avatarData = "";
        bool connected = true;
};


#endif //CAPICITY2_CAPIPLAYER_H
