#include "JsonNullValueCheck.h"

JsonNullValueCheck::JsonNullValueCheck() = default;

JsonNullValueCheck::~JsonNullValueCheck() = default;

void JsonNullValueCheck::check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) {
    if (!checkValue.isNull()) {
        errors.append(JsonError(
            path,
            "Must be null"
        ));
    }
}
