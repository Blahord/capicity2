#ifndef CAPICITY2_JSONARRAYCHECK_H
#define CAPICITY2_JSONARRAYCHECK_H

#include <initializer_list>
#include <QJsonArray>
#include <QList>
#include <QPair>

#include "JsonValueCheck.h"
#include "../JsonError.h"

class JsonArrayCheck {

    public:
        virtual ~JsonArrayCheck();

        virtual void check(const QJsonArray& checkValue, const QString& path, QList<JsonError>& errors) = 0;
};

#endif //CAPICITY2_JSONARRAYCHECK_H
