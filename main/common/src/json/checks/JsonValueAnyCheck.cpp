#include "JsonValueAnyCheck.h"

JsonValueAnyCheck::JsonValueAnyCheck(const QList<QPair<QString, std::shared_ptr<JsonValueCheck>>>& subChecks)
    : subChecks(subChecks) {}

JsonValueAnyCheck::~JsonValueAnyCheck() = default;


void JsonValueAnyCheck::check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) {
    QList<JsonError> localErrors;

    for (auto subCheck : subChecks) {
        QString localPath = path + QString("{%1}").arg(subCheck.first);
        localErrors.clear();
        subCheck.second->check(checkValue, localPath, localErrors);

        if (localErrors.isEmpty()) {
            return;
        }
    }

    errors.append(localErrors);
}
