#ifndef CAPICITY2_JSONOBJECTVALUECHECK_H
#define CAPICITY2_JSONOBJECTVALUECHECK_H

#include "JsonValueCheck.h"
#include "JsonObjectCheck.h"

class JsonObjectValueCheck : public JsonValueCheck {

    public:
        JsonObjectValueCheck(std::initializer_list<std::shared_ptr<JsonObjectCheck>> checks);
        ~JsonObjectValueCheck() override;

        void check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        QList<std::shared_ptr<JsonObjectCheck>> checks;
};


#endif //CAPICITY2_JSONOBJECTVALUECHECK_H
