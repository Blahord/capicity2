#include "JsonArrayContainsCheck.h"

JsonArrayContainsCheck::JsonArrayContainsCheck(JsonArrayContainsCheck::Type type, const QList<QPair<QString, shared_ptr<JsonValueCheck>>>& checks) :
    type(type), checks(checks) {}

JsonArrayContainsCheck::~JsonArrayContainsCheck() = default;

void JsonArrayContainsCheck::check(const QJsonArray& checkValue, const QString& path, QList<JsonError>& errors) {
    switch (type) {
        case Type::CONTAINS_ANY_ORDER:
            for (const auto& check : checks) {
                bool ok = false;
                for (int i = 0; i < checkValue.size(); i++) {
                    QList<JsonError> localErrors;
                    check.second->check(checkValue.at(i), QString("%1[%2]").arg(path).arg(i), localErrors);

                    if (localErrors.isEmpty()) {
                        ok = true;
                        break;
                    }
                }
                if (!ok) {
                    errors.append(JsonError(path, QString("Could not find match for '%1'").arg(check.first)));
                }
            }
            break;
        default:
            errors.append(JsonError(path, QString("BUG: Unhandled type %1").arg(static_cast<int>(type))));
    }
}
