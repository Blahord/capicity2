#ifndef CAPICITY2_JSONARRAYFORALLCHECK_H
#define CAPICITY2_JSONARRAYFORALLCHECK_H

#include "JsonArrayCheck.h"

using std::unique_ptr;

class JsonArrayForAllCheck : public JsonArrayCheck {

    public:
        explicit JsonArrayForAllCheck(unique_ptr<JsonValueCheck> subCheck);
        ~JsonArrayForAllCheck() override;

        void check(const QJsonArray& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        unique_ptr<JsonValueCheck> subCheck;
};


#endif //CAPICITY2_JSONARRAYFORALLCHECK_H
