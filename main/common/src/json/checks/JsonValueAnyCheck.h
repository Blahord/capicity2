#ifndef CAPICITY2_JSONVALUEANYCHECK_H
#define CAPICITY2_JSONVALUEANYCHECK_H

#include "JsonValueCheck.h"

class JsonValueAnyCheck : public JsonValueCheck {

    public:
        explicit JsonValueAnyCheck(const QList<QPair<QString, std::shared_ptr<JsonValueCheck>>>& subChecks);
        ~JsonValueAnyCheck() override;

        void check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        QList<QPair<QString, std::shared_ptr<JsonValueCheck>>> subChecks;
};

#endif //CAPICITY2_JSONVALUEANYCHECK_H
