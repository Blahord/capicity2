#ifndef CAPICITY2_JSONBOOLEANVALUECHECK_H
#define CAPICITY2_JSONBOOLEANVALUECHECK_H

#include "JsonBooleanCheck.h"
#include "JsonValueCheck.h"

using std::shared_ptr;

class JsonBooleanValueCheck : public JsonValueCheck {

    public:
        JsonBooleanValueCheck(std::initializer_list<shared_ptr<JsonBooleanCheck>> checks);
        ~JsonBooleanValueCheck() override;

        void check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        QList<shared_ptr<JsonBooleanCheck>> checks;
};


#endif //CAPICITY2_JSONBOOLEANVALUECHECK_H
