#include "JsonImageValueCheck.h"

#include <QJsonObject>

#include "JsonObjectValueCheck.h"
#include "JsonIntegerValueCheck.h"
#include "JsonStringValueCheck.h"
#include "JsonNullValueCheck.h"

JsonImageValueCheck::JsonImageValueCheck() {
    baseCheck = std::make_unique<JsonObjectValueCheck>(
        std::initializer_list<std::shared_ptr<JsonObjectCheck>>{
            std::make_shared<JsonObjectCheck>(
                "width", false,
                std::move(std::make_unique<JsonIntegerValueCheck>(
                    std::initializer_list<std::shared_ptr<JsonIntegerCheck>>{
                        std::make_shared<JsonIntegerCheck>(JsonIntegerCheck::Type::MIN, 16),
                        std::make_shared<JsonIntegerCheck>(JsonIntegerCheck::Type::MAX, 256)
                    }
                ))
            ),
            std::make_shared<JsonObjectCheck>(
                "height", false,
                std::move(std::make_unique<JsonIntegerValueCheck>(
                    std::initializer_list<std::shared_ptr<JsonIntegerCheck>>{
                        std::make_shared<JsonIntegerCheck>(JsonIntegerCheck::Type::MIN, 16),
                        std::make_shared<JsonIntegerCheck>(JsonIntegerCheck::Type::MAX, 256)
                    }
                ))
            ),
            std::make_shared<JsonObjectCheck>(
                "data", false,
                std::move(std::make_unique<JsonStringValueCheck>(JsonStringValueCheck({})))
            )
        }
    );


    allowedDataCharacters = QSet<QChar>(
        {
            QChar('0'), QChar('1'), QChar('2'), QChar('3'),
            QChar('4'), QChar('5'), QChar('6'), QChar('7'),
            QChar('8'), QChar('9'), QChar('a'), QChar('b'),
            QChar('c'), QChar('d'), QChar('e'), QChar('f')
        }
    );
}

JsonImageValueCheck::~JsonImageValueCheck() = default;

void JsonImageValueCheck::check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) {
    auto localErrors = QList<JsonError>();

    baseCheck->check(checkValue, path, localErrors);

    if (!localErrors.isEmpty()) {
        errors.append(localErrors);
        return;
    }

    auto checkObject = checkValue.toObject();

    int const width = checkObject.value("width").toInt();
    int const height = checkObject.value("height").toInt();
    QString const data = checkObject.value("data").toString();

    int const requiredDataLength = 8 * width * height;
    if (data.size() != requiredDataLength) {
        errors.append(JsonError(path + "/data", "Length of data mus be 8 * width * height"));
    }

    for (QChar const c : data) {
        if (!allowedDataCharacters.contains(c)) {
            errors.append(JsonError(path + "/data", "Contains illegal characters"));
            return;
        }
    }
}
