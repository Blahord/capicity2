#ifndef CAPICITY2_JSONVALUECHECK_H
#define CAPICITY2_JSONVALUECHECK_H

#include <QList>
#include <QJsonValue>

#include "../JsonError.h"

class JsonValueCheck {

    public:
        virtual ~JsonValueCheck();

        virtual void check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) = 0;
};


#endif //CAPICITY2_JSONVALUECHECK_H
