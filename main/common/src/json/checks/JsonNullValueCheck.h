#ifndef CAPICITY2_JSONNULLVALUECHECKTEST_H
#define CAPICITY2_JSONNULLVALUECHECKTEST_H

#include "JsonValueCheck.h"

class JsonNullValueCheck : public JsonValueCheck {

    public:
        explicit JsonNullValueCheck();
        ~JsonNullValueCheck() override;

        void check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) override;
};


#endif //CAPICITY2_JSONNULLVALUECHECKTEST_H
