#include "JsonIntegerValueCheck.h"

#include <cmath>

JsonIntegerValueCheck::JsonIntegerValueCheck(std::initializer_list<std::shared_ptr<JsonIntegerCheck>> checks)
    : checks(checks) {}

JsonIntegerValueCheck::~JsonIntegerValueCheck() = default;

void JsonIntegerValueCheck::check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) {
    if (!isInteger(checkValue)) {
        errors.append(JsonError(path, "Must be an integer"));
        return;
    }

    int const checkIntegerValue = checkValue.toInt(0);
    for (auto check : checks) {
        check->check(checkIntegerValue, path, errors);
    }
}

bool JsonIntegerValueCheck::isInteger(const QJsonValue& value) {
    if (!value.isDouble()) {
        return false;
    }
    double nodeValue = value.toDouble(0);
    return std::modf(nodeValue, &nodeValue) == 0;
}
