#include "JsonBooleanValueCheck.h"

JsonBooleanValueCheck::JsonBooleanValueCheck(std::initializer_list<shared_ptr<JsonBooleanCheck>> checks)
    : checks(checks) {}

JsonBooleanValueCheck::~JsonBooleanValueCheck() = default;

void JsonBooleanValueCheck::check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) {
    if (!checkValue.isBool()) {
        errors.append(JsonError(path, "Must be a boolean"));
        return;
    }

    bool const checkBoolValue = checkValue.toBool(false);

    for (const auto& check : checks) {
        check->check(checkBoolValue, path, errors);
    }
}
