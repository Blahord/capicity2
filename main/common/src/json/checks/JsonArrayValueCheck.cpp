#include "JsonArrayValueCheck.h"

JsonArrayValueCheck::JsonArrayValueCheck(std::initializer_list<shared_ptr<JsonArrayCheck>> checks)
    : checks(checks) {}

JsonArrayValueCheck::~JsonArrayValueCheck() = default;

void JsonArrayValueCheck::check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) {

    if (!checkValue.isArray()) {
        errors.append(JsonError(path, "Must be an array"));
        return;
    }

    const QJsonArray& checkArrayValue = checkValue.toArray();
    for (const auto& check : checks) {
        check->check(checkArrayValue, path, errors);
    }
}
