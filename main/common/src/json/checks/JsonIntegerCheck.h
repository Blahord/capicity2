#ifndef CAPICITY2_JSONINTEGERCHECK_H
#define CAPICITY2_JSONINTEGERCHECK_H

#include <QString>

#include "../JsonError.h"

class JsonIntegerCheck {

    public:
        enum class Type : int {
            IS = 0,
            MIN = 1,
            MAX = 2,
            NO_TYPE = -1 //For testing only
        };

        explicit JsonIntegerCheck(Type type, int value);

        void check(int checkValue, const QString& path, QList<JsonError>& errors);

    private:
        Type type;
        int value = 0;
};


#endif //CAPICITY2_JSONINTEGERCHECK_H
