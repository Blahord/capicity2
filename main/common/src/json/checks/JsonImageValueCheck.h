#ifndef CAPICITY2_JSONIMAGEVALUECHECK_H
#define CAPICITY2_JSONIMAGEVALUECHECK_H

#include <QSet>

#include "JsonValueCheck.h"

using std::unique_ptr;

class JsonImageValueCheck : public JsonValueCheck {

    public:
        explicit JsonImageValueCheck();
        ~JsonImageValueCheck() override;

        void check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        unique_ptr<JsonValueCheck> baseCheck;
        QSet<QChar> allowedDataCharacters;
};


#endif //CAPICITY2_JSONIMAGEVALUECHECK_H
