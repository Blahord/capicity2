#ifndef CAPICITY2_JSONSTRINGVALUECHECK_H
#define CAPICITY2_JSONSTRINGVALUECHECK_H

#include "JsonStringCheck.h"
#include "JsonValueCheck.h"

class JsonStringValueCheck : public JsonValueCheck {

    public:
        JsonStringValueCheck(std::initializer_list<std::shared_ptr<JsonStringCheck>> checks);
        ~JsonStringValueCheck() override;

        void check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        QList<std::shared_ptr<JsonStringCheck>> checks;
};


#endif //CAPICITY2_JSONSTRINGVALUECHECK_H
