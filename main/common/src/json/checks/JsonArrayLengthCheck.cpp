#include "JsonArrayLengthCheck.h"

JsonArrayLengthCheck::JsonArrayLengthCheck(JsonArrayLengthCheck::Type type, int value)
    : type(type), value(value) {}

JsonArrayLengthCheck::~JsonArrayLengthCheck() = default;

void JsonArrayLengthCheck::check(const QJsonArray& checkValue, const QString& path, QList<JsonError>& errors) {
    switch (type) {
        case Type::MIN_LENGTH:
            if (checkValue.size() < value) {
                errors.append(JsonError(
                    path,
                    QString("Array size (%1) too short. Minimum length is %2").arg(checkValue.size()).arg(value)
                ));
            }
            break;
        case Type::MAX_LENGTH:
            if (checkValue.size() > value) {
                errors.append(JsonError(
                    path,
                    QString("Array size (%1) too long. Maximum length is %2").arg(checkValue.size()).arg(value)
                ));
            }
            break;
        case Type::IS_LENGTH:
            if (checkValue.size() != value) {
                errors.append(JsonError(
                    path,
                    QString("Array size (%1) has wrong size. Expected size is %2").arg(checkValue.size()).arg(value)
                ));
            }
            break;
        default:
            errors.append(JsonError(path, QString("BUG: Unhandled type %1").arg(static_cast<int>(type))));
    }
}
