#ifndef CAPICITY2_JSONBOOLEANCHECK_H
#define CAPICITY2_JSONBOOLEANCHECK_H

#include <QString>

#include "../JsonError.h"

class JsonBooleanCheck {
    public:
        explicit JsonBooleanCheck(bool value);

        void check(bool checkValue, const QString& path, QList<JsonError>& errors) const;

    private:
        bool value;

        static QString toString(bool v);
};


#endif //CAPICITY2_JSONBOOLEANCHECK_H
