#ifndef CAPICITY2_JSONARRAYLENGTHCHECK_H
#define CAPICITY2_JSONARRAYLENGTHCHECK_H

#include "JsonArrayCheck.h"

class JsonArrayLengthCheck : public JsonArrayCheck {

    public:
        enum class Type : int {
            MIN_LENGTH = 0,
            MAX_LENGTH = 1,
            IS_LENGTH = 2,
            NO_TYPE = -1 //For testing only
        };

        JsonArrayLengthCheck(Type type, int value);
        ~JsonArrayLengthCheck() override;

        void check(const QJsonArray& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        Type type = Type::MIN_LENGTH;
        int value = 0;
};


#endif //CAPICITY2_JSONARRAYLENGTHCHECK_H
