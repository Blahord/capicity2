#include "JsonObjectValueCheck.h"

JsonObjectValueCheck::JsonObjectValueCheck(std::initializer_list<std::shared_ptr<JsonObjectCheck>> checks)
    : checks(checks) {}

JsonObjectValueCheck::~JsonObjectValueCheck() = default;

void JsonObjectValueCheck::check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) {
    if (!checkValue.isObject()) {
        errors.append(JsonError(path, "Must be an object"));
        return;
    }

    const QJsonObject& checkObjectValue = checkValue.toObject();

    for (auto check : checks) {
        check->check(checkObjectValue, path, errors);
    }
}
