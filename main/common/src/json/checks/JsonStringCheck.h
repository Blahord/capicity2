#ifndef CAPICITY2_JSONSTRINGCHECK_H
#define CAPICITY2_JSONSTRINGCHECK_H

#include <QString>

#include "../JsonError.h"

class JsonStringCheck {

    public:
        enum class Type : int {
            IS = 0,
            REGEXP = 1,
            NO_TYPE = -1 //For testing only
        };

        JsonStringCheck(Type type, const QString& value);

        void check(const QString& checkValue, const QString& path, QList<JsonError>& errors);

    private:
        Type type;
        QString value;

};


#endif //CAPICITY2_JSONSTRINGCHECK_H
