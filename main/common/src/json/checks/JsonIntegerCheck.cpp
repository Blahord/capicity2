#include "JsonIntegerCheck.h"

#include <QList>

JsonIntegerCheck::JsonIntegerCheck(JsonIntegerCheck::Type type, int value) {
    this->type = type;
    this->value = value;
}

void JsonIntegerCheck::check(int checkValue, const QString& path, QList<JsonError>& errors) {
    switch (type) {
        case Type::IS:
            if (value != checkValue) {
                errors.append(JsonError(
                    path, QString("Expected %1 but was %2").arg(value).arg(checkValue)
                ));
            }
            break;
        case Type::MIN:
            if (value > checkValue) {
                errors.append(JsonError(
                    path, QString("Expected a minimum value of %1 but was %2").arg(value).arg(checkValue)
                ));
            }
            break;
        case Type::MAX:
            if (value < checkValue) {
                errors.append(JsonError(
                    path, QString("Expected a maximum value of %1 but was %2").arg(value).arg(checkValue)
                ));
            }
            break;
        default:
            errors.append(JsonError(path, QString("BUG: Unhandled type %1").arg(static_cast<int>(type))));
    }
}
