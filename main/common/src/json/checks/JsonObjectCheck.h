#ifndef CAPICITY2_JSONOBJECTCHECK_H
#define CAPICITY2_JSONOBJECTCHECK_H

#include <QString>
#include <QJsonObject>

#include "JsonValueCheck.h"
#include "../JsonError.h"

class JsonObjectCheck {

    public:
        explicit JsonObjectCheck(const QString& name, bool optional, std::unique_ptr<JsonValueCheck> valueCheck);
        virtual ~JsonObjectCheck();

        void check(const QJsonObject& checkValue, const QString& path, QList<JsonError>& errors);

    private:
        QString fieldName;
        bool optional = false;
        std::unique_ptr<JsonValueCheck> valueCheck;
};


#endif //CAPICITY2_JSONOBJECTCHECK_H
