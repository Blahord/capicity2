#ifndef CAPICITY2_JSONARRAYVALUECHECK_H
#define CAPICITY2_JSONARRAYVALUECHECK_H

#include "JsonArrayCheck.h"
#include "JsonValueCheck.h"

using std::shared_ptr;

class JsonArrayValueCheck : public JsonValueCheck {

    public:
        JsonArrayValueCheck(std::initializer_list<shared_ptr<JsonArrayCheck>> checks);
        ~JsonArrayValueCheck() override;

        void check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        QList<shared_ptr<JsonArrayCheck>> checks;
};


#endif //CAPICITY2_JSONARRAYVALUECHECK_H
