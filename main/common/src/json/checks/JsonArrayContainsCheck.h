#ifndef CAPICITY2_JSONARRAYCONTAINSCHECK_H
#define CAPICITY2_JSONARRAYCONTAINSCHECK_H

#include <QString>

#include "JsonArrayCheck.h"
#include "JsonValueCheck.h"

using std::shared_ptr;

class JsonArrayContainsCheck : public JsonArrayCheck {

    public:
        enum class Type : int {
            CONTAINS_ANY_ORDER = 0,
            NO_TYPE = -1// Testing only
        };

        JsonArrayContainsCheck(Type type, const QList<QPair<QString, shared_ptr<JsonValueCheck>>>& checks);
        ~JsonArrayContainsCheck() override;

        void check(const QJsonArray& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        Type type = Type::CONTAINS_ANY_ORDER;
        QList<QPair<QString, shared_ptr<JsonValueCheck>>> checks;

};


#endif //CAPICITY2_JSONARRAYCONTAINSCHECK_H
