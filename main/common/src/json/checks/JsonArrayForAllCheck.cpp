#include "JsonArrayForAllCheck.h"

JsonArrayForAllCheck::JsonArrayForAllCheck(unique_ptr<JsonValueCheck> subCheck)
    : subCheck(std::move(subCheck)) {}

JsonArrayForAllCheck::~JsonArrayForAllCheck() = default;

void JsonArrayForAllCheck::check(const QJsonArray& checkValue, const QString& path, QList<JsonError>& errors) {
    int i = 0;
    for (QJsonValue const value : checkValue) {
        subCheck->check(value, path + QString("[%1]").arg(i), errors);
        i++;
    }
}
