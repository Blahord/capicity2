#include "JsonStringValueCheck.h"

JsonStringValueCheck::JsonStringValueCheck(std::initializer_list<std::shared_ptr<JsonStringCheck>> checks)
    : checks(checks) {}

JsonStringValueCheck::~JsonStringValueCheck() = default;

void JsonStringValueCheck::check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) {
    if (!checkValue.isString()) {
        errors.append(JsonError(path, "Must be a string"));
        return;
    }

    QString const checkStringValue = checkValue.toString();
    for (auto check : checks) {
        check->check(checkStringValue, path, errors);
    }
}
