#include "JsonStringCheck.h"

#include <QList>
#include <QRegularExpression>

JsonStringCheck::JsonStringCheck(JsonStringCheck::Type type, const QString& value) {
    this->type = type;
    this->value = value;
}

void JsonStringCheck::check(const QString& checkValue, const QString& path, QList<JsonError>& errors) {
    switch (type) {
        case Type::IS:
            if (value != checkValue) {
                errors.append(
                    JsonError(
                        path,
                        QString("Expected '%1' but was '%2'").arg(value, checkValue)
                    )
                );
            }
            break;
        case Type::REGEXP: {
            QRegularExpression const matcher(value);
            if (!matcher.match(checkValue).hasMatch()) {
                errors.append(JsonError(path, QString("Value '%1' does not match regular expression '%2'").arg(checkValue, value)));
            }
            break;
        }
        default:
            errors.append(JsonError(path, QString("BUG: Unhandled type %1").arg(static_cast<int>(type))));
    }
}
