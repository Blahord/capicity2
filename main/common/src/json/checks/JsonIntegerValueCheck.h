#ifndef CAPICITY2_JSONINTEGERVALUECHECK_H
#define CAPICITY2_JSONINTEGERVALUECHECK_H

#include "JsonIntegerCheck.h"
#include "JsonValueCheck.h"

class JsonIntegerValueCheck : public JsonValueCheck {

    public:
        JsonIntegerValueCheck(std::initializer_list<std::shared_ptr<JsonIntegerCheck>> checks);
        ~JsonIntegerValueCheck() override;

        void check(const QJsonValue& checkValue, const QString& path, QList<JsonError>& errors) override;

    private:
        static bool isInteger(const QJsonValue& value);

        QList<std::shared_ptr<JsonIntegerCheck>> checks;
};


#endif //CAPICITY2_JSONINTEGERVALUECHECK_H
