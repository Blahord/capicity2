#include "JsonObjectCheck.h"

#include "JsonValueCheck.h"

JsonObjectCheck::JsonObjectCheck(const QString& name, bool optional, std::unique_ptr<JsonValueCheck> valueCheck) {
    this->fieldName = name;
    this->optional = optional;
    this->valueCheck = std::move(valueCheck);
}

JsonObjectCheck::~JsonObjectCheck() = default;

void JsonObjectCheck::check(const QJsonObject& checkValue, const QString& path, QList<JsonError>& errors) {
    QJsonValue const fieldValue = checkValue.value(fieldName);
    QString const localPath = path + "/" + fieldName;

    if (!optional) {
        if (fieldValue.isUndefined()) {
            errors.append(JsonError(localPath, "Field not present"));
            return;
        }
    }

    if (valueCheck && !fieldValue.isUndefined() && !fieldValue.isNull()) {
        valueCheck->check(fieldValue, localPath, errors);
    }
}
