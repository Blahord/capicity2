#include "JsonBooleanCheck.h"

#include <QList>

JsonBooleanCheck::JsonBooleanCheck(bool value) {
    this->value = value;
}

void JsonBooleanCheck::check(bool checkValue, const QString& path, QList<JsonError>& errors) const {
    if (value != checkValue) {
        errors.append(JsonError(
            path,
            QString("Expected %1 but was %2").arg(toString(value)).arg(toString(checkValue))
        ));
    }
}

QString JsonBooleanCheck::toString(bool v) {
    return v ? "true" : "false";
}
