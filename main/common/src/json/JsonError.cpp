#include "JsonError.h"

JsonError::JsonError(const QString& path, const QString& message) {
    this->path = path;
    this->message = message;
}

const QString& JsonError::getPath() const {
    return path;
}

const QString& JsonError::getMessage() const {
    return message;
}
