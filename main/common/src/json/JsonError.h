#ifndef CAPICITY2_JSONERROR_H
#define CAPICITY2_JSONERROR_H

#include <QString>

class JsonError {

    public:
        JsonError(const QString& path, const QString& message);

        const QString& getPath() const;
        const QString& getMessage() const;
    private:
        QString path;
        QString message;

};


#endif //CAPICITY2_JSONERROR_H
