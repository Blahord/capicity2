#include "JsonChecks.h"

#include <QList>
#include <QPair>


#include "checks/JsonArrayLengthCheck.h"
#include "checks/JsonArrayContainsCheck.h"
#include "checks/JsonArrayForAllCheck.h"
#include "checks/JsonValueAnyCheck.h"
#include "checks/JsonNullValueCheck.h"
#include "checks/JsonImageValueCheck.h"
#include "checks/JsonStringValueCheck.h"
#include "checks/JsonBooleanValueCheck.h"
#include "checks/JsonIntegerValueCheck.h"
#include "checks/JsonObjectValueCheck.h"
#include "checks/JsonArrayValueCheck.h"

namespace JsonChecks {

    std::unique_ptr<JsonValueCheck> any(const QString& name1, const std::shared_ptr<JsonValueCheck>& check1, const QString& name2, const std::shared_ptr<JsonValueCheck>& check2) {
        return any({{name1, check1},
                    {name2, check2}});
    }

    std::unique_ptr<JsonValueCheck> any(std::initializer_list<QPair<QString, std::shared_ptr<JsonValueCheck>>> checks) {
        return std::make_unique<JsonValueAnyCheck>(checks);
    }

    std::unique_ptr<JsonValueCheck> nullValue() {
        return std::make_unique<JsonNullValueCheck>();
    }

    std::unique_ptr<JsonValueCheck> stringValue() {
        return std::make_unique<JsonStringValueCheck>(std::initializer_list<std::shared_ptr<JsonStringCheck>>{});
    }

    std::unique_ptr<JsonValueCheck> stringValue(const std::shared_ptr<JsonStringCheck>& check) {
        return std::make_unique<JsonStringValueCheck>(std::initializer_list<std::shared_ptr<JsonStringCheck>>{check});
    }

    std::unique_ptr<JsonValueCheck> booleanValue() {
        return std::make_unique<JsonBooleanValueCheck>(std::initializer_list<shared_ptr<JsonBooleanCheck>>{});
    }

    std::unique_ptr<JsonValueCheck> integerValue() {
        return std::make_unique<JsonIntegerValueCheck>(std::initializer_list<std::shared_ptr<JsonIntegerCheck>>{});
    }

    std::unique_ptr<JsonValueCheck> integerValue(const shared_ptr<JsonIntegerCheck>& check) {
        return std::make_unique<JsonIntegerValueCheck>(std::initializer_list<std::shared_ptr<JsonIntegerCheck>>{check});
    }

    std::unique_ptr<JsonValueCheck> integerValue(const shared_ptr<JsonIntegerCheck>& check1, const shared_ptr<JsonIntegerCheck>& check2) {
        return std::make_unique<JsonIntegerValueCheck>(std::initializer_list<std::shared_ptr<JsonIntegerCheck>>{check1, check2});
    }

    std::unique_ptr<JsonValueCheck> objectValue() {
        return std::make_unique<JsonObjectValueCheck>(std::initializer_list<std::shared_ptr<JsonObjectCheck>>{});
    }

    std::unique_ptr<JsonValueCheck> imageValue() {
        return std::make_unique<JsonImageValueCheck>();
    }

    std::unique_ptr<JsonValueCheck> objectValue(const std::shared_ptr<JsonObjectCheck>& fieldCheck) {
        return objectValue({fieldCheck});
    }

    std::unique_ptr<JsonValueCheck> objectValue(const std::shared_ptr<JsonObjectCheck>& fieldCheck1, const std::shared_ptr<JsonObjectCheck>& fieldCheck2) {
        return objectValue({fieldCheck1, fieldCheck2});
    }

    std::unique_ptr<JsonValueCheck> objectValue(const std::shared_ptr<JsonObjectCheck>& fieldCheck1, const std::shared_ptr<JsonObjectCheck>& fieldCheck2, const std::shared_ptr<JsonObjectCheck>& fieldCheck3) {
        return objectValue({fieldCheck1, fieldCheck2, fieldCheck3});
    }

    std::unique_ptr<JsonValueCheck> objectValue(std::initializer_list<std::shared_ptr<JsonObjectCheck>> fieldChecks) {
        return std::make_unique<JsonObjectValueCheck>(fieldChecks);
    }

    std::unique_ptr<JsonValueCheck> arrayValue(const std::shared_ptr<JsonArrayCheck>& check) {
        return arrayValue({check});
    }

    std::unique_ptr<JsonValueCheck> arrayValue(std::initializer_list<std::shared_ptr<JsonArrayCheck>> checks) {
        return std::make_unique<JsonArrayValueCheck>(checks);
    }

    std::unique_ptr<JsonObjectCheck> requiredField(const QString& name) {
        return std::make_unique<JsonObjectCheck>(name, false, nullptr);
    }

    std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, std::unique_ptr<JsonValueCheck> valueCheck) {
        return std::make_unique<JsonObjectCheck>(name, false, std::move(valueCheck));
    }

    std::unique_ptr<JsonObjectCheck> optionalField(const QString& name, std::unique_ptr<JsonValueCheck> valueCheck) {
        return std::make_unique<JsonObjectCheck>(name, true, std::move(valueCheck));
    }

    std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonStringCheck>& check) {
        return requiredField(name, std::make_unique<JsonStringValueCheck>(std::initializer_list<std::shared_ptr<JsonStringCheck>>{check}));
    }

    std::unique_ptr<JsonObjectCheck> optionalField(const QString& name, const std::shared_ptr<JsonStringCheck>& check) {
        return optionalField(name, std::make_unique<JsonStringValueCheck>(std::initializer_list<std::shared_ptr<JsonStringCheck>>{check}));
    }

    std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonBooleanCheck>& check) {
        return requiredField(name, std::make_unique<JsonBooleanValueCheck>(std::initializer_list<shared_ptr<JsonBooleanCheck>>{check}));
    }

    std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonArrayCheck>& check1, const std::shared_ptr<JsonArrayCheck>& check2) {
        return requiredField(name, {check1, check2});
    }

    std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, std::initializer_list<std::shared_ptr<JsonArrayCheck>> checks) {
        return requiredField(name, std::make_unique<JsonArrayValueCheck>(checks));
    }

    std::unique_ptr<JsonObjectCheck> optionalField(const QString& name, const std::shared_ptr<JsonArrayCheck>& check) {
        return optionalField(name, {check});
    }

    std::unique_ptr<JsonObjectCheck> optionalField(const QString& name, std::initializer_list<std::shared_ptr<JsonArrayCheck>> checks) {
        return optionalField(name, std::make_unique<JsonArrayValueCheck>(checks));
    }

    std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonIntegerCheck>& check) {
        return requiredField(name, std::make_unique<JsonIntegerValueCheck>(std::initializer_list<std::shared_ptr<JsonIntegerCheck>>{check}));
    }

    std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonIntegerCheck>& check1, const std::shared_ptr<JsonIntegerCheck>& check2) {
        return requiredField(name, std::make_unique<JsonIntegerValueCheck>(std::initializer_list<std::shared_ptr<JsonIntegerCheck>>{check1, check2}));
    }

    std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, std::initializer_list<std::shared_ptr<JsonObjectCheck>> checks) {
        return requiredField(name, std::make_unique<JsonObjectValueCheck>(checks));
    }

    std::unique_ptr<JsonStringCheck> is(const QString& expectedValue) {
        return std::make_unique<JsonStringCheck>(JsonStringCheck::Type::IS, expectedValue);
    }

    std::unique_ptr<JsonStringCheck> regexp(const QString& regexp) {
        return std::make_unique<JsonStringCheck>(JsonStringCheck::Type::REGEXP, regexp);
    }

    std::unique_ptr<JsonIntegerCheck> is(int expectedValue) {
        return std::make_unique<JsonIntegerCheck>(JsonIntegerCheck::Type::IS, expectedValue);
    }

    std::unique_ptr<JsonIntegerCheck> min(int expectedValue) {
        return std::make_unique<JsonIntegerCheck>(JsonIntegerCheck::Type::MIN, expectedValue);
    }

    std::unique_ptr<JsonIntegerCheck> max(int expectedValue) {
        return std::make_unique<JsonIntegerCheck>(JsonIntegerCheck::Type::MAX, expectedValue);
    }

    std::unique_ptr<JsonBooleanCheck> isTrue() {
        return std::make_unique<JsonBooleanCheck>(true);
    }

    std::unique_ptr<JsonBooleanCheck> isFalse() {
        return std::make_unique<JsonBooleanCheck>(false);
    }

    std::unique_ptr<JsonArrayCheck> minLength(int minLength) {
        return std::make_unique<JsonArrayLengthCheck>(JsonArrayLengthCheck::Type::MIN_LENGTH, minLength);
    }

    std::unique_ptr<JsonArrayCheck> maxLength(int maxLength) {
        return std::make_unique<JsonArrayLengthCheck>(JsonArrayLengthCheck::Type::MAX_LENGTH, maxLength);
    }

    std::unique_ptr<JsonArrayCheck> contains(const QString& name, const std::shared_ptr<JsonValueCheck>& valueCheck) {
        return std::make_unique<JsonArrayContainsCheck>(JsonArrayContainsCheck::Type::CONTAINS_ANY_ORDER, QList<QPair<QString, std::shared_ptr<JsonValueCheck>>>{{name, valueCheck}});
    }

    std::unique_ptr<JsonArrayCheck> contains(const QString& name, const std::shared_ptr<JsonStringCheck>& check) {
        return std::make_unique<JsonArrayContainsCheck>(JsonArrayContainsCheck::Type::CONTAINS_ANY_ORDER, QList<QPair<QString, std::shared_ptr<JsonValueCheck>>>{{name, std::make_shared<JsonStringValueCheck>(std::initializer_list<std::shared_ptr<JsonStringCheck>>{check})}});
    }

    std::unique_ptr<JsonArrayCheck> forAll(std::unique_ptr<JsonValueCheck> subCheck) {
        return std::make_unique<JsonArrayForAllCheck>(std::move(subCheck));
    }
}
