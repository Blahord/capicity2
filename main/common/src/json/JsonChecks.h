#ifndef CAPICITY2_JSONCHECKS_H
#define CAPICITY2_JSONCHECKS_H

#include "checks/JsonValueCheck.h"
#include "checks/JsonStringCheck.h"
#include "checks/JsonObjectCheck.h"
#include "checks/JsonArrayCheck.h"
#include "checks/JsonBooleanCheck.h"
#include "checks/JsonIntegerCheck.h"

namespace JsonChecks {

    //Quantifier checks
    extern std::unique_ptr<JsonValueCheck> any(const QString& name1, const std::shared_ptr<JsonValueCheck>& check1,
                                               const QString& name2, const std::shared_ptr<JsonValueCheck>& check2);

    extern std::unique_ptr<JsonValueCheck> any(std::initializer_list<QPair<QString, std::shared_ptr<JsonValueCheck>>> checks);

    //Only type checks

    extern std::unique_ptr<JsonValueCheck> nullValue();
    extern std::unique_ptr<JsonValueCheck> stringValue();
    extern std::unique_ptr<JsonValueCheck> stringValue(const std::shared_ptr<JsonStringCheck>& check);
    extern std::unique_ptr<JsonValueCheck> booleanValue();
    extern std::unique_ptr<JsonValueCheck> integerValue();
    extern std::unique_ptr<JsonValueCheck> integerValue(const std::shared_ptr<JsonIntegerCheck>& check);
    extern std::unique_ptr<JsonValueCheck> integerValue(const std::shared_ptr<JsonIntegerCheck>& check1, const std::shared_ptr<JsonIntegerCheck>& check2);
    extern std::unique_ptr<JsonValueCheck> objectValue();
    extern std::unique_ptr<JsonValueCheck> imageValue();

    //Value checks - object

    extern std::unique_ptr<JsonValueCheck> objectValue(const std::shared_ptr<JsonObjectCheck>& fieldCheck);
    extern std::unique_ptr<JsonValueCheck> objectValue(const std::shared_ptr<JsonObjectCheck>& fieldCheck1, const std::shared_ptr<JsonObjectCheck>& fieldCheck2);
    extern std::unique_ptr<JsonValueCheck> objectValue(const std::shared_ptr<JsonObjectCheck>& fieldCheck1, const std::shared_ptr<JsonObjectCheck>& fieldCheck2, const std::shared_ptr<JsonObjectCheck>& fieldCheck3);
    extern std::unique_ptr<JsonValueCheck> objectValue(std::initializer_list<std::shared_ptr<JsonObjectCheck>> fieldChecks);

    //Value checks - array

    extern std::unique_ptr<JsonValueCheck> arrayValue(const std::shared_ptr<JsonArrayCheck>& check);
    extern std::unique_ptr<JsonValueCheck> arrayValue(std::initializer_list<std::shared_ptr<JsonArrayCheck>> checks);

    // Object checks

    extern std::unique_ptr<JsonObjectCheck> requiredField(const QString& name);
    extern std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, std::unique_ptr<JsonValueCheck> valueCheck);
    extern std::unique_ptr<JsonObjectCheck> optionalField(const QString& name, std::unique_ptr<JsonValueCheck> valueCheck);

    //Object checks - String

    extern std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonStringCheck>& check);
    extern std::unique_ptr<JsonObjectCheck> optionalField(const QString& name, const std::shared_ptr<JsonStringCheck>& check);

    //Object checks - Boolean

    extern std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonBooleanCheck>& check);

    //Object checks - Array

    extern std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonArrayCheck>& check1, const std::shared_ptr<JsonArrayCheck>& check2);
    extern std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, std::initializer_list<std::shared_ptr<JsonArrayCheck>> checks);
    extern std::unique_ptr<JsonObjectCheck> optionalField(const QString& name, const std::shared_ptr<JsonArrayCheck>& check);
    extern std::unique_ptr<JsonObjectCheck> optionalField(const QString& name, std::initializer_list<std::shared_ptr<JsonArrayCheck>> checks);

    //Object checks - Integer

    extern std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonIntegerCheck>& check);
    extern std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, const std::shared_ptr<JsonIntegerCheck>& check1, const std::shared_ptr<JsonIntegerCheck>& check2);

    //Object checks - Object

    extern std::unique_ptr<JsonObjectCheck> requiredField(const QString& name, std::initializer_list<std::shared_ptr<JsonObjectCheck>> checks);

    //String checks

    extern std::unique_ptr<JsonStringCheck> is(const QString& expectedValue);
    extern std::unique_ptr<JsonStringCheck> regexp(const QString& regexp);

    //Integer checks

    extern std::unique_ptr<JsonIntegerCheck> is(int expectedValue);
    extern std::unique_ptr<JsonIntegerCheck> min(int expectedValue);
    extern std::unique_ptr<JsonIntegerCheck> max(int expectedValue);

    //Boolean checks

    extern std::unique_ptr<JsonBooleanCheck> isTrue();
    extern std::unique_ptr<JsonBooleanCheck> isFalse();

    //Array checks

    extern std::unique_ptr<JsonArrayCheck> minLength(int minLength);
    extern std::unique_ptr<JsonArrayCheck> maxLength(int maxLength);
    extern std::unique_ptr<JsonArrayCheck> contains(const QString& name, const std::shared_ptr<JsonValueCheck>& valueCheck);
    extern std::unique_ptr<JsonArrayCheck> contains(const QString& name, const std::shared_ptr<JsonStringCheck>& check);
    extern std::unique_ptr<JsonArrayCheck> forAll(std::unique_ptr<JsonValueCheck> subCheck);
}

#endif //CAPICITY2_JSONCHECKS_H
