#include "CapiConnection.h"

#include <QJsonParseError>
#include <QTextCodec>

CapiConnection::CapiConnection(QAbstractSocket* socket, int maxBufferSize, QObject* parent)
    : QObject(parent),
      decoder(QTextCodec::codecForName("UTF-8")->makeDecoder()) {
    this->socket = socket;
    this->maxBufferSize = maxBufferSize;

    if (socket) {
        connect(socket, &QAbstractSocket::readyRead, this, &CapiConnection::readData);
        connect(socket, &QAbstractSocket::disconnected, this, &CapiConnection::disconnected);
    }
}

CapiConnection::CapiConnection(QAbstractSocket* socket, QObject* parent)
    : CapiConnection(socket, 1024 * 1024, parent) {
}

CapiConnection::~CapiConnection() {
    if (socket) {
        socket->disconnect();
        socket->deleteLater();

        socket = nullptr;
    }
}

void CapiConnection::send(const QJsonObject& data) {
    qDebug() << ">>" << data;
    socket->write(QString(R"(""")").toUtf8());
    socket->write(QJsonDocument(data).toJson());
    socket->flush();
}

void CapiConnection::readData() {
    QByteArray buffer(1024, 0);

    while (socket->bytesAvailable() > 0) {
        int const readBytes = (int) socket->read(buffer.data(), 1024);
        readCharacters(decoder->toUnicode(buffer.left(readBytes)));
    }

    //Prevent pollution, if json-object does not get finished
    if (lastData.size() > maxBufferSize) {
        resetMessageRead();
    }
}

void CapiConnection::readCharacters(const QString& characters) {
    for (QChar const c : characters) {
        lastData.append(c);

        if (lastData.endsWith(R"(""")")) {
            resetMessageRead();
            lastData.clear();
            continue;
        }

        if (charMasked) {
            charMasked = false;
        } else if (c == '\\' && inTextLiteral) {
            charMasked = true;
        } else if (c == '{' && !inTextLiteral) {
            jsonBracesOpen++;
        } else if (c == '}' && !inTextLiteral) {
            jsonBracesOpen--;
            if (jsonBracesOpen == 0) {
                emitGotMessage();
            }
        } else if (c == '"' && !inTextLiteral) {
            inTextLiteral = true;
        } else if (c == '"') {
            inTextLiteral = false;
        }
    }
}

void CapiConnection::emitGotMessage() {
    QJsonParseError error{};
    QJsonDocument const document = QJsonDocument::fromJson(lastData.toUtf8(), &error);

    QString const dataString(document.toJson(QJsonDocument::Compact));

    if (error.error == QJsonParseError::NoError) {
        qDebug() << "<<" << document;
        emit(gotMessage(document.object()));
    } else {
        qDebug() << "Ignoring" << lastData;
    }

    lastData.clear();
}

void CapiConnection::resetMessageRead() {
    lastData.clear();
    charMasked = false;
    jsonBracesOpen = 0;
    inTextLiteral = false;
}
