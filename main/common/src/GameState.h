#ifndef CAPICITY2_GAMESTATE_H
#define CAPICITY2_GAMESTATE_H

enum class GameState {
    CONFIG,
    RUN,
    END,
    NOTHING //Used for tests
};

#endif //CAPICITY2_GAMESTATE_H
