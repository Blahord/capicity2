#include <gtest/gtest.h>

#include "json/checks/JsonObjectValueCheck.h"
#include "json/JsonChecks.h"

TEST(JsonObjectValueCheckTest, string) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectValueCheck({});

    auto jsonValue = "Foo";
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

TEST(JsonObjectValueCheckTest, floatingPoint) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectValueCheck({});

    auto jsonValue = 1.2;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

TEST(JsonObjectValueCheckTest, boolean) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectValueCheck({});

    auto jsonValue = false;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

TEST(JsonObjectValueCheckTest, null) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectValueCheck({});

    auto jsonValue = QJsonValue::Null;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

TEST(JsonObjectValueCheckTest, object_2FailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectValueCheck({JsonChecks::requiredField("f", JsonChecks::integerValue()), JsonChecks::requiredField("g", JsonChecks::integerValue())});

    auto jsonValue = QJsonObject({});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 2);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path/f");
    EXPECT_EQ(errorList.at(0).getMessage(), "Field not present");
    EXPECT_EQ(errorList.at(1).getPath(), "some/path/g");
    EXPECT_EQ(errorList.at(1).getMessage(), "Field not present");
}

TEST(JsonObjectValueCheckTest, object_1FailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectValueCheck({JsonChecks::requiredField("f", JsonChecks::integerValue()), JsonChecks::requiredField("g", JsonChecks::integerValue())});

    auto jsonValue = QJsonObject({{"f", 12345}});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path/g");
    EXPECT_EQ(errorList.at(0).getMessage(), "Field not present");
}

TEST(JsonObjectValueCheckTest, object_NoFailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectValueCheck({JsonChecks::requiredField("f", JsonChecks::integerValue()), JsonChecks::requiredField("g", JsonChecks::integerValue())});

    auto jsonValue = QJsonObject{
        {"f", 12345},
        {"g", 12345}
    };
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonObjectValueCheckTest, array) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectValueCheck({});

    auto jsonValue = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

