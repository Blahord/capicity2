#include <gtest/gtest.h>

#include "json/checks/JsonStringValueCheck.h"
#include "json/JsonChecks.h"

TEST(JsonStringValueCheckTest, string_2FailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringValueCheck({JsonChecks::is("Foo"), JsonChecks::is("Foo")});

    auto jsonValue = "Bar";
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 2);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected 'Foo' but was 'Bar'");
    EXPECT_EQ(errorList.at(1).getPath(), "some/path");
    EXPECT_EQ(errorList.at(1).getMessage(), "Expected 'Foo' but was 'Bar'");
}

TEST(JsonStringValueCheckTest, string_1FailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringValueCheck({JsonChecks::is("Foo"), JsonChecks::is("Bar")});

    auto jsonValue = "Foo";
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected 'Bar' but was 'Foo'");
}

TEST(JsonStringValueCheckTest, string_NoFailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringValueCheck({JsonChecks::is("Foo"), JsonChecks::is("Foo")});

    auto jsonValue = "Foo";
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonStringValueCheckTest, integer) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringValueCheck({});

    auto jsonValue = 1;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a string");
}

TEST(JsonStringValueCheckTest, floatingPoint) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringValueCheck({});

    auto jsonValue = 1.2;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a string");
}

TEST(JsonStringValueCheckTest, boolean) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringValueCheck({});

    auto jsonValue = false;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a string");
}

TEST(JsonStringValueCheckTest, null) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringValueCheck({});

    auto jsonValue = QJsonValue::Null;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a string");
}

TEST(JsonStringValueCheckTest, object) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringValueCheck({});

    auto jsonValue = QJsonObject();
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a string");
}

TEST(JsonStringValueCheckTest, array) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringValueCheck({});

    auto jsonValue = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a string");
}

