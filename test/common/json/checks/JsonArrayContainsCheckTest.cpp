#include <gtest/gtest.h>

#include "json/checks/JsonArrayContainsCheck.h"
#include "json/JsonChecks.h"
#include "json/checks/JsonIntegerValueCheck.h"

TEST(JsonArrayContainsCheckTest, containsAnyOrder_emptyArray) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayContainsCheck(
        JsonArrayContainsCheck::Type::CONTAINS_ANY_ORDER,
        {
            {"1", JsonChecks::integerValue(JsonChecks::is(1))},
            {"2", JsonChecks::integerValue(JsonChecks::is(2))}
        }
    );
    auto array = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 2);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Could not find match for '1'");
    EXPECT_EQ(errorList.at(1).getPath(), "some/path");
    EXPECT_EQ(errorList.at(1).getMessage(), "Could not find match for '2'");
}

TEST(JsonArrayContainsCheckTest, containsAnyOrder_only1) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayContainsCheck(
        JsonArrayContainsCheck::Type::CONTAINS_ANY_ORDER,
        {
            {"1", JsonChecks::integerValue(JsonChecks::is(1))},
            {"2", JsonChecks::integerValue(JsonChecks::is(2))}
        }
    );
    auto array = QJsonArray({1});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Could not find match for '2'");
}

TEST(JsonArrayContainsCheckTest, containsAnyOrder_only2) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayContainsCheck(
        JsonArrayContainsCheck::Type::CONTAINS_ANY_ORDER,
        {
            {"1", JsonChecks::integerValue(JsonChecks::is(1))},
            {"2", JsonChecks::integerValue(JsonChecks::is(2))}
        }
    );
    auto array = QJsonArray({2});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Could not find match for '1'");
}

TEST(JsonArrayContainsCheckTest, containsAnyOrder_21) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayContainsCheck(
        JsonArrayContainsCheck::Type::CONTAINS_ANY_ORDER,
        {
            {"1", JsonChecks::integerValue(JsonChecks::is(1))},
            {"2", JsonChecks::integerValue(JsonChecks::is(2))}
        }
    );
    auto array = QJsonArray({2, 1});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayContainsCheckTest, containsAnyOrder_12) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayContainsCheck(
        JsonArrayContainsCheck::Type::CONTAINS_ANY_ORDER,
        {
            {"1", JsonChecks::integerValue(JsonChecks::is(1))},
            {"2", JsonChecks::integerValue(JsonChecks::is(2))}
        }
    );
    auto array = QJsonArray({1, 2});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayContainsCheckTest, containsAnyOrder_11) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayContainsCheck(
        JsonArrayContainsCheck::Type::CONTAINS_ANY_ORDER,
        {
            {"1", JsonChecks::integerValue(JsonChecks::is(1))},
            {"2", JsonChecks::integerValue(JsonChecks::is(2))}
        }
    );
    auto array = QJsonArray({1, 1});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Could not find match for '2'");
}

TEST(JsonArrayContainsCheckTest, containsAnyOrder_123) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayContainsCheck(
        JsonArrayContainsCheck::Type::CONTAINS_ANY_ORDER,
        {
            {"1", JsonChecks::integerValue(JsonChecks::is(1))},
            {"2", JsonChecks::integerValue(JsonChecks::is(2))}
        }
    );
    auto array = QJsonArray({1, 2, 3});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayContainsCheckTest, invalid_type) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayContainsCheck(
        JsonArrayContainsCheck::Type::NO_TYPE,
        {
            {"1", JsonChecks::integerValue(JsonChecks::is(1))},
            {"2", JsonChecks::integerValue(JsonChecks::is(2))}
        }
    );
    auto array = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "BUG: Unhandled type -1");
}
