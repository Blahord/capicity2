#include <gtest/gtest.h>

#include "json/checks/JsonBooleanCheck.h"
#include "json/JsonChecks.h"

TEST(JsonBooleanCheckTest, isTrue_false) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanCheck(true);
    auto errorList = QList<JsonError>();
    check.check(false, "some/path", errorList);

    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected true but was false");
}

TEST(JsonBooleanCheckTest, isTrue_true) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanCheck(true);
    auto errorList = QList<JsonError>();
    check.check(true, "some/path", errorList);

    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonBooleanCheckTest, isFalse_true) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanCheck(false);
    auto errorList = QList<JsonError>();
    check.check(true, "some/path", errorList);

    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected false but was true");
}

TEST(JsonBooleanCheckTest, isFalse_false) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanCheck(false);
    auto errorList = QList<JsonError>();
    check.check(false, "some/path", errorList);

    EXPECT_TRUE(errorList.isEmpty());
}
