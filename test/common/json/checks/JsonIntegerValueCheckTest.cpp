#include <gtest/gtest.h>

#include "json/checks/JsonIntegerValueCheck.h"
#include "json/JsonChecks.h"

TEST(JsonIntegerValueCheckTest, string) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerValueCheck({});

    auto jsonValue = "Foo";
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an integer");
}

TEST(JsonIntegerValueCheckTest, integer_2FailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerValueCheck({JsonChecks::is(42), JsonChecks::is(1337)});

    auto jsonValue = 1;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 2);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected 42 but was 1");
    EXPECT_EQ(errorList.at(1).getPath(), "some/path");
    EXPECT_EQ(errorList.at(1).getMessage(), "Expected 1337 but was 1");
}

TEST(JsonIntegerValueCheckTest, integer_1FailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerValueCheck({JsonChecks::is(42), JsonChecks::is(1337)});

    auto jsonValue = 1337;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected 42 but was 1337");
}

TEST(JsonIntegerValueCheckTest, integer_NoFailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerValueCheck({JsonChecks::is(1337), JsonChecks::is(1337)});

    auto jsonValue = 1337;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonIntegerValueCheckTest, floatingPoint) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerValueCheck({});

    auto jsonValue = 1.2;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an integer");
}

TEST(JsonIntegerValueCheckTest, boolean) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerValueCheck({});

    auto jsonValue = false;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an integer");
}

TEST(JsonIntegerValueCheckTest, null) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerValueCheck({});

    auto jsonValue = QJsonValue::Null;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an integer");
}

TEST(JsonIntegerValueCheckTest, object) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerValueCheck({});

    auto jsonValue = QJsonObject();
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an integer");
}

TEST(JsonIntegerValueCheckTest, array) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerValueCheck({});

    auto jsonValue = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an integer");
}

