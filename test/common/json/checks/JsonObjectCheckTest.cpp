#include <gtest/gtest.h>

#include "json/checks/JsonObjectCheck.h"
#include "json/JsonChecks.h"

TEST(JsonObjectCheck, required_notPresent) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", false, JsonChecks::integerValue());
    auto object = QJsonObject({});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path/f");
    EXPECT_EQ(errorList.at(0).getMessage(), "Field not present");
}

TEST(JsonObjectCheck, required_isNull) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", false, JsonChecks::nullValue());
    auto object = QJsonObject({{"f", QJsonValue::Null}});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonObjectCheck, required_noCheck) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", false, nullptr);
    auto object = QJsonObject({{"f", QJsonValue::Null}});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonObjectCheck, required_valueNotMatching) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", false, JsonChecks::integerValue());
    auto object = QJsonObject({{"f", "Foo"}});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path/f");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an integer");
}

TEST(JsonObjectCheck, required_valueMatching) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", false, JsonChecks::integerValue());
    auto object = QJsonObject({{"f", 12345}});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonObjectCheck, optional_notPresent) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", true, JsonChecks::integerValue());
    auto object = QJsonObject({});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonObjectCheck, optional_isNull) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", true, JsonChecks::integerValue());
    auto object = QJsonObject({{"f", QJsonValue::Null}});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonObjectCheck, optional_valueNotMatching) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", true, JsonChecks::integerValue());
    auto object = QJsonObject({{"f", "Foo"}});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path/f");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an integer");
}

TEST(JsonObjectCheck, optional_valueMatching) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", true, JsonChecks::integerValue());
    auto object = QJsonObject({{"f", 12345}});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonObjectCheck, optional_noCheck) { // NOLINT(cert-err58-cpp)
    auto check = JsonObjectCheck("f", true, nullptr);
    auto object = QJsonObject({{"f", QJsonValue::Null}});
    auto errorList = QList<JsonError>();

    check.check(object, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}
