#include <gtest/gtest.h>

#include "json/checks/JsonArrayForAllCheck.h"
#include "json/JsonChecks.h"
#include "json/checks/JsonIntegerValueCheck.h"

TEST(JsonArrayForAllCheck, allFailing) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayForAllCheck(JsonChecks::stringValue());
    auto array = QJsonArray({1, 2, 3});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 3);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path[0]");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a string");
    EXPECT_EQ(errorList.at(1).getPath(), "some/path[1]");
    EXPECT_EQ(errorList.at(1).getMessage(), "Must be a string");
    EXPECT_EQ(errorList.at(2).getPath(), "some/path[2]");
    EXPECT_EQ(errorList.at(2).getMessage(), "Must be a string");
}

TEST(JsonArrayForAllCheck, twoFailing) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayForAllCheck(JsonChecks::stringValue());
    auto array = QJsonArray({1, "2", 3});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 2);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path[0]");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a string");
    EXPECT_EQ(errorList.at(1).getPath(), "some/path[2]");
    EXPECT_EQ(errorList.at(1).getMessage(), "Must be a string");
}

TEST(JsonArrayForAllCheck, noFailing) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayForAllCheck(JsonChecks::stringValue());
    auto array = QJsonArray({"1", "2", "3"});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayForAllCheck, empytArray) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayForAllCheck(JsonChecks::integerValue(JsonChecks::is(42), JsonChecks::is(1337)));
    auto array = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

