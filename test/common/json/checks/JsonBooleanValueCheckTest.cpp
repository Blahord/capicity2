#include <gtest/gtest.h>

#include <QList>

#include "json/JsonError.h"
#include "json/checks/JsonBooleanValueCheck.h"
#include "json/JsonChecks.h"

TEST(JsonBooleanValueCheckTest, string) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanValueCheck({});

    auto jsonValue = "Foo";
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a boolean");
}

TEST(JsonBooleanValueCheckTest, integer) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanValueCheck({});

    auto jsonValue = 1;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a boolean");
}

TEST(JsonBooleanValueCheckTest, floatingPoint) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanValueCheck({});

    auto jsonValue = 1.2;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a boolean");
}

TEST(JsonBooleanValueCheckTest, boolean_2failingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanValueCheck({JsonChecks::isTrue(), JsonChecks::isTrue()});

    auto jsonValue = false;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 2);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected true but was false");
    EXPECT_EQ(errorList.at(1).getPath(), "some/path");
    EXPECT_EQ(errorList.at(1).getMessage(), "Expected true but was false");
}

TEST(JsonBooleanValueCheckTest, boolean_1failingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanValueCheck({JsonChecks::isFalse(), JsonChecks::isTrue()});

    auto jsonValue = false;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected true but was false");
}

TEST(JsonBooleanValueCheckTest, boolean_NoFailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanValueCheck({JsonChecks::isFalse(), JsonChecks::isFalse()});

    auto jsonValue = false;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonBooleanValueCheckTest, null) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanValueCheck({});

    auto jsonValue = QJsonValue::Null;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a boolean");
}

TEST(JsonBooleanValueCheckTest, object) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanValueCheck({});

    auto jsonValue = QJsonObject();
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a boolean");
}

TEST(JsonBooleanValueCheckTest, array) { // NOLINT(cert-err58-cpp)
    auto check = JsonBooleanValueCheck({});

    auto jsonValue = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be a boolean");
}

