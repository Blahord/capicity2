#include <gtest/gtest.h>

#include "json/checks/JsonImageValueCheck.h"
#include "json/JsonChecks.h"

TEST(JsonImageValueCheckTest, string) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    auto jsonValue = "Foo";
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

TEST(JsonImageValueCheckTest, integer) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    auto jsonValue = 1;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

TEST(JsonImageValueCheckTest, floatingPoint) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    auto jsonValue = 1.2;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

TEST(JsonImageValueCheckTest, boolean) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    auto jsonValue = false;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

TEST(JsonImageValueCheckTest, null) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    auto jsonValue = QJsonValue::Null;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}

TEST(JsonImageValueCheckTest, object_empty) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    auto jsonValue = QJsonObject();
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 3);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path/width");
    EXPECT_EQ(errorList.at(0).getMessage(), "Field not present");

    EXPECT_EQ(errorList.at(1).getPath(), "some/path/height");
    EXPECT_EQ(errorList.at(1).getMessage(), "Field not present");

    EXPECT_EQ(errorList.at(2).getPath(), "some/path/data");
    EXPECT_EQ(errorList.at(2).getMessage(), "Field not present");
}

TEST(JsonImageValueCheckTest, object_dataLengthNotFitting) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    auto jsonValue = QJsonObject{
        {"width",  16},
        {"height", 16},
        {"data",   "0123456789"}
    };
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path/data");
    EXPECT_EQ(errorList.at(0).getMessage(), "Length of data mus be 8 * width * height");
}

TEST(JsonImageValueCheckTest, object_dataContainsIllegalCharacters) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    QString data = "";
    for (int i = 0; i < 16 * 16; i++) {
        data += "0123456z";
    }

    auto jsonValue = QJsonObject{
        {"width",  16},
        {"height", 16},
        {"data",   data}
    };
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path/data");
    EXPECT_EQ(errorList.at(0).getMessage(), "Contains illegal characters");
}

TEST(JsonImageValueCheckTest, object) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    QString data = "";
    for (int i = 0; i < 16 * 16; i++) {
        data += "01234568";
    }

    auto jsonValue = QJsonObject({{"width",  16},
                                  {"height", 16},
                                  {"data",   data}});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonImageValueCheckTest, array) { // NOLINT(cert-err58-cpp)
    auto check = JsonImageValueCheck();

    auto jsonValue = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an object");
}


