#include <gtest/gtest.h>

#include <QtCore/QList>

#include "json/checks/JsonStringCheck.h"

TEST(JsonStringCheckTest, is_notEqual) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringCheck(JsonStringCheck::Type::IS, "FooBar");
    auto errorList = QList<JsonError>();

    check.check("BarFoo", "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected 'FooBar' but was 'BarFoo'");
}

TEST(JsonStringCheckTest, is_equal) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringCheck(JsonStringCheck::Type::IS, "FooBar");
    auto errorList = QList<JsonError>();

    check.check("FooBar", "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonStringCheckTest, regexp_notMatching) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringCheck(JsonStringCheck::Type::REGEXP, "^a*b*$");
    auto errorList = QList<JsonError>();

    check.check("bbbaaa", "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Value 'bbbaaa' does not match regular expression '^a*b*$'");
}

TEST(JsonStringCheckTest, is_matching) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringCheck(JsonStringCheck::Type::REGEXP, "^a*b*$");
    auto errorList = QList<JsonError>();

    check.check("bbb", "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonStringCheckTest, invalidType) { // NOLINT(cert-err58-cpp)
    auto check = JsonStringCheck(JsonStringCheck::Type::NO_TYPE, "");
    auto errorList = QList<JsonError>();

    check.check("bbbaaa", "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "BUG: Unhandled type -1");
}
