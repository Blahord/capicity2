#include <gtest/gtest.h>

#include "json/checks/JsonArrayCheck.h"
#include "json/JsonChecks.h"
#include "json/checks/JsonArrayValueCheck.h"

TEST(JsonArrayValueCheckTest, string) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayValueCheck({});

    auto jsonValue = "Foo";
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an array");
}

TEST(JsonArrayValueCheckTest, integer) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayValueCheck({});

    auto jsonValue = 1;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an array");
}

TEST(JsonArrayValueCheckTest, floatingPoint) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayValueCheck({});

    auto jsonValue = 1.2;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an array");
}

TEST(JsonArrayValueCheckTest, boolean) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayValueCheck({});

    auto jsonValue = false;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an array");
}

TEST(JsonArrayValueCheckTest, null) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayValueCheck({});

    auto jsonValue = QJsonValue::Null;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an array");
}

TEST(JsonArrayValueCheckTest, object) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayValueCheck({});

    auto jsonValue = QJsonObject();
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an array");
}

TEST(JsonArrayValueCheckTest, array_2FailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayValueCheck({JsonChecks::minLength(3), JsonChecks::maxLength(1)});

    auto jsonValue = QJsonArray({1, 2});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 2);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (2) too short. Minimum length is 3");

    EXPECT_EQ(errorList.at(1).getPath(), "some/path");
    EXPECT_EQ(errorList.at(1).getMessage(), "Array size (2) too long. Maximum length is 1");
}

TEST(JsonArrayValueCheckTest, array_1FailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayValueCheck({JsonChecks::minLength(0), JsonChecks::maxLength(1)});

    auto jsonValue = QJsonArray({1, 2});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (2) too long. Maximum length is 1");
}

TEST(JsonArrayValueCheckTest, array_NoFailingChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayValueCheck({JsonChecks::minLength(0), JsonChecks::maxLength(3)});

    auto jsonValue = QJsonArray({1, 2});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}
