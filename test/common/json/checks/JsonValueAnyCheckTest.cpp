#include <gtest/gtest.h>

#include "json/checks/JsonValueAnyCheck.h"
#include "json/JsonChecks.h"

TEST(JsonValueAnyCheckTest, accept_1) { // NOLINT(cert-err58-cpp)
    JsonValueAnyCheck check(
        {
            {"string",  JsonChecks::stringValue()},
            {"integer", JsonChecks::integerValue()}
        }
    );

    QList<JsonError> errorList;
    check.check("foo bar", "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonValueAnyCheckTest, accept_2) { // NOLINT(cert-err58-cpp)
    JsonValueAnyCheck check(
        {
            {"string",  JsonChecks::stringValue()},
            {"integer", JsonChecks::integerValue()}
        }
    );

    QList<JsonError> errorList;
    check.check(123, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonValueAnyCheckTest, fail) { // NOLINT(cert-err58-cpp)
    JsonValueAnyCheck check(
        {
            {"string",  JsonChecks::stringValue()},
            {"integer", JsonChecks::integerValue()}
        }
    );

    QList<JsonError> errorList;
    check.check(123.5, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path{integer}");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be an integer");
}
