#include <gtest/gtest.h>

#include "json/checks/JsonNullValueCheck.h"
#include "json/JsonChecks.h"

TEST(JsonNullValueCheckTest, string) { // NOLINT(cert-err58-cpp)
    auto check = JsonNullValueCheck();

    auto jsonValue = "Foo";
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be null");
}

TEST(JsonNullValueCheckTest, integer) { // NOLINT(cert-err58-cpp)
    auto check = JsonNullValueCheck();

    auto jsonValue = 1;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be null");
}

TEST(JsonNullValueCheckTest, floatingPoint) { // NOLINT(cert-err58-cpp)
    auto check = JsonNullValueCheck();

    auto jsonValue = 1.2;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be null");
}

TEST(JsonNullValueCheckTest, boolean) { // NOLINT(cert-err58-cpp)
    auto check = JsonNullValueCheck();

    auto jsonValue = false;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be null");
}

TEST(JsonNullValueCheckTest, null) { // NOLINT(cert-err58-cpp)
    auto check = JsonNullValueCheck();

    auto jsonValue = QJsonValue::Null;
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonNullValueCheckTest, object) { // NOLINT(cert-err58-cpp)
    auto check = JsonNullValueCheck();

    auto jsonValue = QJsonObject();
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be null");
}

TEST(JsonNullValueCheckTest, array) { // NOLINT(cert-err58-cpp)
    auto check = JsonNullValueCheck();

    auto jsonValue = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(jsonValue, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Must be null");
}


