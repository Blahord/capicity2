#include <gtest/gtest.h>

#include <QJsonArray>

#include "json/checks/JsonArrayLengthCheck.h"
#include "json/JsonChecks.h"

TEST(JsonArrayLengthCheck, minLength_0) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MIN_LENGTH, 3);
    auto array = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (0) too short. Minimum length is 3");
}

TEST(JsonArrayLengthCheck, minLength_1) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MIN_LENGTH, 3);
    auto array = QJsonArray({(QJsonValue) 0});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (1) too short. Minimum length is 3");
}

TEST(JsonArrayLengthCheck, minLength_2) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MIN_LENGTH, 3);
    auto array = QJsonArray({0, 1});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (2) too short. Minimum length is 3");
}

TEST(JsonArrayLengthCheck, minLength_3) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MIN_LENGTH, 3);
    auto array = QJsonArray({0, 1, 2});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayLengthCheck, minLength_4) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MIN_LENGTH, 3);
    auto array = QJsonArray({0, 1, 2, 3});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayLengthCheck, maxLength_0) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MAX_LENGTH, 3);
    auto array = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayLengthCheck, maxLength_1) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MAX_LENGTH, 3);
    auto array = QJsonArray({(QJsonValue) 0});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayLengthCheck, maxLength_2) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MAX_LENGTH, 3);
    auto array = QJsonArray({0, 1});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayLengthCheck, maxLength_3) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MAX_LENGTH, 3);
    auto array = QJsonArray({0, 1, 2});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayLengthCheck, maxLength_4) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::MAX_LENGTH, 3);
    auto array = QJsonArray({0, 1, 2, 3});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (4) too long. Maximum length is 3");
}

TEST(JsonArrayLengthCheck, isLength_0) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::IS_LENGTH, 3);
    auto array = QJsonArray({});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (0) has wrong size. Expected size is 3");
}

TEST(JsonArrayLengthCheck, isLength_1) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::IS_LENGTH, 3);
    auto array = QJsonArray({(QJsonValue) 0});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (1) has wrong size. Expected size is 3");
}

TEST(JsonArrayLengthCheck, isLength_2) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::IS_LENGTH, 3);
    auto array = QJsonArray({0, 1});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (2) has wrong size. Expected size is 3");
}

TEST(JsonArrayLengthCheck, isLength_3) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::IS_LENGTH, 3);
    auto array = QJsonArray({0, 1, 2});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonArrayLengthCheck, isLength_4) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::IS_LENGTH, 3);
    auto array = QJsonArray({0, 1, 2, 3});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Array size (4) has wrong size. Expected size is 3");
}

TEST(JsonArrayLengthCheck, invlaidType) { // NOLINT(cert-err58-cpp)
    auto check = JsonArrayLengthCheck(JsonArrayLengthCheck::Type::NO_TYPE, 3);
    auto array = QJsonArray({0, 1, 2, 3});
    auto errorList = QList<JsonError>();

    check.check(array, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "BUG: Unhandled type -1");
}
