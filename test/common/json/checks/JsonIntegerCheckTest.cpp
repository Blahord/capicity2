#include <gtest/gtest.h>

#include "json/checks/JsonIntegerCheck.h"
#include "json/JsonChecks.h"

TEST(JsonIntegerCheckTest, is_wrongNumber) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerCheck(JsonIntegerCheck::Type::IS, 42);
    auto errorList = QList<JsonError>();

    check.check(1337, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected 42 but was 1337");
}

TEST(JsonIntegerCheckTest, is_rightNumber) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerCheck(JsonIntegerCheck::Type::IS, 42);
    auto errorList = QList<JsonError>();

    check.check(42, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonIntegerCheckTest, min_tooSmallNumber) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerCheck(JsonIntegerCheck::Type::MIN, 1337);
    auto errorList = QList<JsonError>();

    check.check(42, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected a minimum value of 1337 but was 42");
}

TEST(JsonIntegerCheckTest, min_bigEnoughNumber) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerCheck(JsonIntegerCheck::Type::MIN, 42);
    auto errorList = QList<JsonError>();

    check.check(42, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonIntegerCheckTest, max_tooBigNumber) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerCheck(JsonIntegerCheck::Type::MAX, 42);
    auto errorList = QList<JsonError>();

    check.check(1337, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "Expected a maximum value of 42 but was 1337");
}

TEST(JsonIntegerCheckTest, max_smallEnoughNumber) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerCheck(JsonIntegerCheck::Type::MAX, 42);
    auto errorList = QList<JsonError>();

    check.check(42, "some/path", errorList);
    EXPECT_TRUE(errorList.isEmpty());
}

TEST(JsonIntegerCheckTest, invlaidType) { // NOLINT(cert-err58-cpp)
    auto check = JsonIntegerCheck(JsonIntegerCheck::Type::NO_TYPE, 42);
    auto errorList = QList<JsonError>();

    check.check(42, "some/path", errorList);
    ASSERT_EQ(errorList.size(), 1);
    EXPECT_EQ(errorList.at(0).getPath(), "some/path");
    EXPECT_EQ(errorList.at(0).getMessage(), "BUG: Unhandled type -1");
}
