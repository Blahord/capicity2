#include <gtest/gtest.h>

#include "json/JsonChecks.h"

TEST(JsonChecksTest, any_2_options) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::any(
        "1", JsonChecks::integerValue(),
        "2", JsonChecks::stringValue()
    );
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({1, 2}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, nullValue) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::nullValue();
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({1, 2}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, stringValue) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::stringValue();
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({1, 2}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, stringValue_check_array) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::stringValue(JsonChecks::is("foo"));
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({1, 2}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, stringValue_check_string) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::stringValue(JsonChecks::is("foo"));
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check("bar", "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, booleanValue) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::booleanValue();
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({1, 2}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, integerValue) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::integerValue();
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({1, 2}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, integerValue_1_check) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::integerValue(JsonChecks::is(42));
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonValue(43), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, integerValue_2_checks) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::integerValue(JsonChecks::is(42), JsonChecks::is(1337));
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonValue(43), "/", errorList);
        EXPECT_EQ(errorList.size(), 2);
    }
}

TEST(JsonChecksTest, imageValue) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::imageValue();
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({1, 2}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, objectValue_0_args) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::objectValue();
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({1, 2}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, objectValue_1_arg) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::objectValue(
        JsonChecks::requiredField("foo", JsonChecks::stringValue())
    );
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, objectValue_2_args) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::objectValue(
        JsonChecks::requiredField("foo", JsonChecks::stringValue()),
        JsonChecks::requiredField("bar", JsonChecks::stringValue())
    );
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({}), "/", errorList);
        EXPECT_EQ(errorList.size(), 2);
    }
}

TEST(JsonChecksTest, objectValue_3_args) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::objectValue(
        JsonChecks::requiredField("foo", JsonChecks::stringValue()),
        JsonChecks::requiredField("bar", JsonChecks::stringValue()),
        JsonChecks::requiredField("doo", JsonChecks::stringValue())
    );
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({}), "/", errorList);
        EXPECT_EQ(errorList.size(), 3);
    }
}

TEST(JsonChecksTest, arrayValue_1_arg) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::arrayValue(
        JsonChecks::minLength(2)
    );
    EXPECT_NE(check, nullptr);
    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, requiredField_noChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::requiredField("foo");
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, requiredField_valueCheck) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::requiredField("foo", JsonChecks::stringValue(JsonChecks::is("bar")));
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({{"foo", "BBB"}}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, optionalField_valueCheck_fieldPresent) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::optionalField(
        "foo",
        JsonChecks::stringValue(JsonChecks::is("bar"))
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({{"foo", "BBB"}}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, optionalField_valueCheck_fieldAbsent) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::optionalField(
        "foo",
        JsonChecks::stringValue(JsonChecks::is("bar"))
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({}), "/", errorList);
        EXPECT_EQ(errorList.size(), 0);
    }
}

TEST(JsonChecksTest, requiredField_stringCheck) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::requiredField("foo", JsonChecks::is("bar"));
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({{"foo", "BBB"}}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, optionalField_stringCheck_fieldPresent) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::optionalField(
        "foo",
        JsonChecks::is("bar")
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({{"foo", "BBB"}}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, optionalField_stringCheck_fieldAbsent) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::optionalField(
        "foo",
        JsonChecks::stringValue(JsonChecks::is("bar"))
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({}), "/", errorList);
        EXPECT_EQ(errorList.size(), 0);
    }
}

TEST(JsonChecksTest, requiredField_booleanCheck) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::requiredField("foo", JsonChecks::isTrue());
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({{"foo", false}}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, requiredField_arrayCheck_2args) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::requiredField(
        "foo",
        JsonChecks::minLength(2),
        JsonChecks::minLength(3)
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({{"foo", QJsonArray()}}), "/", errorList);
        EXPECT_EQ(errorList.size(), 2);
    }
}

TEST(JsonChecksTest, optionalField_arrayCheck_fieldPresent) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::optionalField(
        "foo",
        JsonChecks::minLength(2)
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({{"foo", QJsonObject({})}}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, optionalField_arrayCheck_fieldAbsent) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::optionalField(
        "foo",
        JsonChecks::minLength(2)
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({}), "/", errorList);
        EXPECT_EQ(errorList.size(), 0);
    }
}

TEST(JsonChecksTest, requiredField_integerCheck) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::requiredField(
        "foo",
        JsonChecks::is(42)
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({{"foo", 1337}}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, requiredField_integerCheck_2arg) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::requiredField(
        "foo",
        JsonChecks::min(42),
        JsonChecks::max(1337)
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonObject({{"foo", 4711}}), "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, requiredField_objectChecks) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::requiredField(
        "foo",
        {
            JsonChecks::requiredField("foo", JsonChecks::is(42)),
            JsonChecks::requiredField("bar", JsonChecks::is(1337))
        }
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(
            QJsonObject{
                {"foo", QJsonObject{
                    {"foo", 42},
                    {"bar", 42}
                }}
            },
            "/", errorList
        );
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, is_string_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::is("foo");
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check("foo", "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, is_string_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::is("foo");
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check("bar", "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, regexp_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::regexp("^a*b*$");
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check("bbb", "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, regexp_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::regexp("^a*b*$");
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check("bbbaaa", "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, is_integer_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::is(42);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(42, "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, is_integer_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::is(42);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(1337, "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, min_integer_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::min(42);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(42, "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, min_integer_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::min(1337);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(42, "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, max_integer_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::max(42);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(42, "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, max_integer_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::max(42);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(1337, "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, isTrue_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::isTrue();
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(true, "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, isTrue_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::isTrue();
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(false, "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, isFalse_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::isFalse();
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(false, "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, isFalse_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::isFalse();
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(true, "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, minLength_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::minLength(2);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check({1, 2, 3}, "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, minLength_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::minLength(2);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check({1}, "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, maxLength_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::maxLength(2);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check({1}, "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, maxLength_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::maxLength(2);
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check({1, 2, 3}, "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, contains_value_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::contains(
        "42",
        JsonChecks::integerValue(JsonChecks::is(42))
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check({1, 2, 3, 42, 1337}, "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, contains_value_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::contains(
        "42",
        JsonChecks::integerValue(JsonChecks::is(42))
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check({1, 2, 3, 1337}, "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, contains_string_passing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::contains(
        "Foo",
        JsonChecks::is("Foo")
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check({"Foo", "Bar"}, "/", errorList);
        EXPECT_TRUE(errorList.isEmpty());
    }
}

TEST(JsonChecksTest, contains_string_failing) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::contains(
        "Foo",
        JsonChecks::stringValue(JsonChecks::is("Foo"))
    );
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check({"Bar"}, "/", errorList);
        EXPECT_EQ(errorList.size(), 1);
    }
}

TEST(JsonChecksTest, forAll) { // NOLINT(cert-err58-cpp)
    auto check = JsonChecks::forAll(JsonChecks::stringValue());
    EXPECT_NE(check, nullptr);

    if (check) {
        auto errorList = QList<JsonError>();
        check->check(QJsonArray({1, 2}), "/", errorList);
        EXPECT_EQ(errorList.size(), 2);
    }
}
