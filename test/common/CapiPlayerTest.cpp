#include <gtest/gtest.h>

#include "CapiPlayer.h"

class CapiPlayerTest : public ::testing::Test {

};

TEST_F(CapiPlayerTest, getId) { // NOLINT(cert-err58-cpp)
    CapiPlayer p1("1");
    CapiPlayer p2("2");

    ASSERT_EQ(p1.getId(), "1");
    ASSERT_EQ(p2.getId(), "2");
}


TEST_F(CapiPlayerTest, getName) { // NOLINT(cert-err58-cpp)
    CapiPlayer p("1");

    p.setName("foo");
    ASSERT_EQ(p.getName(), "foo");

    p.setName("bar");
    ASSERT_EQ(p.getName(), "bar");
}

TEST_F(CapiPlayerTest, getAvatarData) { // NOLINT(cert-err58-cpp)
    CapiPlayer p("1");

    p.setAvatarData("foo");
    ASSERT_EQ(p.getAvatarData(), "foo");

    p.setAvatarData("bar");
    ASSERT_EQ(p.getAvatarData(), "bar");
}

TEST_F(CapiPlayerTest, isConnected) { // NOLINT(cert-err58-cpp)
    CapiPlayer p("1");

    p.setConnected(true);
    ASSERT_TRUE(p.isConnected());

    p.setConnected(false);
    ASSERT_FALSE(p.isConnected());
}
