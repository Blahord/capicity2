#include <gtest/gtest.h>
#include <QTcpSocket>
#include <QTcpServer>
#include <QSignalSpy>

#include "CapiConnection.h"

class CapiConnectionTest : public ::testing::Test {

    protected:
        void SetUp() override {
            Test::SetUp();

            server = new QTcpServer(nullptr);
            server->listen(QHostAddress::LocalHostIPv6, 1234);

            clientSocket = new QTcpSocket(nullptr);
            clientSocket->connectToHost(QHostAddress::LocalHostIPv6, 1234);
            ASSERT_TRUE(clientSocket->waitForConnected(100));

            ASSERT_TRUE(server->waitForNewConnection(100));
            serverSocket = server->nextPendingConnection();

            connection = new CapiConnection(clientSocket, 1024);
        }

        void TearDown() override {
            delete connection;

            server->close();
            server->deleteLater();
            server = nullptr;

            Test::TearDown();
        }

        void sendToClient(const QString& message) {
            serverSocket->write(message.toUtf8());
            serverSocket->flush();
            ASSERT_TRUE(clientSocket->waitForReadyRead(100));
        }

        QTcpServer* server = nullptr;
        CapiConnection* connection = nullptr;
        
        QAbstractSocket* clientSocket = nullptr;
        QAbstractSocket* serverSocket = nullptr;
};

TEST_F(CapiConnectionTest, testDeleteWithoutSocket) { // NOLINT(cert-err58-cpp)
    auto localConnection = new CapiConnection(nullptr);
    delete localConnection;
}

TEST_F(CapiConnectionTest, testParent) { // NOLINT(cert-err58-cpp)
    auto obj = new QObject();
    auto socket = new QTcpSocket();
    auto connection = new CapiConnection(socket, 1024, obj);

    ASSERT_EQ(connection->parent(), obj);

    delete connection;
    delete obj;
}

TEST_F (CapiConnectionTest, testReadSocketInput_simple) { // NOLINT(cert-err58-cpp)
    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    sendToClient("\"\"\"\n{}");
    ASSERT_EQ(newMessageSpy.count(), 1);
}

TEST_F (CapiConnectionTest, testReadSocketInput_complete_tow_parts) { // NOLINT(cert-err58-cpp)
    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    sendToClient("\"\"");
    ASSERT_EQ(newMessageSpy.count(), 0);

    sendToClient("\"{}");
    ASSERT_EQ(newMessageSpy.count(), 1);
}

TEST_F (CapiConnectionTest, testReadSocketInput_restart) { // NOLINT(cert-err58-cpp)
    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    sendToClient(R"("""{)");
    ASSERT_EQ(newMessageSpy.count(), 0);

    sendToClient(R"("""{})");
    ASSERT_EQ(newMessageSpy.count(), 1);
}

TEST_F (CapiConnectionTest, testReadSocketInput_restartInOneMessage) { // NOLINT(cert-err58-cpp)
    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    sendToClient(R"("""{"""{})");
    ASSERT_EQ(newMessageSpy.count(), 1);
}

TEST_F (CapiConnectionTest, testReadSocketInput_multiMessage) { // NOLINT(cert-err58-cpp)
    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    sendToClient(R"("""{}{}{})");
    ASSERT_EQ(newMessageSpy.count(), 3);
}

TEST_F(CapiConnectionTest, testOverflowBuffer) { // NOLINT(cert-err58-cpp)
    QString message = "abababababababababababababababababababababababababababababababab";
    message = message + message;
    message = message + message;
    message = message + message;
    message = message + message;
    message = message + message;
    message = message + message;

    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    sendToClient(message);
    ASSERT_EQ(newMessageSpy.count(), 0);

    sendToClient(R"("""{})");
    ASSERT_EQ(newMessageSpy.count(), 1);
}

TEST_F(CapiConnectionTest, testEscapeCharacters) { // NOLINT(cert-err58-cpp)
    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    QJsonObject expectedMessage;
    expectedMessage.insert("Foo", "Bar");

    sendToClient(R"("""{"Foo": "B\ar"})");
    ASSERT_EQ(newMessageSpy.count(), 1);
    ASSERT_EQ(newMessageSpy.at(0).at(0), QVariant(expectedMessage));
}

TEST_F(CapiConnectionTest, testJsonCharacters) { // NOLINT(cert-err58-cpp)
    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    QJsonObject expectedMessage;
    expectedMessage.insert("Foo", "B\\ar{}\"");

    sendToClient(R"("""{"Foo": "B\\ar{}\""})");
    ASSERT_EQ(newMessageSpy.count(), 1);
    ASSERT_EQ(newMessageSpy.at(0).at(0), QVariant(expectedMessage));
}

TEST_F(CapiConnectionTest, testMultiLevelJson) { // NOLINT(cert-err58-cpp)
    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    QJsonObject subObject;
    subObject.insert("Bar", "Doo Goo");

    QJsonObject expectedMessage;
    expectedMessage.insert("Foo", subObject);

    sendToClient(R"("""{"Foo": {"B\ar" : "Doo Goo"}})");
    ASSERT_EQ(newMessageSpy.count(), 1);
    ASSERT_EQ(newMessageSpy.at(0).at(0), QVariant(expectedMessage));
}

TEST_F(CapiConnectionTest, testInvalidJson) { // NOLINT(cert-err58-cpp)
    QSignalSpy newMessageSpy(connection, &CapiConnection::gotMessage);

    sendToClient(R"("""{\\"Foo": abc})");
    ASSERT_EQ(newMessageSpy.count(), 0);
}

TEST_F(CapiConnectionTest, testSendData) { // NOLINT(cert-err58-cpp)
    auto serverConnection = new CapiConnection(serverSocket);
    auto serverSpy = new QSignalSpy(serverConnection, &CapiConnection::gotMessage);

    QJsonObject subObject;
    subObject.insert("Bar", "Doo Goo");

    QJsonObject message;
    message.insert("Foo", subObject);

    connection->send(message);

    ASSERT_TRUE(serverSocket->waitForReadyRead(100));
    ASSERT_EQ(serverSpy->count(), 1);
    ASSERT_EQ(serverSpy->at(0).at(0), message);

    delete serverSpy;
    delete serverConnection;
}
