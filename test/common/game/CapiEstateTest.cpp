#include <gtest/gtest.h>

#include "game/CapiEstate.h"

class CapiEstateTest : public ::testing::Test {

    protected:
        CapiEstate estate = CapiEstate("id-1", 12, 13, 14, 15, 16, 17, 18, 19, 20, "Foo");
};

TEST_F(CapiEstateTest, _descruct) { // NOLINT(cert-err58-cpp)
    delete new CapiEstate("id-2", 12, 13, 14, 15, 16, 17, 18, 19, 20, "foo");
}

TEST_F(CapiEstateTest, getId) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(estate.getId(), "id-1");
}

TEST_F(CapiEstateTest, owner) { // NOLINT(cert-err58-cpp)
    auto p1 = std::make_shared<CapiPlayer>("1");
    auto gp1 = std::make_shared<CapiGamePlayer>(p1);
    auto gp2 = std::make_shared<CapiGamePlayer>(p1);

    estate.setOwner(gp1);
    ASSERT_EQ(estate.getOwner(), gp1);

    estate.setOwner(gp2);
    ASSERT_EQ(estate.getOwner(), gp2);
}


TEST_F(CapiEstateTest, getImage) { // NOLINT(cert-err58-cpp)
    estate.setImage("img");
    ASSERT_EQ(estate.getImage(), "img");
}

TEST_F(CapiEstateTest, getPrice) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(estate.getPrice(), 12);
}

TEST_F(CapiEstateTest, getRent0) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(estate.getRent0(), 13);
}

TEST_F(CapiEstateTest, getRent1) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(estate.getRent1(), 14);
}

TEST_F(CapiEstateTest, getRent2) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(estate.getRent2(), 15);
}

TEST_F(CapiEstateTest, getRent3) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(estate.getRent3(), 16);
}

TEST_F(CapiEstateTest, getRent4) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(estate.getRent4(), 17);
}

TEST_F(CapiEstateTest, getRent5) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(estate.getRent5(), 18);
}

TEST_F(CapiEstateTest, getTaxAmount) {
    ASSERT_EQ(estate.getTaxAmount(), 19);
}

TEST_F(CapiEstateTest, getTaxPercent) {
    ASSERT_EQ(estate.getTaxPercent(), 20);
}

TEST_F(CapiEstateTest, getPayTargetId) {
    ASSERT_EQ(estate.getPayTargetId(), "Foo");
}
