#include <gtest/gtest.h>

#include "game/CapiGame.h"
#include "CapiPlayer.h"

class CapiGameTest : public ::testing::Test {

    protected:
        CapiGame game = CapiGame("12345");
};

TEST_F(CapiGameTest, getId) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(game.getId(), "12345");
}

TEST_F(CapiGameTest, bgColor) { // NOLINT(cert-err58-cpp)
    game.setBgColor("001122");
    ASSERT_EQ(game.getBgColor(), "001122");

    game.setBgColor("123456");
    ASSERT_EQ(game.getBgColor(), "123456");
}

TEST_F(CapiGameTest, getPlayers) { // NOLINT(cert-err58-cpp)
    auto p1 = std::make_shared<CapiPlayer>("1");
    auto p2 = std::make_shared<CapiPlayer>("2");
    auto p3 = std::make_shared<CapiPlayer>("3");

    ASSERT_EQ(game.getPlayers().size(), 0);

    game.addPlayer(p1);
    ASSERT_EQ(game.getPlayers().size(), 1);
    ASSERT_EQ(game.getPlayers().at(0)->getPlayer(), p1);

    game.addPlayer(p1);
    ASSERT_EQ(game.getPlayers().size(), 1);
    ASSERT_EQ(game.getPlayers().at(0)->getPlayer(), p1);

    game.addPlayer(p2);
    ASSERT_EQ(game.getPlayers().size(), 2);
    ASSERT_EQ(game.getPlayers().at(0)->getPlayer(), p1);
    ASSERT_EQ(game.getPlayers().at(1)->getPlayer(), p2);

    game.addPlayer(p3);
    ASSERT_EQ(game.getPlayers().size(), 3);
    ASSERT_EQ(game.getPlayers().at(0)->getPlayer(), p1);
    ASSERT_EQ(game.getPlayers().at(1)->getPlayer(), p2);
    ASSERT_EQ(game.getPlayers().at(2)->getPlayer(), p3);

    game.removePlayer(p3);
    game.removePlayer(p2);
    game.removePlayer(p1);
}

TEST_F(CapiGameTest, containsPlayer) { // NOLINT(cert-err58-cpp)
    auto p1 = std::make_shared<CapiPlayer>("1");
    auto p2 = std::make_shared<CapiPlayer>("2");
    auto p3 = std::make_shared<CapiPlayer>("3");

    ASSERT_EQ(game.containsPlayer(p1), false);
    ASSERT_EQ(game.containsPlayer(p2), false);
    ASSERT_EQ(game.containsPlayer(p3), false);

    game.addPlayer(p1);
    ASSERT_EQ(game.containsPlayer(p1), true);
    ASSERT_EQ(game.containsPlayer(p2), false);
    ASSERT_EQ(game.containsPlayer(p3), false);

    game.addPlayer(p2);
    ASSERT_EQ(game.containsPlayer(p1), true);
    ASSERT_EQ(game.containsPlayer(p2), true);
    ASSERT_EQ(game.containsPlayer(p3), false);

    game.addPlayer(p3);
    ASSERT_EQ(game.containsPlayer(p1), true);
    ASSERT_EQ(game.containsPlayer(p2), true);
    ASSERT_EQ(game.containsPlayer(p3), true);

    game.removePlayer(p2);
    ASSERT_EQ(game.containsPlayer(p1), true);
    ASSERT_EQ(game.containsPlayer(p2), false);
    ASSERT_EQ(game.containsPlayer(p3), true);

    game.removePlayer(p1);
    ASSERT_EQ(game.containsPlayer(p1), false);
    ASSERT_EQ(game.containsPlayer(p2), false);
    ASSERT_EQ(game.containsPlayer(p3), true);

    game.removePlayer(p3);
    ASSERT_EQ(game.containsPlayer(p1), false);
    ASSERT_EQ(game.containsPlayer(p2), false);
    ASSERT_EQ(game.containsPlayer(p3), false);
}

TEST_F(CapiGameTest, findPlayer_byPlayer) { // NOLINT(cert-err58-cpp)
    auto p1 = std::make_shared<CapiPlayer>("1");
    auto p2 = std::make_shared<CapiPlayer>("2");
    auto p3 = std::make_shared<CapiPlayer>("3");

    ASSERT_EQ(game.findPlayer(p1), nullptr);

    game.addPlayer(p1);
    game.addPlayer(p2);
    game.addPlayer(p3);

    ASSERT_NE(game.findPlayer(p1), nullptr);
    ASSERT_EQ(game.findPlayer(p1)->getPlayer(), p1);

    ASSERT_NE(game.findPlayer(p2), nullptr);
    ASSERT_EQ(game.findPlayer(p2)->getPlayer(), p2);

    ASSERT_NE(game.findPlayer(p3), nullptr);
    ASSERT_EQ(game.findPlayer(p3)->getPlayer(), p3);

    game.removePlayer(p3);
    game.removePlayer(p2);
    game.removePlayer(p1);
}

TEST_F(CapiGameTest, findPlayer_byName) { // NOLINT(cert-err58-cpp)
    auto p1 = std::make_shared<CapiPlayer>("1");
    auto p2 = std::make_shared<CapiPlayer>("2");
    auto p3 = std::make_shared<CapiPlayer>("3");

    p1->setName("1");
    p2->setName("2");
    p3->setName("3");

    ASSERT_EQ(game.findPlayer("1"), nullptr);

    game.addPlayer(p1);
    game.addPlayer(p2);
    game.addPlayer(p3);

    ASSERT_NE(game.findPlayer("1"), nullptr);
    ASSERT_EQ(game.findPlayer("1")->getPlayer(), p1);

    ASSERT_NE(game.findPlayer("2"), nullptr);
    ASSERT_EQ(game.findPlayer("2")->getPlayer(), p2);

    ASSERT_NE(game.findPlayer("3"), nullptr);
    ASSERT_EQ(game.findPlayer("3")->getPlayer(), p3);

    game.removePlayer(p3);
    game.removePlayer(p2);
    game.removePlayer(p1);
}

TEST_F(CapiGameTest, removeFromPlayerPlayer) { // NOLINT(cert-err58-cpp)
    auto p1 = std::make_shared<CapiPlayer>("1");
    auto p2 = std::make_shared<CapiPlayer>("2");
    auto p3 = std::make_shared<CapiPlayer>("3");

    game.addPlayer(p1);
    game.addPlayer(p2);
    game.addPlayer(p3);

    game.setPlayerOrder({"1", "2", "3"});

    game.removeFromPlayerOrder(p2);

    ASSERT_EQ(game.getPlayerOrder().size(), 2);
    ASSERT_EQ(game.getPlayerOrder().at(0)->getPlayer(), p1);
    ASSERT_EQ(game.getPlayerOrder().at(1)->getPlayer(), p3);

    game.removePlayer(p3);
    game.removePlayer(p2);
    game.removePlayer(p1);
}

TEST_F(CapiGameTest, removePlayer) { // NOLINT(cert-err58-cpp)
    auto p1 = std::make_shared<CapiPlayer>("1");
    auto p2 = std::make_shared<CapiPlayer>("2");
    auto p3 = std::make_shared<CapiPlayer>("3");

    game.addPlayer(p1);
    game.addPlayer(p2);
    ASSERT_EQ(game.getPlayers().size(), 2);

    game.removePlayer(p3);
    ASSERT_EQ(game.getPlayers().size(), 2);

    game.removePlayer(p1);
    ASSERT_EQ(game.getPlayers().size(), 1);

    game.removePlayer(p2);
    ASSERT_EQ(game.getPlayers().size(), 0);

    game.removePlayer(p1);
    game.removePlayer(p2);
    game.removePlayer(p3);
}

TEST_F(CapiGameTest, owner) { // NOLINT(cert-err58-cpp)
    auto p1 = std::make_shared<CapiPlayer>("1");
    auto p2 = std::make_shared<CapiPlayer>("2");

    game.setOwner(p1);
    ASSERT_EQ(game.getOwner(), p1);

    game.setOwner(p2);
    ASSERT_EQ(game.getOwner(), p2);

    game.setOwner(nullptr);
    ASSERT_EQ(game.getOwner(), nullptr);
}

TEST_F(CapiGameTest, maxHouses) { // NOLINT(cert-err58-cpp)
    game.setMaxHouses(12);
    ASSERT_EQ(game.getMaxHouses(), 12);

    game.setMaxHouses(23);
    ASSERT_EQ(game.getMaxHouses(), 23);
}

TEST_F(CapiGameTest, maxHotels) { // NOLINT(cert-err58-cpp)
    game.setMaxHotels(12);
    ASSERT_EQ(game.getMaxHotels(), 12);

    game.setMaxHotels(23);
    ASSERT_EQ(game.getMaxHotels(), 23);
}

TEST_F(CapiGameTest, startMoney) { // NOLINT(cert-err58-cpp)
    game.setStartMoney(1500);
    ASSERT_EQ(game.getStartMoney(), 1500);

    game.setStartMoney(2000);
    ASSERT_EQ(game.getStartMoney(), 2000);
}

TEST_F(CapiGameTest, collectFine) { // NOLINT(cert-err58-cpp)
    game.setCollectFine(true);
    ASSERT_EQ(game.isCollectFine(), true);

    game.setCollectFine(false);
    ASSERT_EQ(game.isCollectFine(), false);
}

TEST_F(CapiGameTest, shuffleCards) { // NOLINT(cert-err58-cpp)
    game.setShuffleCards(true);
    ASSERT_EQ(game.isShuffleCards(), true);

    game.setShuffleCards(false);
    ASSERT_EQ(game.isShuffleCards(), false);
}

TEST_F(CapiGameTest, auctions) { // NOLINT(cert-err58-cpp)
    game.setAuctions(true);
    ASSERT_EQ(game.isAuctions(), true);

    game.setAuctions(false);
    ASSERT_EQ(game.isAuctions(), false);
}

TEST_F(CapiGameTest, doubleMoneyOnExactLanding) { // NOLINT(cert-err58-cpp)
    game.setDoubleMoneyOnExactLanding(true);
    ASSERT_EQ(game.isDoubleMoneyOnExactLanding(), true);

    game.setDoubleMoneyOnExactLanding(false);
    ASSERT_EQ(game.isDoubleMoneyOnExactLanding(), false);
}

TEST_F(CapiGameTest, unlimitedHouses) { // NOLINT(cert-err58-cpp)
    game.setUnlimitedHouses(true);
    ASSERT_EQ(game.isUnlimitedHouses(), true);

    game.setUnlimitedHouses(false);
    ASSERT_EQ(game.isUnlimitedHouses(), false);
}

TEST_F(CapiGameTest, jailNoRent) { // NOLINT(cert-err58-cpp)
    game.setJailNoRent(true);
    ASSERT_EQ(game.isJailNoRent(), true);

    game.setJailNoRent(false);
    ASSERT_EQ(game.isJailNoRent(), false);
}

TEST_F(CapiGameTest, autoTax) { // NOLINT(cert-err58-cpp)
    game.setAutoTax(true);
    ASSERT_EQ(game.isAutoTax(), true);

    game.setAutoTax(false);
    ASSERT_EQ(game.isAutoTax(), false);
}

TEST_F(CapiGameTest, state) { // NOLINT(cert-err58-cpp)
    game.setState(GameState::RUN);
    ASSERT_EQ(game.getState(), GameState::RUN);

    game.setState(GameState::CONFIG);
    ASSERT_EQ(game.getState(), GameState::CONFIG);
}

TEST_F(CapiGameTest, turnState) { // NOLINT(cert-err58-cpp)
    game.setTurnState(TurnState::ROLL);
    ASSERT_EQ(game.getTurnState(), TurnState::ROLL);

    game.setTurnState(TurnState::MOVE);
    ASSERT_EQ(game.getTurnState(), TurnState::MOVE);
}

TEST_F(CapiGameTest, playerOrder) { // NOLINT(cert-err58-cpp)
    auto p1 = std::make_shared<CapiPlayer>("1");
    auto p2 = std::make_shared<CapiPlayer>("2");
    auto p3 = std::make_shared<CapiPlayer>("3");

    p1->setName("1");
    p2->setName("2");
    p3->setName("3");

    QList<QString> playerNames;
    playerNames.append("2");

    game.setPlayerOrder(playerNames);
    ASSERT_EQ(game.getPlayerOrder().size(), 0);

    game.addPlayer(p1);
    game.addPlayer(p2);
    game.addPlayer(p3);

    game.setPlayerOrder(playerNames);
    ASSERT_EQ(game.getPlayerOrder().size(), 1);
    ASSERT_EQ(game.getPlayerOrder().at(0)->getPlayer(), p2);

    playerNames.append("1");

    game.setPlayerOrder(playerNames);
    ASSERT_EQ(game.getPlayerOrder().size(), 2);
    ASSERT_EQ(game.getPlayerOrder().at(0)->getPlayer(), p2);
    ASSERT_EQ(game.getPlayerOrder().at(1)->getPlayer(), p1);

    game.removePlayer(p3);
    game.removePlayer(p2);
    game.removePlayer(p1);
}
