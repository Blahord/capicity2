#include <gtest/gtest.h>

#include "game/CapiEstateGroup.h"

class CapiEstateGroupTest : public ::testing::Test {

    protected:
        CapiEstateGroup group = CapiEstateGroup("id-1", "00bb88", 123);
};

TEST_F(CapiEstateGroupTest, _descruct) { // NOLINT(cert-err58-cpp)
    delete new CapiEstateGroup("id-2", "00bb88", 1);
}


TEST_F(CapiEstateGroupTest, getId) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(group.getId(), "id-1");
}

TEST_F(CapiEstateGroupTest, getColor) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(group.getColor(), "00bb88");
}

TEST_F(CapiEstateGroupTest, getousePrice) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(group.getHousePrice(), 123);
}


