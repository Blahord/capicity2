#include <gtest/gtest.h>

#include "game/CapiGameTemplate.h"

class CapiGameTemplateTest : public ::testing::Test {

    protected:
        CapiGameTemplate gameTemplate = CapiGameTemplate("id-1");
};

TEST_F(CapiGameTemplateTest, _descruct) { // NOLINT(cert-err58-cpp)
    delete new CapiGameTemplate("id-2");
}

TEST_F(CapiGameTemplateTest, getId) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gameTemplate.getId(), "id-1");
}

TEST_F(CapiGameTemplateTest, minPlayers) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gameTemplate.getMinPlayers(), 2);

    gameTemplate.setMinPlayers(6);
    ASSERT_EQ(gameTemplate.getMinPlayers(), 6);

    gameTemplate.setMinPlayers(2);
    ASSERT_EQ(gameTemplate.getMinPlayers(), 2);
}

TEST_F(CapiGameTemplateTest, maxPlayers) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gameTemplate.getMaxPlayers(), 2);

    gameTemplate.setMaxPlayers(6);
    ASSERT_EQ(gameTemplate.getMaxPlayers(), 6);

    gameTemplate.setMaxPlayers(2);
    ASSERT_EQ(gameTemplate.getMaxPlayers(), 2);
}

TEST_F(CapiGameTemplateTest, languages) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gameTemplate.getLanguages().size(), 0);

    QList<QString> languages;
    languages.append("eng");

    gameTemplate.setLanguages(languages);
    ASSERT_EQ(gameTemplate.getLanguages().size(), 1);
    ASSERT_EQ(gameTemplate.getLanguages().at(0), "eng");

    languages.clear();
    languages.append("deu");
    languages.append("fra");

    gameTemplate.setLanguages(languages);
    ASSERT_EQ(gameTemplate.getLanguages().size(), 2);
    ASSERT_EQ(gameTemplate.getLanguages().at(0), "deu");
    ASSERT_EQ(gameTemplate.getLanguages().at(1), "fra");
}

TEST_F(CapiGameTemplateTest, defaultLanguage) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gameTemplate.getDefaultLanguage(), "eng");

    gameTemplate.setDefaultLanguage("deu");
    ASSERT_EQ(gameTemplate.getDefaultLanguage(), "deu");

    gameTemplate.setDefaultLanguage("fra");
    ASSERT_EQ(gameTemplate.getDefaultLanguage(), "fra");
}