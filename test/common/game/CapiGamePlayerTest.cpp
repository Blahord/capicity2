#include <gtest/gtest.h>

#include "game/CapiGamePlayer.h"

class CapiGamePlayerTest : public ::testing::Test {

    protected:
        void SetUp() override {
            Test::SetUp();

            player = std::make_shared<CapiPlayer>("1");
            gamePlayer = std::make_unique<CapiGamePlayer>(player);
        }

        std::shared_ptr<CapiPlayer> player;
        std::unique_ptr<CapiGamePlayer> gamePlayer;
};

TEST_F(CapiGamePlayerTest, getPlayer) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gamePlayer->getPlayer(), player);
}

TEST_F(CapiGamePlayerTest, ready) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gamePlayer->isReady(), false);

    gamePlayer->setReady(true);
    ASSERT_EQ(gamePlayer->isReady(), true);

    gamePlayer->setReady(false);
    ASSERT_EQ(gamePlayer->isReady(), false);
}

TEST_F(CapiGamePlayerTest, position) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gamePlayer->getPosition(), 0);

    gamePlayer->setPosition(12);
    ASSERT_EQ(gamePlayer->getPosition(), 12);

    gamePlayer->setPosition(32);
    ASSERT_EQ(gamePlayer->getPosition(), 32);
}

TEST_F(CapiGamePlayerTest, money) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gamePlayer->getMoney(), 0);

    gamePlayer->setMoney(1500);
    ASSERT_EQ(gamePlayer->getMoney(), 1500);

    gamePlayer->setMoney(815);
    ASSERT_EQ(gamePlayer->getMoney(), 815);
}

TEST_F(CapiGamePlayerTest, inJail) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gamePlayer->isInJail(), false);

    gamePlayer->setInJail(true);
    ASSERT_EQ(gamePlayer->isInJail(), true);

    gamePlayer->setInJail(false);
    ASSERT_EQ(gamePlayer->isInJail(), false);
}

TEST_F(CapiGamePlayerTest, moved) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(gamePlayer->isMoved(), false);

    gamePlayer->setMoved(true);
    ASSERT_EQ(gamePlayer->isMoved(), true);

    gamePlayer->setMoved(false);
    ASSERT_EQ(gamePlayer->isMoved(), false);
}
