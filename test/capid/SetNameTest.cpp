#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class SetNameTest : public CapidTestBase {

};

TEST_F(SetNameTest, notLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand(
        "1", "server", "setName",
        {{"name", "Foo"}}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_LOGGED_IN"))}
        )
    );
}

TEST_F(SetNameTest, invalidData) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setName",
        {}
    );

    checkNewMessage(
        "1", "Command result",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(SetNameTest, newName) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setName",
        {
            {"name", "Bar"}
        }
    );

    checkNewMessage("1", "commandResult", commandResponseOk(nonce));
    checkNewMessage(
        "1", "new Name set",
        serverMessage(
            "playerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("name", is("Bar"))
        )
    );
}

TEST_F(SetNameTest, newName_sameName) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce1 = sendCommand(
        "1", "server", "setName",
        {
            {"name", "Foo"}
        }
    );

    checkNewMessage("1", "commandResult", commandResponseOk(nonce1));
    QString nonce2 = sendCommand(
        "1", "server", "setName",
        {
            {"name", "Foo"}
        }
    );

    checkNewMessage("1", "commandResult", commandResponseOk(nonce2));
}

TEST_F(SetNameTest, newName_differentNameThenSameName) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    sendLogin("1", "Bar");

    sendCommand(
        "1", "server", "setName",
        {
            {"name", "Foo"}
        }
    );
    discardNextMessage("1");

    checkNewMessage(
        "1", "new Name set",
        serverMessage(
            "playerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("name", is("Foo"))
        )
    );

    sendCommand(
        "1", "server", "setName",
        {
            {"name", "Foo"}
        }
    );
    discardNextMessage("1");

    checkNoNewMessages("1", "no message");
}
