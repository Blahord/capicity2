#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class SkipBuyCommandTest : public CapidTestBase {

};

TEST_F(SkipBuyCommandTest, notOnTurn) {
    addConnection("1");
    addConnection("2");

    sendLogin("1", "1");
    sendLogin("2", "2");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    auto nonce = sendCommand("2", "game", "skipBuy", {});
    checkNewMessage(
        "2", "Player 2 - Not on Turn",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_ON_TURN"))}
        )
    );
}

TEST_F(SkipBuyCommandTest, wrongTurnState) {
    addConnection("1");
    addConnection("2");

    sendLogin("1", "1");
    sendLogin("2", "2");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 2});
    sendCommand("1", "game", "roll", {});

    auto nonce = sendCommand("1", "game", "skipBuy", {});
    checkNewMessage(
        "1", "Player 1 - Wrong Turn state",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("WRONG_TURN_STATE"))}
        )
    );
}

TEST_F(SkipBuyCommandTest, skipBuy) {
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");

    sendLogin("1", "1");
    sendLogin("2", "2");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 2});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});

    sendCommand("1", "game", "skipBuy", {});
    discardNextMessage("1");

    checkNewMessage(
        {"1", "2"}, "Skip buy event",
        eventMessage(
            "skipBuy",
            nullValue()
        )
    );

    checkNewMessage(
        {"1", "2"}, "Game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnPlayer", is(p2->getId())),
                requiredField("turnState", is("ROLL"))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Turn event",
        eventMessage(
            "turn",
            stringValue(is(p2->getId()))
        )
    );

    checkNoNewMessages({"1", "2"}, "No message");
}
