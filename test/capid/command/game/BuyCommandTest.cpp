#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class BuyCommandTest : public CapidTestBase {

};

TEST_F(BuyCommandTest, notOnTurn) {
    addConnection("1");
    addConnection("2");

    sendLogin("1", "1");
    sendLogin("2", "2");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    auto nonce = sendCommand("2", "game", "buy", {});
    checkNewMessage(
        "2", "Player 2 - Not on Turn",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_ON_TURN"))}
        )
    );
}

TEST_F(BuyCommandTest, wrongTurnState) {
    addConnection("1");
    addConnection("2");

    sendLogin("1", "1");
    sendLogin("2", "2");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 2});
    sendCommand("1", "game", "roll", {});

    auto nonce = sendCommand("1", "game", "buy", {});
    checkNewMessage(
        "1", "Player 1 - Wrong Turn state",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("WRONG_TURN_STATE"))}
        )
    );
}

TEST_F(BuyCommandTest, notEnoughMoney) {
    addConnection("1");
    addConnection("2");

    sendLogin("1", "1");
    sendLogin("2", "2");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 4});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});

    auto nonce = sendCommand("1", "game", "buy", {});
    checkNewMessage(
        "1", "Player 1 - Not enough money",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_ENOUGH_MONEY"))}
        )
    );
}

TEST_F(BuyCommandTest, buy) {
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");

    sendLogin("1", "1");
    sendLogin("2", "2");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 2});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});
    discardNextMessages("1", 3);
    discardNextMessages("2", 4);

    checkNewMessage(
        {"1", "2"}, "Land update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("BUY"))
            }
        )
    );

    sendCommand("1", "game", "buy", {});
    discardNextMessage("1");

    checkNewMessage(
        {"1", "2"}, "Estate update",
        serverMessage(
            "estateUpdate",
            {
                requiredField("id", is("e3")),
                requiredField("owner", is(p1->getId()))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Game player update",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p1->getId())),
                requiredField("money", is(1280))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Buy event",
        eventMessage(
            "buy",
            objectValue(
                {
                    requiredField("player", is(p1->getId())),
                    requiredField("estate", is("e3")),
                }
            )
        )
    );

    checkNewMessage(
        {"1", "2"}, "Game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnPlayer", is(p2->getId())),
                requiredField("turnState", is("ROLL"))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Turn event",
        eventMessage(
            "turn",
            stringValue(is(p2->getId()))
        )
    );

    checkNoNewMessages({"1", "2"}, "No message");
}
