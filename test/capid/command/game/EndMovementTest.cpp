#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class EndMovementCommandTest : public CapidTestBase {

};

TEST_F(EndMovementCommandTest, cannotBuy) {
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");

    sendLogin("1", "1");
    sendLogin("2", "2");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 1});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});
    discardNextMessages("1", 3);
    discardNextMessages("2", 4);

    checkNewMessage(
        {"1", "2"}, "GameUpdate",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("ROLL")),
                requiredField("turnPlayer", is(p2->getId()))
            }
        )
    );

}

TEST_F(EndMovementCommandTest, alreadyOwned) {
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");

    sendLogin("1", "1");
    sendLogin("2", "2");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 2});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});
    sendCommand("1", "game", "buy", {});

    rng->addNextNumbers({1, 2});
    sendCommand("2", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});

    discardNextMessages("1", 3);
    discardNextMessages("2", 4);

    checkNewMessage(
        {"1", "2"}, "GameUpdate",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("ROLL")),
                requiredField("turnPlayer", is(p1->getId()))
            }
        )
    );
}
