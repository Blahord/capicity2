#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class PayTaxCommandTest : public CapidTestBase {

    protected:
        void SetUp() override {
            CapidTestBase::SetUp();

            p1 = addConnection("1");
            p2 = addConnection("2");

            sendLogin("1", "1");
            sendLogin("2", "2");

            gameId = sendCreateGame("1", "taxHell");
            sendCommand(
                "2", "server", "joinGame",
                {{"id", gameId}}
            );

            rng->addNextNumbers({0, 1});
            sendCommand("1", "game", "setReady", {{"ready", true}});
            sendCommand("2", "game", "setReady", {{"ready", true}});
        }

        QString gameId;
        std::shared_ptr<CapidPlayer> p1;
        std::shared_ptr<CapidPlayer> p2;
};

TEST_F(PayTaxCommandTest, notOnTurn) {

    auto nonce = sendCommand("2", "game", "payTax", {});
    checkNewMessage(
        "2", "Player 2 - Not on Turn",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_ON_TURN"))}
        )
    );
}

TEST_F(PayTaxCommandTest, wrongTurnState) {

    auto nonce = sendCommand("1", "game", "payTax", {});
    checkNewMessage(
        "1", "Player 1 - Wrong Turn state",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("WRONG_TURN_STATE"))}
        )
    );
}

TEST_F(PayTaxCommandTest, notEnoughMoney) {

    rng->addNextNumbers({1, 3});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});

    discardNextMessages("1", 3);
    discardNextMessages("2", 4);

    checkNewMessage(
        {"1", "2"}, "Tax turn state",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("TAX"))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Need to pay tax event",
        eventMessage(
            "taxPayNeeded",
            objectValue(
                {
                    requiredField("amount", is(10000)),
                    requiredField("percent", is(10)),
                }
            )
        )
    );

    auto nonce = sendCommand("1", "game", "payTax", {{"mode", "amount"}});
    checkNewMessage(
        "1", "Player 1 - Not enough money",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_ENOUGH_MONEY"))}
        )
    );
}

TEST_F(PayTaxCommandTest, pay) {

    rng->addNextNumbers({1, 3});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});

    discardNextMessages("1", 3);
    discardNextMessages("2", 4);

    checkNewMessage(
        {"1", "2"}, "Tax turn state",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("TAX"))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Need to pay tax event",
        eventMessage(
            "taxPayNeeded",
            objectValue(
                {
                    requiredField("amount", is(10000)),
                    requiredField("percent", is(10)),
                }
            )
        )
    );

    auto nonce = sendCommand("1", "game", "payTax", {{"mode", "percent"}});
    checkNewMessage(
        "1", "Command response",
        commandResponseOk(nonce)
    );

    checkNewMessage(
        {"1", "2"}, "Estate update Go",
        serverMessage(
            "estateUpdate",
            {
                requiredField("id", is("go")),
                requiredField("money", is(150))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Game Player Update 1",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p1->getId())),
                requiredField("money", is(1350))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Payed tax event",
        eventMessage(
            "taxPayed",
            integerValue(is(150))
        )
    );
}

TEST_F(PayTaxCommandTest, payOnlyAmount) {

    capid->getGames().at(0)->getPlayerOnTurn()->setMoney(9);

    rng->addNextNumbers({1, 1});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});

    discardNextMessages("1", 3);
    discardNextMessages("2", 4);

    checkNewMessage(
        {"1", "2"}, "Tax turn state",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("TAX"))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Need to pay tax event",
        eventMessage(
            "taxPayNeeded",
            objectValue(
                {
                    requiredField("amount", is(10)),
                    requiredField("percent", is(0)),
                }
            )
        )
    );

    capid->getGames().at(0)->getPlayerOnTurn()->setMoney(1500);

    auto nonce = sendCommand("1", "game", "payTax", {});
    checkNewMessage(
        "1", "Command response",
        commandResponseOk(nonce)
    );

    checkNewMessage(
        {"1", "2"}, "Game Player Update 1",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p1->getId())),
                requiredField("money", is(1490))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Payed tax event",
        eventMessage(
            "taxPayed",
            integerValue(is(10))
        )
    );

}

TEST_F(PayTaxCommandTest, payOnlyPercent) {

    auto const game = capid->getGames().at(0);
    game->getEstates().at(6)->setOwner(game->getPlayerOnTurn());

    rng->addNextNumbers({1, 2});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});

    discardNextMessages("1", 3);
    discardNextMessages("2", 4);

    checkNewMessage(
        {"1", "2"}, "Tax turn state",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("TAX"))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Need to pay tax event",
        eventMessage(
            "taxPayNeeded",
            objectValue(
                {
                    requiredField("amount", is(0)),
                    requiredField("percent", is(10)),
                }
            )
        )
    );

    capid->getGames().at(0)->getPlayerOnTurn()->setMoney(200000);

    auto nonce = sendCommand("1", "game", "payTax", {});
    checkNewMessage(
        "1", "Command response",
        commandResponseOk(nonce)
    );

    checkNewMessage(
        {"1", "2"}, "Estate update Go",
        serverMessage(
            "estateUpdate",
            {
                requiredField("id", is("go")),
                requiredField("money", is(30000))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Game Player Update 1",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p1->getId())),
                requiredField("money", is(200000 - 30000))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Payed tax event",
        eventMessage(
            "taxPayed",
            integerValue(is(30000))
        )
    );

}
