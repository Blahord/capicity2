#include "CapidTestBase.h"

#include "json/JsonChecks.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class GetImageCommandTest : public CapidTestBase {

};

TEST_F(GetImageCommandTest, getImage_notLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand("1", "data", "getImage", {{"hash", "0000"}});

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_LOGGED_IN"))}
        )
    );
}

TEST_F(GetImageCommandTest, getImage_missingHash) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand("1", "data", "getImage", {});

    checkNewMessage(
        "1", "error response",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(GetImageCommandTest, getImage_unknownHash) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand("1", "data", "getImage", {{"hash", "0123456789"}});

    checkNewMessage(
        "1", "response",
        commandResponseOk(
            nonce,
            {
                requiredField("found", isFalse())
            }
        )
    );
}

TEST_F(GetImageCommandTest, getImage_success) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString imageData = "";
    for (int i = 0; i < 16 * 16; i++) {
        imageData += "00000000";
    }
    sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  16},
            {"height", 16},
            {"data",   imageData}
        }
    );

    Image img(16, 16, imageData);

    QString nonce = sendCommand("1", "data", "getImage", {{"hash", img.hash()}});

    checkNewMessage(
        "1", "response",
        commandResponseOk(
            nonce,
            {
                requiredField("found", isTrue()),
                requiredField(
                    "image",
                    objectValue(
                        {
                            requiredField("width", is(16)),
                            requiredField("height", is(16)),
                            requiredField("data", is(imageData)),
                        }
                    )
                )
            }
        )
    );
}
