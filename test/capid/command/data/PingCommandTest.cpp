#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"
#include "command/data/PingCommand.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class PingCommandTest : public CapidTestBase {

};

TEST_F(PingCommandTest, ping_notLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand("1", "data", "ping", {{"message", "Foo"}});

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_LOGGED_IN"))}
        )
    );
}

TEST_F(PingCommandTest, ping_invalidFormat) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand("1", "data", "ping", {});

    checkNewMessage(
        "1",
        "command result",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(PingCommandTest, ping) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand("1", "data", "ping", {{"message", "Foo"}});

    checkNewMessage(
        "1",
        "command result",
        commandResponseOk(
            nonce,
            {requiredField("pong", is("Foo"))}
        )
    );
}

TEST_F(PingCommandTest, deleteCommand) {
    auto serverManager = std::make_shared<ServerManager>();

    auto command = new PingCommand(serverManager);
    delete command;
}