#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"
#include "json/checks/JsonStringValueCheck.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class ServerCommandTest : public CapidTestBase {

    protected:
        void SetUp() override {
            CapidTestBase::SetUp();

            p1 = addConnection("1");
            p2 = addConnection("2");
            p3 = addConnection("3");

            sendLogin("1", "1");
            sendLogin("2", "2");
            sendLogin("3", "3");
        }

        std::shared_ptr<CapidPlayer> p1;
        std::shared_ptr<CapidPlayer> p2;
        std::shared_ptr<CapidPlayer> p3;
};

TEST_F(ServerCommandTest, quit_playerOnTurn) { // NOLINT(cert-err58-cpp)

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand("2", "server", "joinGame", {{"id", gameId}});
    sendCommand("3", "server", "joinGame", {{"id", gameId}});

    rng->addNextNumbers({0, 1, 2});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});
    sendCommand("3", "game", "setReady", {{"ready", true}});

    QString p1QuitNonce = sendCommand("1", "server", "quit", {});

    checkNewMessage("1", "quitResult", commandResponseOk(p1QuitNonce));
    checkNewMessage(
        {"2", "3"}, "Turn event",
        eventMessage("turn", stringValue(is(p2->getId())))
    );
    checkNewMessage(
        {"2", "3"}, "Game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("owner", is(p2->getId())),
                requiredField("state", is("RUN")),
                requiredField("players", {minLength(2), maxLength(2), contains("P2", is(p2->getId())), contains("P3", is(p3->getId()))}),
                requiredField("playerOrder", {minLength(2), maxLength(2), contains("P2", is(p2->getId())), contains("P3", is(p3->getId()))}),
                requiredField("turnPlayer", is(p2->getId()))
            }
        )
    );
    checkNewMessage(
        {"2", "3"}, "Quit",
        serverMessage(
            "quit",
            requiredField("id", is(p1->getId()))
        )
    );
}

TEST_F(ServerCommandTest, quit_laterOnTurn) { // NOLINT(cert-err58-cpp)

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand("2", "server", "joinGame", {{"id", gameId}});
    sendCommand("3", "server", "joinGame", {{"id", gameId}});

    rng->addNextNumbers({0, 1, 2});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});
    sendCommand("3", "game", "setReady", {{"ready", true}});

    sendCommand("2", "server", "quit", {});
    discardNextMessage("2");

    checkNewMessage(
        {"1", "3"}, "Game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("owner", is(p1->getId())),
                requiredField("state", is("RUN")),
                requiredField("players", {minLength(2), maxLength(2), contains("P1", is(p1->getId())), contains("P3", is(p3->getId()))}),
                requiredField("playerOrder", {minLength(2), maxLength(2), contains("P1", is(p1->getId())), contains("P3", is(p3->getId()))}),
                requiredField("turnPlayer", is(p1->getId()))
            }
        )
    );
    checkNewMessage(
        {"1", "3"}, "Quit",
        serverMessage(
            "quit",
            requiredField("id", is(p2->getId()))
        )
    );
}

TEST_F(ServerCommandTest, quit_earlierOnTurn) { // NOLINT(cert-err58-cpp)

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand("2", "server", "joinGame", {{"id", gameId}});
    sendCommand("3", "server", "joinGame", {{"id", gameId}});

    rng->addNextNumbers({0, 1, 2});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});
    sendCommand("3", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 1});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});
    sendCommand("3", "game", "moveComplete", {});

    sendCommand("1", "server", "quit", {});
    discardNextMessage("1");

    checkNewMessage(
        {"2", "3"}, "Game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("owner", is(p2->getId())),
                requiredField("state", is("RUN")),
                requiredField("players", {minLength(2), maxLength(2), contains("P2", is(p2->getId())), contains("P3", is(p3->getId()))}),
                requiredField("playerOrder", {minLength(2), maxLength(2), contains("P2", is(p2->getId())), contains("P3", is(p3->getId()))}),
                requiredField("turnPlayer", is(p2->getId()))
            }
        )
    );
    checkNewMessage(
        {"2", "3"}, "Quit",
        serverMessage(
            "quit",
            requiredField("id", is(p1->getId()))
        )
    );
}

TEST_F(ServerCommandTest, leaveGame_playerOnTurn) { // NOLINT(cert-err58-cpp)

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand("2", "server", "joinGame", {{"id", gameId}});
    sendCommand("3", "server", "joinGame", {{"id", gameId}});

    rng->addNextNumbers({0, 1, 2});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});
    sendCommand("3", "game", "setReady", {{"ready", true}});

    sendCommand("1", "server", "leaveGame", {});
    discardNextMessage("1");

    checkNewMessage(
        {"2", "3"}, "Turn event",
        eventMessage("turn", stringValue(is(p2->getId())))
    );
    checkNewMessage(
        {"1", "2", "3"}, "Game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("owner", is(p2->getId())),
                requiredField("state", is("RUN")),
                requiredField("players", {minLength(2), maxLength(2), contains("P2", is(p2->getId())), contains("P3", is(p3->getId()))}),
                requiredField("playerOrder", {minLength(2), maxLength(2), contains("P2", is(p2->getId())), contains("P3", is(p3->getId()))}),
                requiredField("turnPlayer", is(p2->getId()))
            }
        )
    );
    checkNoNewMessages({"1", "2", "3"}, "No messages");
}

TEST_F(ServerCommandTest, leaveGame_laterOnTurn) { // NOLINT(cert-err58-cpp)

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand("2", "server", "joinGame", {{"id", gameId}});
    sendCommand("3", "server", "joinGame", {{"id", gameId}});

    rng->addNextNumbers({0, 1, 2});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});
    sendCommand("3", "game", "setReady", {{"ready", true}});

    sendCommand("2", "server", "leaveGame", {});
    discardNextMessage("2");

    checkNewMessage(
        {"1", "2", "3"}, "Game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("owner", is(p1->getId())),
                requiredField("state", is("RUN")),
                requiredField("players", {minLength(2), maxLength(2), contains("P1", is(p1->getId())), contains("P3", is(p3->getId()))}),
                requiredField("playerOrder", {minLength(2), maxLength(2), contains("P1", is(p1->getId())), contains("P3", is(p3->getId()))}),
                requiredField("turnPlayer", is(p1->getId()))
            }
        )
    );
    checkNoNewMessages({"1", "2", "3"}, "No messages");
}

TEST_F(ServerCommandTest, leaveGame_earlierOnTurn) { // NOLINT(cert-err58-cpp)

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand("2", "server", "joinGame", {{"id", gameId}});
    sendCommand("3", "server", "joinGame", {{"id", gameId}});

    rng->addNextNumbers({0, 1, 2});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});
    sendCommand("3", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 1});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});
    sendCommand("3", "game", "moveComplete", {});

    sendCommand("1", "server", "leaveGame", {});
    discardNextMessage("1");

    checkNewMessage(
        {"1", "2", "3"}, "Game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("owner", is(p2->getId())),
                requiredField("state", is("RUN")),
                requiredField("players", {minLength(2), maxLength(2), contains("P2", is(p2->getId())), contains("P3", is(p3->getId()))}),
                requiredField("playerOrder", {minLength(2), maxLength(2), contains("P2", is(p2->getId())), contains("P3", is(p3->getId()))}),
                requiredField("turnPlayer", is(p2->getId()))
            }
        )
    );
    checkNoNewMessages({"1", "2", "3"}, "No messages");
}

TEST_F(ServerCommandTest, gameExodus) { // NOLINT(cert-err58-cpp)

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand("2", "server", "joinGame", {{"id", gameId}});
    sendCommand("3", "server", "joinGame", {{"id", gameId}});

    rng->addNextNumbers({0, 1, 2});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});
    sendCommand("3", "game", "setReady", {{"ready", true}});

    sendCommand("1", "server", "leaveGame", {});
    sendCommand("2", "server", "leaveGame", {});
    sendCommand("3", "server", "leaveGame", {});

    discardNextMessage("3");

    checkNewMessage(
        {"1", "2", "3"}, "Game delete",
        serverMessage("gameDelete", requiredField("id", is(gameId)))
    );
}
