#include <gtest/gtest.h>

#include "image/ImageRegistry.h"

TEST(ImageRegistryTest, createDelete) { // NOLINT(cert-err58-cpp)
    auto registry = new ImageRegistry();

    delete registry;
}

TEST(ImageRegistryTest, containsImage) { // NOLINT(cert-err58-cpp)
    auto registry = ImageRegistry();

    auto image1 = Image(1, 1, "11111111");
    auto image2 = Image(1, 1, "22222222");

    EXPECT_FALSE(registry.containsImage(image1.hash()));
    EXPECT_FALSE(registry.containsImage(image2.hash()));

    registry.addImage(image1, "use1");
    registry.addImage(image2, "use2");

    EXPECT_TRUE(registry.containsImage(image1.hash()));
    EXPECT_TRUE(registry.containsImage(image2.hash()));

    registry.removeUsage(image1.hash(), "use1");
    registry.removeUsage(image2.hash(), "use2");

    EXPECT_FALSE(registry.containsImage(image1.hash()));
    EXPECT_FALSE(registry.containsImage(image2.hash()));
}

TEST(ImageRegistryTest, getImage) { // NOLINT(cert-err58-cpp)
    auto registry = ImageRegistry();

    auto image1 = Image(1, 1, "11111111");
    auto image2 = Image(1, 1, "22222222");

    EXPECT_TRUE(registry.getImage(image1.hash()).isNull());
    EXPECT_TRUE(registry.getImage(image2.hash()).isNull());

    registry.addImage(image1, "use1");
    registry.addImage(image2, "use2");

    EXPECT_FALSE(registry.getImage(image1.hash()).isNull());
    EXPECT_FALSE(registry.getImage(image2.hash()).isNull());

    registry.removeUsage(image1.hash(), "use1");
    registry.removeUsage(image2.hash(), "use2");

    EXPECT_TRUE(registry.getImage(image1.hash()).isNull());
    EXPECT_TRUE(registry.getImage(image2.hash()).isNull());
}

TEST(ImageRegistry, addImage) { // NOLINT(cert-err58-cpp)
    auto registry = ImageRegistry();

    auto image1 = Image(1, 1, "11111111");

    registry.addImage(image1, "use1");
    registry.addImage(image1, "use2");

    EXPECT_TRUE(registry.containsImage(image1.hash()));

    registry.removeUsage(image1.hash(), "use1");
    EXPECT_TRUE(registry.containsImage(image1.hash()));

    registry.removeUsage(image1.hash(), "use1");
    EXPECT_TRUE(registry.containsImage(image1.hash()));

    registry.removeUsage(image1.hash(), "use2");
    EXPECT_FALSE(registry.containsImage(image1.hash()));
}

TEST(ImageRegistry, removeImage) { // NOLINT(cert-err58-cpp)
    auto registry = ImageRegistry();

    auto image1 = Image(1, 1, "11111111");

    registry.removeUsage(image1.hash(), "use1");
    EXPECT_FALSE(registry.containsImage(image1.hash()));

    registry.addImage(image1, "use1");
    EXPECT_TRUE(registry.containsImage(image1.hash()));

    registry.removeUsage(image1.hash(), "use1");
    EXPECT_FALSE(registry.containsImage(image1.hash()));
}

TEST(ImageRegistryTest, descuctNotEmpty) { // NOLINT(cert-err58-cpp)
    auto registry = ImageRegistry();

    auto image1 = Image(1, 1, "11111111");
    auto image2 = Image(1, 1, "22222222");

    registry.addImage(image1, "use1");
    registry.addImage(image2, "use2");
}
