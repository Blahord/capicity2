#include <gtest/gtest.h>

#include "image/Image.h"

TEST(ImageTest, create_normal) {
    auto image = Image(1, 2, "012345679abcdef");

    EXPECT_FALSE(image.isNull());

    auto imageObj = image.toJsonObject();
    EXPECT_TRUE(imageObj.contains("width"));
    EXPECT_TRUE(imageObj.value("width").isDouble());
    EXPECT_EQ(imageObj.value("width").toDouble(), 1.0);

    EXPECT_TRUE(imageObj.contains("height"));
    EXPECT_TRUE(imageObj.value("height").isDouble());
    EXPECT_EQ(imageObj.value("height").toDouble(), 2.0);

    EXPECT_TRUE(imageObj.contains("data"));
    EXPECT_TRUE(imageObj.value("data").isString());
    EXPECT_EQ(imageObj.value("data").toString(), "012345679abcdef");
}

TEST(ImageTest, create_null) {
    auto image = Image();

    EXPECT_TRUE(image.isNull());

    auto imageObj = image.toJsonObject();
    EXPECT_TRUE(imageObj.contains("width"));
    EXPECT_TRUE(imageObj.value("width").isDouble());
    EXPECT_EQ(imageObj.value("width").toDouble(), 0.0);

    EXPECT_TRUE(imageObj.contains("height"));
    EXPECT_TRUE(imageObj.value("height").isDouble());
    EXPECT_EQ(imageObj.value("height").toDouble(), 0.0);

    EXPECT_TRUE(imageObj.contains("data"));
    EXPECT_TRUE(imageObj.value("data").isString());
    EXPECT_EQ(imageObj.value("data").toString(), "");
}

TEST(ImageTest, hash_nullImage) {
    EXPECT_EQ(Image().hash(), "");
}

TEST(ImageTest, hash_normalImage) {
    auto imageHash = Image(1, 2, "012345679abcdef").hash();
    EXPECT_EQ(imageHash.size(), 64);
}
