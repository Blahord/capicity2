#include <gtest/gtest.h>

#include "image/PngReader.h"

TEST(PngReaderTest, readNonPngFile) { // NOLINT(cert-err58-cpp)
    Image img = PngReader::readFile("test/resources/games/testGame/game.xml");

    ASSERT_EQ(img.isNull(), true);
}

TEST(PngReaderTest, readPngFile) { // NOLINT(cert-err58-cpp)
    Image img = PngReader::readFile("test/resources/games/testGame/dollar.png");

    ASSERT_EQ(img.isNull(), false);
}
