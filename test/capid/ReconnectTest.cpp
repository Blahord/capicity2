#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class ReconnectTest : public CapidTestBase {

};

TEST_F(ReconnectTest, alreadyLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "reconnect",
        {{"token", "12345"}}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("ALREADY_LOGGED_IN"))}
        )
    );
    checkNoNewMessages("1", "commandResult");
}

TEST_F(ReconnectTest, tokenMissing) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand(
        "1", "server", "reconnect",
        {}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {
                minLength(1),
                maxLength(1)
            }
        )
    );
}

TEST_F(ReconnectTest, invalidToken) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    sendLogin("1", "Foo1");
    QJsonObject playerData = getNextMessage("1", "playerData").toObject();
    QString token1 = playerData.value("data").toObject().value("token").toString();

    removeConnection("1");

    addConnection("2");
    QString nonce = sendCommand(
        "2", "server", "reconnect",
        {{"token", token1 + "-"}}
    );

    checkNewMessage(
        "2", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("INVALID_TOKEN"))}
        )
    );
}

TEST_F(ReconnectTest, playerUpdateBroadCast) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");
    addConnection("3");
    addConnection("4");

    sendLogin("1", "Foo1");
    QJsonObject playerData = getNextMessage("1", "playerData").toObject();
    QString token1 = playerData.value("data").toObject().value("token").toString();

    sendLogin("2", "Foo2");
    sendLogin("3", "Foo3");

    sendCommand("2", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("2");

    removeConnection("1");

    checkNewMessage(
        {"2", "3"}, "Foo1 offline",
        serverMessage(
            "playerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("connected", isFalse())
        )
    );

    QString nonce = sendCommand(
        "4", "server", "reconnect",
        {{"token", token1}}
    );

    checkNewMessage("4", "reconnectResult", commandResponseOk(nonce));
    checkNewMessage(
        "4", "playerData after reconnect",
        serverMessage(
            "playerData", {
                requiredField("id", is(p1->getId())),
                requiredField("name", is("Foo1")),
                requiredField("token", is(token1))
            }
        )
    );

    checkNewMessage(
        "4", "player list after reconnect",
        serverMessage(
            "playerList",
            {
                requiredField("list", arrayValue({minLength(3), maxLength(3)}))
            }
        )
    );

    checkNewMessage(
        "4", "game template list after reconnect",
        serverMessage(
            "gameTemplateList",
            {
                requiredField("list", arrayValue({minLength(4), maxLength(4)}))
            }
        )
    );

    checkNewMessage(
        "4", "game list after reconnect",
        serverMessage(
            "gameList",
            {
                requiredField(
                    "list",
                    arrayValue(
                        {
                            minLength(1),
                            maxLength(1),
                            contains(
                                "testGame",
                                objectValue(
                                    {
                                        requiredField("id", stringValue()),
                                        requiredField("type", is("testGame")),
                                        requiredField("owner", is(p2->getId())),
                                        requiredField("players", {minLength(1), maxLength(1), contains("P2", is(p2->getId()))}),
                                        requiredField("state", is("CONFIG"))
                                    }
                                )
                            )
                        }
                    )
                )
            }
        )
    );

    checkNewMessage(
        {"2", "3"}, "Foo1 online",
        serverMessage(
            "playerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("connected", isTrue())
        )
    );
}

TEST_F(ReconnectTest, playerInGame) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    addConnection("2");
    addConnection("3");
    addConnection("4");

    sendLogin("1", "Foo1");
    QJsonObject playerData = getNextMessage("1", "playerData").toObject();
    QString token1 = playerData.value("data").toObject().value("token").toString();

    sendLogin("2", "Foo2");
    sendLogin("3", "Foo3");

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QJsonObject gameData = getNextMessage("1", "newGame").toObject();
    QString gameId = gameData.value("data").toObject().value("id").toString();

    removeConnection("1");

    sendCommand(
        "4", "server", "reconnect",
        {{"token", token1}}
    );
    discardNextMessage("4");

    checkNewMessage(
        "4", "playerData after reconnect",
        serverMessage(
            "playerData", {
                requiredField("id", is(p1->getId())),
                requiredField("name", is("Foo1")),
                requiredField("token", is(token1))
            }
        )
    );

    checkNewMessage(
        "4", "player list after reconnect",
        serverMessage(
            "playerList",
            {
                requiredField("list", arrayValue({minLength(3), maxLength(3)}))
            }
        )
    );

    checkNewMessage(
        "4", "game template list after reconnect",
        serverMessage(
            "gameTemplateList",
            {
                requiredField("list", arrayValue({minLength(4), maxLength(4)}))
            }
        )
    );

    checkNewMessage(
        "4", "game list after reconnect",
        serverMessage(
            "gameList",
            {
                requiredField(
                    "list",
                    arrayValue(
                        {
                            minLength(1),
                            maxLength(1),
                            contains(
                                "testGame",
                                objectValue(
                                    {
                                        requiredField("id", is(gameId)),
                                        requiredField("type", is("testGame")),
                                        requiredField("owner", is(p1->getId())),
                                        requiredField("players", {minLength(1), maxLength(1), contains("P1", is(p1->getId()))}),
                                        requiredField("state", is("CONFIG"))
                                    }
                                )
                            )
                        }
                    )
                )
            }
        )
    );

    checkNewMessage(
        "4", "game data",
        serverMessage(
            "gameData",
            {
                requiredField("id", is(gameId))
            }
        )
    );

    checkNewMessage(
        "4", "game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId))
            }
        )
    );

    checkNewMessage(
        "4", "game player update 1",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p1->getId())),
                requiredField("ready", isFalse()),
                requiredField("money", is(0)),
                requiredField("jailed", isFalse()),
                requiredField("position", is(0))
            }
        )
    );
}
