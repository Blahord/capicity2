#include <gtest/gtest.h>
#include "CapidRNG.h"

TEST(CapidRNGTest, randomNumber) { // NOLINT(cert-err58-cpp)
    auto rng = new CapidRNG();
    rng->next();
    rng->next();
    delete rng;
}

TEST(CapidRNGTest, randonInBound) { // NOLINT(cert-err58-cpp)
    auto rng = new CapidRNG();
    ASSERT_EQ(rng->next(1, 1), 1);
    ASSERT_LE(rng->next(1, 6), 6);
    ASSERT_GE(rng->next(1, 6), 1);
    delete rng;
}

