#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class ChatTest : public CapidTestBase {

};

TEST_F(ChatTest, notLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    sendChat("1", "Hi there");

    checkNoNewMessages("1", "sendChat");
}

TEST_F(ChatTest, noArguments) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    sendLogin("1", "Foo");

    send(
        "1",
        {
            {"scope",   "server"},
            {"command", "chat"}
        }
    );

    checkNewMessage(
        "1", "commandResult",
        errorMessage(
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(ChatTest, argumentsNotObject) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    sendLogin("1", "Foo");

    send(
        "1",
        {
            {"nonce",     "Foo"},
            {"scope",     "server"},
            {"command",   "chat"},
            {"arguments", 1234}
        }
    );

    checkNewMessage(
        "1", "commandResult",
        errorMessage(
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(ChatTest, noMessage) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "chat",
        {}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(ChatTest, globalMessage) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    addConnection("2");
    auto p3 = addConnection("3");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");

    checkNewMessage(
        "1", "3rd login P1",
        serverMessage(
            "newPlayer",
            requiredField("connected", isTrue()),
            requiredField("id", is(p3->getId()))
        )
    );

    checkNewMessage(
        "2", "3rd login P2",
        serverMessage(
            "newPlayer",
            requiredField("connected", isTrue()),
            requiredField("id", is(p3->getId()))
        )
    );

    sendChat("1", "Hi there");

    checkNewMessage(
        "1", "receive for 1",
        serverMessage(
            "chat",
            {
                requiredField("sender", is(p1->getId())),
                requiredField("message", is("Hi there"))
            }
        )
    );

    checkNewMessage(
        "2", "receive for 2",
        serverMessage(
            "chat",
            {
                requiredField("sender", is(p1->getId())),
                requiredField("message", is("Hi there"))
            }
        )
    );

    checkNewMessage(
        "3", "receive for 3",
        serverMessage(
            "chat",
            {
                requiredField("sender", is(p1->getId())),
                requiredField("message", is("Hi there"))
            }
        )
    );
}

TEST_F(ChatTest, gameMessage) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    addConnection("2");
    auto p3 = addConnection("3");
    addConnection("4");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");
    sendLogin("4", "Goo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );
    discardNextMessage("2");

    sendChat("1", "Hello!");
    checkNewMessage(
        "1", "Chat message in Game 1",
        serverMessage(
            "chat",
            {
                requiredField("sender", is(p1->getId())),
                requiredField("message", is("Hello!"))
            }
        )
    );
    checkNewMessage(
        "2", "Chat message in Game 2",
        serverMessage(
            "chat",
            {
                requiredField("sender", is(p1->getId())),
                requiredField("message", is("Hello!"))
            }
        )
    );
    checkNoNewMessages("3", "No chat message 3");
    checkNoNewMessages("4", "No chat message 4");

    sendChat("3", "Hi!");
    checkNoNewMessages("1", "No chat message 1");
    checkNoNewMessages("2", "No chat message 2");
    checkNewMessage(
        "3", "Chat message in lobby 3",
        serverMessage(
            "chat",
            {
                requiredField("sender", is(p3->getId())),
                requiredField("message", is("Hi!"))
            }
        )
    );
    checkNewMessage(
        "4", "Chat message in lobby 4",
        serverMessage(
            "chat",
            {
                requiredField("sender", is(p3->getId())),
                requiredField("message", is("Hi!"))
            }
        )
    );
}

TEST_F(ChatTest, private_PlayerNotFound) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    addConnection("2");
    addConnection("3");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");

    QString nonce = sendChatKeepResponse("1", "Not there", "Hi there");

    checkNewMessage(
        "1", "receive for 1",
        commandResponseStateError(
            nonce,
            {minLength(1), maxLength(1), contains("E1", is("UNKNOWN_TARGET"))}
        )
    );
}

TEST_F(ChatTest, privateChat) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");
    addConnection("3");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");

    sendChat("1", p2->getId(), "Hi there");

    checkNewMessage(
        "1", "receive for 1",
        serverMessage(
            "chat",
            {
                requiredField("sender", is(p1->getId())),
                requiredField("message", is("Hi there")),
                requiredField("target", is(p2->getId()))
            }
        )
    );

    checkNewMessage(
        "2", "receive for 2",
        serverMessage(
            "chat",
            {
                requiredField("sender", is(p1->getId())),
                requiredField("message", is("Hi there")),
                requiredField("target", is(p2->getId()))
            }
        )
    );

    checkNoNewMessages("3", "receive for 3");
}
