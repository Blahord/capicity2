#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"
#include "json/checks/JsonStringValueCheck.h"

#include <QTest>

using namespace JsonChecks;
using namespace JsonResultChecks;

class GameTest : public CapidTestBase {

};

TEST_F(GameTest, createGame_notLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    checkNewMessage(
        "1", "commandResponse",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_LOGGED_IN"))}
        )
    );

    checkNoNewMessages("1", "commandResult");
}

TEST_F(GameTest, createGame_invalidType) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "createGame",
        {{"type", "fooBar"}}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {
                maxLength(1),
                contains("E1", is("UNKNOWN_TYPE"))
            }
        )
    );
}

TEST_F(GameTest, createGame_missingTemplate) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "createGame",
        {}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {
                minLength(1),
                maxLength(1)
            }
        )
    );
}

TEST_F(GameTest, createGame_templateIsNotString) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "createGame",
        {{"type", 12345}}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {
                minLength(1),
                maxLength(1)
            }
        )
    );
}

TEST_F(GameTest, createGame_alreadyInGame) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );

    QString nonce = sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );

    checkNewMessage(
        "1", "Error on create game",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("ALREADY_IN_GAME"))}
        )
    );
}

TEST_F(GameTest, createGame) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );

    checkNewMessage(
        "1", "commandResponse",
        commandResponseOk(nonce)
    );
    checkNewMessage(
        "1", "newGame message",
        serverMessage(
            "newGame",
            {
                requiredField("id", stringValue()),
                requiredField("owner", is(p1->getId())),
                requiredField("type", is("testGame"))
            }
        )
    );

    checkNewMessage(
        "1", "gameData message",
        serverMessage(
            "gameData",
            {
                requiredField("id", stringValue()),
                requiredField("bgColor", is("d5ffd5")),
                requiredField("estateGroups", minLength(2), maxLength(2)),
                requiredField("estates", minLength(8), maxLength(8)),
            }
        )
    );
}

TEST_F(GameTest, joinGame_noIdGiven) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "joinGame",
        {}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {
                minLength(1),
                maxLength(1)
            }
        )
    );
}

TEST_F(GameTest, joinGame_idNotString) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "joinGame",
        {{"id", 12345}}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {
                minLength(1),
                maxLength(1)
            }
        )
    );
}

TEST_F(GameTest, joinGame_invalidId) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    addConnection("2");
    sendLogin("2", "Bar");

    QString nonce = sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId + "-1"}}
    );

    checkNoNewMessages("1", "No message on 1");
    checkNewMessage(
        "2", "commandResult",
        commandResponseStateError(
            nonce, {maxLength(1), contains("E1", is("UNKNOWN_GAME"))})
    );
}

TEST_F(GameTest, joinGame_notLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    addConnection("2");

    QString nonce = sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    checkNoNewMessages("1", "No message on 1");
    checkNewMessage(
        "2", "commandResponse",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_LOGGED_IN"))}
        )
    );
}

TEST_F(GameTest, joinGame_gameFull) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    addConnection("2");
    addConnection("3");
    addConnection("4");
    sendLogin("1", "P1");
    sendLogin("2", "P2");
    sendLogin("3", "P3");
    sendLogin("4", "P4");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );
    sendCommand(
        "3", "server", "joinGame",
        {{"id", gameId}}
    );
    QString nonce = sendCommand(
        "4", "server", "joinGame",
        {{"id", gameId}}
    );

    checkNoNewMessages("1", "No message on 1");
    checkNoNewMessages("2", "No message on 2");
    checkNoNewMessages("3", "No message on 3");
    checkNewMessage(
        "4", "Error message 4",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("GAME_FULL"))}
        )
    );
}

TEST_F(GameTest, joinGame) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");
    addConnection("3");
    sendLogin("1", "P1");
    sendLogin("2", "P2");
    sendLogin("3", "P3");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    QString p2JoinNonce = sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    checkNewMessage(
        "1", "P2 joined 1",
        serverMessage(
            "gameUpdate",
            requiredField("id", is(gameId)),
            requiredField("players", contains("p1", is(p1->getId())), contains("p2", is(p2->getId())))
        )
    );

    checkNewMessage("2", "joinResponse", commandResponseOk(p2JoinNonce));
    checkNewMessage(
        "2", "gameData message",
        serverMessage(
            "gameData",
            {
                requiredField("id", stringValue()),
                requiredField("bgColor", is("d5ffd5")),
                requiredField("estateGroups", minLength(2), maxLength(2)),
                requiredField("estates", minLength(8), maxLength(8)),
            }
        )
    );
    checkNewMessage(
        "2", "P2 joined 2",
        serverMessage(
            "gameUpdate",
            requiredField("id", is(gameId)),
            requiredField("players", contains("p1", is(p1->getId())), contains("p2", is(p2->getId())))
        )
    );
    checkNewMessage(
        "3", "P2 joined 3",
        serverMessage(
            "gameUpdate",
            requiredField("id", is(gameId)),
            requiredField("players", contains("p1", is(p1->getId())), contains("p2", is(p2->getId())))
        )
    );
}

TEST_F(GameTest, joinGame_alreadyInGame) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    addConnection("2");
    sendLogin("1", "P1");
    sendLogin("2", "P2");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    QString nonce = sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    checkNoNewMessages("1", "No message 1");
    checkNewMessage(
        "2", "Already logged in error",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("ALREADY_IN_GAME"))}
        )
    );
}

TEST_F(GameTest, leaveGame) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    auto p2 = addConnection("2");
    auto p3 = addConnection("3");
    sendLogin("1", "P1");
    sendLogin("2", "P2");
    sendLogin("3", "P3");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    QString p1LeaveNonce = sendCommand("1", "server", "leaveGame", {});

    checkNewMessage("1", "leaveResult", commandResponseOk(p1LeaveNonce));
    checkNewMessage(
        "1", "gameUpdate 1 1",
        serverMessage(
            "gameUpdate", {
                requiredField("id", is(gameId)),
                requiredField("players", {minLength(1), maxLength(1), contains("p2", is(p2->getId()))}),
                requiredField("owner", is(p2->getId()))
            }
        )
    );

    checkNewMessage(
        "2", "gameUpdate 1 2",
        serverMessage(
            "gameUpdate", {
                requiredField("id", is(gameId)),
                requiredField("players", {minLength(1), maxLength(1), contains("p2", is(p2->getId()))}),
                requiredField("owner", is(p2->getId()))
            }
        )
    );

    checkNewMessage(
        "3", "gameUpdate 1 3",
        serverMessage(
            "gameUpdate", {
                requiredField("id", is(gameId)),
                requiredField("players", {minLength(1), maxLength(1), contains("p2", is(p2->getId()))}),
                requiredField("owner", is(p2->getId()))
            }
        )
    );

    QString p3JoinNonce = sendCommand(
        "3", "server", "joinGame",
        {{"id", gameId}}
    );

    checkNewMessage(
        "1", "gameUpdate 2 1",
        serverMessage(
            "gameUpdate", {
                requiredField("id", is(gameId)),
                requiredField("players", {minLength(2), maxLength(2), contains("p2", is(p2->getId())), contains("p3", is(p3->getId()))}),
                requiredField("owner", is(p2->getId()))
            }
        )
    );

    checkNewMessage(
        "2", "gameUpdate 2 2",
        serverMessage(
            "gameUpdate", {
                requiredField("id", is(gameId)),
                requiredField("players", {minLength(2), maxLength(2), contains("p2", is(p2->getId())), contains("p3", is(p3->getId()))}),
                requiredField("owner", is(p2->getId()))
            }
        )
    );

    checkNewMessage("3", "gameJoinResponse-3", commandResponseOk(p3JoinNonce));
    checkNewMessage(
        "3", "gameData 3",
        serverMessage(
            "gameData", requiredField("bgColor", stringValue())
        )
    );

    checkNewMessage(
        "3", "gameUpdate 2 3",
        serverMessage(
            "gameUpdate", {
                requiredField("id", is(gameId)),
                requiredField("players", {minLength(2), maxLength(2), contains("p2", is(p2->getId())), contains("p3", is(p3->getId()))}),
                requiredField("owner", is(p2->getId()))
            }
        )
    );

    QString p3LeaveNonce = sendCommand("3", "server", "leaveGame", {});

    checkNewMessage(
        "1", "gameUpdate 3 1",
        serverMessage(
            "gameUpdate", {
                requiredField("id", is(gameId)),
                requiredField("players", {minLength(1), maxLength(1), contains("p2", is(p2->getId()))}),
                requiredField("owner", is(p2->getId()))
            }
        )
    );

    checkNewMessage(
        "2", "gameUpdate 3 2",
        serverMessage(
            "gameUpdate", {
                requiredField("id", is(gameId)),
                requiredField("players", {minLength(1), maxLength(1), contains("p2", is(p2->getId()))}),
                requiredField("owner", is(p2->getId()))
            }
        )
    );

    checkNewMessage("3", "leaveResult 3", commandResponseOk(p3LeaveNonce));
    checkNewMessage(
        "3", "gameUpdate 3 3",
        serverMessage(
            "gameUpdate", {
                requiredField("id", is(gameId)),
                requiredField("players", {minLength(1), maxLength(1), contains("p2", is(p2->getId()))}),
                requiredField("owner", is(p2->getId()))
            }
        )
    );
}

TEST_F(GameTest, leaveGame_notLogedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand("1", "server", "leaveGame", {});

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_LOGGED_IN"))}
        )
    );
}

TEST_F(GameTest, leaveGame_notInGame) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    addConnection("2");
    addConnection("3");
    sendLogin("1", "P1");
    sendLogin("2", "P2");
    sendLogin("3", "P3");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );

    QString nonce = sendCommand("2", "server", "leaveGame", {});

    checkNewMessage(
        "2", "error result",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_IN_GAME"))}
        )
    );

    checkNoNewMessages("1", "error message 1");
    checkNoNewMessages("3", "error message 3");
}

TEST_F(GameTest, leaveGame_lastPlayer) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    addConnection("2");
    addConnection("3");
    sendLogin("1", "P1");
    sendLogin("2", "P2");
    sendLogin("3", "P3");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    sendCommand("1", "server", "leaveGame", {});
    QString p2LeaveNonce = sendCommand("2", "server", "leaveGame", {});

    checkNewMessage(
        "1", "gameDelete 1",
        serverMessage(
            "gameDelete",
            requiredField("id", is(gameId))
        )
    );

    checkNewMessage("2", "leaveResult 2", commandResponseOk(p2LeaveNonce));
    checkNewMessage(
        "2", "gameDelete 2",
        serverMessage(
            "gameDelete",
            requiredField("id", is(gameId))
        )
    );

    checkNewMessage(
        "3", "gameDelete 3",
        serverMessage(
            "gameDelete",
            requiredField("id", is(gameId))
        )
    );
}

TEST_F(GameTest, leaveGame_running) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    addConnection("2");
    sendLogin("1", "P1");
    sendLogin("2", "P2");

    QString player2Id = getNextMessage("2", "Player2 login").toObject().value("data").toObject().value("id").toString();

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );
    discardNextMessage("2");

    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    sendCommand("1", "server", "leaveGame", {});
    discardNextMessage("1");

    checkNewMessage(
        "1", "P1 game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("state", is("END")),
                requiredField("turnState", is("NOTHING"))
            }
        )
    );
    checkNoNewMessages("1", "P1 no more messages");

    checkNewMessage(
        "2", "P2 game end event",
        eventMessage(
            "end",
            stringValue(is(player2Id))
        )
    );

    checkNewMessage(
        "2", "P2 game update",
        serverMessage(
            "gameUpdate",
            {
                requiredField("state", is("END")),
                requiredField("turnState", is("NOTHING"))
            }
        )
    );

    QString p2LeaveNonce = sendCommand("2", "server", "leaveGame", {});

    checkNewMessage(
        "1", "gameDelete 1",
        serverMessage(
            "gameDelete",
            requiredField("id", is(gameId))
        )
    );

    checkNewMessage("2", "leaveResult 2", commandResponseOk(p2LeaveNonce));
    checkNewMessage(
        "2", "gameDelete 2",
        serverMessage(
            "gameDelete",
            requiredField("id", is(gameId))
        )
    );
}

TEST_F(GameTest, startGame_onePlayer) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    sendLogin("1", "Foo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );

    QString nonce = sendCommand("1", "game", "setReady", {{"ready", true}});

    checkNewMessage(
        "1", "Command result",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("TOO_FEW_PLAYERS"))}
        )
    );
}

TEST_F(GameTest, startGame_twoPlayers) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");
    addConnection("3");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    QString p1ReadyNonce = sendCommand("1", "game", "setReady", {{"ready", true}});

    checkNewMessage("1", "readyResult 1", commandResponseOk(p1ReadyNonce));
    checkNewMessage(
        {"1", "2"}, "Player Foo Ready",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("ready", isTrue())
        )
    );

    checkNoNewMessages("3", "No game player Update - Doo");

    rng->addNextNumbers({0, 0});

    QString p2ReadyNonce = sendCommand("2", "game", "setReady", {{"ready", true}});

    checkNewMessage("2", "readyResult 2", commandResponseOk(p2ReadyNonce));
    checkNewMessage(
        {"1", "2"}, "Player Bar Ready",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p2->getId())),
            requiredField("ready", isTrue())
        )
    );

    checkNewMessage(
        {"1", "2"}, "Player Foo Data",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p1->getId())),
                requiredField("money", is(1500)),
                requiredField("position", is(2)),
                requiredField("inJail", isFalse())
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Player Bar Data",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p2->getId())),
                requiredField("money", is(1500)),
                requiredField("position", is(2)),
                requiredField("inJail", isFalse())
            }
        )
    );

    checkNewMessage(
        {"1", "2", "3"}, "Game start",
        serverMessage(
            "gameUpdate",
            requiredField("id", is(gameId)),
            requiredField("state", is("RUN"))
        )
    );

    checkNewMessage(
        {"1", "2"}, "Game player Order",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("ROLL")),
                requiredField(
                    "playerOrder",
                    arrayValue(
                        {
                            maxLength(2),
                            contains("Foo", is(p1->getId())),
                            contains("Bar", is(p2->getId()))
                        }
                    )
                ),
                requiredField("turnPlayer", is(p2->getId()))
            }
        )
    );
}

TEST_F(GameTest, startGame_twoPlayers_gameHasInvalid_startEstate) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");
    addConnection("3");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame2"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    sendCommand("1", "game", "setReady", {{"ready", true}});
    discardNextMessage("1");

    checkNewMessage(
        "1", "Player Foo Ready - Foo",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("ready", isTrue())
        )
    );

    checkNewMessage(
        "2", "Player Foo Ready - Bar",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("ready", isTrue())
        )
    );

    checkNoNewMessages("3", "No game player Update - Doo");

    sendCommand("2", "game", "setReady", {{"ready", true}});
    discardNextMessage("2");

    checkNewMessage(
        "1", "Player Bar Ready - Foo",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p2->getId())),
            requiredField("ready", isTrue())
        )
    );

    checkNewMessage(
        "1", "Player Foo Data - Foo",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p1->getId())),
                requiredField("money", is(42)),
                requiredField("position", is(0)),
                requiredField("inJail", isFalse())
            }
        )
    );

    checkNewMessage(
        "1", "Player Bar Data - Foo",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p2->getId())),
                requiredField("money", is(42)),
                requiredField("position", is(0)),
                requiredField("inJail", isFalse())
            }
        )
    );

    checkNewMessage(
        "1", "Game start - Foo",
        serverMessage(
            "gameUpdate",
            requiredField("id", is(gameId)),
            requiredField("state", is("RUN"))
        )
    );

    checkNewMessage(
        "2", "Player Bar Ready - Bar",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p2->getId())),
            requiredField("ready", isTrue())
        )
    );

    checkNewMessage(
        "2", "Player Foo Data - Bar",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p1->getId())),
                requiredField("money", is(42)),
                requiredField("position", is(0)),
                requiredField("inJail", isFalse())
            }
        )
    );

    checkNewMessage(
        "2", "Player Bar Data - Bar",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p2->getId())),
                requiredField("money", is(42)),
                requiredField("position", is(0)),
                requiredField("inJail", isFalse())
            }
        )
    );

    checkNewMessage(
        "2", "Game start - Bar",
        serverMessage(
            "gameUpdate",
            requiredField("id", is(gameId)),
            requiredField("state", is("RUN"))
        )
    );

    checkNewMessage(
        "3", "Game start - Doo",
        serverMessage(
            "gameUpdate",
            requiredField("id", is(gameId)),
            requiredField("state", is("RUN"))
        )
    );
}

TEST_F(GameTest, command_notLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand("1", "game", "roll", {});

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_LOGGED_IN"))}
        )
    );
}

TEST_F(GameTest, roll) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");
    addConnection("3");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 4});
    sendCommand("1", "game", "roll", {});
    discardNextMessage("1");

    checkNewMessage(
        {"1", "2"}, "Roll event",
        eventMessage(
            "roll",
            objectValue(
                requiredField("dice1", is(1)),
                requiredField("dice2", is(4))
            )
        )
    );

    checkNewMessage(
        {"1", "2"}, "GamePlayerUpdate Moved 1",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("moved", isFalse())
        )
    );

    checkNewMessage(
        {"1", "2"}, "GamePlayerUpdate Moved 2",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p2->getId())),
            requiredField("moved", isFalse())
        )
    );

    checkNewMessage(
        {"1", "2"}, "GameUpdate TurnState",
        serverMessage(
            "gameUpdate",
            requiredField("id", is(gameId)),
            requiredField("turnState", is("MOVE"))
        )
    );

    checkNewMessage(
        {"1", "2"}, "GamePlayerMove",
        serverMessage(
            "gamePlayerMove",
            {
                requiredField("id", is(p1->getId())),
                requiredField("from", is(2)),
                requiredField("to", is(7)),
                requiredField("mode", is("FORWARD"))
            }
        )
    );

    checkNoNewMessages("3", "No messages for Doo");
}

TEST_F(GameTest, roll_whileMoving) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    addConnection("2");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 4});
    sendCommand("1", "game", "roll", {});

    QString nonce1 = sendCommand("1", "game", "roll", {});
    checkNewMessage(
        "1", "Invalid roll 1",
        commandResponseStateError(
            nonce1,
            {maxLength(1), contains("E1", is("WRONG_TURN_STATE"))}
        )
    );

    QString nonce2 = sendCommand("2", "game", "roll", {});
    checkNewMessage(
        "2", "Invalid roll 2",
        commandResponseStateError(
            nonce2,
            {maxLength(1), contains("E1", is("NOT_ON_TURN"))}
        )
    );
}

TEST_F(GameTest, roll_noMoveFinish) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");
    addConnection("3");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    discardMessages({"1", "2", "3"});
    QTest::qWait(2 * 50);
    checkNoNewMessages({"1", "2", "3"}, "No messages - Config");

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    discardMessages({"1", "2", "3"});
    QTest::qWait(2 * 50);
    checkNoNewMessages({"1", "2", "3"}, "No messages - In roll state");

    rng->addNextNumbers({1, 3});
    sendCommand("1", "game", "roll", {});

    discardMessages({"1", "2", "3"});
    QTest::qWait(6 * 50);

    checkNewMessage(
        {"1", "2"}, "GamePlayerUpdate Landing",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("position", is(6))
        )
    );

    checkNewMessage(
        {"1", "2"}, "Land event",
        eventMessage(
            "land",
            integerValue(is(6))
        )
    );

    checkNewMessage(
        {"1", "2"}, "GameUpdate",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("ROLL")),
                requiredField("turnPlayer", is(p2->getId()))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Turn event",
        eventMessage(
            "turn",
            stringValue(is(p2->getId()))
        )
    );

    checkNoNewMessages("3", "No messages for Doo");
}

TEST_F(GameTest, finishMove_notMoving) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    sendLogin("1", "Foo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    QString nonce = sendCommand("1", "game", "moveComplete", {});

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("WRONG_TURN_STATE"))}
        )
    );
}

TEST_F(GameTest, finishMove) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");
    addConnection("3");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");

    QString gameId = sendCreateGame("1", "testGame");
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    rng->addNextNumbers({0, 1});
    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    rng->addNextNumbers({1, 3});
    sendCommand("1", "game", "roll", {});

    sendCommand("1", "game", "moveComplete", {});
    discardNextMessage("1");

    checkNewMessage(
        {"1", "2"}, "GamePlayerUpdate Move 1",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("moved", isTrue())
        )
    );

    sendCommand("2", "game", "moveComplete", {});
    discardNextMessage("2");

    checkNewMessage(
        {"1", "2"}, "GamePlayerUpdate Move 2",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p2->getId())),
            requiredField("moved", isTrue())
        )
    );

    checkNewMessage(
        {"1", "2"}, "GamePlayerUpdate Landing",
        serverMessage(
            "gamePlayerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("position", is(6))
        )
    );

    checkNewMessage(
        {"1", "2"}, "Land event",
        eventMessage(
            "land",
            integerValue(is(6))
        )
    );

    checkNewMessage(
        {"1", "2"}, "GameUpdate",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("turnState", is("ROLL")),
                requiredField("turnPlayer", is(p2->getId()))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Turn event",
        eventMessage(
            "turn",
            stringValue(is(p2->getId()))
        )
    );

    checkNoNewMessages("3", "No messages for Doo");
}

TEST_F(GameTest, setReady_gameRunning) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    addConnection("2");
    addConnection("3");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");
    sendLogin("3", "Doo");

    sendCommand(
        "1", "server", "createGame",
        {{"type", "testGame"}}
    );
    discardNextMessage("1");

    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand(
        "2", "server", "joinGame",
        {{"id", gameId}}
    );

    sendCommand("1", "game", "setReady", {{"ready", true}});
    sendCommand("2", "game", "setReady", {{"ready", true}});

    QString nonce = sendCommand("1", "game", "setReady", {{"ready", true}});

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("GAME_RUNNING"))}
        )
    );
}
