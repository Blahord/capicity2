#include <gtest/gtest.h>

#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class ServerManagerTest : public CapidTestBase {

    protected:
        void SetUp() override {
            CapidTestBase::SetUp();

            serverManager = new ServerManager();

            addConnection("1");
            addConnection("2");
            addConnection("3");

            player1 = std::make_shared<CapidPlayer>();
            player2 = std::make_shared<CapidPlayer>();
            player3 = std::make_shared<CapidPlayer>();

            auto connection1 = std::make_unique<CapidPlayerConnection>(player1, testConnections.value("1"));
            auto connection2 = std::make_unique<CapidPlayerConnection>(player2, testConnections.value("2"));
            auto connection3 = std::make_unique<CapidPlayerConnection>(player3, testConnections.value("3"));

            player1->setName("1");
            player2->setName("2");
            player3->setName("3");

            serverManager->addConnectingPlayer(std::move(connection1));
            serverManager->addConnectingPlayer(std::move(connection2));
            serverManager->addConnectingPlayer(std::move(connection3));

            serverManager->addPlayer(player1);
            serverManager->addPlayer(player2);
            serverManager->addPlayer(player3);
        }

        void TearDown() override {
            delete serverManager;

            CapidTestBase::TearDown();
        }

        ServerManager* serverManager = nullptr;

        std::shared_ptr<CapidPlayer> player1;
        std::shared_ptr<CapidPlayer> player2;
        std::shared_ptr<CapidPlayer> player3;
};

TEST_F(ServerManagerTest, broadcastAllWithExlcude) { // NOLINT(cert-err58-cpp)
    serverManager->broadcast(
        "test",
        QJsonObject(
            {
                {"a", 1},
                {"b", 2}
            }
        ),
        player3
    );

    checkNewMessage(
        "1", "Player 1",
        serverMessage(
            "test",
            requiredField("a", is(1)),
            requiredField("b", is(2))
        )
    );

    checkNewMessage(
        "2", "Player 2",
        serverMessage(
            "test",
            requiredField("a", is(1)),
            requiredField("b", is(2))
        )
    );

    checkNoNewMessages("3", "Player 3");
}

TEST_F(ServerManagerTest, broadcastAllWithoutExlucde) { // NOLINT(cert-err58-cpp)
    serverManager->broadcast(
        "test",
        QJsonObject(
            {
                {"a", 1},
                {"b", 2}
            }
        ),
        nullptr
    );

    checkNewMessage(
        "1", "Player 1",
        serverMessage(
            "test",
            requiredField("a", is(1)),
            requiredField("b", is(2))
        )
    );

    checkNewMessage(
        "2", "Player 2",
        serverMessage(
            "test",
            requiredField("a", is(1)),
            requiredField("b", is(2))
        )
    );

    checkNewMessage(
        "3", "Player 3",
        serverMessage(
            "test",
            requiredField("a", is(1)),
            requiredField("b", is(2))
        )
    );
}

TEST_F(ServerManagerTest, broadcastInGame) { // NOLINT(cert-err58-cpp)
    auto game = std::make_shared<CapidGame>(CapidGameTemplate("template"));
    game->addPlayer(player1);
    game->addPlayer(player2);

    serverManager->broadcast(
        game,
        "test",
        QJsonObject(
            {
                {"a", 1},
                {"b", 2}
            }
        )
    );

    checkNewMessage(
        "1", "Player 1",
        serverMessage(
            "test",
            requiredField("a", is(1)),
            requiredField("b", is(2))
        )
    );

    checkNewMessage(
        "2", "Player 2",
        serverMessage(
            "test",
            requiredField("a", is(1)),
            requiredField("b", is(2))
        )
    );

    checkNoNewMessages("3", "Player 3");
}

TEST_F(ServerManagerTest, sendToPlayer) { // NOLINT(cert-err58-cpp)
    serverManager->sendToPlayer(
        player1,
        "test",
        QJsonObject(
            {
                {"a", 1},
                {"b", 2}
            }
        )
    );

    checkNewMessage(
        "1", "Player 1",
        serverMessage(
            "test",
            requiredField("a", is(1)),
            requiredField("b", is(2))
        )
    );
    checkNoNewMessages("2", "Player 1");
    checkNoNewMessages("3", "Player 3");
}

TEST_F(ServerManagerTest, connectingPlayer) { // NOLINT(cert-err58-cpp)
    auto connectingPlayer = std::make_shared<CapidPlayer>();
    auto connection = std::make_unique<CapidPlayerConnection>(connectingPlayer, new TestClientConnection());

    ASSERT_EQ(serverManager->isConnecting(player1), false);
    ASSERT_EQ(serverManager->isConnecting(player2), false);
    ASSERT_EQ(serverManager->isConnecting(player3), false);
    ASSERT_EQ(serverManager->isConnecting(connectingPlayer), false);

    serverManager->addConnectingPlayer(std::move(connection));
    ASSERT_EQ(serverManager->isConnecting(player1), false);
    ASSERT_EQ(serverManager->isConnecting(player2), false);
    ASSERT_EQ(serverManager->isConnecting(player3), false);
    ASSERT_EQ(serverManager->isConnecting(connectingPlayer), true);

    serverManager->deleteConnectingPlayer(connectingPlayer);
    ASSERT_EQ(serverManager->isConnecting(player1), false);
    ASSERT_EQ(serverManager->isConnecting(player2), false);
    ASSERT_EQ(serverManager->isConnecting(player3), false);
}

TEST_F(ServerManagerTest, players) { // NOLINT(cert-err58-cpp)
    std::shared_ptr<CapidPlayer> newPlayer = std::make_shared<CapidPlayer>();
    newPlayer->setName("new");

    ASSERT_EQ(serverManager->findPlayer("new"), nullptr);
    ASSERT_EQ(serverManager->findPlayer(player1->getId()), player1);
    ASSERT_EQ(serverManager->findPlayer(player2->getId()), player2);
    ASSERT_EQ(serverManager->findPlayer(player3->getId()), player3);

    ASSERT_EQ(serverManager->findServerPlayerByToken(newPlayer->getToken()), nullptr);
    ASSERT_EQ(serverManager->findServerPlayerByToken(player1->getToken()), player1);
    ASSERT_EQ(serverManager->findServerPlayerByToken(player2->getToken()), player2);
    ASSERT_EQ(serverManager->findServerPlayerByToken(player3->getToken()), player3);

    ASSERT_EQ(serverManager->isLoggedIn(newPlayer), false);
    ASSERT_EQ(serverManager->isLoggedIn(player1), true);
    ASSERT_EQ(serverManager->isLoggedIn(player2), true);
    ASSERT_EQ(serverManager->isLoggedIn(player3), true);

    ASSERT_EQ(serverManager->getPlayers().size(), 3);
    ASSERT_EQ(serverManager->getPlayers().contains(player1), true);
    ASSERT_EQ(serverManager->getPlayers().contains(player2), true);
    ASSERT_EQ(serverManager->getPlayers().contains(player3), true);

    serverManager->deletePlayer(player1);

    ASSERT_EQ(serverManager->getPlayers().size(), 2);
    ASSERT_EQ(serverManager->getPlayers().contains(player2), true);
    ASSERT_EQ(serverManager->getPlayers().contains(player3), true);
}

TEST_F(ServerManagerTest, gameTemplates) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(serverManager->getGameTemplates().isEmpty(), true);

    serverManager->addGameTemplate(CapidGameTemplate("1"));
    ASSERT_EQ(serverManager->getGameTemplates().size(), 1);

    serverManager->addGameTemplate(CapidGameTemplate("2"));
    ASSERT_EQ(serverManager->getGameTemplates().size(), 2);

    ASSERT_EQ(serverManager->findGameTemplate("1").getId(), "1");
    ASSERT_EQ(serverManager->findGameTemplate("2").getId(), "2");
}

TEST_F(ServerManagerTest, games) { // NOLINT(cert-err58-cpp)
    auto game1 = std::make_shared<CapidGame>(CapidGameTemplate("template"));
    auto game2 = std::make_shared<CapidGame>(CapidGameTemplate("template"));

    game1->addPlayer(player1);
    game1->addPlayer(player2);

    ASSERT_EQ(serverManager->findGameById(game1->getId()), nullptr);
    ASSERT_EQ(serverManager->findGameById(game2->getId()), nullptr);
    ASSERT_EQ(serverManager->getGames().isEmpty(), true);

    serverManager->addGame(game1);
    ASSERT_EQ(serverManager->findGameById(game1->getId()), game1);
    ASSERT_EQ(serverManager->findGameById(game2->getId()), nullptr);
    ASSERT_EQ(serverManager->getGames().size(), 1);
    ASSERT_EQ(serverManager->getGames().contains(game1), true);

    serverManager->addGame(game2);
    ASSERT_EQ(serverManager->findGameById(game1->getId()), game1);
    ASSERT_EQ(serverManager->findGameById(game2->getId()), game2);
    ASSERT_EQ(serverManager->getGames().size(), 2);
    ASSERT_EQ(serverManager->getGames().contains(game1), true);
    ASSERT_EQ(serverManager->getGames().contains(game2), true);

    ASSERT_EQ(serverManager->findGameByPlayer(player1), game1);
    ASSERT_EQ(serverManager->findGameByPlayer(player2), game1);
    ASSERT_EQ(serverManager->findGameByPlayer(player3), nullptr);

    game2->addPlayer(player3);
    ASSERT_EQ(serverManager->findGameByPlayer(player3), game2);

    serverManager->deleteGame(game2);
    serverManager->deleteGame(game1);
}

TEST_F(ServerManagerTest, time) { // NOLINT(cert-err58-cpp)
    ASSERT_EQ(serverManager->getTime(), 0);

    serverManager->incrTime();
    ASSERT_EQ(serverManager->getTime(), 1);

    serverManager->incrTime();
    ASSERT_EQ(serverManager->getTime(), 2);
}