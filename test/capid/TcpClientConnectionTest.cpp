#include <gtest/gtest.h>
#include "Capid.h"
#include <QtCore/QCoreApplication>
#include <QtTest/QtTest>
#include "TcpClientConnection.h"
#include "TestRNG.h"

class TcpClientConnectionTest : public ::testing::Test {

    protected:
        void SetUp() override {
            Test::SetUp();

            int argc = 0;
            app = new QCoreApplication(argc, nullptr);
            capid = new Capid("test/resources/games", std::make_shared<TestRNG>());
        }

        void TearDown() override {
            delete capid;
            delete app;

            Test::TearDown();
        }

        QCoreApplication* app = nullptr;
        Capid* capid = nullptr;
};

TEST_F(TcpClientConnectionTest, connectToServer) { // NOLINT(cert-err58-cpp)
    auto clientSocket = new QTcpSocket();
    clientSocket->connectToHost(QHostAddress::LocalHostIPv6, 1234);

    ASSERT_TRUE(clientSocket->waitForConnected(1)) << "Connect to server failed";
    QTest::qWait(50);

    auto clientConnection = new CapiConnection(clientSocket);

    clientConnection->send(
        QJsonObject({
                        {"scope",     "server"},
                        {"command",   "login"},
                        {"arguments", QJsonObject({{"name", "Foo"}})}
                    })
    );

    QTest::qWait(500);

    clientConnection->send(
        QJsonObject({
                        {"scope",   "server"},
                        {"command", "quit"}
                    })
    );

    delete clientConnection;
}

TEST_F(TcpClientConnectionTest, createAndDelete) { // NOLINT(cert-err58-cpp)
    auto clientSocket = new QTcpSocket();
    auto tcpConnection = new TcpClientConnection(clientSocket);

    delete tcpConnection;
}