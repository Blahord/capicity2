#include <gtest/gtest.h>
#include "CapidPlayer.h"
#include "CapidPlayerConnection.h"

TEST(CapidPlayerConnectionTest, SendWithOutConnection) { // NOLINT(cert-err58-cpp)
    auto connection = new CapidPlayerConnection(std::make_shared<CapidPlayer>(), nullptr);
    connection->send({});
    delete connection;
}

TEST(CapidPlayerConnectionTest, disconnectWithOutConnection) { // NOLINT(cert-err58-cpp)
    auto connection = new CapidPlayerConnection(std::make_shared<CapidPlayer>(), nullptr);
    connection->disconnectFromClient();
    delete connection;
}
