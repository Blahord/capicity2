#include <gtest/gtest.h>
#include "game/CapidEstate.h"

TEST(CapidEstateTest, Id) { // NOLINT(cert-err58-cpp)
    auto estate = std::make_unique<CapidEstate>("123", 1, 1, 1, 2, 3, 4, 5, 6, 7, "Foo");
    ASSERT_EQ(estate->getId(), "123");
}

TEST(CapidEstateTest, Owner) { // NOLINT(cert-err58-cpp)
    auto player = std::make_shared<CapiPlayer>("1");
    auto gamePlayer = std::make_shared<CapiGamePlayer>(player);
    auto estate = std::make_unique<CapidEstate>("123", 1, 1, 1, 2, 3, 4, 5, 6, 7, "Foo");

    estate->setOwner(gamePlayer);
    ASSERT_EQ(estate->getOwner(), gamePlayer);
}


TEST(CapidEstateTest, PayTarget) {
    auto estate1 = std::make_shared<CapidEstate>("123", 1, 1, 1, 2, 3, 4, 5, 6, 7, "321");
    auto estate2 = std::make_shared<CapidEstate>("321", 1, 1, 1, 2, 3, 4, 5, 6, 7, "123");

    estate1->setPayTarget(estate2);
    estate2->setPayTarget(estate1);

    ASSERT_EQ(estate1->getPayTarget().lock(), estate2);
    ASSERT_EQ(estate2->getPayTarget().lock(), estate1);
}
