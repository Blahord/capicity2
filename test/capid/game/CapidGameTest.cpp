#include <gtest/gtest.h>

#include "json/JsonChecks.h"
#include "game/CapidGame.h"
#include "TestClientConnection.h"
#include "JsonResultChecker.h"
#include "JsonResultChecks.h"
#include "TestRNG.h"
#include "ServerManager.h"
#include "GameManager.h"
#include "command/game/GameScope.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

TEST(CapidGameTest, deleteGame) { // NOLINT(cert-err58-cpp)
    auto player = std::make_shared<CapidPlayer>();
    auto estate = std::make_shared<CapidEstate>("1", 1, 1, 1, 2, 3, 4, 5, 6, 7, "Foo");
    auto group = std::make_shared<CapidEstateGroup>("1", "001122", 12);
    auto game = new CapidGame(CapidGameTemplate("1"));

    game->addEstateGroup(group);
    game->addEstate(estate);
    game->addPlayer(player);

    delete game;
}

TEST(CapidGameTest, getStateName) { // NOLINT(cert-err58-cpp)
    auto gameTemplate = CapidGameTemplate("1");
    gameTemplate.setMaxPlayers(2);
    auto player1 = std::make_shared<CapidPlayer>();
    auto game = new CapidGame(gameTemplate);
    game->addPlayer(player1);

    game->setState(GameState::CONFIG);
    QString const state1 = game->getStateName();
    EXPECT_EQ(state1, "CONFIG");

    game->setState(GameState::RUN);
    QString const state2 = game->getStateName();
    EXPECT_EQ(state2, "RUN");

    game->setState(GameState::END);
    QString const state3 = game->getStateName();
    EXPECT_EQ(state3, "END");

    game->setState(GameState::NOTHING);
    QString const state4 = game->getStateName();
    EXPECT_EQ(state4, "");

    delete game;
}

TEST(CapidGameTest, setReady_notInGame) { // NOLINT(cert-err58-cpp)
    auto rng = std::make_shared<TestRNG>();
    auto gameTemplate = CapidGameTemplate("1");
    gameTemplate.setMaxPlayers(2);
    auto connection1 = new TestClientConnection();
    auto player1 = std::make_shared<CapidPlayer>();
    auto playerConnection1 = std::make_unique<CapidPlayerConnection>(player1, connection1);
    auto game = std::make_shared<CapidGame>(gameTemplate);
    auto serverManager = std::make_shared<ServerManager>();
    auto gameManager = std::make_shared<GameManager>(serverManager);
    auto commandExecutor = new GameScope(serverManager, rng, gameManager);

    serverManager->addConnectingPlayer(std::move(playerConnection1));
    serverManager->addPlayer(player1);
    serverManager->addGame(game);
    commandExecutor->executeCommand(player1, "foo-bar", "setReady", {{"ready", true}});

    EXPECT_TRUE(connection1->hasMessage());
    if (connection1->hasMessage()) {
        JsonResultChecker::checkJsonResult(
            connection1->getNewMessage().toObject(),
            commandResponseStateError(
                "foo-bar",
                {maxLength(1), contains("E1", is("NOT_IN_GAME"))}
            ),
            "setReady_invalidData",
            "Error message"
        );
    }

    delete commandExecutor;
}

TEST(CapidGameTest, setReady_invalidData) { // NOLINT(cert-err58-cpp)
    auto rng = std::make_shared<TestRNG>();
    auto gameTemplate = CapidGameTemplate("1");
    gameTemplate.setMaxPlayers(2);
    auto connection1 = new TestClientConnection();
    auto player1 = std::make_shared<CapidPlayer>();
    auto playerConnection1 = std::make_unique<CapidPlayerConnection>(player1, connection1);
    auto game = std::make_shared<CapidGame>(gameTemplate);
    auto serverManager = std::make_shared<ServerManager>();
    auto gameManager = std::make_shared<GameManager>(serverManager);
    auto commandExecutor = new GameScope(serverManager, rng, gameManager);

    serverManager->addConnectingPlayer(std::move(playerConnection1));
    serverManager->addPlayer(player1);
    serverManager->addGame(game);
    game->addPlayer(player1);

    commandExecutor->executeCommand(player1, "foo-bar", "setReady", {});

    EXPECT_TRUE(connection1->hasMessage());
    if (connection1->hasMessage()) {
        JsonResultChecker::checkJsonResult(
            connection1->getNewMessage().toObject(),
            commandResponseFormalError(
                "foo-bar",
                {minLength(1), maxLength(1)}
            ),
            "setReady_invalidData",
            "Error message"
        );
    }

    delete commandExecutor;
}

TEST(CapidGameTest, roll_notStarted) { // NOLINT(cert-err58-cpp)
    auto rng = std::make_shared<TestRNG>();
    auto gameTemplate = CapidGameTemplate("1");
    gameTemplate.setMaxPlayers(2);
    auto connection1 = new TestClientConnection();
    auto connection2 = new TestClientConnection();
    auto player1 = std::make_shared<CapidPlayer>();
    auto player2 = std::make_shared<CapidPlayer>();
    auto playerConnection1 = std::make_unique<CapidPlayerConnection>(player1, connection1);
    auto playerConnection2 = std::make_unique<CapidPlayerConnection>(player2, connection2);

    auto game = std::make_shared<CapidGame>(gameTemplate);
    auto serverManager = std::make_shared<ServerManager>();
    auto gameManager = std::make_shared<GameManager>(serverManager);
    auto commandExecutor = new GameScope(serverManager, rng, gameManager);

    serverManager->addConnectingPlayer(std::move(playerConnection1));
    serverManager->addConnectingPlayer(std::move(playerConnection2));
    serverManager->addPlayer(player1);
    serverManager->addPlayer(player2);
    serverManager->addGame(game);

    game->addPlayer(player1);
    game->addPlayer(player2);

    rng->addNextNumbers({0, 1});
    commandExecutor->executeCommand(player1, "foo-bar", "setReady", {{"ready", true}});

    connection1->discardMessages();
    connection2->discardMessages();

    commandExecutor->executeCommand(player1, "foo-bar", "roll", {});

    EXPECT_TRUE(connection1->hasMessage());
    if (connection1->hasMessage()) {
        JsonResultChecker::checkJsonResult(
            connection1->getNewMessage().toObject(),
            commandResponseStateError(
                "foo-bar",
                {maxLength(1), contains("E1", is("NOT_ON_TURN"))}
            ),
            "roll_gameNotStarted",
            "Error message"
        );
    }

    delete commandExecutor;
}

TEST(CapidGameTest, roll_notOnTurn) { // NOLINT(cert-err58-cpp)
    auto rng = std::make_shared<TestRNG>();
    auto gameTemplate = CapidGameTemplate("1");
    gameTemplate.setMaxPlayers(2);
    auto connection1 = new TestClientConnection();
    auto connection2 = new TestClientConnection();
    auto player1 = std::make_shared<CapidPlayer>();
    auto player2 = std::make_shared<CapidPlayer>();
    auto playerConnection1 = std::make_unique<CapidPlayerConnection>(player1, connection1);
    auto playerConnection2 = std::make_unique<CapidPlayerConnection>(player2, connection2);
    auto game = std::make_shared<CapidGame>(gameTemplate);
    auto serverManager = std::make_shared<ServerManager>();
    auto gameManager = std::make_shared<GameManager>(serverManager);
    auto commandExecutor = new GameScope(serverManager, rng, gameManager);

    player1->setName("1");
    player2->setName("2");

    serverManager->addConnectingPlayer(std::move(playerConnection1));
    serverManager->addConnectingPlayer(std::move(playerConnection2));
    serverManager->addPlayer(player1);
    serverManager->addPlayer(player2);
    serverManager->addGame(game);
    game->addPlayer(player1);
    game->addPlayer(player2);

    rng->addNextNumbers({0, 1});
    commandExecutor->executeCommand(player1, "foo-bar", "setReady", {{"ready", true}});
    commandExecutor->executeCommand(player2, "foo-bar", "setReady", {{"ready", true}});

    connection1->discardMessages();
    connection2->discardMessages();

    commandExecutor->executeCommand(player2, "foo-bar", "roll", {});

    EXPECT_TRUE(connection2->hasMessage());
    if (connection2->hasMessage()) {
        JsonResultChecker::checkJsonResult(
            connection2->getNewMessage().toObject(),
            commandResponseStateError(
                "foo-bar",
                {maxLength(1), contains("E1", is("NOT_ON_TURN"))}
            ),
            "roll_notOnTurn",
            "Error message"
        );
    }

    delete commandExecutor;
}

TEST(CapidGameTest, findEstate) {
    auto game = std::make_shared<CapidGame>(CapidGameTemplate("1"));

    auto estate1 = std::make_shared<CapidEstate>("1", 0, 0, 0, 0, 0, 0, 0, 0, 0, "");
    auto estate2 = std::make_shared<CapidEstate>("2", 0, 0, 0, 0, 0, 0, 0, 0, 0, "");

    game->addEstate(estate1);
    game->addEstate(estate2);

    ASSERT_EQ(game->findEstate("1"), estate1);
    ASSERT_EQ(game->findEstate("2"), estate2);
    ASSERT_EQ(game->findEstate("3"), nullptr);
}