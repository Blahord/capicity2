#include <gtest/gtest.h>
#include "game/CapidEstateGroup.h"

TEST(CapidEstateGroupTest, Create_Delete) { // NOLINT(cert-err58-cpp)
    auto group = new CapidEstateGroup("1", "001122", 12);

    delete group;
}

TEST(CapidEstateGroupTest, rentMath) { // NOLINT(cert-err58-cpp)
    auto group = new CapidEstateGroup("1", "001122", 12);

    group->setRentMath("Pi * Thumb");
    ASSERT_EQ(group->getRentMath(), "Pi * Thumb");

    delete group;
}
