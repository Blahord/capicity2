#include <gtest/gtest.h>
#include "game/CapidGameReader.h"

TEST(CapidGameReaderTest, load) { // NOLINT(cert-err58-cpp)
    ImageRegistry imageRegistry;
    auto game = CapidGameReader::readGame("test/resources/games", "fullTest", &imageRegistry);

    ASSERT_EQ(game->getEstates().size(), 40);

    ASSERT_EQ(game->getEstates().at(0)->getId(), "los");
    ASSERT_EQ(game->getEstates().at(1)->getId(), "aldi");
    ASSERT_EQ(game->getEstates().at(2)->getId(), "comm1");
    ASSERT_EQ(game->getEstates().at(3)->getId(), "lidl");
    ASSERT_EQ(game->getEstates().at(4)->getId(), "tax1");
    ASSERT_EQ(game->getEstates().at(5)->getId(), "kölnBF");
    ASSERT_EQ(game->getEstates().at(6)->getId(), "obi");
    ASSERT_EQ(game->getEstates().at(7)->getId(), "chance1");
    ASSERT_EQ(game->getEstates().at(8)->getId(), "hornbach");
    ASSERT_EQ(game->getEstates().at(9)->getId(), "profiWesch");

    ASSERT_EQ(game->getEstates().at(0)->getPayTarget().lock(), nullptr);
    ASSERT_EQ(game->getEstates().at(1)->getPayTarget().lock(), nullptr);
    ASSERT_EQ(game->getEstates().at(2)->getPayTarget().lock(), nullptr);
    ASSERT_EQ(game->getEstates().at(3)->getPayTarget().lock(), nullptr);
    ASSERT_EQ(game->getEstates().at(4)->getPayTarget().lock(), game->getEstates().at(20));
    ASSERT_EQ(game->getEstates().at(5)->getPayTarget().lock(), nullptr);
    ASSERT_EQ(game->getEstates().at(6)->getPayTarget().lock(), nullptr);
    ASSERT_EQ(game->getEstates().at(7)->getPayTarget().lock(), nullptr);
    ASSERT_EQ(game->getEstates().at(8)->getPayTarget().lock(), nullptr);
    ASSERT_EQ(game->getEstates().at(9)->getPayTarget().lock(), nullptr);
}
