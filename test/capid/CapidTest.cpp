#include <QtTest/QtTest>
#include <QtCore/QJsonParseError>

#include "json/JsonChecks.h"
#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class CapidTest : public CapidTestBase {

};

TEST_F(CapidTest, command_noField) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    send("1", {});
    checkNewMessage(
        "1", "error",
        errorMessage(
            {
                minLength(3),
                maxLength(3),
                contains("E1", is("/nonce : Field not present")),
                contains("E2", is("/scope : Field not present")),
                contains("E3", is("/command : Field not present"))
            }
        )
    );
}

TEST_F(CapidTest, command_noNonce) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    send("1", {{"scope",   "server"},
               {"command", "foo"}});
    checkNewMessage(
        "1", "error",
        errorMessage(
            {
                minLength(1),
                maxLength(1),
                contains("E1", is("/nonce : Field not present"))
            }
        )
    );
}

TEST_F(CapidTest, command_noScope) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    send("1", {{"nonce",   "server"},
               {"command", "foo"}});
    checkNewMessage(
        "1", "error",
        errorMessage(
            {
                minLength(1),
                maxLength(1),
                contains("E1", is("/scope : Field not present"))
            }
        )
    );
}

TEST_F(CapidTest, command_noCommand) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    send("1", {{"nonce", "server"},
               {"scope", "foo"}});
    checkNewMessage(
        "1", "error",
        errorMessage(
            {
                minLength(1),
                maxLength(1),
                contains("E1", is("/command : Field not present"))
            }
        )
    );
}


TEST_F(CapidTest, command_scopeNotString) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    send("1", {{"nonce",   "Foo"},
               {"scope",   12345},
               {"command", "Foo"}});
    checkNewMessage(
        "1", "error",
        errorMessage(
            {
                minLength(1),
                maxLength(1)
            }
        )
    );
}

TEST_F(CapidTest, command_commandNotString) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    send(
        "1",
        {
            {"nonce",   "Foo"},
            {"scope",   "server"},
            {"command", 12345}
        }
    );
    checkNewMessage(
        "1", "error",
        errorMessage(
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(CapidTest, command_unknownScope) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand("1", "Foo", "Bar", {});
    checkNewMessage(
        "1", "commandResponse",
        commandResponseUnknownScope(nonce)
    );
}

TEST_F(CapidTest, command_unknownServerCommand) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand("1", "server", "Bar", {});

    checkNewMessage(
        "1", "commandResponse",
        commandResponseUnknownCommand(nonce)
    );
}

TEST_F(CapidTest, command_scopeGame_notInGame) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    sendLogin("1", "Foo");
    QString nonce = sendCommand("1", "game", "roll", {});

    checkNewMessage(
        "1", "commandResponse",
        commandResponseStateError(
            nonce,
            {
                minLength(1),
                maxLength(1),
                contains("E1", is("NOT_IN_GAME"))
            }
        )
    );
}

TEST_F(CapidTest, command_scopeGame_unknownCommand) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    sendLogin("1", "Foo");
    sendCommand("1", "server", "createGame", {{"type", "testGame"}});

    QString nonce = sendCommand("1", "game", "createGame", {});

    checkNewMessage(
        "1", "commandResponse",
        commandResponseUnknownCommand(nonce)
    );
}

TEST_F(CapidTest, DisconnectNotLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    addConnection("2");
    checkNoNewMessages("1", "no message about connecting player");

    removeConnection("2");
    checkNoNewMessages("1", "no message about disconnect");
}

TEST_F(CapidTest, command_reuseNonce) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    QString nonce = sendLogin("1", "Foo");

    send(
        "1",
        {
            {"nonce",     nonce},
            {"scope",     "data"},
            {"command",   "ping"},
            {"arguments", QJsonObject{{"message", "pong"}}}
        }
    );

    checkNewMessage(
        "1", "error",
        errorMessage(
            {minLength(1), maxLength(1)}
        )
    );
}


TEST_F(CapidTest, tick) { // NOLINT(cert-err58-cpp)
    QTest::qWait(100);

    addConnection("1");
    QTest::qWait(50);
    sendLogin("1", "foo");

    QTest::qWait(100);
}