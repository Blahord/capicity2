#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class LoginTest : public CapidTestBase {
};

TEST_F(LoginTest, onePlayer) { // NOLINT(cert-err58-cpp)
    auto player = addConnection("1");
    checkNoNewMessages("1", "initial");

    sendLogin("1", "FooBar");

    checkNewMessage(
        "1", "playerObject",
        serverMessage(
            "playerData", {
                requiredField("id", is(player->getId())),
                requiredField("name", is("FooBar")),
                requiredField("token", stringValue())
            }
        )
    );
    checkNewMessage(
        "1", "playerList",
        serverMessage(
            "playerList",
            {
                requiredField("list", arrayValue({minLength(1), maxLength(1)}))
            }
        )
    );
    checkNewMessage(
        "1", "gameTemplateList",
        serverMessage(
            "gameTemplateList",
            {
                requiredField("list", arrayValue({minLength(4), maxLength(4)}))
            }
        )
    );
}

TEST_F(LoginTest, doubleLogin) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    sendLogin("1", "Bar");

    checkNoNewMessages("1", "No message after double login");
}

TEST_F(LoginTest, noArguments) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    send(
        "1",
        {
            {"scope",   "server"},
            {"command", "login"},
        }
    );

    checkNewMessage(
        "1", "commandResult",
        errorMessage(
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(LoginTest, argumentsNotObject) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    send(
        "1",
        {
            {"nonce",     "foo"},
            {"scope",     "server"},
            {"command",   "login"},
            {"arguments", 12345}
        }
    );

    checkNewMessage(
        "1", "commandResult",
        errorMessage(
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(LoginTest, playerNameMissing) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand(
        "1", "server", "login",
        {}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(LoginTest, playerNameNotString) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand(
        "1", "server", "login",
        {{"name", 123456}}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(LoginTest, reusePlayerName) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    auto p2 = addConnection("2");
    auto p3 = addConnection("3");

    sendCommand(
        "1", "server", "login",
        {{"name", "Foo"}}
    );

    sendCommand(
        "2", "server", "login",
        {{"name", "Foo"}}
    );
    discardNextMessage("2");

    checkNewMessage(
        "1", "2nd login - 1",
        serverMessage(
            "newPlayer", {
                requiredField("id", is(p2->getId())),
                requiredField("connected", isTrue()),
                requiredField("name", is("Foo2"))
            }
        )
    );

    checkNewMessage(
        "2", "player 2 login response - playerObject",
        serverMessage(
            "playerData", {
                requiredField("id", is(p2->getId())),
                requiredField("name", is("Foo2")),
                requiredField("token", stringValue())
            }
        )
    );

    checkNewMessage(
        "2", "player 2 login response - playerList",
        serverMessage(
            "playerList",
            {
                requiredField("list", arrayValue({minLength(2), maxLength(2)}))
            }
        )
    );
    checkNewMessage(
        "2", "player 2 login response - gameTemplateList",
        objectValue({})
    );

    sendCommand(
        "3", "server", "login",
        {{"name", "Foo"}}
    );
    discardNextMessage("3");

    checkNewMessage(
        "1", "3rd login - 1",
        serverMessage(
            "newPlayer", {
                requiredField("id", is(p3->getId())),
                requiredField("connected", isTrue()),
                requiredField("name", is("Foo3"))
            }
        )
    );

    checkNewMessage(
        "2", "3rd login - 2",
        serverMessage(
            "newPlayer", {
                requiredField("id", is(p3->getId())),
                requiredField("connected", isTrue()),
                requiredField("name", is("Foo3"))
            }
        )
    );

    checkNewMessage(
        "3", "player 3 login response - playerObject",
        serverMessage(
            "playerData", {
                requiredField("id", is(p3->getId())),
                requiredField("name", is("Foo3")),
                requiredField("token", stringValue())
            }
        )
    );
    checkNewMessage(
        "3", "player 3 login response - playerList",
        serverMessage(
            "playerList",
            {
                requiredField("list", {minLength(3), maxLength(3)})
            }
        )
    );
    checkNewMessage(
        "3", "player 3 login response - gameTemplateList",
        objectValue({})
    );
}

TEST_F(LoginTest, withLanguage) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendCommand(
        "1", "server", "login",
        {
            {"name",     "Foo"},
            {"language", "deu"}
        }
    );
    discardNextMessage("1");

    checkNewMessage(
        "1", "playerObject",
        serverMessage(
            "playerData",
            {requiredField("name", stringValue())}
        )
    );
    checkNewMessage(
        "1", "playerList",
        serverMessage(
            "playerList",
            {
                requiredField("list", arrayValue({minLength(1), maxLength(1)}))
            }
        )
    );
    checkNewMessage(
        "1", "gameTemplateList",
        serverMessage(
            "gameTemplateList",
            {
                requiredField(
                    "list",
                    arrayValue(contains(
                        "gameTemplate in 'deu' translation",
                        objectValue(
                            {
                                requiredField("name", is("Testspiel"))
                            }
                        )
                    ))
                )
            }
        )
    );
}

TEST_F(LoginTest, withUnsupprotedGameLanguage) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendCommand(
        "1", "server", "login",
        {
            {"name",     "Foo"},
            {"language", "tlh"}
        }
    );
    discardNextMessage("1");

    checkNewMessage(
        "1", "playerObject",
        serverMessage(
            "playerData",
            {requiredField("name", stringValue())}
        )
    );
    checkNewMessage(
        "1", "playerList",
        serverMessage(
            "playerList",
            {
                requiredField("list", arrayValue({minLength(1), maxLength(1)}))
            }
        )
    );
    checkNewMessage(
        "1", "gameTemplateList",
        serverMessage(
            "gameTemplateList",
            {
                requiredField(
                    "list",
                    arrayValue(contains(
                        "gameTemplate in 'deu' translation",
                        objectValue(
                            {
                                requiredField("name", is("Test game"))
                            }
                        )
                    ))
                )
            }

        )
    );
}
