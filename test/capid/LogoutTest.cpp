#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class LogoutTest : public CapidTestBase {

};

TEST_F(LogoutTest, logout) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    addConnection("2");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");

    sendCommand("1", "server", "quit", {});

    checkNewMessage(
        "2", "logoutMessage",
        serverMessage(
            "quit",
            requiredField("id", is(p1->getId()))
        )
    );
}

TEST_F(LogoutTest, notLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    addConnection("2");

    sendLogin("2", "Bar");

    sendCommand("1", "server", "quit", {});

    checkNoNewMessages("2", "no message");
}

TEST_F(LogoutTest, logout_inGame_lastPlayer) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    addConnection("2");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();

    sendCommand("1", "server", "quit", {});
    discardNextMessage("1");

    checkNewMessage(
        "2", "P2: LogoutMessage",
        serverMessage(
            "quit",
            requiredField("id", is(p1->getId()))
        )
    );

    checkNewMessage(
        "2", "P2: Game delete",
        serverMessage(
            "gameDelete",
            requiredField("id", is(gameId))
        )
    );

    checkNoNewMessages("1", "P1: No message");
}

TEST_F(LogoutTest, logout_inGame) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    auto p2 = addConnection("2");

    sendLogin("1", "Foo");
    sendLogin("2", "Bar");

    sendCommand("1", "server", "createGame", {{"type", "testGame"}});
    discardNextMessage("1");
    QString gameId = getNextMessage("1", "newGame message").toObject().value("data").toObject().value("id").toString();
    sendCommand("2", "server", "joinGame", {{"id", gameId}});

    sendCommand("1", "server", "quit", {});
    discardNextMessage("1");

    checkNewMessage(
        "2", "game update 2",
        serverMessage(
            "gameUpdate",
            {
                requiredField("id", is(gameId)),
                requiredField("owner", is(p2->getId())),
                requiredField("players", maxLength(1), contains("Bar", is(p2->getId())))
            }
        )
    );

    checkNewMessage(
        "2", "logout message 2",
        serverMessage(
            "quit",
            requiredField("id", is(p1->getId()))
        )
    );

    checkNoNewMessages("1", "P1: No message");
}
