#include <gtest/gtest.h>
#include "CapidPlayer.h"

TEST(CapidPlayerConnectionTest, addNonce) { // NOLINT(cert-err58-cpp)
    CapidPlayer player;

    player.addUsedNonce("1", 42);
    EXPECT_TRUE(player.isNonceUsed("1", 42, 1));
    EXPECT_TRUE(player.isNonceUsed("1", 43, 1));
    EXPECT_FALSE(player.isNonceUsed("1", 44, 1));
}

TEST(CapidPlayerTest, clearNonces) { // NOLINT(cert-err58-cpp)
    CapidPlayer player;

    player.addUsedNonce("1", 42);
    player.addUsedNonce("2", 43);
    player.addUsedNonce("3", 44);

    player.clearOldNonce(44, 1);

    EXPECT_FALSE(player.isNonceUsed("1", 42, 42));
    EXPECT_TRUE(player.isNonceUsed("2", 42, 0));
    EXPECT_TRUE(player.isNonceUsed("3", 42, 0));
}

