#include <gtest/gtest.h>

#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class GameManagerTest : public CapidTestBase {

    protected:
        void SetUp() override {
            CapidTestBase::SetUp();

            p1 = addConnection("1");
            p2 = addConnection("2");

            sendLogin("1", "1");
            sendLogin("2", "2");

        }

        void createGame(const QString& templateName) {
            gameId = sendCreateGame("1", templateName);
            sendCommand(
                "2", "server", "joinGame",
                {{"id", gameId}}
            );

            rng->addNextNumbers({0, 1});
            sendCommand("1", "game", "setReady", {{"ready", true}});
            sendCommand("2", "game", "setReady", {{"ready", true}});
        }

        QString gameId;
        std::shared_ptr<CapidPlayer> p1;
        std::shared_ptr<CapidPlayer> p2;
};

TEST_F(GameManagerTest, endMovement) {
    createGame("taxHell");

    rng->addNextNumbers({1, 2});
    sendCommand("1", "game", "roll", {});
    sendCommand("1", "game", "moveComplete", {});
    sendCommand("2", "game", "moveComplete", {});
    discardNextMessages("1", 3);
    discardNextMessages("2", 4);

    checkNewMessage(
        {"1", "2"}, "Estate update Go",
        serverMessage(
            "estateUpdate",
            {
                requiredField("id", is("go")),
                requiredField("money", is(150))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Game Player Update 1",
        serverMessage(
            "gamePlayerUpdate",
            {
                requiredField("id", is(p1->getId())),
                requiredField("money", is(1350))
            }
        )
    );

    checkNewMessage(
        {"1", "2"}, "Payed tax event",
        eventMessage(
            "taxPayed",
            integerValue(is(150))
        )
    );

}