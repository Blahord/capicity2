#include "json/JsonChecks.h"

#include "CapidTestBase.h"
#include "JsonResultChecks.h"

using namespace JsonChecks;
using namespace JsonResultChecks;

class SetAvatarTest : public CapidTestBase {

    protected:
        static QString createAvatar(int size, bool includeUpperCase, bool includeNonHex) {
            QString data = "";

            int resultLength = 8 * size * size;
            for (int i = 0; i < resultLength; i += 2) {
                if (includeUpperCase && includeNonHex) {
                    data += "F+";
                } else if (includeUpperCase) {
                    data += "Ff";
                } else if (includeNonHex) {
                    data + " f+";
                } else {
                    data += "ff";
                }
            }

            return data;
        }
};

TEST_F(SetAvatarTest, notLoggedIn) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  16},
            {"height", 16},
            {"data",   createAvatar(16, false, false)}
        }
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_LOGGED_IN"))}
        )
    );
}

TEST_F(SetAvatarTest, invalidData) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {}
    );

    checkNewMessage(
        "1", "Command result",
        commandResponseFormalError(
            nonce,
            {minLength(3), maxLength(3)}
        )
    );
}

TEST_F(SetAvatarTest, avatarTooSmall) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  15},
            {"height", 15},
            {"data",   createAvatar(63, false, false)}
        }
    );

    checkNewMessage(
        "1", "Command result",
        commandResponseFormalError(
            nonce,
            {minLength(2), maxLength(2)}
        )
    );
}

TEST_F(SetAvatarTest, avatarTooBig) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  257},
            {"height", 257},
            {"data",   createAvatar(257, false, false)}
        }
    );

    checkNewMessage(
        "1", "Command result",
        commandResponseFormalError(
            nonce,
            {minLength(2), maxLength(2)}
        )
    );
}

TEST_F(SetAvatarTest, avatarLengthNotFitting) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  16},
            {"height", 16},
            {"data",   createAvatar(17, false, false)}
        }
    );

    checkNewMessage(
        "1", "Command result",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(SetAvatarTest, avatarIllegalChars1) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  16},
            {"height", 16},
            {"data",   createAvatar(16, true, false)}
        }
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(SetAvatarTest, avatarIllegalChars2) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  16},
            {"height", 16},
            {"data",   createAvatar(16, false, true)}
        }
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(SetAvatarTest, avatarIllegalChars3) { // NOLINT(cert-err58-cpp)
    addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  16},
            {"height", 16},
            {"data",   createAvatar(16, true, true)}
        }
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseFormalError(
            nonce,
            {minLength(1), maxLength(1)}
        )
    );
}

TEST_F(SetAvatarTest, avatar) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    sendLogin("1", "Foo");

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  16},
            {"height", 16},
            {"data",   createAvatar(16, false, false)}
        }
    );

    checkNewMessage("1", "commandResult", commandResponseOk(nonce));
    checkNewMessage(
        "1", "avatar set",
        serverMessage(
            "playerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("avatar", regexp("^[0-9a-f]{64}$"))
        )
    );
}

TEST_F(SetAvatarTest, avatar_override) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    sendLogin("1", "Foo");

    sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  16},
            {"height", 16},
            {"data",   createAvatar(16, false, false)}
        }
    );

    QString nonce = sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  17},
            {"height", 17},
            {"data",   createAvatar(17, false, false)}
        }
    );

    checkNewMessage("1", "commandResult", commandResponseOk(nonce));
    checkNewMessage(
        "1", "avatar set",
        serverMessage(
            "playerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("avatar", regexp("^[0-9a-f]{64}$"))
        )
    );
}

TEST_F(SetAvatarTest, clearAvatar_not_logged_in) { // NOLINT(cert-err58-cpp)
    addConnection("1");

    QString nonce = sendCommand(
        "1", "server", "clearAvatar",
        {}
    );

    checkNewMessage(
        "1", "commandResult",
        commandResponseStateError(
            nonce,
            {maxLength(1), contains("E1", is("NOT_LOGGED_IN"))}
        )
    );
}

TEST_F(SetAvatarTest, clearAvatar) { // NOLINT(cert-err58-cpp)
    auto p1 = addConnection("1");
    sendLogin("1", "Foo");

    sendCommand(
        "1", "server", "setAvatar",
        {
            {"width",  16},
            {"height", 16},
            {"data",   createAvatar(16, false, false)}
        }
    );

    QString nonce = sendCommand(
        "1", "server", "clearAvatar",
        {}
    );

    checkNewMessage("1", "commandResult", commandResponseOk(nonce));
    checkNewMessage(
        "1", "playerUpdate",
        serverMessage(
            "playerUpdate",
            requiredField("id", is(p1->getId())),
            requiredField("avatar", is(""))
        )
    );
}
