macro(addFile module path extension)
    set(
            ${module}

            ${${module}}
            ${path}.${extension}
    )
endmacro()

macro(addHeader module path)
    addFile(${module} ${path} h)
endmacro()

macro(addSource module path)
    addFile(${module} ${path} cpp)
endmacro()

macro(addClass module path)
    addHeader(${module} ${path})
    addSource(${module} ${path})
endmacro()
