set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}")

find_package(Qt5Core)
find_package(Qt5Xml)
find_package(Qt5Network)
find_package(Qt5Widgets)
find_package(Qt5Test)

find_package(PNG REQUIRED)